This project will reimplement the CoU client (https://gitlab.com/ChildrenOfUr/coUclient) in Python.
The original client was implemented using Dart but due to Dart for the web falling out of favor
and the relative unpopularity with programming in Dart for the web, this project will use PyScript
(https://github.com/pyscript/pyscript) to allow for a Python implementation of the same interface.

Getting Started
===

One advantage to using Python/PyScript over Dart is the lack of a need for a build/compile step when
running the application. Loading the webpage will start the runtime and all of the dependencies/scripts
will be loaded along with it.

The Live Server extension for VSCode is handy as it will serve the files on your localhost address and
it will automatically reload the page whenever a change is made to the source code: https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer
