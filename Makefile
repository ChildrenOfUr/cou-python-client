clean:
	rm -rf _dist

dist: clean
	mkdir _dist
	cp -r web/* _dist
	echo '{"gameServer":"${SERVER_ADDRESS}","gameUtilPort": "${SERVER_UTIL_PORT}","gameWebsocketPort": "${SERVER_WS_PORT}","authServer": "server.childrenofur.com","testing": true}' > _dist/server_domain.json

all: dist
