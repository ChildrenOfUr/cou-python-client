// Build a custom element for <ur-button> (originally from https://gitlab.com/ChildrenOfUr/cou_toolkit/-/blob/master/lib/toolkit/button/button.html?ref_type=heads)
class UrButton extends HTMLElement {
    static observedAttributes = ["theme", "big", "darkui"]

    constructor() {
        // Always call super first in constructor
        super();

        this.initialized = false;
    }

    connectedCallback() {
        // Create a shadow root
        const shadow = this.attachShadow({ mode: "open" });

        // add the font-awesome styles to the root
        const font_awesome = document.createElement("link")
        font_awesome.rel = "stylesheet"
        font_awesome.href = "https://use.fontawesome.com/releases/v5.0.13/css/all.css"
        font_awesome.integrity = "sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
        font_awesome.crossOrigin = "anonymous"
        shadow.appendChild(font_awesome)

        // Create some CSS to apply to the shadow dom
        const style = document.createElement("style");
        style.textContent = `
            :host {
                display: inline-block;
            }

            [hidden], .hidden {
                display: none;
            }

            button {
                font-family: 'Fredoka One', cursive;
                position: relative;
                min-width: 30px;
                border-radius: 7px;
                padding: 6px;
                background-color: #4b2e4c;
                border: 1px solid rgba(0, 0, 0, 0.3);
                border-bottom: 3px solid rgba(0, 0, 0, 0.3);
                text-align: center;
                cursor: pointer;
                box-shadow: inset 0 -2px 5px 1px rgba(0, 0, 0, 0.1);
                transition: box-shadow 0.1s;
            }

            button:hover {
                box-shadow: inset 0 -2px 5px 3px rgba(255, 0, 200, 0.1);
            }

            button, button * {
                color: #fff;
            }

            button:active {
                border-bottom: 1px solid rgba(0, 0, 0, 0.2);
                padding-bottom: 8px;
                box-shadow: 0 1px 50px 20px rgba(255, 255, 255, 0.1);
                transform: translateY(1px);
            }

            button.warn {
                background-color: #ba381a;
            }

            button.success {
                background-color: #57AA4B;
            }

            button.light {
                background-color: #fff;
            }

            button.light[darkui="true"] {
                background-color: #282828;
            }

            button.light, button.light * {
                color: #4b2e4c;
            }

            button.light[darkui="true"],
            button.light[darkui="true"] * {
                color: #eee;
            }

            button[big="true"] {
                font-size: 20px;
            }
        `;

        this.button = document.createElement("button");
        this.content = document.createElement("content");
        this.content.innerHTML = this.innerHTML;
        this.button.appendChild(this.content);

        // Attach the created elements to the shadow dom
        shadow.appendChild(style);
        shadow.appendChild(this.button);

        // watch for changes to our contents
        this.observer = new MutationObserver(() => {this.content.innerHTML = this.innerHTML;});
        this.observer.observe(this, {childList: true, subtree: true})

        this.initialized = true;
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (this.isConnected && this.initialized) {
            if (name == "theme") {
                this.button.className = newValue;
            }
            if (name == "big") {
                this.button.setAttribute("big", newValue);
            }
            if (name == "darkui") {
                this.button.setAttribute("darkui", newValue);
            }
        }
    }
}

customElements.define("ur-button", UrButton);
