// Build a custom element for <ur-progress> (originally from https://gitlab.com/ChildrenOfUr/cou_toolkit/-/tree/master/lib/toolkit/progress?ref_type=heads)
class UrProgress extends HTMLElement {
    static observedAttributes = ["percent", "status"]

    constructor() {
        // Always call super first in constructor
        super();

        this.initialized = false;
    }

    connectedCallback() {
        // Create a shadow root
        const shadow = this.attachShadow({ mode: "open" });

        // Create some CSS to apply to the shadow dom
        const style = document.createElement("style");
        style.textContent = `
            :host {
                background-color: #e6ecee;
                display: inline-block;
                overflow: hidden;
                font-size: 18px;
                height: 20px;
                width: 80%;
                color: rgba(0, 0, 0, 0.7);
                border: 2px solid #fff;
                -moz-border-radius: 4px;
                border-radius: 4px;
            }

            #bar {
                display: block;
                position: relative;
                height: 18px;
                background-color: #c5dc81;
                width: 0%;
                border: 1px solid #fff;
                -moz-border-radius: 5px;
                border-radius: 5px;
                transition-property: width;
                transition-duration: 0.2s;
                transition-timing-function: linear;
                transition-delay: 0.5s;
                -webkit-transition-property: width;
                -webkit-transition-duration: 0.2s;
                -webkit-transition-timing-function: linear;
                -webkit-transition-delay: 0.5s;
            }

            #label {
                font-family: 'Lato', sans-serif;
                position: relative;
                top: -1.3em;
                left: 0.5em;
            }
        `;

        this.bar = document.createElement("span");
        this.bar.id = "bar";
        this.label = document.createElement("span");
        this.label.id = "label";

        // Attach the created elements to the shadow dom
        shadow.appendChild(style);
        shadow.appendChild(this.bar);
        shadow.appendChild(this.label);

        this.initialized = true;
    }

    attributeChangedCallback(name, oldValue, newValue) {
        if (this.isConnected && this.initialized) {
            if (name == "percent") {
                this.bar.style.width = `${newValue}%`
            }
            if (name == "status") {
                this.label.innerText = newValue
            }
        }
    }
}

customElements.define("ur-progress", UrProgress);
