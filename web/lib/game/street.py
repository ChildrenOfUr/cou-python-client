from datetime import timedelta
import logging
import time

from pyscript import document
from pyodide import http

from assets import Asset, Batch
from configs import Configs
from display.render.camera import Camera
from display.render.ladder import Ladder
from display.render.platform import Platform
from game.entities.signpost import Signpost
from display.render.wall import Wall
from drawing import Rectangle
from message_bus import MessageBus
from network.server_interop.so_chat import send_joined_message, send_left_message
from systems import util
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)
cou_globals.player_tele_from = ""


class Street:
    def __init__(self, street_data: dict) -> None:
        self.street_data = street_data
        self.loaded = False
        self._tsid = street_data["tsid"]
        self.label = cou_globals.map_data.get_label(self.tsid) or street_data["label"]
        self.hub_id = street_data["hub_id"]
        self.hub_name = cou_globals.map_data.hubData[self.hub_id]["name"]

        self.platforms: list[Platform] = []
        self.ladders: list[Ladder] = []
        self.walls: list[Wall] = []
        self.signposts: list[Signpost] = []
        self.ground_y = 0

        if cou_globals.game.username is not None and cou_globals.current_street is not None:
            send_left_message(cou_globals.current_street.label)

        self.bounds = Rectangle(
            self.street_data["dynamic"]["l"],
            self.street_data["dynamic"]["t"],
            abs(self.street_data["dynamic"]["l"]) + abs(self.street_data["dynamic"]["r"]),
            abs(self.street_data["dynamic"]["t"] - self.street_data["dynamic"]["b"]),
        )
        self.min_bounds_width = self.bounds.width
        self.min_bounds_height = self.bounds.height

        cou_globals.view.playerHolder.style.width = f"{self.bounds.width}px"
        cou_globals.view.playerHolder.style.height = f"{self.bounds.height}px"
        cou_globals.view.playerHolder.classList.add("streetcanvas")
        cou_globals.view.playerHolder.style.position = "absolute"
        cou_globals.view.playerHolder.setAttribute("ground_y", "0")
        cou_globals.view.playerHolder.setAttribute("width", str(self.bounds.width))
        cou_globals.view.playerHolder.setAttribute("height", str(self.bounds.height))
        cou_globals.view.playerHolder.style.transform = "translateZ(0)"

        # set the street
        cou_globals.current_street = self
        send_joined_message(self.label)


    @property
    def tsid(self) -> str:
        if self._tsid.startswith("G"):
            return self._tsid.replace("G", "L", 1)
        else:
            return self._tsid

    @property
    def tsid_g(self) -> str:
        return self._tsid

    @property
    def use_letters(self) -> bool:
        return cou_globals.map_data.hubData.get(self.hub_id, {}).get("players_have_letters") == True

    async def load(self) -> None:
        # clean up old street data
        cou_globals.entities = {}
        cou_globals.quoins = {}
        cou_globals.other_players = {}
        if getattr(cou_globals, "current_player", None) is not None:
            cou_globals.current_player.intersecting_objects.clear()

        cou_globals.view.layers.replaceChildren()
        cou_globals.view.playerHolder.replaceChildren()
        cou_globals.view.location = self.label

        # Load the music
        if self.street_data.get("music") is not None:
            # Attempt to get music from server
            MessageBus.publish("playSong", self.street_data["music"])
            LOGGER.info(f"[StreetService] Loaded song {self.street_data['music']} from server")
        else:
            # Otherwise, get stored preset music
            MessageBus.publish("playSong", cou_globals.map_data.get_song(self.label))

        # Collect the urls of each deco to load
        decos_to_load = []
        for layer in self.street_data["dynamic"]["layers"].values():
            layer_name = layer["name"].replace(" ", "_")
            tsid = self.tsid.split(".")[0]  # don't consider the instance owner (for now?)
            url = f"https://childrenofur.com/assets/streetLayers/{tsid}/{layer_name}.png"
            if Configs.testing:
                status = await http.pyfetch(f"https://childrenofur.com/assets/street_layer_exists.php?tsid={tsid}&layer={layer_name}")
                if await status.string() == "dev":
                    url += f"?time={time.time_ns()}"
            if url not in decos_to_load:
                decos_to_load.append(url)

        # turn them into assets
        assets_to_load = [Asset(deco) for deco in decos_to_load]

        # load each of them and then continue
        decos = Batch(assets_to_load)
        await decos.load(set_loading_percent)
        # decos should all be loaded at this point

        self.ground_y = -abs(float(self.street_data["dynamic"]["ground_y"]))

        # # /// Gradient Canvas /// #
        gradient_canvas = document.createElement("div")

        # Color the gradient canvas
        top = self.street_data["gradient"].get("top")
        bottom = self.street_data["gradient"].get("bottom")

        gradient_canvas.classList.add("streetcanvas")
        gradient_canvas.id = "gradient"
        gradient_canvas.setAttribute("ground_y", "0")
        gradient_canvas.setAttribute("width", str(self.bounds.width))
        gradient_canvas.setAttribute("height", str(self.bounds.height))
        gradient_canvas.style.zIndex = str(-100)
        gradient_canvas.style.width = f"{self.bounds.width}px"
        gradient_canvas.style.height = f"{self.bounds.height}px"
        gradient_canvas.style.position = "absolute"
        gradient_canvas.style.background = f"linear-gradient(to bottom, #{top}, #{bottom})"

        # append it to the screen
        cou_globals.view.layers.appendChild(gradient_canvas)

        # /// Scenary Canvases /// #
        # for each layer on the street ...
        self.scenery_canvases = {
            "gradient": {
                "canvas": gradient_canvas, "width": self.bounds.width,
                "height": self.bounds.height, "ground_y": 0,
            },
            "playerHolder": {
                "canvas": cou_globals.view.playerHolder, "width": self.bounds.width,
                "height": self.bounds.height, "ground_y": 0,
            }
        }

        # clear old signposts before loading new ones from layers
        cou_globals.minimap.current_street_exits = []

        for layer in self.street_data["dynamic"]["layers"].values():
            deco_canvas = document.createElement("div")
            deco_canvas.classList.add("streetcanvas")
            deco_canvas.id = layer["name"].replace(" ", "_")

            deco_canvas.style.zIndex = str(layer["z"])
            deco_canvas.style.width = f"{layer['w']}px"
            deco_canvas.style.height = f"{layer['h']}px"
            deco_canvas.style.position = "absolute"
            self.scenery_canvases[layer["name"]] = {
                "canvas": deco_canvas, "width": layer["w"], "height": layer["h"],
                "ground_y": self.ground_y,
            }
            self.min_bounds_width = min(self.min_bounds_width, layer["w"])
            self.min_bounds_height = min(self.min_bounds_height, layer["h"])

            # put the one layer image in
            try:
                layer_name = layer["name"].replace(" ", "_")
                layer_image = cou_globals.ASSETS[layer_name].get()
                layer_image.style.transform = f"translateY({self.ground_y}px)"
                deco_canvas.appendChild(layer_image)
            except Exception:
                LOGGER.exception(f"Could not load layer image {layer['name']}")

            # bottom edge of street
            # self.platforms.append(Platform(
            #     {
            #         "id": "_",
            #         "endpoints": [
            #             {
            #                 "name": "start",
            #                 "x": 0,
            #                 "y": ground_y,
            #             },
            #             {
            #                 "name": "end",
            #                 "x": self.bounds.right,
            #                 "y": ground_y,
            #             },
            #         ],
            #     },
            #     layer, ground_y,
            # ))

            for platform_line in layer["platformLines"]:
                self.platforms.append(Platform(platform_line, layer, self.ground_y - self.street_data["dynamic"]["b"]))

            self.platforms.sort(reverse=True)

            for ladder in layer["ladders"]:
                self.ladders.append(Ladder(ladder, layer, self.ground_y - self.street_data["dynamic"]["b"]))

            for wall in layer["walls"]:
                if wall.get("pc_perm") == 0:
                    continue
                self.walls.append(Wall(wall, layer, self.ground_y - self.street_data["dynamic"]["b"]))

            for signpost in layer["signposts"]:
                h = 200
                w = 100

                if signpost.get("h") is not None:
                    h = signpost["h"]
                if signpost.get("w") is not None:
                    w = signpost["w"]

                x = signpost["x"] - w // 2
                y = signpost["y"] - h - self.street_data["dynamic"]["b"]

                if layer["name"] == "middleground":
                    # middleground has different layout needs
                    y += layer["h"]
                    x += layer["w"] // 2

                Signpost(signpost, x, y)

                # show signpost in minimap
                streets = [exit["label"] for exit in signpost["connects"]]
                cou_globals.minimap.current_street_exits.append({"streets": streets, "x": x, "y": y})

            # append the canvas to the screen
            cou_globals.view.layers.appendChild(deco_canvas)


        # make sure to redraw the screen (in case of street switching)
        cou_globals.camera.dirty = True
        self.loaded = True

        # done intializing street

    def render(self) -> None:
        """Parallaxing: Adjust the position of each canvas in #GameScreen
        based on the camera position and relative size of canvas to Street
        """
        camera: Camera = cou_globals.camera
        view = cou_globals.view

        # only update if camera x,y have changed since last render cycle
        if not self.loaded or not camera.dirty:
            return

        world_width = view.worldElementWidth
        world_height = view.worldElementHeight

        # if the screen is too hi-rez for the street, we don't want the percent becoming NaN
        x_denom = self.bounds.width - world_width
        y_denom = self.bounds.height - world_height
        if x_denom == 0:
            x_denom = 1
        if y_denom == 0:
            y_denom = 1
        current_percent_x = camera.x / x_denom
        current_percent_y = camera.y / y_denom

        # modify left and top for parallaxing
        for canvas_details in self.scenery_canvases.values():
            canvas = canvas_details["canvas"]
            canvas_width = canvas_details["width"]
            canvas_height = canvas_details["height"]
            offset_x = (canvas_width - world_width) * current_percent_x
            offset_y = (canvas_height - world_height) * current_percent_y

            ground_y = canvas_details["ground_y"]
            offset_y += ground_y

            if world_height > self.bounds.height:
                offset_y -= (world_height - self.bounds.height) * 1.5

            canvas.style.transform = f"translateZ(0) translateX({-offset_x}px) translateY({-offset_y}px)"

        camera.dirty = False


cou_globals.current_street = None


def set_loading_percent(percent: int) -> None:
    """the callback function for our deco loading 'Batch'"""

    cou_globals.game.street_service.street_loading_screen.loading_percent = percent

    if percent >= 99:
        def cleanup_load():
            cou_globals.gps_indicator.loading_new = False
        util.Timer(timedelta(milliseconds=1000), cleanup_load)
