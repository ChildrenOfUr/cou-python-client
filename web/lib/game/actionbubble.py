import asyncio
from datetime import timedelta
from typing import Callable

from pyscript import document
from pyodide.ffi import create_proxy

from display.overlays.module_skills import SkillIndicator
from systems import util
from systems.global_gobbler import cou_globals


class ActionBubble:
    occuring: bool = False

    def __init__(self, action_name: str, duration: int, assoc_skill: str = None):
        self.duration = duration
        # Only the first word, ignore anything after the first space
        text = action_name.split(" ")[0].lower()
        cssName = action_name.lower().replace(" ", "_")

        self.outline = document.createElement("span")
        self.outline.innerText = text
        self.outline.className = f"border {cssName}"
        self.outline.style.zIndex = "99"

        self.fill = document.createElement("span")
        self.fill.innerText = text
        self.fill.className = f"fill {cssName}"
        self.fill.style.transition = f"width {duration / 1000}s linear"
        self.fill.style.zIndex = "99"
        self.fill.style.whiteSpace = "nowrap"

        cou_globals.current_player.super_parent_element.appendChild(self.outline)
        cou_globals.current_player.super_parent_element.appendChild(self.fill)

        # position the action bubble
        outline_rect = self.outline.getBoundingClientRect()
        outline_width = outline_rect.width
        outline_height = outline_rect.height

        player_x = cou_globals.current_player.translate_x
        player_y = cou_globals.current_player.translate_y
        x = int(player_x) - outline_width // 2 + cou_globals.current_player.width // 2
        y = int(player_y) - outline_height - 25

        self.fill.style.top = f"{y}px"
        self.fill.style.left = f"{x}px"
        self.outline.style.top = f"{y}px"
        self.outline.style.left = f"{x}px"

        # start the "fill animation"
        self.fill.style.width = f"{outline_width}px"

        if assoc_skill is not None:
            self.assoc_skill_indicator = SkillIndicator(assoc_skill)
        else:
            self.assoc_skill_indicator = None

        type(self).occuring = True
        def _stop_occuring():
            type(self).occuring = False
        util.Timer(timedelta(milliseconds=duration), _stop_occuring)

    @classmethod
    def withAction(cls, action):
        return ActionBubble(action.action_name, action.time_required, action.associated_skill)

    async def wait(self) -> bool:
        completer = asyncio.Future()

        def _finish_action():
            self.outline.remove()
            self.fill.remove()
            esc_listener[0].removeEventListener(*esc_listener[1:])
            if self.assoc_skill_indicator is not None:
                self.assoc_skill_indicator.close()
            type(self).occuring = False
            completer.set_result(True)
        action_timer = util.Timer(timedelta(milliseconds=self.duration + 300), _finish_action)

        def _esc_action(event):
            if event.keyCode != 27:
                return
            self.outline.remove()
            self.fill.remove()
            action_timer.cancel()
            esc_listener[0].removeEventListener(*esc_listener[1:])
            if self.assoc_skill_indicator is not None:
                self.assoc_skill_indicator.close()
            type(self).occuring = False
            completer.set_result(False)
        esc_listener = (document, "keyup", create_proxy(_esc_action))
        esc_listener[0].addEventListener(*esc_listener[1:])

        await completer
        return completer.result()
