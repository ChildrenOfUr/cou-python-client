from datetime import timedelta
import logging

from pyscript import document

from configs import Configs
from drawing import Rectangle
from systems import util


LOGGER = logging.getLogger(__name__)


class Animation:
    def __init__(
            self, url: str, animation_name: str, num_rows: int, num_columns: int, frame_list: list[int],
            fps: int = 30, loop_delay: timedelta = None, delay_initially: bool = False, loops: bool = True,
            loop_frames: list[int] = None, still_frame: "Animation" = None,
        ):
        self.url = url.replace('"', "")
        self.animation_name = animation_name
        self.num_rows = num_rows
        self.num_columns = num_columns
        self.frame_list = frame_list
        self._full_frame_list = frame_list[:]
        self.fps = fps
        self.loop_delay = loop_delay
        self.delay_initially = delay_initially
        self.loops = loops
        self.loop_frames = loop_frames or frame_list
        self.still_frame = still_frame

        self.frame_num = 0
        self.time_in_millis = 0.0
        self.delay_consumed = 0.0
        self.dirty = True
        self.paused = False
        self.loaded = False
        self.complete = False

        if "http://" in self.url:
            self.url = Configs.proxyAvatarImage(self.url)

    async def load(self) -> None:
        if self.loop_delay is None:
            self.loop_delay = timedelta(milliseconds=0)

        if not self.delay_initially:
            self.delay_consumed = self.loop_delay / timedelta(milliseconds=1)

        # need to get the avatar background image size dynamically
        # because we cannot guarantee that every glitchen has the same dimensions
        # additionally each animation sprite has different dimensions even for the same glitchen
        self.spritesheet = document.createElement("img")
        self.spritesheet.src = self.url
        await util.await_event_once(self.spritesheet, "load")

        self._orig_spritesheet = self.spritesheet
        if self.still_frame:
            await self.still_frame.load()

        self.width = self.spritesheet.naturalWidth // self.num_columns
        self.height = self.spritesheet.naturalHeight // self.num_rows

        self.source_rect = Rectangle(0, 0, self.width, self.height)
        self.loaded = True

    def reset(self) -> None:
        self.time_in_millis = 0.0
        self.frame_num = 0
        self.delay_consumed = 0.0
        self.dirty = True  # will cause the first frame to be shown right away
                           # even if there is a delay of the rest of the animation (should be a good thing)
        self.paused = False
        self.complete = False
        self.frame_list = self._full_frame_list

    def update_source_rect(self, dt: float, hold_at_last_frame: bool = False) -> None:
        if self.paused or not self.loaded:
            return

        self.time_in_millis += dt
        self.delay_consumed += dt * 1000

        if self.time_in_millis > 1 / self.fps and self.delay_consumed >= self.loop_delay / timedelta(milliseconds=1):
            self.spritesheet = self._orig_spritesheet
            # advance the frame cycling around if necessary
            if self.frame_num >= len(self.frame_list) - 1 and (hold_at_last_frame or not self.loops):
                self.frame_num = len(self.frame_list) - 1
                self.complete = True
            else:
                old_frame = self.frame_num
                self.frame_num = int(self.frame_num + self.time_in_millis // (1 / self.fps)) % len(self.frame_list)
                self.time_in_millis = 0.0

                if old_frame != self.frame_num:
                    self.dirty = True

                if self.frame_num >= len(self.frame_list) - 1:
                    self.complete = True
                    self.frame_list = self.loop_frames
                    self.frame_num = 0
                    self.delay_consumed = 0.0
                    if self.still_frame:
                        self.frame_num = self.still_frame.frame_list[0]
                        self._orig_spritesheet = self.spritesheet
                        self.spritesheet = self.still_frame.spritesheet

        column = self.frame_list[self.frame_num] % self.num_columns
        row = self.frame_list[self.frame_num] // self.num_columns

        self.source_rect.left = column * self.width
        self.source_rect.top = row * self.height
