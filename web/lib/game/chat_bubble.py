import json
import logging
from typing import Any

from html_sanitizer import Sanitizer
from pyscript import document
from pyodide.ffi import create_proxy

from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)
VALIDATOR = Sanitizer(
    {
        "tags": {"span", "a", "i", "p", "b", "del", "img"},
        "attributes": {
            "span": ("class",),
            "a": ("href", "title", "target", "rel", "class",),
            "i": ("class", "title"),
            "img": ("src", "class", "title"),
            "p": ("class", "id"),
        },
        "empty": ("img",),
        "separate": set("a",),
    }
)


class ChatBubble:
    def __init__(
            self, text: str, host_object, parent, auto_dismiss: bool = True, remove_parent: bool = False,
            add_username: bool = False, gains: dict[str, Any] = None, buttons: str = None,
        ) -> None:
        self.text = text
        self.host_object = host_object
        self.parent = parent
        self.auto_dismiss = auto_dismiss
        self.remove_parent = remove_parent

        if auto_dismiss:
            self.time_to_live = len(text) * 0.05 + 3  # minimum 3s plus 0.05 per character
            if self.time_to_live > 10:  # max 10s
                self.time_to_live = 10  # messages over 10s will only display for 10s
        else:
            self.time_to_live = 0

        self.bubble = document.createElement("div")
        self.bubble.className = "chat-bubble"
        self.text_element = document.createElement("div")
        self.text_element.className = "cb-content"

        self.text_element.innerHTML = VALIDATOR.sanitize(self.text)

        if buttons:
            self.text_element.insertAdjacentHTML("beforeend", "<br>")

            for button in buttons.split("|"):
                btn_id, btn_text = button.split(",")

                btn = document.createElement("button")
                btn.innerText = btn_text
                def on_click(_):
                    # send id to server
                    LOGGER.info(f"Sending chat bubble action: {btn_id}")
                    cou_globals.street_socket.send(json.dumps({"bubbleButton": btn_id}))
                btn.addEventListener("click", create_proxy(on_click))
                self.text_element.appendChild(btn)

        self.arrow_element = document.createElement("div")
        self.arrow_element.classList.add("cb-arrow")

        if gains:
            gains_total = 0
            awarded = document.createElement("div")
            awarded.className = "awarded"
            for metabolic, value in gains.items():
                value = int(value)
                if value == 0:
                    continue
                gains_total += value
                span = document.createElement("span")
                span.className = metabolic
                text_value = f"+{value}" if value > 0 else str(value)
                span.innerText = text_value
                awarded.appendChild(span)

            self.text_element.appendChild(awarded)

            if gains_total == 0 and text == "":
                # if you can't say anything nice...
                self.remove_bubble()

        self.bubble.appendChild(self.text_element)
        self.bubble.appendChild(self.arrow_element)

        # force a player update to be sent right now
        cou_globals.time_last = 5.0

    def update(self, dt: float) -> None:
        if self.time_to_live <= 0 and self.auto_dismiss:
            self.remove_bubble()
            # force a player update to be sent right now
            cou_globals.time_last = 5.0
        else:
            self.time_to_live -= dt
            self.parent.appendChild(self.bubble)

    def remove_bubble(self) -> None:
        self.bubble.remove()
        self.host_object.chat_bubble = None

        if self.remove_parent:
            self.parent.remove()
