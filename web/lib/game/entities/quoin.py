from datetime import timedelta
import json
import logging
from typing import Any

from pyscript import document

from display.toast import Toast
from drawing import Rectangle
from game.animation import Animation
from game.entities.entity import Entity
from systems import util
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)


class Quoin(Entity):
    quoins = {"img": 0, "mood": 1, "energy": 2, "currant": 3, "mystery": 4, "favor": 5, "time": 6, "quarazy": 7}
    notified: dict[str, bool] = {}

    def __init__(self, map: dict):
        super().__init__()
        self.ready = False
        self.update_waited = 0
        util.create_task(self._init(map))

    async def _init(self, map: dict[str, Any]) -> None:
        self.type_string = map["type"]
        self.first_render = True
        self.collected = False  # whether the quoin is waiting to respawn
        self.checking = False  # whether the client is waiting for the server to verify/award the quoin
        self.hit = False  # whethter the client is sending the intersection to the server
        self._greyed_out = False

        # Don't show Quarazy Quoins more than once for a street
        if self.type_string == "quarazy":
            await cou_globals.metabolics.load

            tsid_g = cou_globals.current_street.tsid_g
            tsid = cou_globals.current_street.tsid
            location_history = getattr(cou_globals.metabolics, "location_history", [])
            if tsid_g or tsid in location_history:
                return

        self.id = map["id"]
        quoin_value = Quoin.quoins[self.type_string.lower()]

        frame_list = [quoin_value * 24 + index for index in range(24)]
        self.animation = Animation(map["url"], self.type_string.lower(), 8, 24, frame_list, fps=22)
        await self.animation.load()

        canvas = document.createElement("canvas")
        canvas.width = self.animation.width
        canvas.height = self.animation.height
        canvas.id = self.id
        canvas.className = map["type"] + " quoin"
        canvas.style.position = "absolute"
        canvas.style.left = f"{map['x']}px"
        canvas.style.top = f"{map['y']}px"
        canvas.style.tranform = "translateZ(0)"
        canvas.setAttribute("collected", "false")
        self.canvas = canvas

        self.left = map["x"]
        self.top = map["y"]

        self.quoin_rect = Rectangle(self.left, self.top, self.canvas.width, self.canvas.height)

        circle = document.createElement("div")
        circle.id = f"q{self.id}"
        circle.className = "circle"
        circle.style.position = "absolute"
        circle.style.left = f"{map['x']}px"
        circle.style.top = f"{map['y']}px"
        self.circle = circle

        parent = document.createElement("div")
        parent.id = f"qq{self.id}"
        parent.className = "parent"
        parent.style.position = "absolute"
        parent.style.left = f"{map['x']}px"
        parent.style.top = f"{map['y']}px"
        self.parent = parent

        inner = document.createElement("div")
        inner.className = "inner"
        content = document.createElement("div")
        content.className = "quoinString"
        self.parent.appendChild(inner)
        inner.appendChild(content)

        # Grey out quoins if their stats are maxed out
        if self.stat_is_maxed:
            self.greyed_out = True

        cou_globals.view.playerHolder.appendChild(canvas)
        cou_globals.view.playerHolder.appendChild(circle)
        cou_globals.view.playerHolder.appendChild(parent)

        self.ready = True
        cou_globals.adding_locks[self.id] = False

    def update(self, dt: float) -> None:
        self.update_waited += dt
        if not self.ready or self.update_waited < 1/60:
            return

        # if a player collides with us, tell the server
        if not self.hit and (self._check_player_collision() and self._can_collect()):
            # intersecting and able to collect
            self._send_to_server()

            # don't check again
            self.hit = True
        elif not self.collected:
            self.hit = False

        if self.quoin_rect.intersects(cou_globals.camera.visible_rect):
            # in view
            self.animation.update_source_rect(self.update_waited)
            self.greyed_out = self.stat_is_maxed
        self.update_waited = 0

    def _check_notified(self, key: str) -> bool:
        # checks and updates notification history for this session

        if self.notified.get(key):
            # already notified, do not send it
            return True
        # first notification, update status
        self.notified[key] = True

        # send it this time
        return False

    def _toast_if_not_notified(self, message: str, key: str = None) -> bool:
        # not notified yet?
        if not self._check_notified(key or self.type_string):
            # send message
            Toast(message)

        return False

    def _can_collect(self) -> bool:
        """Returns true if the quoin can be collected, false (and possibly toasts) is not"""

        if cou_globals.metabolics.player_metabolics.quoins_collected >= cou_globals.constants.quoin_limit:
            return self._toast_if_not_notified(
                f"You've reached your daily limit of {cou_globals.constants.quoin_limit} quoins", "daily_limit",
            )
        elif self.type_string == "mood" and cou_globals.metabolics.player_metabolics.mood >= cou_globals.metabolics.player_metabolics.max_mood:
            return self._toast_if_not_notified(
                "You tried to collect a mood quoin, but your mood was already full.", "full_mood",
            )
        elif self.type_string == "energy" and cou_globals.metabolics.player_metabolics.energy >= cou_globals.metabolics.player_metabolics.max_energy:
            return self._toast_if_not_notified(
                "You tried to collect a energy quoin, but your energy was already full.", "full_energy",
            )
        else:
            return True

    def _check_player_collision(self) -> bool:
        if self.collected or self.checking:
            return False

        return self.quoin_rect.intersects(cou_globals.current_player.entity_rect)

    def _send_to_server(self) -> None:
        # don't try to collect the same quoin again before we get a response
        self.checking = True

        util.create_task(cou_globals.audio.play_sound("quoinSound"))

        self.circle.classList.add("circleExpand")
        self.parent.classList.add("circleExpand")
        util.Timer(timedelta(seconds=2), lambda: self._remove_circle_expand(self.parent))
        util.Timer(timedelta(milliseconds=800), lambda: self._remove_circle_expand(self.circle))
        self.canvas.style.display = "none"  # .remove() is very slow

        if getattr(cou_globals, "street_socket", None) and cou_globals.street_socket.readyState == 1:  # OPEN
            map = {
                "remove": self.id,
                "type": "quoin",
                "username": cou_globals.game.username,
                "streetName": cou_globals.current_street.label,
            }
            cou_globals.street_socket.send(json.dumps(map))

    def _remove_circle_expand(self, element):
        if element is not None:
            element.classList.remove("circleExpand")

    def render(self):
        if not self.ready or not self.animation.dirty or self.canvas.getAttribute("collected") != "false":
            return
        if not self.first_render:
            # if the entity is not visible, don't render it
            if not self.quoin_rect.intersects(cou_globals.camera.visible_rect):
                return

        self.first_render = False

        # fastest way to clear a canvas (without using a solid color)
        # source: http://jsperf.com/ctx-clearrect-vs-canvas-width-canvas-width/6
        self.canvas.getContext("2d").clearRect(0, 0, self.animation.width, self.animation.height)

        dest_rect = Rectangle(0, 0, self.animation.width, self.animation.height)
        self.canvas.getContext("2d").drawImage(
            self.animation.spritesheet, self.animation.source_rect.left, self.animation.source_rect.top,
            self.animation.source_rect.width, self.animation.source_rect.height,
            dest_rect.left, dest_rect.top, dest_rect.width, dest_rect.height,
        )
        self.animation.dirty = False

    @property
    def stat_is_maxed(self) -> bool:
        if cou_globals.metabolics.player_metabolics.quoins_collected >= cou_globals.constants.quoin_limit:
            return True

        match self.type_string:
            case "mood":
                return cou_globals.metabolics.player_metabolics.mood >= cou_globals.metabolics.player_metabolics.max_mood
            case "energy":
                return cou_globals.metabolics.player_metabolics.energy >= cou_globals.metabolics.player_metabolics.max_energy
            case _:
                return False

    @property
    def greyed_out(self) -> bool:
        return self._greyed_out

    @greyed_out.setter
    def greyed_out(self, new_value: bool) -> None:
        grey_class = "quoin-disabled"

        if new_value:
            self.canvas.classList.add(grey_class)
        else:
            self.canvas.classList.remove(grey_class)


cou_globals.quoins = {}
