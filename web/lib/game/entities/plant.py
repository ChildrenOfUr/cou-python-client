import logging
import json

from pyscript import document
from pyodide.ffi import create_proxy
from pyodide import http

from configs import Configs
from drawing import Rectangle
from game.entities.entity import Entity
from network.server_interop.itemdef import Action
from systems import util
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)


class Plant(Entity):
    def __init__(self, map):
        super().__init__()
        self._map = map
        self.ready = False
        self.first_render = True

        if "actions" in map:
            self._actions = map["actions"]

        self.canvas = document.createElement("canvas")
        self.canvas.id = map["id"]
        self.id = map["id"]

        self.num_rows = map["numRows"]
        self.num_columns = map["numColumns"]

        self.frame_list = [index for index in range(map["numFrames"])]

        url = map["url"].replace('"', "")
        async def set_canvas_height():
            response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/getActualImageHeight?url={url}&numRows={self.num_rows}&numColumns={self.num_columns}")
            self.canvas.setAttribute("actualHeight", await response.string())
        util.create_task(set_canvas_height())

        self.spritesheet = document.createElement("img")
        self.spritesheet.src = url
        def on_load(event):
            self.width = int(self.spritesheet.width // map["numColumns"])
            self.height = int(self.spritesheet.height // map["numRows"])
            self.x = float(map["x"])
            self.y = float(map["y"]) - self.height
            self.z = float(map["z"])
            self.rotation = float(map.get("rotation", 0))
            self.h_flip = str(map.get("h_flip")).lower() == "true"
            self.left = self.x
            self.top = self.y

            self.canvas.setAttribute("actions", json.dumps(map["actions"]))
            self.canvas.setAttribute("type", map["type"])
            self.canvas.classList.add("plant", "entity")
            self.canvas.style.zIndex = str(self.z)
            self.canvas.width = self.width
            self.canvas.height = self.height
            self.canvas.style.position = "absolute"
            self.canvas.style.transform = f"rotate({self.rotation}deg) translateX({self.x}px) translateY({self.y}px)" + ("scale3d(-1,1,1)" if self.h_flip else "scale3d(1,1,1)")
            self.canvas.setAttribute("translateX", str(self.x))
            self.canvas.setAttribute("translateY", str(self.y))
            self.canvas.setAttribute("width", str(self.width))
            self.canvas.setAttribute("height", str(self.height))
            self.state = map["state"]
            cou_globals.view.playerHolder.appendChild(self.canvas)
            self.source_rect = Rectangle(0, 0, self.width, self.height)
            self.ready = True
            cou_globals.adding_locks[self.id] = False
        self.spritesheet.addEventListener("load", create_proxy(on_load))

    def update_state(self, new_state: int) -> None:
        self.state = new_state
        self.dirty = True

    def update(self, dt: float) -> None:
        if not self.ready:
            return

        super().update(dt)

        column = self.state % self.num_columns
        row = self.state // self.num_columns

        self.source_rect = Rectangle(column * self.width, row * self.height, self.width, self.height)

    def render(self) -> None:
        if not self.ready or not self.dirty:
            return

        if not self.first_render:
            if not self.entity_rect.intersects(cou_globals.camera.visible_rect):
                return

        self.first_render = False

        context = self.canvas.getContext("2d")
        context.clearRect(0, 0, self.width, self.height)

        if self.glow:
            context.shadowBlur = 20
            context.shadowColor = "cyan"
            context.shadowOffsetX = 0
            context.shadowOffsetY = 1
        else:
            context.shadowColor = "0"
            context.shadowBlur = 0
            context.shadowOffsetX = 0
            context.shadowOffsetY = 0

        context.drawImage(
            self.spritesheet, self.source_rect.left, self.source_rect.top, self.source_rect.width, self.source_rect.height,
            self.dest_rect.left, self.dest_rect.top, self.dest_rect.width, self.dest_rect.height,
        )
        self.dirty = False
