import json
import logging
from datetime import timedelta

from pyscript import document
from pyodide import http

from configs import Configs
from game.animation import Animation
from network.server_interop.itemdef import Action
from game.entities.entity import Entity
from systems import util
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)


class NPC(Entity):
    def __init__(self, map):
        super().__init__()
        self._map = map
        self.speed = map["speed"]
        self.y_speed = map.get("ySpeed", 0)
        self.ready = False
        self._facing_right = True
        self.dont_flip = map.get("dontFlip", False)
        self.first_render = True
        self.chat_bubble = None
        self.type = map["type"]
        self.name_override = map.get("nameOverride")

        self.target_update_fps = 60
        self.update_waited = 0
        self._current_street = None
        self._camera = None
        self._is_hidden_spritesheet = False
        self._current_transform = None

        if "actions" in map:
            self._actions = map["actions"]

        canvas = document.createElement("canvas")

        frame_list = [index for index in range(map["numFrames"])]

        self.animation = Animation(
            map["url"], "npc", map["numRows"], map["numColumns"], frame_list,
            loop_delay=timedelta(milliseconds=map["loopDelay"]), loops=map["loops"],
        )

        async def load_spritesheet():
            await self.animation.load()
            if not self._is_hidden_spritesheet:
                response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/getActualImageHeight?url={map['url']}&numRows={map['numRows']}&numColumns={map['numColumns']}")
                self.canvas.setAttribute("actualHeight", await response.string())
            else:
                self.canvas.setAttribute("actualHeight", "1")

            self.id = map["id"]

            try:
                self.top = map["y"] - self.animation.height
                self.left = map["x"]
                self.z = map["z"]
                self.rotation = map["rotation"]
                self.h_flip = map["h_flip"]
                self.width = map["width"]
                self.height = map["height"]
            except Exception:
                LOGGER.exception(f"Error animating NPC {self.id}")
                self.top = self.left = self.width = self.height = 0

            canvas.id = map["id"]
            canvas.setAttribute("actions", json.dumps(map["actions"]))
            canvas.setAttribute("type", map["type"])
            canvas.classList.add("npc", "entity")
            canvas.width = map["width"]
            canvas.height = map["height"]
            canvas.style.position = "absolute"
            canvas.style.zIndex = str(self.z)
            canvas.setAttribute("translateX", str(self.left))
            canvas.setAttribute("translateY", str(self.top))
            canvas.setAttribute("width", str(canvas.width))
            canvas.setAttribute("height", str(canvas.height))
            cou_globals.view.playerHolder.appendChild(canvas)
            self.ready = True
            cou_globals.adding_locks[self.id] = False

        util.create_task(load_spritesheet())

        self.canvas = canvas
        if self.name_override is not None:
            self.canvas.dataset.nameOverride = self.name_override

    @property
    def facing_right(self) -> bool:
        return self._facing_right

    @facing_right.setter
    def facing_right(self, new_facing: bool) -> None:
        if self.h_flip:
            new_facing = not new_facing
        self._facing_right = new_facing

    def is_hidden_spritesheet(self, url: str) -> bool:
        url == "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"

    @property
    def x(self) -> int:
        return self.left

    @x.setter
    def x(self, new_x: int) -> None:
        if not self.ready or (self.left == new_x and not self.first_render):
            return
        self.left = new_x

    @property
    def y(self) -> int:
        return self.top

    @y.setter
    def y(self, new_y: int) -> None:
        if not self.ready or (self.left == new_y and not self.first_render):
            return
        self.top = new_y - (self.animation.height or 0)

    def update_animation(self, npc_map: dict) -> None:
        # new animation
        if self.ready and self.animation.animation_name != npc_map["animation_name"]:
            self.ready = False

            frame_list = [index for index in range(npc_map["numFrames"])]

            self.animation = Animation(
                npc_map["url"], npc_map["animation_name"], npc_map["numRows"], npc_map["numColumns"],
                frame_list, loops=npc_map["loops"],
            )
            self._is_hidden_spritesheet = self.is_hidden_spritesheet(self.animation.url)
            async def do_load():
                await self.animation.load()
                self.canvas.width = self.animation.width
                self.canvas.height = self.animation.height
                self.width = self.animation.width
                self.height = self.animation.height
                self.ready = True
            util.create_task(do_load())

    def _set_translate(self) -> None:
        if self._current_transform == (self.left, self.top, self.rotation):
            return
        if self.facing_right or self.dont_flip:
            self.canvas.style.transform = f"translateX({self.left}px) translateY({self.top}px) rotate({self.rotation}deg) scale3d(1,1,1)"
        else:
            self.canvas.style.transform = f"translateX({self.left}px) translateY({self.top}px) rotate({self.rotation}deg) scale3d(-1,1,1)"
        self._current_transform = (self.left, self.top, self.rotation)

    def update(self, dt: float) -> None:
        # saving these global variables to the local object saves a bit of time in each call to update
        # and because they never change for the life of the NPC, we don't have to worry about them being
        # out of date
        if not self._current_street:
            self._current_street = cou_globals.current_street
        if not self._camera:
            self._camera = cou_globals.camera

        self.update_waited += dt
        if self._current_street is None or self.canvas is None or self.update_waited < 1 / self.target_update_fps:
            return

        super().update(self.update_waited)

        self._set_translate()

        if not self._is_hidden_spritesheet and self.entity_rect.intersects(self._camera.visible_rect):
            self.animation.update_source_rect(self.update_waited)

        self.update_waited = 0

    def render(self) -> None:
        if not self.ready or (not self.dirty and not self.animation.dirty):
            return

        if not self.first_render:
            # if the entity is not visible, don't render it
            if not self.entity_rect.intersects(cou_globals.camera.visible_rect):
                return

        self.first_render = False

        context = self.canvas.getContext("2d")
        context.clearRect(0, 0, self.width, self.height)

        if self.glow:
            context.shadowBlur = 2
            context.shadowColor = "cyan"
            context.shadowOffsetX = 0
            context.shadowOffsetY = 1
        else:
            context.shadowColor = "0"
            context.shadowBlur = 0
            context.shadowOffsetX = 0
            context.shadowOffsetY = 0

        context.drawImage(
            self.animation.spritesheet, self.animation.source_rect.left, self.animation.source_rect.top,
            self.animation.source_rect.width, self.animation.source_rect.height,
            self.dest_rect.left, self.dest_rect.top, self.dest_rect.width, self.dest_rect.height,
        )
        self.animation.dirty = False
        self.dirty = False
