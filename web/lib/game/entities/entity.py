import asyncio
import logging

from pyscript import document
from pyodide import http

from configs import Configs
from drawing import Rectangle
from game.chat_bubble import ChatBubble
from network.server_interop.itemdef import Action
from systems import util
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)


class Entity:
    def __init__(self) -> None:
        self.glow = False
        self.dirty = True
        self.multi_unselect = False
        self.h_flip = False
        self.chat_bubble: ChatBubble = None
        self.canvas = None

        self.left = 0
        self.top = 0
        self.z = 0
        self.rotation = 0
        self.width = 0
        self.height = 0

        self.id = None

        self._entity_rect = Rectangle(0, 0, 0, 0)
        self._dest_rect = None
        self._current_player = None

        self._actions = []

    @property
    def actions(self) -> list[Action]:
        """Because we receive updated actions from the server quite often
        but we only need to read them seldomly, we'll just store the data and
        then decode it on demand.
        """

        return Action.from_json(self._actions, many=True)

    def update(self, dt: float) -> None:
        if not self._current_player:
            self._current_player = cou_globals.current_player
        entity_rect = self.entity_rect
        if self != self._current_player and self._current_player.entity_rect.intersects(entity_rect):
            self.update_glow(True)
            self._current_player.intersecting_objects[self.id] = entity_rect
        else:
            self._current_player.intersecting_objects.pop(self.id, None)
            self.update_glow(False)

    @property
    def dest_rect(self) -> Rectangle:
        if self._dest_rect is None:
            self._dest_rect = Rectangle(0, 0, self.width, self.height)
        else:
            self._dest_rect.left = 0
            self._dest_rect.top = 0
            self._dest_rect.width = self.width
            self._dest_rect.height = self.height

        return self._dest_rect

    @property
    def entity_rect(self) -> Rectangle:
        if self._entity_rect is None:
            self._entity_rect = Rectangle(self.left, self.top, self.width, self.height)
        else:
            self._entity_rect.left = self.left
            self._entity_rect.top = self.top
            self._entity_rect.width = self.width
            self._entity_rect.height = self.height
        return self._entity_rect

    def render(self) -> None:
        # each subclass must implement render for itself
        raise NotImplementedError

    def update_glow(self, new_glow: bool) -> None:
        if self.multi_unselect:
            self.glow = False
            self.dirty = True
            return

        if self.glow != new_glow:
            self.dirty = True
        self.glow = new_glow

    async def get_actions(self) -> list[Action]:
        """Returns a list of actions which currently applies to the player and the entity"""

        enabled = False
        url = f"{Configs.http}//{Configs.utilServerAddress}/getActions"
        url += f"?email={cou_globals.game.email}&id={self.id}&label={cou_globals.current_street.label}"
        response = await http.pyfetch(url)

        action_list: list[Action] = []
        self._actions = await response.json()
        for action in self.actions:
            enabled = action.enabled
            action.action_name = f"{action.action_name[0].upper()}{action.action_name[1:]}"
            error = ""
            if enabled:
                enabled = action.has_requirements()
                if enabled:
                    error = action.description
                else:
                    error = action.get_requirement_string()
            else:
                error = action.error
            menu_action = Action.clone(action)
            menu_action.enabled = enabled
            menu_action.error = error
            action_list.append(menu_action)

        return action_list

    def interact(self, id: str) -> None:
        from game.entities.player import Player

        element = document.querySelector(f"#{self.id}")
        if not element:
            element = document.querySelector(f"#player-{self.id}")

        def show_menu(result: asyncio.Future):
            name = getattr(element.dataset, "nameOverride", element.getAttribute("type"))
            if name is None:
                name = id
            server_class = "global_action_monster" if isinstance(self, Player) else name
            cou_globals.input_manager.showClickMenu(None, title=name, id=id, server_class=server_class, actions=result.result())
        actions_task = util.create_task(self.get_actions())
        actions_task.add_done_callback(show_menu)

    def __hash__(self) -> int:
        return hash(self.id)

    def __eq__(self, other: "Entity"):
        if not isinstance(other, Entity):
            return False
        return other.id == self.id


def get_entity(id: str) -> Entity:
    return cou_globals.entities.get(id, cou_globals.other_players.get(id))


def get_entity_element(id: str):
    element = document.querySelector(f"#{id}")
    if not element:
        element = document.querySelector(f"#player-{id}")
    return element


cou_globals.entities = {}
