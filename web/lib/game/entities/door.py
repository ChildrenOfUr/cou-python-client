import json

from pyscript import document
from pyodide.ffi import create_proxy

from drawing import Rectangle
from network.server_interop.itemdef import Action
from game.entities.entity import Entity
from systems.global_gobbler import cou_globals


class Door(Entity):
    def __init__(self, map):
        super().__init__()
        self._map = map
        self.ready = False
        self.first_render = True

        if "actions" in map:
            self._actions = map["actions"]

        canvas = document.createElement("canvas")
        canvas.id = map["id"]
        self.id = map["id"]
        url = map["url"]
        self.spritesheet = document.createElement("img")
        self.spritesheet.src = url
        def on_load(_):
            self.width = self.spritesheet.width // int(map["numColumns"])
            self.height = self.spritesheet.height // int(map["numRows"])
            self.x = float(map["x"])
            self.y = float(map["y"]) - self.height
            self.left = self.x
            self.top = self.y

            canvas.setAttribute("actions", json.dumps(map["actions"]))
            canvas.setAttribute("type", map["type"])
            canvas.classList.add("door", "entity")
            canvas.style.zIndex = "-1"  # make sure plants are behind animals
            canvas.width = self.width
            canvas.height = self.height
            canvas.style.position = "absolute"
            canvas.style.transform = f"translateX({self.x}px) translateY({self.y}px)"
            canvas.setAttribute("translateX", str(self.x))
            canvas.setAttribute("translateY", str(self.y))
            canvas.setAttribute("width", str(self.width))
            canvas.setAttribute("height", str(self.height))
            cou_globals.view.playerHolder.appendChild(canvas)
            self.source_rect = Rectangle(0, 0, self.width, self.height)
            self.ready = True
            cou_globals.adding_locks[self.id] = False
        self.spritesheet.addEventListener("load", create_proxy(on_load))
        self.canvas = canvas

    def render(self) -> None:
        if not self.ready or not self.dirty:
            return

        if not self.first_render and not self.entity_rect.intersects(cou_globals.camera.visible_rect):
            return

        self.first_render = False

        context = self.canvas.getContext("2d")
        context.clearRect(0, 0, self.width, self.height)
        if self.glow:
            context.shadowBlur = 20
            context.shadowColor = "cyan"
            context.shadowOffsetX = 0
            context.shadowOffsetY = 1
        else:
            context.shadowBlur = 0
            context.shadowColor = "0"
            context.shadowOffsetX = 0
            context.shadowOffsetY = 0

        context.drawImage(
            self.spritesheet, self.dest_rect.left, self.dest_rect.top, self.dest_rect.width, self.dest_rect.height,
        )
        self.dirty = False
