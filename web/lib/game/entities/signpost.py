from datetime import datetime, timedelta
import random

from pyscript import document, window
from pyodide.ffi import create_proxy

from game.entities.entity import Entity
from systems.global_gobbler import cou_globals
from systems import util


class Signpost(Entity):
    def __init__(self, signpost: dict, x: int, y: int):
        super().__init__()
        self.signs = []
        self.interacting = False
        self.let_go = False
        self.click_listener = None

        h = 200
        w = 100

        if signpost.get("h") is not None:
            h = signpost["h"]
        if signpost.get("w") is not None:
            w = signpost["w"]

        self.left = x
        self.top = y
        self.width = w
        self.height = h

        pole = document.createElement("div")
        pole.className = "entity"

        pole.setAttribute("translateX", str(x))
        pole.setAttribute("translateY", str(y))
        pole.setAttribute("width", str(w))
        pole.setAttribute("height", str(h))

        pole.style.backgroundImage = "url('https://childrenofur.com/locodarto/scenery/sign_pole.png')"
        pole.style.backgroundRepeat = "no-repeat"
        pole.style.backgroundPosition = "center bottom"
        pole.style.pointerEvents = "auto"
        pole.style.width = f"{w}px"
        pole.style.height = f"{h}px"
        pole.style.position = "absolute"
        pole.style.top = f"{y}px"
        pole.style.left = f"{x}px"

        self.id = f"pole{random.randint(0, 50)}"
        pole.id = self.id
        cou_globals.entities[self.id] = self

        for index, exit in enumerate(signpost["connects"]):
            if exit["label"] == cou_globals.player_tele_from or cou_globals.player_tele_from == "console":
                cou_globals.current_player.left = x
                cou_globals.current_player.top = y

            tsid = util.tsidG(exit["tsid"])
            span = document.createElement("span")
            span.style.top = f"{index * 25 + 10}px"
            span.innerText = exit["label"]
            span.className = "ExitLabel"
            span.setAttribute("url", f"https://RobertMcDermot.github.io/CAT422-glitch-location-viewer/locations/{tsid}.callback.json")
            span.setAttribute("tsid", tsid)
            span.setAttribute("from", cou_globals.current_street.label)

            pole.appendChild(span)
            self.signs.append(span)

            if index % 2 == 0:
                span.style.right = "50%"
                span.style.transform = "rotate(5deg)"
            else:
                span.style.left = "50%"
                span.style.transform = "rotate(-5deg)"

        self.pole = pole
        cou_globals.view.playerHolder.append(pole)

    def update(self, dt) -> None:
        super().update(dt)

    def render(self) -> None:
        if self.dirty:
            if self.glow:
                self.pole.classList.add("hovered")
            else:
                self.pole.classList.remove("hovered")
                for sign in self.signs:
                    sign.classList.remove("hovered")

            self.dirty = False

    def interact(self, id: str) -> None:
        if len(self.signs) == 1:
            # only one exit -> Go to that one immediately
            self.signs[0].click()
            return
        elif cou_globals.GPS.active:
            # multiple exits -> Go to the one matching the next street along the GPS path
            next_street = next((sign for sign in self.signs if sign.innerText == cou_globals.GPS.next_street_name()), None)
            if next_street:
                next_street.click()

        if not self.interacting:
            # remove the glow around the pole and put one on the first sign
            self.pole.classList.remove("hovered")
            for sign in self.signs:
                sign.classList.remove("hovered")
            self.signs[0].classList.add("hovered")

            self.interacting = True
            self.let_go = False

        # check for gamepad input
        self.gamepad_loop(0.0)

        def on_keydown(event):
            keys = cou_globals.input_manager.keys
            ignore_keys = cou_globals.input_manager.ignoreKeys
            stop_keys = (
                keys["LeftBindingPrimary"], keys["LeftBindingAlt"],
                keys["RightBindingPrimary"], keys["RightBindingAlt"],
                keys["JumpBindingPrimary"], keys["JumpBindingAlt"],
                27, # escape key
            )

            # up arrow or w and not typing
            if (event.keyCode == keys["UpBindingPrimary"] or event.keyCode == keys["UpBindingAlt"]) and not ignore_keys:
                self.select_up()
            if (event.keyCode == keys["DownBindingPrimary"] or event.keyCode == keys["DownBindingAlt"]) and not ignore_keys:
                self.select_down()
            if not ignore_keys and event.keyCode in stop_keys:
                self.stop()
            if (event.keyCode == keys["ActionBindingPrimary"] or event.keyCode == keys["ActionBindingAlt"]) and not ignore_keys:
                self.click_selected()
                self.stop()
        key_handler = create_proxy(on_keydown)
        cou_globals.input_manager.menuKeyListener = ("keydown", key_handler)
        document.addEventListener("keydown", key_handler)

        def on_click(event):
            self.stop()
        click_handler = create_proxy(on_click)
        self.click_listener = ("click", click_handler)
        document.addEventListener("click", click_handler)

    def gamepad_loop(self, dt: float) -> None:
        # only select a new option once every 300ms
        select_again = cou_globals.input_manager.lastSelect + timedelta(milliseconds=300) < datetime.now()
        if cou_globals.input_manager.upKey.active and select_again:
            self.select_up()

        if cou_globals.input_manager.downKey.active and select_again:
            self.select_down()

        if cou_globals.input_manager.leftKey.active or cou_globals.input_manager.rightKey.active or cou_globals.input_manager.jumpKey.active:
            self.stop()

        if cou_globals.input_manager.actionKey.active and self.let_go:
            self.click_selected()
            self.stop()

        if not self.let_go and not cou_globals.input_manager.actionKey.active:
            self.let_go = True

        if self.interacting:
            window.requestAnimationFrame(create_proxy(self.gamepad_loop))

    def stop(self) -> None:
        cou_globals.input_manager.stopMenu(None)
        if self.click_listener:
            document.removeEventListener(*self.click_listener)
        self.interacting = False
        self.let_go = False
        for sign in self.signs:
            sign.classList.remove("hovered")

    def select_up(self) -> None:
        removed = 0
        for index, sign in enumerate(self.signs):
            if sign.classList.contains("hovered"):
                sign.classList.remove("hovered")
                removed = index
        if removed == 0:
            self.signs[-1].classList.add("hovered")
        else:
            self.signs[removed - 1].classList.add("hovered")

        cou_globals.input_manager.lastSelect = datetime.now()

    def select_down(self) -> None:
        removed = len(self.signs) - 1
        for index, sign in enumerate(self.signs):
            if sign.classList.contains("hovered"):
                sign.classList.remove("hovered")
                removed = index
        if removed == len(self.signs) - 1:
            self.signs[0].classList.add("hovered")
        else:
            self.signs[removed + 1].classList.add("hovered")

        cou_globals.input_manager.lastSelect = datetime.now()

    def click_selected(self) -> None:
        for sign in self.signs:
            if sign.classList.contains("hovered"):
                cou_globals.input_manager.stopMenu(None)
                sign.click()
        self.interacting = False
