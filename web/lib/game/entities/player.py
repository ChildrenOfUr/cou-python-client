import asyncio
from datetime import timedelta
import logging
import random

from pyscript import document
from pyodide import http

from configs import Configs
from display.buff import Buff
from display.render.camera import Camera
from display.render.platform import Platform
from drawing import Rectangle
from game.animation import Animation
from game.entities.entity import Entity
from game.entities.physics import Physics
from game.input import InputManager
from game.street import Street
from message_bus import MessageBus
from network.server_interop.itemdef import Action
from systems import util
from systems.global_gobbler import cou_globals


follow_action = Action.with_name("follow")
follow_action.description = "We already know it's buggy, but we thought you'd have fun with it."
PLAYER_ACTIONS = [follow_action, Action.with_name("profile")]
MIN_SCALE = 0.2
LOGGER = logging.getLogger(__name__)


class Player(Entity):
    def __init__(self, username: str) -> None:
        super().__init__()
        self.id = username

        self.physics = Physics.get("normal")
        self.street_z = 0
        self.y_vel = 0
        self.y_accel = -2400

        self.active_climb = False
        self.climbing_down = False
        self.climbing_up = False
        self.facing_right = True
        self.first_render = True
        self.is_guide = False
        self.jumping = False
        self.last_climb_status = False
        self.moving = False
        self.last_move = 0.0
        self.collision_rect = None

        self.animations: dict[str, Animation] = {}
        self.current_animation: Animation = None

        self.intersecting_objects: dict[str, Rectangle] = {}

        self.jump_count = 0
        self.jump_timer: util.Timer = None
        self.following_player: Player = None
        self.lost_focus = False

        # for testing purposes
        # if false, player can move around with wasd and arrows, no falling
        self.do_physics_apply = True

        self.left = cou_globals.metabolics.current_street_x
        self.top = cou_globals.metabolics.current_street_y
        self.width = 116
        self.height = 137
        self.ladder_rect = Rectangle(self.left + self.width / 2, self.top + cou_globals.current_street.ground_y, self.width / 2 , self.height)
        self.ladder_landing = None
        self.best_platform = None

        self.translate_x = self.left
        self.translate_y = self.top
        self._current_transform = None

        if self.left == 1.0 and self.top == 0.0:
            found = False
            leftmost = None

            for platform in cou_globals.current_street.platforms:
                if leftmost is None or platform.start.x < leftmost.start.x:
                    leftmost = platform
                if platform.start.x == 1:
                    found = True
                    self.top = platform.start.y - self.height

            if not found:
                self.top = 0.0

        self.canvas = document.createElement("canvas")
        self.canvas.classList.add("canvas")
        self.canvas.style.position = "absolute"
        self.canvas.style.overflow = "auto"
        self.canvas.style.margin = "auto"

        self.player_name = document.createElement("div")
        self.player_name.classList.add("playerName")
        self.player_name.innerText = self.id

        self.player_parent_element = document.createElement("div")
        self.player_parent_element.classList.add("playerParent")
        self.player_parent_element.id = f"pc-player={self.id}"
        self.player_parent_element.style.width = f"{self.width}px"
        self.player_parent_element.style.height = f"{self.height}px"

        self.super_parent_element = document.createElement("div")
        self.super_parent_element.classList.add("playerParent")
        self.super_parent_element.appendChild(self.player_parent_element)

        if self.id != cou_globals.game.username:
            self.player_parent_element.appendChild(self.player_name)
            self._actions = PLAYER_ACTIONS.to_json()
        self.player_parent_element.appendChild(self.canvas)
        cou_globals.view.worldElement.appendChild(self.super_parent_element)

        # save these as local copies (saves on performance of lookups)
        # and allows for typing
        self._view = cou_globals.view
        self._camera: Camera = cou_globals.camera
        self._street: Street = cou_globals.current_street

        self.update_physics()

        def on_street_load(street):
            self.update_physics(street)
            self._street = cou_globals.current_street
        MessageBus.subscribe("streetLoaded", on_street_load)

    @property
    def climbing(self) -> bool:
        if self.id == cou_globals.game.username:
            return self.climbing_up or self.climbing_down
        return self.current_animation.animation_name == "climb"

    @property
    def scale(self) -> float:
        if self.z == 0:
            return 1
        scale = self.street_z / self.z
        scale *= 0.01
        scale = max(1, min(scale, MIN_SCALE))
        return scale

    async def load_animations(self):
        # need to get background images from some server for each player baed on name
        idle1_frames = (list(range(16)) + list(range(14, 3, -1))) * 3
        idle2_frames = list(range(57))
        idle3_frames = list(range(50))
        fall_asleep_frames = list(range(42))
        sleep_frames = list(range(41, 15, -1)) + list(range(15, 42))
        base_frames = list(range(12))
        jump_up_frames = list(range(16))
        fall_down_frames = list(range(16, 24))
        land_frames = list(range(24, 33))
        climb_frames = list(range(19))

        response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/getSpritesheets?username={self.id}")
        spritesheets = await response.json()
        if "base" not in spritesheets:
            idle2 = "files/sprites/idle.png"
            base = "files/sprites/base.png"
            jump = "files/sprites/jump.png"
            climb = "files/sprites/climb.png"
        else:
            idle1 = spritesheets["idle1"]
            idle2 = spritesheets["idle2"]
            idle3 = spritesheets["idle3"]
            fall_asleep = spritesheets["idleSleepy"]
            base = spritesheets["base"]
            jump = spritesheets["jump"]
            climb = spritesheets["climb"]

        still = Animation(base, "still", 1, 15, [14], fps=1/10)
        self.animations = {
            "still": still,
            # "idle1": Animation(idle1, "idle1", 1, 16, idle1_frames, loop_delay=timedelta(seconds=10), still_frame=still),
            "idle2": Animation(idle2, "idle2", 2, 29, idle2_frames, loop_delay=timedelta(seconds=10), delay_initially=True),
            # "idle3": Animation(idle3, "idle3", 2, 25, idle3_frames, loop_delay=timedelta(seconds=10), delay_initially=True),
            # "fallasleep": Animation(fall_asleep, "fallasleep", 2, 21, fall_asleep_frames, loop_frames=sleep_frames),
            "base": Animation(base, "base", 1, 15, base_frames),
            "die": Animation(base, "die", 1, 15, [12, 13], loops=False),
            "jumpup": Animation(jump, "jumpup", 1, 33, jump_up_frames),
            "falldown": Animation(jump, "falldown", 1, 33, fall_down_frames),
            "land": Animation(jump, "land", 1, 33, land_frames),
            "climb": Animation(climb, "climb", 1, 19, climb_frames),
        }

        await asyncio.gather(*(animation.load() for animation in self.animations.values()))

    def update_physics(self, street: dict = None):
        if street is None:
            street = {}
        self.physics = Physics.get_street(street.get("label"))
        self.street_z = cou_globals.map_data.check_int_setting("depth", default_value=0)

    def update(self, dt: float, partial: bool = False):
        if not cou_globals.current_street.loaded:
            return

        # references to some globals with types for easier coding below
        input_manager: InputManager = cou_globals.input_manager
        current_street: Street = cou_globals.current_street

        context = self.canvas.getContext("2d")
        if self.glow:
            context.shadowBlur = 2
            context.shadowColor = "cyan"
            context.shadowOffsetX = 0
            context.shadowOffsetY = 1
        else:
            context.shadowBlur = 0
            context.shadowColor = "0"
            context.shadowOffsetX = 0
            context.shadowOffsetY = 0

        if partial:
            super().update(dt)
            return

        came_from = self.top

        # show chat message if it exists and decrement its time_to_live
        if self.chat_bubble is not None:
            self.chat_bubble.update(dt)

        self.ladder_rect.left = self.left + self.width / 3
        self.ladder_rect.top = self.top + current_street.ground_y
        self.update_ladder_status(dt)

        if not input_manager.windowFocused and (self.moving or self.jumping or self.active_climb) and not self.lost_focus:
            self.lost_focus = True

            # reset all input controls
            for control in input_manager.controlCounts.values():
                control.signals.clear()

        if input_manager.windowFocused and self.lost_focus:
            self.lost_focus = False
        if self.do_physics_apply and not input_manager.upKey.active and not input_manager.downKey.active:
            # not moving up or down

            for ladder in current_street.ladders:
                if ladder.bounds.intersects(self.ladder_rect):
                    # touching a ladder
                    break
            else:
                # not touching a ladder
                self.climbing_down = False
                self.climbing_up = False

            self.active_climb = False

        if input_manager.rightKey.active:
            # moving right
            self.left += self.physics.speed * dt * self.scale
            self.facing_right = True if not self.climbing else self.facing_right
            self.moving = True
            self.update_ladder_status(dt)
        elif input_manager.leftKey.active:
            # moving left
            self.left -= self.physics.speed * dt * self.scale
            self.facing_right = False if not self.climbing else self.facing_right
            self.moving = True
            self.update_ladder_status(dt)
        else:
            # not moving
            self.moving = False

        if not self.climbing and self.street_z != 0:
            # not near a ladder, and this street is 3d
            if input_manager.upKey.active and self.scale > MIN_SCALE:
                # moving back
                self.z += self.physics.z_speed * self.scale * dt
                self.moving = True
            elif input_manager.downKey.active and self.scale < 1:
                # moving foward
                self.z -= self.physics.z_speed * self.scale * dt
                self.moving = True
            else:
                # not moving
                self.moving = False

        # primitive jumping
        if input_manager.jumpKey.active and (not self.jumping or self.physics.infinite_jump) and not self.climbing_up and not self.climbing_down:
            spinach_buff = Buff.is_running("spinach")
            pie_buff = Buff.is_running("full_of_pie")

            if self.physics.jump_multiplier != Physics.get(Physics.DEFAULT_ID).jump_multiplier:
                # not normal jumping
                jump_multiplier = self.physics.jump_multiplier
            elif spinach_buff:
                jump_multiplier = 1.65
            elif pie_buff:
                jump_multiplier = 0.65
            else:
                jump_multiplier = 1

            self.jumping = True
            if self.physics.can_triple_jump and not pie_buff:
                if self.jump_timer is None:
                    # start timer
                    def do_jump():
                        # normal jump
                        self.jump_count = 0
                        self.jump_timer.cancel()
                        self.jump_timer = None
                    self.jump_timer = util.Timer(timedelta(seconds=3), do_jump)

                if self.jump_count == 2:
                    # triple jump
                    self.y_vel = -(self.physics.y_vel_triple_jump * jump_multiplier) * self.scale
                    self.jump_count = 0
                    self.jump_timer.cancel()
                    self.jump_timer = None

                    if not self.climbing:
                        util.create_task(cou_globals.audio.play_sound("tripleJump"))
                else:
                    # normal jump
                    self.jump_count += 1
                    self.y_vel = -(self.physics.y_vel_jump * jump_multiplier) * self.scale
            else:
                # triple jumping disabled
                self.y_vel = -(self.physics.y_vel_jump * jump_multiplier) * self.scale

        # needs acceleration, some gravity const somewhere
        # for jumps/falling
        if self.do_physics_apply and not self.climbing_up and not self.climbing_down:
            # walking
            self.y_vel -= self.y_accel * dt * self.scale
            self.top += self.y_vel * dt * self.scale
        else:
            # climbing
            if input_manager.downKey.active:
                self.top += self.physics.speed * dt * self.scale

            if input_manager.upKey.active:
                self.top -= self.physics.speed * dt * self.scale

        if self.left < 0:
            self.left = 0.0
            self.moving = False
        elif self.left > current_street.bounds.width - self.width:
            self.left = current_street.bounds.width - self.width
            self.moving = False
        elif input_manager.leftKey.active or input_manager.rightKey.active:
            self.moving = True

        if self.facing_right:
            self.collision_rect = Rectangle(
                self.left + self.width / 2, self.top + current_street.ground_y + self.height / 4,
                self.width / 2, self.height * 3 / 4 - 35,
            )
        else:
            self.collision_rect = Rectangle(
                self.left, self.top + current_street.ground_y + self.height / 4,
                self.width / 2, self.height * 3 / 4 - 35,
            )

        # check for collisions with walls
        if self.do_physics_apply and input_manager.leftKey.active or input_manager.rightKey.active:
            for wall in current_street.walls:
                if self.collision_rect.intersects(wall.bounds):
                    if self.facing_right:
                        if self.collision_rect.right >= wall.bounds.left:
                            self.left = wall.bounds.left - self.width - 1
                    else:
                        if self.collision_rect.left < wall.bounds.left:
                            self.left = wall.bounds.right + 1

        # check for collisions with platforms
        if self.do_physics_apply and not self.climbing_down and not self.climbing_up and self.y_vel >= 0:
            x = self.left + self.width / 2
            self.best_platform = self._get_best_platform(came_from)
            if self.best_platform:
                going_to = self.top + self.height + cou_globals.current_street.ground_y
                slope = (self.best_platform.end.y - self.best_platform.start.y) / (self.best_platform.end.x - self.best_platform.start.x)
                y_int = self.best_platform.start.y - slope * self.best_platform.start.x
                line_y = slope * x + y_int

                if going_to >= line_y:
                    self.top = line_y - self.height - cou_globals.current_street.ground_y
                    self.y_vel = 0
                    self.jumping = False

        # check for collisions with ceilings
        if self.do_physics_apply and not self.climbing_down and not self.climbing_up and self.y_vel < 0:
            for platform in current_street.platforms:
                if platform.ceiling and platform.bounds.intersects(self.collision_rect):
                    x = self.left + self.width / 2
                    slope = (platform.end.y - platform.start.y) / (platform.end.x - platform.start.x)
                    y_int = platform.start.y - slope * platform.start.x
                    line_y = slope * x + y_int

                    self.top = line_y - current_street.ground_y
                    self.y_vel = 0
                    break

        self.z = max(self.street_z, min(0, self.z))

        self.update_animation(dt)
        self.update_transform()

        # update the entity rect so that other entities know if the player is colliding with them
        self._entity_rect.left = self.left
        self._entity_rect.top = self.top
        self._entity_rect.width = self.width
        self._entity_rect.height = self.height

        # if following a player, go to them
        if self.id == cou_globals.current_player.id and self.following_player is not None:
            # turn to face the same direction
            self.facing_right = self.following_player.facing_right

            # copy their animation state
            self.update_animation(dt, override=self.following_player.current_animation.animation_name)

            # go to their X coordinate (+/- a body width or so if on a platform)
            x_offset = (0 if self.following_player.climbing else (-1 if self.facing_right else 1) * self._entity_rect.width)
            self.left = self.following_player.left + x_offset

            # go to their y coordinate (if not on a platform)
            if self.following_player.jumping or self.following_player.climbing:
                self.top = self.following_player.top

        if not self.moving:
            self.last_move += dt
        else:
            self.last_move = 0.0

        super().update(dt)

    def update_ladder_status(self, dt: float) -> None:
        if self.do_physics_apply and cou_globals.input_manager.upKey.active:
            # moving up

            for ladder in cou_globals.current_street.ladders:
                if not self.climbing_up:
                    # if we haven't started climbing yet, then pretend the ladder is further below our feet
                    # this should prevent the problem where if you stand on top of a ladder and hold up, you jiggle/vibrate
                    # because you start and stop climbing over and over
                    ladder_bounds = Rectangle(
                        ladder.bounds.left, ladder.bounds.top + min(ladder.bounds.height / 2, 20),
                        ladder.bounds.width, ladder.bounds.height,
                    )
                else:
                    ladder_bounds = ladder.bounds
                if ladder_bounds.intersects(self.ladder_rect):
                    # touching a ladder, stop our y velocity so we don't "pop up" when we come off the top if we were jumping
                    self.y_vel = 0

                    self.top -= self.physics.speed / 4 * dt
                    self.climbing_up = True
                    self.active_climb = True
                    self.jumping = False
                    break
            else:
                # not touching a ladder
                self.ladder_landing = None
                self.climbing_up = False
                self.climbing_down = False
                self.active_climb = False

        if self.do_physics_apply and cou_globals.input_manager.downKey.active:
            # moving down

            for ladder in cou_globals.current_street.ladders:
                if not self.climbing_down:
                    # If we haven't started descending yet, then pretend the ladder is further above our head
                    # this should prevent the problem where if you hold down after reaching the end of the
                    # ladder, you would jiggle/vibrate because you've reached the landing over and over
                    ladder_bounds = Rectangle(
                        ladder.bounds.left, ladder.bounds.top,
                        ladder.bounds.width, ladder.bounds.height - min(ladder.bounds.height / 2, self.height + 20),
                    )
                else:
                    ladder_bounds = ladder.bounds
                if ladder_bounds.intersects(self.ladder_rect):
                    # touching a ladder

                    self.top += self.physics.speed / 4 * dt
                    self.climbing_down = True
                    self.active_climb = True
                    self.jumping = False

                    # if we've reached the bottom of the ladder, stop climbing down unless the ladder
                    # continues on through this platform
                    if not self.ladder_landing:
                        self.ladder_landing = self._get_best_platform(ladder.bounds.bottom - self.height - cou_globals.current_street.ground_y)
                    if self.ladder_rect.bottom < self.ladder_landing.bounds.top or ladder.bounds.bottom > self.ladder_landing.bounds.top:
                        break
            else:
                # not touching a ladder
                self.ladder_landing = None
                self.climbing_down = False
                self.climbing_up = False
                self.active_climb = False

        if cou_globals.input_manager.rightKey.active or cou_globals.input_manager.leftKey.active:
            # left or right on a ladder

            for ladder in cou_globals.current_street.ladders:
                if ladder.bounds.intersects(self.ladder_rect):
                    # touching a ladder
                    break
            else:
                # not touching a ladder
                self.ladder_landing = None
                self.climbing_down = False
                self.climbing_up = False
                self.active_climb = False

    def update_animation(self, dt: float, override: str = None):
        previous = self.current_animation

        if override is not None:
            self.current_animation = self.animations[override]
        elif "fallasleep" in self.animations and self.last_move > 120:
            self.current_animation = self.animations["fallasleep"]
        else:
            climbing = self.climbing_up or self.climbing_down

            idle_choices = [anim for name, anim in self.animations.items() if "idle" in name]
            if not self.moving and not self.jumping and not self.climbing:
                if self.current_animation.complete:
                    for anim in idle_choices:
                        anim.reset()
                    self.current_animation = random.choice(idle_choices)
            else:
                # reset idle animations so that the 10 second delay starts over
                for anim in idle_choices:
                    anim.reset()
                if "fallasleep" in self.animations:
                    self.animations["fallasleep"].reset()

                if climbing:
                    if self.active_climb != self.last_climb_status:
                        self.last_climb_status = self.active_climb

                    self.current_animation = self.animations["climb"]
                    self.current_animation.paused = not self.active_climb
                else:
                    if self.moving and not self.jumping:
                        self.current_animation = self.animations["base"]
                    elif self.jumping and self.y_vel < 0:
                        self.current_animation = self.animations["jumpup"]
                        self.animations["falldown"].reset()
                    elif self.jumping and self.y_vel >= 0:
                        self.current_animation = self.animations["falldown"]
                        self.animations["jumpup"].reset()

        self.current_animation.update_source_rect(dt, hold_at_last_frame=self.jumping)

        if previous != self.current_animation:
            # force a player update to be sent right now
            cou_globals.time_last = 5.0

    def update_transform(self) -> None:
        translate_x = self.left
        translate_y = self._view.worldElementWidth - self.height

        if self.left > self._street.bounds.width - self.width / 2 - self._view.worldElementWidth / 2:
            translate_x = self.left - self._street.bounds.width + self._view.worldElementWidth
            # allow character to move to screen right
        elif self.left + self.width / 2 > self._view.worldElementWidth / 2:
            translate_x = self._view.worldElementWidth / 2 - self.width / 2
            # keep character in center of screen

        if self.top + self.height / 2 < self._view.worldElementHeight / 2:
            translate_y = self.top
        elif self.top < self._street.bounds.height - self.height / 2 - self._view.worldElementHeight / 2:
            translate_y = self._view.worldElementHeight / 2 - self.height / 2
        else:
            translate_y = self._view.worldElementHeight - (self._street.bounds.height - self.top)

        # move up to simulate moving back
        translate_y -= self.z

        # if the game window is larger than the street, then the street gets centered between
        # the difference in sizes
        if self._view.worldElementHeight > self._street.bounds.height:
            translate_y -= (self._view.worldElementHeight - self._street.bounds.height) * .5

        # translateZ forces the whole operation to be gpu accelerated
        transform = f"translateX({translate_x}px) translateY({translate_y}px) translateZ(0)"

        if not self.facing_right:
            transform += f" scale3d(-{self.scale}, {self.scale}, {self.scale})"

            if self.chat_bubble is not None:
                self.chat_bubble.text_element.style.transform = "scale3d(-1,1,1)"
        else:
            transform += f" scale3d({self.scale}, {self.scale}, {self.scale})"

            if self.chat_bubble is not None:
                self.chat_bubble.text_element.style.transform = "scale3d(1,1,1)"

        if self._current_transform != (translate_x, translate_y, self.scale, self.facing_right):
            self.player_parent_element.style.transform = transform
            self._current_transform = (translate_x, translate_y, self.scale, self.facing_right)
        self.translate_x = translate_x
        self.translate_y = translate_y

    def render(self):
        if self.current_animation is None or not self.current_animation.loaded or not self.current_animation.dirty:
            return
        if not self.first_render:
            entity_rect = Rectangle(self.left, self.top, self.current_animation.width, self.current_animation.height)
            if not entity_rect.intersects(cou_globals.camera.visible_rect):
                return

        self.first_render = False

        if self.canvas.width != self.current_animation.width or self.canvas.height != self.current_animation.height:
            self.canvas.style.width = f"{self.current_animation.width}px"
            self.canvas.style.height = f"{self.current_animation.height}px"
            self.canvas.width = self.current_animation.width
            self.canvas.height = self.current_animation.height
            x = -(self.current_animation.width // 2)
            y = -(self.current_animation.height - self.height)

            if Buff.is_running("grow"):
                self.y -= 30

            self.canvas.style.transform = f"translateX({x}px) translateY({y}px) translateZ(0)"

            if Buff.is_running("grow"):
                self.canvas.style.transform += " scale(1.5)"
            elif Buff.is_running("shrink"):
                self.canvas.style.transform += " scale(0.75)"
        else:
            self.canvas.getContext("2d").clearRect(0, 0, self.current_animation.width, self.current_animation.height)

        self.canvas.getContext("2d").drawImage(
            self.current_animation.spritesheet, self.current_animation.source_rect.left, self.current_animation.source_rect.top,
            self.current_animation.source_rect.width,self.current_animation.source_rect.height,
            0, 0, self.current_animation.width, self.current_animation.height,
        )
        self.current_animation.dirty = False

    def follow_player(self, to_follow: str = None) -> str:
        # running follow() on yourself?
        if self.id != cou_globals.game.username:
            return "You cannot make people follow each other!"

        if to_follow is None or to_follow == "" or to_follow == cou_globals.current_player.id:
            # unset or follow yourself to cancel following
            self.following_player = None
            return "You no longer follow anyone."
        else:
            # player exists/is loaded?
            if cou_globals.other_players.get(to_follow) is None:
                return "You can only follow players on the same street as you!"

            # unfollow the current one first
            if self.following_player != None:
                self.follow_player()

            # follow another player
            self.following_player = cou_globals.other_players[to_follow]
            return f"You now follow {to_follow}. Move to stop."

    def _get_best_platform(self, top: float) -> Platform:
        """ONLY WORKS IF PLATFORMS ARE SORTED WITH THE HIGHEST
        (SMALLEST Y VALUE) FIRST IN THE LIST
        """

        best_platform = None
        x = self.left + self.width / 2
        feet_y = top + self.height + cou_globals.current_street.ground_y
        best_diff_y = 10000000000000

        for platform in cou_globals.current_street.platforms:
            if platform.ceiling:
                continue

            if x >= platform.start.x and x <= platform.end.x:
                slope = (platform.end.y - platform.start.y) / max(platform.end.x - platform.start.x, .00001)
                y_int = platform.start.y - slope * platform.start.x
                line_y = slope * x + y_int # the y-value of the line in the center of our feet
                diff_y = abs(feet_y - line_y)

                if best_platform is None:
                    # don't have one yet, so we'll use this one
                    best_platform = platform
                    best_diff_y = diff_y
                else:
                    match = False
                    if line_y >= feet_y:
                        # the line is below our feet (walking downhill or falling)
                        match = diff_y < best_diff_y  # and it's closer than the previously found line
                    elif not self.jumping and feet_y - (self.height / 2) < line_y < feet_y:
                        # this happens if we're walking uphill (both ways, in the snow)
                        # the offset is so large because of some particularly steep slopes
                        # (Anrasan Glance, just by the shrine, for example)
                        match = diff_y < best_diff_y

                    if match:
                        best_platform = platform
                        best_diff_y = diff_y

        return best_platform


cou_globals.other_players = {}
