from drawing import Rectangle
from message_bus import MessageBus
from systems import util
from systems.global_gobbler import cou_globals


class Wormhole:
    wormholes: dict[str, "Wormhole"] = {}

    @classmethod
    def init(cls) -> None:
        ##
        # Structure:
        # cls.wormholes["<NameReadByHumans>"] = WormHole(<left x>, <top y>, <width>, <height>, "<tsid_of_wormhole>", "<destination_tsid>")
        #
        # Getting values:
        # Use the mapfiller to place a mailbox on the top left and bottom right corners (aligning the top left corner of the
        # mailbox's rectangle), then load up the game and find their CSS x/y transform.
        ##

        cls.wormholes["GroddleLadder"] = Wormhole(2648, -3, 752, 336, "LM4115NJ46G8M", "LLI2VDR4JRD1AEG")
        cls.wormholes["HellDescend"] = Wormhole(2574, 831, 199, 152, "LA5PPFP86NF2FOS", "LA5PV4T79OE2AOA")
        # wormholes["TheEntrance"] = Wormhole(5714, 465, 243, 100, "LIF102FDNU11314", "LHH101L162117H2")
        cls.wormholes["HoleToIx"] = Wormhole(218, 639, 194, 196, "LLI2VDR4JRD1AEG", "LM4115NJ46G8M")
        cls.wormholes["WintryPlace"] = Wormhole(522, -1, 177, 160, "LLI23D3LDHD1FQA", "LM11E7ODKHO1QJE")

    @classmethod
    def update_all(cls) -> None:
        for wormhole in cls.get_street():
            wormhole.update()

    @classmethod
    def get_street(cls, tsid: str = None) -> list["Wormhole"]:
        street = tsid if tsid else cou_globals.current_street.tsid
        return [w for w in cls.wormholes.values() if w.on_tsid[1:] == street[1:]]

    def __init__(self, left_x: int, top_y: int, width: int, height: int, on_tsid: str, to_tsid: str):
        self.left_x = left_x
        self.top_y = top_y
        self.width = width
        self.height = height
        self.on_tsid = on_tsid
        self.to_tsid = to_tsid
        self.teleporting = False
        self.hit_box = Rectangle(left_x, top_y, width, height)

    def update(self) -> None:
        if self.hit_box.intersects(cou_globals.current_player.entity_rect):
            self.tp()

    def tp(self) -> None:
        # Don't request the street more than once
        if self.teleporting:
            return

        # Teleport and stop updating
        util.create_task(cou_globals.street_service.request_street(self.to_tsid))
        self.teleporting = True

        # Wait for the street to load before teleporting again
        MessageBus.subscribe("streetLoaded", lambda _: setattr(self, "teleporting", False))

    def __str__(self) -> str:
        return f"Wormhole to {self.to_tsid} located on {self.on_tsid} at ({self.left_x},{self.top_y} {self.width} x {self.height}"
