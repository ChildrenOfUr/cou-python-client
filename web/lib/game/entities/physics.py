from systems.global_gobbler import cou_globals

class Physics:
    DEFAULT_ID = "normal"

    def __init__(
            self, id: str, can_jump: bool = True, can_triple_jump: bool = True, infinite_jump: bool = False,
            speed: float = 300, z_speed: float = 30, jump_multiplier: float = 1, y_vel: float = 1000,
            y_vel_jump: float = 1000, y_vel_triple_jump: float = 1560,
        ):
        self.id = id
        self.can_jump = can_jump
        self.can_triple_jump = can_triple_jump
        self.infinite_jump = infinite_jump
        self.speed = speed
        self.z_speed = z_speed
        self.jump_multiplier = jump_multiplier
        self.y_vel = y_vel
        self.y_vel_jump = y_vel_jump
        self.y_vel_triple_jump = y_vel_triple_jump

    @classmethod
    def get(cls, id: str) -> "Physics":
        return _PHYSICS.get(id, _PHYSICS[Physics.DEFAULT_ID])

    @classmethod
    def get_street(cls, street_name) -> "Physics":
        return Physics.get(cou_globals.map_data.get_street_physics(street_name));


_PHYSICS = {
    Physics.DEFAULT_ID: Physics(Physics.DEFAULT_ID),
    "water": Physics(
        "water",
        can_jump=False,
        can_triple_jump=False,
        infinite_jump=True,
        jump_multiplier=0.5,
        y_vel=500,
    ),
    "plexus": Physics(
        "plexus",
        can_jump=False,
        can_triple_jump=False,
        infinite_jump=True,
        jump_multiplier=0.23,
        speed=250,
    ),
}
