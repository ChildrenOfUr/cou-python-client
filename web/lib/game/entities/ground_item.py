import json

from pyscript import document
from pyodide.ffi import create_proxy

from game.entities.entity import Entity
from network.server_interop.itemdef import Action
from systems.global_gobbler import cou_globals


class GroundItem(Entity):
    def __init__(self, map):
        super().__init__()
        self._map = map

        if "actions" in map:
            self._actions = map["actions"]

        item = document.createElement("img")
        item.src = map["iconUrl"]

        def on_load(event):
            self.left = map["x"]
            self.top = map["y"] - item.height
            self.z = map.get("z")
            self.width = item.width
            self.height = item.height
            self.id = map["id"]

            item.style.transform = f"translateX({self.left}px) translateY({self.top}px)"
            item.style.position = "absolute"
            item.zIndex = str(self.z)
            item.setAttribute("translateX", str(self.left))
            item.setAttribute("translateY", str(self.top))
            item.setAttribute("width", str(item.width))
            item.setAttribute("height", str(item.height))
            item.setAttribute("itemType", map["itemType"])
            item.setAttribute("name", map["name"])
            item.setAttribute("description", map["description"])
            item.setAttribute("actions", json.dumps(map["actions"]))
            item.classList.add("groundItem", "entity")
            item.id = self.id
            cou_globals.view.playerHolder.appendChild(item)
            cou_globals.adding_locks[self.id] = False

        item.addEventListener("load", create_proxy(on_load))
        self.item = item

    def render(self) -> None:
        if not self.dirty:
            return

        if self.glow:
            self.item.classList.add("groundItemGlow")
        else:
            self.item.classList.remove("groundItemGlow")

        self.dirty = False

    def interact(self, id: str) -> None:
        element = document.querySelector(f"#{self.id}")
        action_list: list[Action] = []

        for action in self.actions:
            enabled = action.enabled
            action.action_name = action.action_name.title()
            error = ""
            if enabled:
                enabled = action.has_requirements()
                if enabled:
                    error = action.description
                else:
                    error = action.get_requirement_string()
            else:
                error = action.error
            menu_action = Action.clone(action)
            menu_action.enabled = enabled
            menu_action.error = error
            action_list.append(menu_action)

        cou_globals.input_manager.showClickMenu(
            None, title=element.getAttribute("name"), id=self.id, actions=action_list, item_name=element.getAttribute("name"),
        )
