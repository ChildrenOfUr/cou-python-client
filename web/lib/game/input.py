from datetime import datetime, timedelta
import logging

import js as context
from pyscript import document, window
from pyodide.ffi import create_proxy

from display.chat_panel import advance_chat_focus, Chat
from display.ui_templates.interactions_menu import InteractionWindow
from display.ui_templates.menu_keys import MenuKeys
from display.ui_templates.right_click_menu import RightClickMenu
from display.toast import Toast
from game.actionbubble import ActionBubble
from game.joystick import Joystick
from message_bus import MessageBus
from systems import util
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)


class ControlSignal:
    def __init__(self, source_name: str, active: bool) -> None:
        self.source_name = source_name
        self.active = active


class ControlCount:
    def __init__(self) -> None:
        self.signals: dict[str, ControlSignal] = {}

    @property
    def active(self) -> bool:
        return len([signal for signal in self.signals.values() if signal.active]) > 0


class InputManager:
    keys: dict[str, int] = {
		"LeftBindingPrimary": 65,
		"LeftBindingAlt": 37,
		"RightBindingPrimary": 68,
		"RightBindingAlt": 39,
		"UpBindingPrimary": 87,
		"UpBindingAlt": 38,
		"DownBindingPrimary": 83,
		"DownBindingAlt": 40,
		"JumpBindingPrimary": 32,
		"JumpBindingAlt": 32,
		"ActionBindingPrimary": 13,
		"ActionBindingAlt": 69,
		"MapBindingPrimary": 77,
		"MapBindingAlt": 77,
		"CalendarBindingPrimary": 67,
		"CalendarBindingAlt": 67,
		"SettingsBindingPrimary": 80,
		"SettingsBindingAlt": 80,
		"ChatFocusBindingPrimary": 9,
		"ChatFocusBindingAlt": 9,
		"ImgMenuBindingPrimary": 73,
		"ImgMenuBindingAlt": 73,
		"QuestLogBindingPrimary": 81,
		"QuestLogBindingAlt": 81,
		"AchievementsBindingPrimary": 89,
		"AchievementsBindingAlt": 89,
    }

    def __init__(self) -> None:
        self.windowFocused = True
        self.rightKey = ControlCount()
        self.leftKey = ControlCount()
        self.upKey = ControlCount()
        self.downKey = ControlCount()
        self.jumpKey = ControlCount()
        self.actionKey = ControlCount()
        self._ignoreCount = 0
        self.ignoreChatFocus = False
        self.touched = False
        self.clickUsed = False
        self.keyPressSub = None
        self.keyDownSub = None
        self.menuKeyListener: tuple[str, callable] = None
        self.lastSelect = datetime.now()
        self.controlCounts: dict[str, ControlCount] = {
            "leftKey": self.leftKey,
            "rightKey": self.rightKey,
            "upKey": self.upKey,
            "downKey": self.downKey,
            "jumpKey": self.jumpKey,
            "actionKey": self.actionKey,
        }

        self.setupKeyBindings()

        def _disableInputKeys(value):
            self.ignoreKeys = value
        def _disableChatFocus(value):
            self.ignoreChatFocus = value
        MessageBus.subscribe("disableInputKeys", _disableInputKeys)
        MessageBus.subscribe("disableChatFocus", _disableChatFocus)

        document.addEventListener("click", create_proxy(lambda event: self.clickOrTouch(event, None)))
        document.addEventListener("touchstart", create_proxy(lambda event: self.clickOrTouch(None, event)))

        self.konami_done = False
        self.free_teleport_used = False
        self.initKonami()

        # Track window focus
        def _windowFocused(state: bool):
            self.windowFocused = state
        window.addEventListener("focus", create_proxy(lambda _: _windowFocused(True)))
        window.addEventListener("blur", create_proxy(lambda _: _windowFocused(False)))

    @property
    def ignoreKeys(self) -> bool:
        return self._ignoreCount != 0

    @ignoreKeys.setter
    def ignoreKeys(self, value: bool) -> None:
        if value:
            self._ignoreCount += 1
            return
        self._ignoreCount -= 1
        if self._ignoreCount < 0:
            self._ignoreCount = 0

    def activateControl(self, control: str, active: bool, source_name: str) -> None:
        signal = f"{source_name}-{control}"
        if active and signal not in self.controlCounts[control].signals:
            self.controlCounts[control].signals[signal] = ControlSignal(source_name, active)
        elif not active:
            self.controlCounts[control].signals.pop(signal, None)

        # stop following any player that you might be following
        cou_globals.current_player.follow_player()

        def _worldFocus(focused: bool):
            if focused:
                return
            for control in self.controlCounts.values():
                control.signals.clear()
        MessageBus.subscribe("worldFocus", _worldFocus)

    def update_gamepad(self) -> None:
        # get any gamepads
        for gamepad in [g for g in window.navigator.getGamepads() if g is not None]:
            # don't do anythin in certain situations
            if self.ignoreKeys or ActionBubble.occuring:
                return

            # interact with the menu
            clickMenu = document.querySelector(".RightClickMenu")
            if clickMenu is not None:
                action_list = clickMenu.querySelector(".RCActionList")
                # only select a new option once every 300ms
                selectAgain = self.lastSelect + timedelta(milliseconds=300) < datetime.now()
                if self.upKey.active and selectAgain:
                    self.selectUp(action_list.querySelectorAll(".RCItem"), "RCItemSelected")
                if self.downKey.active and selectAgain:
                    self.selectDown(action_list.querySelectorAll(".RCItem"), "RCItemSelected")
                if self.leftKey.active or self.rightKey.active or self.jumpKey.active:
                    self.stopMenu(clickMenu)

            right_pressed = gamepad.axes[0] > .2 or gamepad.axes[2] > .2
            left_pressed = gamepad.axes[0] < -.2 or gamepad.axes[2] < -.2
            up_pressed = gamepad.axes[1] > .2 or gamepad.axes[3] > .2
            down_pressed = gamepad.axes[1] < -.2 or gamepad.axes[3] < -.2

            self.activateControl("rightKey", right_pressed, "gamepad")
            self.activateControl("leftKey", left_pressed, "gamepad")
            self.activateControl("upKey", up_pressed, "gamepad")
            self.activateControl("downKey", down_pressed, "gamepad")

            button0 = window.navigator.getGamepads()[gamepad.index].buttons[0].pressed
            button1 = window.navigator.getGamepads()[gamepad.index].buttons[1].pressed

            self.activateControl("jumpKey", button0, "gamepad")
            if button1:
                if not self.actionKey.active:
                    self.activateControl("actionKey", True, "gamepad")
                    self.doObjectInteraction()
            else:
                self.activateControl("actionKey", False, "gamepad")

    def clickOrTouch(self, mouse_event, touch_event) -> None:
        # TODO: for now mobile touch targets are not included
        # don't handle too many touch events too fast
        if self.touched:
            return

        self.touched = True
        def _disableTouch():
            self.touched = False
        util.Timer(timedelta(milliseconds=200), _disableTouch)

        target = mouse_event.target if mouse_event is not None else touch_event.target

        def activatePresetBindings(preset: str) -> None:
            # check which preset movement keys to set & apply them
            match preset:
                case "querty":
                    # set WASD
                    window.localStorage.setItem("LeftBindingPrimary", "65.97")
                    window.localStorage.setItem("RightBindingPrimary", "68.100")
                    window.localStorage.setItem("UpBindingPrimary", "87.119")
                    window.localStorage.setItem("DownBindingPrimary", "83.115")
                case "dvorak":
                    # set ,AOE
                    window.localStorage.setItem("LeftBindingPrimary", "65.97")
                    window.localStorage.setItem("RightBindingPrimary", "69.101")
                    window.localStorage.setItem("UpBindingPrimary", "188.44")
                    window.localStorage.setItem("DownBindingPrimary", "79.111")

            # reset all other keys
            window.localStorage.setItem("LeftBindingAlt", "37")
            window.localStorage.setItem("RightBindingAlt", "39")
            window.localStorage.setItem("UpBindingAlt", "38")
            window.localStorage.setItem("DownBindingAlt", "40")
            window.localStorage.setItem("JumpBindingPrimary", "32")
            window.localStorage.setItem("JumpBindingAlt", "32")
            window.localStorage.setItem("ActionBindingPrimary", "13")
            window.localStorage.setItem("ActionBindingAlt", "13")

            # update game vars
            self.setupKeyBindings()

        # listen for clicks on preset buttons and run above function
        keyboardPresetQwerty = document.querySelector("#keyboard-preset-qwerty")
        keyboardPresetDvorak = document.querySelector("#keyboard-preset-dvorak")
        util.add_event_once(keyboardPresetQwerty, "mouseup", lambda _: activatePresetBindings("qwerty"))
        util.add_event_once(keyboardPresetDvorak, "mouseup", lambda _: activatePresetBindings("dvorak"))

        # handle key re-binds
        if "KeyBindingOption" in target.classList:
            if not self.clickUsed:
                self.setupKeyBindings()

            target.innerText = "(press key to change)"

            def _wait_for_keydown(event):
                self.keyDownSub.cancel()
                key = self.fromKeyCode(event.keyCode)
                keyCode = event.keyCode
                if key == "":
                    # it was not a special key
                    def _wait_for_keypress(event):
                        self.keyPressSub.cancel()
                        target.innerText = context.String.fromCharCode(event.charCode).upper()
                        self.keys[target.id] = keyCode
                        # store keycode and charcode
                        window.localStorage.setItem(target.id, f"{keyCode}.{event.charCode}")

                        cou_globals.window_manager.settings.check_duplicate_key_assignments()
                    self.keyPressSub = util.add_event_once(document.body, "keypress", _wait_for_keypress)
                else:
                    target.innerText = key
                    self.keys[target.id] = event.keyCode
                    window.localStorage.setItem(target.id, str(event.keyCode))

                cou_globals.window_manager.settings.check_duplicate_key_assignments()
            self.keyDownSub = util.add_event_once(document.body, "keydown", _wait_for_keydown)

        # handle changing streets via exit signs
        if "ExitLabel" in target.className:
            cou_globals.player_tele_from = target.getAttribute("from")
            util.create_task(cou_globals.street_service.request_street(target.getAttribute("tsid")))

        if "chatSpawn" in target.classList:
            Chat(target.innerText)
            target.classList.remove("unread")

    def setupKeyBindings(self) -> None:
        # this prevents 2 keys from being set at once
        if self.keyPressSub:
            self.keyPressSub.cancel()
        if self.keyDownSub:
            self.keyDownSub.cancel()

        # set up key bindings
        for action, key_code in self.keys.items():
            stored_value: list[str] = None
            if window.localStorage.getItem(action) is not None:
                stored_value = window.localStorage.getItem(action).split(".")
                self.keys[action] = int(stored_value[0])
            else:
                window.localStorage.setItem(action, str(self.keys[action]))

            key = self.fromKeyCode(self.keys[action])
            if key == "":
                if stored_value is not None and len(stored_value) > 1:
                    document.querySelector(f"#{action}").innerText = context.String.fromCharCode(int(stored_value[1])).upper()
                else:
                    document.querySelector(f"#{action}").innerText = context.String.fromCharCode(self.keys[action])
            else:
                document.querySelector(f"#{action}").innerText = key

        # Handle player input
        # KeyUp and KeyDown are necessary for preventing weird movement glitches
        def _handle_keydown(event):
            if self.menuKeyListener is not None or self.ignoreChatFocus:
                return

            # check for chat focus stuff before deciding on ignore keys
            if event.keyCode == self.keys["ChatFocusBindingPrimary"] or event.keyCode == self.keys["ChatFocusBindingAlt"]:
                advance_chat_focus(event)

            if ActionBubble.occuring:
                return

            self.updateKeys(True, event.keyCode)
        document.addEventListener("keydown", create_proxy(_handle_keydown))

        document.addEventListener("keyup", create_proxy(lambda event: self.updateKeys(False, event.keyCode)))

        # listen for right-clicks on entities that we're close to
        def _handle_right_click(event):
            if self.ignoreKeys:
                return
            # just like pressing a key for 10ms
            self.doObjectInteraction()
            util.Timer(timedelta(milliseconds=10), lambda: self.activateControl("actionKey", False, "mouse"))
        document.body.addEventListener("contextmenu", create_proxy(_handle_right_click))

        # only for mobile version
        joystick = Joystick(document.querySelector("#Joystick"), document.querySelector("#Knob"), deadzone_in_percent=.2)
        def _on_joystick_move(event):
            # don't move during harvesting, etc.
            if not ActionBubble.occuring:
                self.activateControl("upKey", joystick.UP, "joystick")
                self.activateControl("downKey", joystick.DOWN, "joystick")
                self.activateControl("leftKey", joystick.LEFT, "joystick")
                self.activateControl("rightKey", joystick.RIGHT, "joystick")

            clickMenu = document.querySelector(".RightClickMenu")
            if clickMenu is not None:
                action_list = clickMenu.querySelector(".RCActionList")
                # only select a new option once every 300ms
                if self.lastSelect + timedelta(milliseconds=300) < datetime.now():
                    if joystick.UP:
                        self.selectUp(action_list.querySelectorAll(".RCItem"), "RCItemSelected")
                    if joystick.DOWN:
                        self.selectDown(action_list.querySelectorAll(".RCItem"), "RCItemSelected")

                if joystick.LEFT or joystick.RIGHT:
                    self.stopMenu(clickMenu)
        MessageBus.subscribe("joystick_move", _on_joystick_move)

        def _on_joystick_release(event):
            for key in ["up", "down", "right", "left"]:
                self.activateControl(f"{key}Key", False, "joystick")
        MessageBus.subscribe("joystick_release", _on_joystick_release)

        def _on_document_touchstart(event):
            target = event.target

            if target.id == "AButton":
                event.preventDefault()  # to disable long press calling the context menu
                self.activateControl("jumpKey", True, "keyboard")

            if target.id == "BButton":
                event.preventDefault()  # to disable long press calling the context menu
                self.doObjectInteraction()
        document.addEventListener("touchstart", create_proxy(_on_document_touchstart), passive=False)

        def _on_document_touchend(event):
            target = event.target

            if target.id == "AButton":
                self.activateControl("jumpKey", False, "keyboard")
        document.addEventListener("touchend", create_proxy(_on_document_touchend))

        document.addEventListener("click", create_proxy(lambda event: self.clickOrTouch(event, None)))
        document.addEventListener("touchstart", create_proxy(lambda event: self.clickOrTouch(None, event)))

        # TouchScroller(document.querySelector("#inventory", TouchScroller.HORIZONTAL)) TODO

        # end mobile specific stuff

    def updateKeys(self, newState: bool, keyCode: int) -> None:
        def _checkControl(control: str) -> bool:
            return keyCode == self.keys[f"{control}BindingPrimary"] or keyCode == self.keys[f"{control}BindingAlt"]

        if self.ignoreKeys:
            return

        for key in ["Up", "Down", "Left", "Right", "Jump", "Action"]:
            if not _checkControl(key):
                continue

            if newState and key == "Action":
                self.doObjectInteraction()
            else:
                self.activateControl(f"{key.lower()}Key", newState, "keyboard")

    def doObjectInteraction(self) -> None:
        if document.querySelector(".RightClickMenu") is not None:
            self.doAction(document.querySelector(".RCActionList"), document.querySelector(".RightClickMenu"), "RCItemSelected")
            return

        if cou_globals.current_player.intersecting_objects and document.querySelector(".RightClickMenu") is None and not ActionBubble.occuring:
            if len(cou_globals.current_player.intersecting_objects) == 1:
                id = next(iter(id for id in cou_globals.current_player.intersecting_objects.keys() if id is not None), None)
                if id is None:
                    return
                if id in cou_globals.entities:
                    cou_globals.entities[id].interact(id)
                else:
                    cou_globals.other_players[id].interact(id)
            else:
                self.createMultiEntityWindow()

    def createMultiEntityWindow(self) -> None:
        current_window = document.querySelector("#InteractionWindow")
        if current_window is not None:
            current_window.remove()
        document.body.append(InteractionWindow.create())

    # Right-click menu functions
    def showClickMenu(self, click, id: str, title: str = "Interact", actions: list = None, item_name: str = None, server_class: str = None) -> None:
        if actions is None:
            actions = []

        RightClickMenu.destroy_all()
        RightClickMenu.create3(click, title, id, actions=actions, item_name=item_name, server_class=server_class)

        click_menu = document.querySelector(".RightClickMenu")
        action_list = click_menu.querySelector(".RCActionList")

        def on_key_down(event):
            stop_keys = (
                self.keys["LeftBindingPrimary"], self.keys["LeftBindingAlt"],
                self.keys["RightBindingPrimary"], self.keys["RightBindingAlt"],
                self.keys["JumpBindingPrimary"], self.keys["JumpBindingAlt"],
                27, # escape key
            )

            # up arrow or w and not typing
            if (event.keyCode == self.keys["UpBindingPrimary"] or event.keyCode == self.keys["UpBindingAlt"]) and not self.ignoreKeys:
                self.selectUp(action_list.querySelectorAll(".RCItem"), "RCItemSelected")
            if (event.keyCode == self.keys["DownBindingPrimary"] or event.keyCode == self.keys["DownBindingAlt"]) and not self.ignoreKeys:
                self.selectDown(action_list.querySelectorAll(".RCItem"), "RCItemSelected")
            if not self.ignoreKeys and event.keyCode in stop_keys:
                self.stopMenu(click_menu)
            if (event.keyCode == self.keys["ActionBindingPrimary"] or event.keyCode == self.keys["ActionBindingAlt"]) and not self.ignoreKeys:
                self.doAction(action_list, click_menu, "RCItemSelected")
        handler = create_proxy(on_key_down)
        self.menuKeyListener = ("keydown", handler)
        document.addEventListener("keydown", handler)

        document.addEventListener("click", create_proxy(lambda _: self.stopMenu(click_menu)))

    def selectUp(self, options, className: str) -> None:
        removed = 0
        for i in range(len(options)):
            options[i].classList.remove("action_hover")
            if options[i].classList.contains(className):
                options[i].classList.remove(className)
                removed = i
        if removed == 0:
            options[-1].classList.add(className)
            options[-1].classList.add("action_hover")
        else:
            options[removed - 1].classList.add(className)
            options[removed - 1].classList.add("action_hover")

        self.lastSelect = datetime.now()

    def selectDown(self, options, className: str) -> None:
        removed = len(options) - 1
        for i in range(len(options)):
            options[i].classList.remove("action_hover")
            if options[i].classList.contains(className):
                options[i].classList.remove(className)
                removed = i
        if removed == len(options) - 1:
            options[0].classList.add(className)
            options[0].classList.add("action_hover")
        else:
            options[removed + 1].classList.add(className)
            options[removed + 1].classList.add("action_hover")

        self.lastSelect = datetime.now()

    def stopMenu(self, window) -> None:
        if window and window.classList.contains(RightClickMenu.MENU_CLASS):
            RightClickMenu.destroy_all()

        MessageBus.publish("menuStopping", window)

        if self.menuKeyListener:
            document.removeEventListener(*self.menuKeyListener)
            self.menuKeyListener = None
        if self.keyPressSub:
            self.keyPressSub.cancel()
            self.keyPressSub = None
        if self.keyDownSub:
            self.keyDownSub.cancel()
            self.keyDownSub = None

        MenuKeys.clear_listeners()
        if window:
            window.remove()

    def doAction(self, action_list, window, className: str) -> None:
        for element in action_list.querySelectorAll(".RCItem"):
            if className in element.classList:
                # click the hard way so that we have coordinates to go with it
                rect = element.getBoundingClientRect()
                element.dispatchEvent(context.MouseEvent.new("click", clientX=int(rect.left + rect.width // 2), clientY=int(rect.top)))
                break
        self.stopMenu(window)

    def fromKeyCode(self, key_code: int) -> str:
        codes = {
			8: "backspace",
			9: "tab",
			13: "enter",
			16: "shift",
			17: "ctrl",
			18: "left alt",
			19: "pause/break",
			20: "caps lock",
			27: "escape",
			32: "space",
			33: "page up",
			34: "page down",
			35: "end",
			36: "home",
			37: "left arrow",
			38: "up arrow",
			39: "right arrow",
			40: "down arrow",
			45: "insert",
			46: "delete",
			91: "left window",
			92: "right window",
			93: "select key",
			96: "numpad 0",
			97: "numpad 1",
			98: "numpad 2",
			99: "numpad 3",
			100: "numpad 4",
			101: "numpad 5",
			102: "numpad 6",
			103: "numpad 7",
			104: "numpad 8",
			105: "numpad 9",
			106: "multiply",
			107: "add",
			109: "subtract",
			110: "decimal point",
			111: "divide",
			112: "F1",
			113: "F2",
			114: "F3",
			115: "F4",
			116: "F5",
			117: "F6",
			118: "F7",
			119: "F8",
			120: "F9",
			121: "F10",
			122: "F11",
			123: "F12",
			144: "num lock",
			145: "scroll lock",
			225: "right alt",
		}
        return codes[key_code] if key_code in codes else ""

    def initKonami(self):
        konami_keys = [38, 38, 40, 40, 37, 39, 37, 39, 66, 65]
        konami = ", ".join([str(i) for i in konami_keys])
        keys = []

        def _listen_for_konami(event):
            if not self.konami_done and event.keyCode in konami_keys:
                keys.append(event.keyCode)

                if konami in str(keys):
                    Toast("Your next teleport is free!")
                    self.konami_done = True
        document.addEventListener("keydown", create_proxy(_listen_for_konami))


cou_globals.input_manager = None
