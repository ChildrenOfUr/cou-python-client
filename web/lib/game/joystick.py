import math

from pyodide.ffi import create_proxy

from message_bus import MessageBus
from systems import util


class JoystickEvent:
    pass

class Joystick:
    def __init__(self, joystick, knob, deadzone_in_pixels: int = 0, deadzone_in_percent: float = 0.0):
        self.joystick = joystick
        self.knob = knob
        self.deadzone_in_pixels = deadzone_in_pixels
        self.deadzone_in_percent = deadzone_in_percent
        self.offsetLeft = knob.offsetLeft
        self.offsetTop = knob.offsetTop

        self._neutralX = 0
        self._neutralY = 0
        self._initialTouchX = 0
        self._initialTouchY = 0

        self.UP = False
        self.DOWN = False
        self.LEFT = False
        self.RIGHT = False

        self.repeatTimer: util.Timer = None


        if deadzone_in_percent != 0:
            self.deadzone_in_pixels = int(joystick.clientWidth * deadzone_in_percent)

        def _handle_knob_touchstart(event):
            event.preventDefault()
            self._neutralX = self.offsetLeft
            self._neutralY = self.offsetTop
            self._initialTouchX = event.changedTouches.item(0).clientX
            self._initialTouchY = event.changedTouches.item(0).clientY
            MessageBus.publish("joystick_move", JoystickEvent())

            # add an event to the stream 4 times per second even if the user does not move
            # the knob - this will, for instance, allow the joystick to be used as a selection
            # device (think menus) even if the user hold the knob steady at the top.
            # Timer.periodic(timedelta(milliseconds=250), lambda: MessageBus.publish("joystick_move", JoystickEvent()))
        knob.addEventListener("touchstart", create_proxy(_handle_knob_touchstart))
        joystick.addEventListener("touchstart", create_proxy(_handle_knob_touchstart))

        def _handle_knob_touchmove(event):
            event.preventDefault()  # prevent page from scrolling/zooming
            x = self._neutralX + (event.changedTouches.item(0).clientX - self._initialTouchX)
            y = self._neutralY + (event.changedTouches.item(0).clientY - self._initialTouchY)

            # keep within containing joystick circle
            radius = joystick.clientWidth // 2
            if not self.inCircle(self._neutralX, self._neutralY, radius, x, y):  # stick to side of circle
                slope = (y - self._neutralY) / (x - self._neutralX)
                angle = math.atan(slope)
                if x - self._neutralX < 0:  # if left side of circle
                    angle += math.pi
                yOnCircle = self._neutralY + math.floor(math.sin(angle) * radius)
                xOnCircle = self._neutralX + math.floor(math.cos(angle) * radius)
                x = xOnCircle
                y = yOnCircle

            self.LEFT = x < self._neutralX - deadzone_in_pixels
            self.RIGHT = x > self._neutralX + deadzone_in_pixels
            self.DOWN = y > self._neutralY + deadzone_in_pixels
            self.UP = y < self._neutralY - deadzone_in_pixels

            knob.style.transform = f"translateX({x - self.offsetLeft}px) translateY({y - self.offsetTop}px) translateZ(0)"
            MessageBus.publish("joystick_move", JoystickEvent())
        knob.addEventListener("touchmove", create_proxy(_handle_knob_touchmove))
        joystick.addEventListener("touchmove", create_proxy(_handle_knob_touchmove))

        def _handle_knob_touchend(event):
            event.preventDefault()
            if knob.hasAttribute("style"):
                knob.attributes.removeNamedItem("style")  # in case the user rotates the screen
            self.UP = self.DOWN = self.LEFT = self.RIGHT = False  # reset

            MessageBus.publish("joystick_release", JoystickEvent())
            if self.repeatTimer is not None:
                self.repeatTimer.cancel()
        knob.addEventListener("touchend", create_proxy(_handle_knob_touchend))
        joystick.addEventListener("touchend", create_proxy(_handle_knob_touchend))

    def inCircle(self, center_x: int, center_y: int, radius: int, x: int, y: int):
        square_dist = math.pow(center_x - x, 2) + math.pow(center_y - y, 2)
        return square_dist <= math.pow(radius, 2)
