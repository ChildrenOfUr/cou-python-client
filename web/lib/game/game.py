import asyncio
from datetime import datetime, timedelta
from cProfile import Profile
import json
import logging
from pstats import SortKey, Stats

from pyscript import document, window
from pyodide.ffi import create_proxy
from pyodide import http

from configs import Configs
from display.buff import Buff
from display.loop import update
from display.overlays.module_skills import Skills
from display.render_loop import render
from game.entities.player import Player
from message_bus import MessageBus
from network.chat import NetChatManager
from network.metabolics import Metabolics
from network.server_interop.so_multiplayer import multiplayer_init
from network.street_service import StreetService
from systems import util
from systems.global_gobbler import cou_globals
from systems.quest_manager import QuestManager
from systems.weather import WeatherManager


LOGGER = logging.getLogger(__name__)


class Game:
    def __init__(self, serverdata: dict):
        self.username = serverdata["playerName"]
        self.location = serverdata["playerStreet"]
        self.email = serverdata["playerEmail"]

        self.start_time = datetime.now()
        self.last_time = datetime.now()
        self.target_fps = 60
        self.frame_count = 0
        self.ignore_gamepads = False
        self.elevation_cache: dict[str, str] = {}
        self.loaded = False

        self.street_service = StreetService()
        cou_globals.street_service = self.street_service
        util.create_task(self._init(Metabolics.from_json(json.loads(serverdata["metabolics"]))))

        elevation_task = util.create_task(self.get_elevation(self.username))
        def set_role(future: asyncio.Future) -> None:
            role = future.result()
            # Hide "Become a Guide" button
            if role in ["dev", "guide"]:
                document.querySelector("#becomeGuideFromChatPanel").hidden = True

            # Display border on avatar image
            # (Devs shouldn't see it, our blog post screenshots would be different)
            if role == "guide":
                document.querySelector("#meters #leftDisk").classList.add("guideDisk")
        elevation_task.add_done_callback(set_role)

    async def get_elevation(self, username: str) -> str:
        if self.elevation_cache.get(username) is not None:
            return self.elevation_cache[username]
        response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/elevation/get/{username}")
        elevation = await response.string()
        self.elevation_cache[username] = elevation
        return elevation

    async def _init(self, m: Metabolics) -> None:
        # load the player's street from the server
        await self.street_service.request_street(self.location)

        # setup the chat and open two initial channels
        NetChatManager()
        MessageBus.publish("startChat", "Global Chat")
        MessageBus.publish("startChat", "Local Chat")

        # show the message of the day
        # window_manager.motd_window.open() TODO

        # init the players metabolics
        await cou_globals.metabolics.init(m)

        # setup the communications for multiplayer events
        multiplayer_init()

        # create the player entity and display their name in the UI
        cou_globals.current_player = Player(self.username)
        cou_globals.view.meters.update_name_display()

        # load the player's animation spritesheets then set them to idle
        await cou_globals.current_player.load_animations()
        cou_globals.current_player.current_animation = cou_globals.current_player.animations["still"]
        MessageBus.publish("playerLoaded")

        # HACK: toggling fixes mute issues
        cou_globals.view.slider.volume_glyph.click()
        cou_globals.view.slider.volume_glyph.click()
        cou_globals.view.slider.do_toasts = True

        # stop loading sounds and load the street's song
        cou_globals.audio.stop_sound("loading", fade_out_duration=timedelta(seconds=1.5))
        MessageBus.publish("playSound", "game_loaded")

        # start time based colors (and rain)
        cou_globals.weather = WeatherManager()

        # send and receive messages from the server about quests
        cou_globals.quest_manager = QuestManager()

        # finally start the main game loop
        self.loop(0.0)

        self.loaded = True
        MessageBus.publish("gameLoaded", self.loaded)
        LOGGER.info(f"Game v{Configs.clientVersion} loaded!")

        # tell the server when we have changed streets, and to assign us a new letter
        async def change_letter(_):
            if not cou_globals.current_street.use_letters:
                return
            await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/letters/newPlayerLetter?username={self.username}")
        MessageBus.subscribe("streetLoaded", change_letter)
        MessageBus.subscribe("gameUnloading", change_letter)

        # load previous GPS state
        if window.localStorage.getItem("gps_navigating"):
            cou_globals.GPS.get_route(cou_globals.current_street.label, window.localStorage.getItem("gps_navigating"))

        # load buffs
        await Buff.load_existing()

        # load skills
        await Skills.load_data()

        MessageBus.subscribe("startProfiler", self.start_profiler)
        MessageBus.subscribe("reportProfiler", self.report_profile)
        MessageBus.subscribe("stopProfiler", self.stop_profiler)

    def start_profiler(self, _):
        self.profiler = Profile()
        self.profiler.enable()

    def report_profile(self, _):
        stats = Stats(self.profiler)
        stats.sort_stats(SortKey.TIME, SortKey.CUMULATIVE)
        stats.print_stats(.1)

    def stop_profiler(self, _):
        self.profiler.disable()
        self.report_profile(None)

    def loop(self, delta: float):
        """GAME LOOP"""

        window.requestAnimationFrame(create_proxy(self.loop))

        now = datetime.now()
        elapsed = (now - self.last_time) / timedelta(seconds=1)

        if elapsed < 1 / self.target_fps:
            return

        # if the gamepad api isn't supported, don't continue to try to get updates from it
        if not self.ignore_gamepads:
            try:
                cou_globals.input_manager.update_gamepad()
            except Exception:
                self.ignore_gamepads = True
                LOGGER.error("[UI] Sorry, this browser does not support the gamepad API")

        self.last_time = now

        update(elapsed)
        render()

        # self.frame_count += 1
        # if self.frame_count % 1000 == 0:
        #     self.start_time = now
        #     self.frame_count = 0
        #     return
        # since_start = (now - self.start_time) / timedelta(seconds=1)
        # current_fps = round(1 / (since_start / self.frame_count), 2)
        # print(f"{current_fps=}")

cou_globals.game = None
