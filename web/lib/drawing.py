import dataclasses

@dataclasses.dataclass(slots=True)
class Rectangle:
    left: float
    top: float
    width: float
    height: float

    @property
    def right(self) -> float:
        return self.left + self.width

    @property
    def bottom(self) -> float:
        return self.top + self.height

    @classmethod
    def from_DOM(cls, dom_rect) -> "Rectangle":
        return Rectangle(dom_rect.left, dom_rect.top, dom_rect.width, dom_rect.height)

    def intersects(self, other: "Rectangle") -> bool:
       """Determine if the two rectangles intersect"""
       if other is None:
           return False

       intersects = (
            self.left <= other.right and
            other.left <= self.right and
            self.top <= other.bottom and
            other.top <= self.bottom
       )
       return intersects


@dataclasses.dataclass
class Point:
    x: float
    y: float
