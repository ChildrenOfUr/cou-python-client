import asyncio
import logging
import math
import re
from typing import Any

from pyscript import document, window
from pyodide.ffi import create_proxy

from systems.global_gobbler import cou_globals
from systems import util

LOGGER = logging.getLogger(__name__)

cou_globals.ASSETS = {}


# // add extentions to this list to allow the loading of non-standard text files.
# List <String> textExtensions =
# [
#  'txt'
# ];

# // add extentions to this list to allow the loading of non-standard json files.
# List <String> jsonExtensions =
# [
#  'json'
# ];

# image extentions cannot be changed, because they have to be embedded into an <img> tag.
# Browsers will not render custom file formats
image_extensions = ["svg", "png", "jpg", "jpeg", "gif", "bmp"]

# // audio extentions cannot be changed, because they have to be embedded into an <audio> tag.
# // Browsers will not play custom audio formats
# final List <String> audioExtensions =
# [
#  'mp3',
#  'ogg'
# ];


class Asset:
    @classmethod
    def from_data(cls, data: Any, name: str) -> "Asset":
        asset = Asset(None)
        asset._asset = data
        asset.name = name
        asset.loaded = True
        cou_globals.ASSETS[name] = asset
        return asset

    def __init__(self, uri: str):
        self._uri = uri
        self._asset = None
        self.name = None
        self.loaded = False

    async def load(self, status_element = None, enable_cross_origin: bool = False) -> None:
        self.name = self._uri.split("/")[-1].split(".")[0]
        # provide some on screen status messages
        if status_element is not None:
            status_element.innerText = f"Loading {self.name} from {self._uri}"

        event = asyncio.Event()
        if not self.loaded:
            loading = False
            data = False
            LOGGER.debug(f"Loading {self._uri}")

            # loads ImageElements into memory
            for ext in image_extensions:
                uri_ext = f".{self._uri.split('.')[-1]}"
                if match := re.search(rf"(\.{ext})\?.*", uri_ext):
                    uri_ext = match.group(1)
                if uri_ext.endswith(f".{ext}"):
                    self._asset = document.createElement("img")
                    if enable_cross_origin:
                        self._asset.crossOrigin = "anonymous"
                    loading = True
                    def on_load(_):
                        cou_globals.ASSETS[self.name] = self
                        self.loaded = True
                        event.set()
                    def on_error(e):
                        self._asset = None
                        LOGGER.error(f"Could not load image: {self._uri}")
                        window.console.log(e)
                        event.set()
                    self._asset.addEventListener("load", create_proxy(on_load))
                    self._asset.addEventListener("error", create_proxy(on_error))
                    self._asset.src = self._uri
                    break
            if loading:
                await event.wait()

    def get(self):
        if not self.loaded or self._asset is None:
            raise ValueError("Asset not yet loaded!")
        return self._asset


# class Asset
# {

# 	Future load({Element statusElement : null, bool enableCrossOrigin : false})
# 	{
# 		name = _uri.split('/')[_uri.split('/').length - 1].split('.')[0];
# 		//provide some on screen status messages
# 		if(statusElement != null)
# 			statusElement.text = "Loading $name from $_uri";

# 		Completer c = new Completer();
# 		if (loaded == false)
# 		{
# 			bool loading = false, data = false;

# 			// loads ImageElements into memory
# 			for (String ext in imageExtensions)
# 			{
# 				String uriExt = _uri.substring(_uri.lastIndexOf('.'));
# 				RegExp extWithQuery = new RegExp('(\\.$ext)\\?.*');
# 				if (uriExt.contains(extWithQuery)) {
# 					uriExt = extWithQuery.firstMatch(uriExt).group(1);
# 				}
# 				if (uriExt.endsWith('.' + ext))
# 				{
# 					this._asset = new ImageElement();
# 					if(enableCrossOrigin)
# 						(_asset as ImageElement).crossOrigin = "anonymous";
# 					loading = true;
# 					_asset.onLoad.listen((_)
# 					{
# 						ASSET[name] = this;
# 						loaded = true;
# 						if(!c.isCompleted)
# 							c.complete(this);
# 					});
# 					_asset.onError.listen((_)
# 					{
# 						_asset = null;
# 						c.complete("Could not load image: $_uri");
# 					});
# 					_asset.src = _uri;
# 					break;
# 				}
# 			}
# 			if (loading == true)
# 				return c.future;

# 			// loads AudioElements into memory
# 			for (String ext in audioExtensions)
# 			{
# 				if (_uri.endsWith('.' + ext))
# 				{
# 					new Timer(new Duration(seconds:2),()
# 					{
# 						//if sound hasn't started loading, complete anyway
# 						if(!data)
# 							c.completeError('could not load resource: $_uri');
# 					});
# 					AudioElement audio = new AudioElement();
# 					loading = true;
# 					if(enableCrossOrigin)
#                     	audio.crossOrigin = "anonymous";
# 					audio.onLoadedData.first.then((_) => data = true);
# 					audio.onError.listen((Event err)
# 					{
# 						print('Error in loading Audio : $_uri');
# 						if(!c.isCompleted)
# 							c.complete(err);
# 					});
# 					audio.onCanPlay.listen((_)
# 					{
# 						ASSET[name] = this;
# 						this._asset= audio;
# 						loaded = true;
# 						if(!c.isCompleted)
# 							c.complete(this);
# 					});

# 					//add 2 audio sources (one mp3 and one ogg) to support multiple browsers
# 					String filename = _uri.substring(0, _uri.lastIndexOf('.'));
# 					if(ext == "ogg")
# 					{
# 						SourceElement source = new SourceElement();
# 						source.type = 'audio/ogg';
# 						source.src = _uri;
# 						audio.append(source);

# 						SourceElement sourceAlt = new SourceElement();
# 						sourceAlt.type = 'audio/mpeg';
# 						sourceAlt.src = filename + ".mp3";
# 						audio.append(sourceAlt);
# 					}
# 					else
# 					{
# 						SourceElement source = new SourceElement();
# 						source.type = 'audio/mpeg';
# 						source.src = _uri;
# 						audio.append(source);
# 						SourceElement sourceAlt = new SourceElement();
# 						sourceAlt.type = 'audio/ogg';
# 						sourceAlt.src = filename + ".ogg";
# 						audio.append(sourceAlt);
# 					}

# 					//this seems to be necessary (when compiled to js) to cause the browser to generate a GET for the src
# 					document.body.append(audio);
# 					break;
# 				}
# 			}
# 			if (loading == true)
# 				return c.future;

# 			// loads simple text files as a string.
# 			for (String ext in textExtensions)
# 			{
# 				if (_uri.endsWith('.' + ext))
# 				{
# 					loading = true;
# 					Future request;
# 					if(enableCrossOrigin)
# 						request = HttpRequest.requestCrossOrigin(_uri).then((String string) => setString(string));
# 					else
# 						request = HttpRequest.getString(_uri).then((String string) => setString(string));

# 					c.complete(request);
# 					break;
# 				}
# 			}
# 			if (loading == true)
# 			  return c.future;

# 			// Returns a decoded object from a json file
# 			for (String ext in jsonExtensions)
# 			{
# 				if (_uri.endsWith('.' + ext))
# 				{
# 					loading = true;
# 					if(enableCrossOrigin)
# 						HttpRequest.requestCrossOrigin(_uri).then((String string) => setString(string,c:c,asJson:true));
# 					else
# 						HttpRequest.getString(_uri).then((String string) => setString(string,c:c,asJson:true));

# 					break;
# 				}
# 			}
# 			if (loading == true)
# 				return c.future;
# 			else
# 				c.completeError('nothing is being loaded!');
# 		}

# 		return null;
# 	}

# 	void setString(String string, {Completer c : null, bool asJson : false})
# 	{
# 		if(asJson)
# 			_asset = jsonDecode(string);
# 		else
# 			_asset = string;
# 		loaded = true;
# 		ASSET[name] = this;

# 		if(c != null)
# 			c.complete(this);
# 	}
# }

class Batch:
    def __init__(self, items_to_load: list[Asset]):
        self._to_load = items_to_load
        self._percent_done = 0

    async def load(self, callback, status_element = None, enable_cross_origin: bool = False) -> list:
        percent_each = 100 / len(self._to_load)

        # creates a list of tasks
        tasks = []
        for asset in self._to_load:
            asset_load_task = util.create_task(asset.load(status_element=status_element, enable_cross_origin=enable_cross_origin))
            def done(result: asyncio.Future):
                # broadcast that we loaded a file
                self._percent_done += percent_each
                if callback is not None:
                    callback(math.floor(self._percent_done))
            asset_load_task.add_done_callback(done)
            tasks.append(asset_load_task)
        if not tasks and callback is not None:
            callback(100)

        return await asyncio.gather(*tasks)
