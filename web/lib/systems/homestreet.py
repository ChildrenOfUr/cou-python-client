from pyodide import http

from configs import Configs
from systems.global_gobbler import cou_globals


class HomeStreet:
    @classmethod
    async def get_for_player(cls, username: str = None) -> str:
        response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/homestreet/get/{username or cou_globals.game.username}")
        home_street = await response.text()
        return None if not home_street else home_street

    @classmethod
    async def set_for_self(cls, tsid: str) -> bool:
        response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/homestreet/set/{cou_globals.game.username}/{tsid}")
        result = await response.text()
        return result == "true"
