from datetime import timedelta
import json
import logging

from js import WebSocket
from pyodide.ffi import create_proxy

from configs import Configs
from network.server_interop.class_defs import Quest
from message_bus import MessageBus
from systems import util
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)


class QuestManager:
    socket = None

    def __new__(cls):
        util.create_task(cls._setup_websocket())
        MessageBus.subscribe("questChoice", lambda map: cls.socket.send(json.dumps(map)))

    @classmethod
    async def _setup_websocket(cls):
        def on_close(event):
            LOGGER.info(f"[Quest] Websocket closed: {event.reason}")
            # wait 5 seconds and try to reconnect
            util.Timer(timedelta(seconds=5), cls._setup_websocket)

        def on_message(event):
            map = json.loads(event.data)
            quest = Quest.from_json(map["quest"])
            if map.get("questComplete"):
                MessageBus.publish("questComplete", quest)
                cou_globals.window_manager.rock_window.create_convo(quest.conversation_end, rewards=quest.rewards)
                cou_globals.window_manager.rock_window.switch_content(f"rwc-{quest.conversation_end.id}")
                cou_globals.window_manager.rock_window.open()
            if map.get("questFail"):
                cou_globals.window_manager.rock_window.create_convo(quest.conversation_fail)
                cou_globals.window_manager.rock_window.switch_content(f"rwc-{quest.conversation_fail.id}")
                cou_globals.window_manager.rock_window.open()
                MessageBus.publish("questFail", quest)
            if map.get("questOffer"):
                cou_globals.window_manager.rock_window.create_convo(quest.conversation_start)
                cou_globals.window_manager.rock_window.switch_content(f"rwc-{quest.conversation_start.id}")
                cou_globals.window_manager.rock_window.open()
            if map.get("questInProgress"):
                MessageBus.publish("questInProgress", quest)
            if map.get("questBegin"):
                MessageBus.publish("questBegin", quest)
            if map.get("questUpdate"):
                MessageBus.publish("questUpdate", quest)

        cls.socket = WebSocket.new(f"{Configs.ws}//{Configs.websocketServerAddress}/quest")
        cls.socket.addEventListener("open", create_proxy(lambda _: cls.socket.send(json.dumps({"connect": True, "email": cou_globals.game.email}))))
        cls.socket.addEventListener("close", create_proxy(on_close))
        cls.socket.addEventListener("error", create_proxy(lambda event: LOGGER.error("[Quest] Error {event}")))
        cls.socket.addEventListener("message", create_proxy(on_message))
