from datetime import timedelta
import logging
import time

from pyodide import http

from configs import Configs
from display.darkui import DarkUI
from message_bus import MessageBus
from systems import util
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)


class ClockManager:
    """Take each of the clock's streams, and when there is an event, broadcast this to the manager's subscribers."""

    def __init__(self) -> None:
        @MessageBus.subscribe("clock_on_update")
        def _do_clock_on_update(data) -> None:
            MessageBus.publish("timeUpdate", data)

        @MessageBus.subscribe("clock_on_new_day")
        async def _do_clock_on_new_day(data) -> None:
            await cou_globals.clock.updateHolidays()
            MessageBus.publish("newDay")
            # The fact the event fires is all that's important here

# Everything below this is for ^that^

# You can make a new clock, but one's already made. It's 'cou_globals.clock'
class Clock:
    def __init__(self) -> None:
        self._dayofweek: str
        self._year: str
        self._day: str
        self._month: str
        self._time: str
        self._dayInt: int
        self._monthInt: int
        self._dPM = [29, 3, 53, 17, 73, 19, 13, 37, 5, 47, 11, 1]
        self._currentHolidays: list[str] = []

        self._sendEvents()
        # Time update Timer
        util.Timer.periodic(timedelta(seconds=10), self._sendEvents)

    # Getters, so they can only be written by the Clock
    @property
    def dayofweek(self) -> str:
        return self._dayofweek

    @property
    def year(self) -> str:
        return self._year

    @property
    def day(self) -> str:
        return self._day

    @property
    def month(self) -> str:
        return self._month

    @property
    def time(self) -> str:
        return self._time

    @property
    def daysPerMonth(self) -> list[int]:
        return self._dPM

    @property
    def currentHolidays(self) -> list[str]:
        return self._currentHolidays

    # Integer versions
    @property
    def dayInt(self) -> int:
        return self._dayInt

    @property
    def monthInt(self) -> int:
        return self._monthInt

    async def getHolidays(self, month: int, day: int) -> list[str]:
        url = f"{Configs.http}//{Configs.utilServerAddress}/getHolidays?month={month}&day={day}"
        response = await http.pyfetch(url, mode="cors")
        return await response.json()

    def isNight(self) -> bool:
        am = "am" in self.time
        hourmin = self.time[:-2].split(":")
        hour = int(hourmin[0])

        if am:
            if hour < 5 or hour == 12:
                return True
            elif 5 <= hour < 6:
                # night to sunrise
                return True
            elif 6 <= hour < 9:
                # sunrise to daylight
                return False
            return False
        else:
            if 5 >= hour < 6:
                # daylight to sunset
                return False
            elif 6 >= hour < 12:
                # sunset to night
                return True
            return False

    def _sendEvents(self) -> None:
        # Year, month, day, week, time
        data = self._getDate()

        self._dayofweek = data[3]
        self._time = data[4]
        self._day = data[2]
        self._month = data[1]
        self._year = data[0]

        self._dayInt = data[5]
        self._monthInt = data[6]

        # Clock update stream
        MessageBus.publish("clock_on_update", [self.time, self.day, self.dayofweek, self.month, self.year, self.dayInt, self.monthInt])

        # New Day update stream
        if self.time == "6:00am":
            MessageBus.publish("clock_on_new_day", "new day!")

        MessageBus.publish("clock_tick", data)

        DarkUI.update()

    async def updateHolidays(self) -> None:
        updatedHolidays = await self.getHolidays(self.monthInt, self.dayInt)
        # Alert for new Holidays
        for holiday in updatedHolidays:
            if holiday not in self._currentHolidays:
                pass

        # Alert for left Holidays
        for holiday in self._currentHolidays:
            if holiday not in updatedHolidays:
                pass

        self._currentHolidays = updatedHolidays

    _Months = [
		'Primuary',
		'Spork',
		'Bruise',
		'Candy',
		'Fever',
		'Junuary',
		'Septa',
		'Remember',
		'Doom',
		'Widdershins',
		'Eleventy',
		'Recurse'
    ]

    _Days_of_Week = [
		'Hairday',
		'Moonday',
		'Twoday',
		'Weddingday',
		'Theday',
		'Fryday',
		'Standday',
		'Fabday'
    ]

    def _getDate(self) -> list:
        #
        # there are 4435200 real seconds in a game year
        # there are 14400 real seconds in a game day
        # there are 600 real seconds in a game hour
        # ther are 10 real seconds in a game minute
        #

        #
        # how many real seconds have elapsed since game epoch?
        #
        ts = int(time.time())
        sec = ts - 1238562000

        year = int(sec / 4435200)
        sec -= year * 4435200

        day_of_year = int(sec / 14400)
        sec -= day_of_year * 14400

        hour = int(sec / 600)
        sec -= hour * 600

        minute = int(sec / 10)
        sec -= minute * 10

        #
        # turn the 0-based day-of-year into a day & month
        #

        month_and_day = self._day_to_md(day_of_year)

        #
        # get day-of-week
        #

        days_since_epoch = day_of_year + (307 * year)

        day_of_week = days_since_epoch % 8

        #
        # Append to our day_of_month
        #
        suffix: str
        _num = str(month_and_day[1])
        match _num[:-1]:
            case "1":
                suffix = "st"
            case "2":
                suffix = "nd"
            case "3":
                suffix = "rd"
            case _:
                suffix = "th"

        #
        # Fix am pm times
        #

        h = str(hour)
        m = str(minute)
        ampm = "am"
        if minute < 10:
            m = f"0{minute}"
        if hour >= 12:
            ampm = "pm"
            if hour > 12:
                h = str(hour - 12)
        if h == "0":
            h = "12"
        current_time = f"{h}:{m}{ampm}"

        return [
            f"Year {year}",
            self._Months[month_and_day[0] - 1],
            f"{month_and_day[1]}{suffix}",
            self._Days_of_Week[day_of_week],
            current_time,
            month_and_day[1],
            month_and_day[0],
        ]

    def _day_to_md(self, id) -> list[int]:
        cd = 0

        days_in_months = (
            self.daysPerMonth[0] + self.daysPerMonth[1] + self.daysPerMonth[2] +
            self.daysPerMonth[3] + self.daysPerMonth[4] + self.daysPerMonth[5] +
            self.daysPerMonth[6] + self.daysPerMonth[7] + self.daysPerMonth[8] +
            self.daysPerMonth[9] + self.daysPerMonth[10] + self.daysPerMonth[11]
        )

        for i in range(days_in_months):
            cd += self.daysPerMonth[i]
            if cd > id:
                m = i + 1
                d = id + 1 - (cd - self.daysPerMonth[i])
                return [m, d]

        return [0, 0]

cou_globals.clock = Clock()
