import logging
from typing import Any

from message_bus import MessageBus


LOGGER = logging.getLogger(__name__)
_GLOBAL_DATA = {}


class GlobalGobbler:
    """This class is used as an attempt to make the dozens of globals
    that get shared between modules easier to access without circular
    import issues.
    """

    def __init__(self):
        MessageBus.subscribe("set_global", self.set_global)

    def set_global(self, data: dict):
        LOGGER.debug(f"[GlobalGobbler] Saving to globals: {data}")
        _GLOBAL_DATA.update(data)

    def __getattr__(self, name: str) -> Any:
        try:
            return _GLOBAL_DATA[name]
        except KeyError:
            LOGGER.error(f"Global variable '{name}' was requested but is not set")
            # could maybe return None here (Dart variables defaulted to null) but raising helps find bugs
            raise AttributeError from None

    def __setattr__(self, name: str, value: Any) -> None:
        _GLOBAL_DATA[name] = value


cou_globals = GlobalGobbler()
