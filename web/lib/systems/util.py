import asyncio
from datetime import datetime, timedelta
import inspect
import json
import logging
from typing import Any, Callable, Coroutine

from js import console, document, location
from pyodide.ffi import create_proxy
from pyodide import http

from configs import Configs
from message_bus import MessageBus
from network.server_interop.inventory import Slot
from systems.global_gobbler import cou_globals


_STARTTIME = datetime.now()
LOGGER = logging.getLogger(__name__)
_TASKS = {}


def create_task(func: Coroutine, unique: bool = False) -> asyncio.Task:
    """Run <func> in the background and retrieve and log any exceptions
    that might be raised.
    """

    task = asyncio.create_task(func)

    if unique and func.__name__ in _TASKS:
        task.cancel()
        return _TASKS[func.__name__]

    def _log_task_errors(task: asyncio.Task, func=func):
        try:
            _TASKS.pop(func.__name__, None)
            task.result()
        except asyncio.CancelledError:
            pass
        except Exception:
            LOGGER.exception("Exception raised by task = %r", task)
    task.add_done_callback(_log_task_errors)
    _TASKS[func.__name__] = task
    return task


def tsidG(tsid: str) -> str:
    """Get a TSID in 'G...' (CAT422) form"""
    return tsidL(tsid).replace("L", "G", 1)


def tsidL(tsid: str) -> str:
    """Get a TSID in 'L...' (TS) form"""
    if not tsid:
        return tsid

    if tsid.startswith("G"):
        # in CAT422 form
        return tsid.replace("G", "L", 1)
    else:
        # Assume in TS form
        return tsid


class ConsoleLogger(logging.Handler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setFormatter(logging.Formatter("[%(asctime)s] [%(levelname)5s] [%(module)s:%(lineno)s] %(message)s %(uptime)s"))

    def emit(self, record: logging.LogRecord):
        record.uptime = getUptime()
        console.log(self.format(record))


def getUptime() -> str:
    """Get how long the window has been open."""

    return str(datetime.now() - _STARTTIME)

class Timer:
    def __init__(self, duration: timedelta, callback: Callable):
        self._cancelled = False
        self._periodic = False
        self._duration = duration
        self._callback = callback
        asyncio.get_event_loop().call_later(duration.total_seconds(), create_task, self._run())
        self.is_active = True

    @classmethod
    def periodic(cls, duration: timedelta, callback: Callable) -> "Timer":
        timer = Timer(duration, callback)
        timer._periodic = True
        return timer

    def cancel(self) -> None:
        self._cancelled = True
        self.is_active = False

    async def _run(self) -> None:
        if self._cancelled:
            return

        if inspect.iscoroutinefunction(self._callback):
            await self._callback()
        else:
            self._callback()

        if self._periodic:
            await asyncio.sleep(self._duration.total_seconds())
            await self._run()
        else:
            self.is_active = False


def add_cancelable_event_listener(target, event_type: str, callback: Callable) -> Any:
    """Starts listening for the provided event_type to be raised on the target calling
    <callback> each time. Returns an object with a cancel() function that can be
    called to ignore future instances of the event.
    """

    def _handler(event) -> None:
        if inspect.iscoroutinefunction(callback):
            create_task(callback(event))
        else:
            callback(event)

    proxy = create_proxy(_handler)
    target.addEventListener(event_type, proxy)

    class Canceller:
        def cancel(self):
            target.removeEventListener(event_type, proxy)

    return Canceller()


def add_event_once(target, event_type: str, callback: Callable) -> Any:
    """Starts listening for the provided event_type to be raised on the target. When
    found, callback will be called with the event that was raised. After the first event,
    callback won't be called for future events.

    Returns a instance with a cancel() function that can be called to cancel the
    listener prior to any events being handled.
    """
    def _handler(event) -> None:
        target.removeEventListener(event_type, proxy)
        if inspect.iscoroutinefunction(callback):
            create_task(callback(event))
        else:
            callback(event)

    proxy = create_proxy(_handler)
    target.addEventListener(event_type, proxy)

    class Canceller:
        def cancel(self):
            target.removeEventListener(event_type, proxy)

    return Canceller()


async def await_event_once(target, event_type: str) -> Any:
    """Starts listening for the provided event_type to be raised on the target.
    Returns the first instance of the event on the target.
    """

    completer = asyncio.Future()

    def _handler(event) -> None:
        target.removeEventListener(event_type, proxy)
        completer.set_result(event)

    proxy = create_proxy(_handler)
    target.addEventListener(event_type, proxy)

    await completer
    return completer.result()


class Util:
    """Counts the number of items of type [item] that the player currently has in their pack.
    If the items are spread across more than one inventory slot, this will return the sum
    of all slots.

    For example, if the player has 50 Chunks of Beryl in 3 slots, then this will return 150

    If slow and sub_slot are set > -1, this will count only items in that slot.
    """

    _instance: "Util" = None
    _item_count_cache: dict[str, int] = {}

    def __new__(cls) -> "Util":
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self) -> None:
        if getattr(self, "initialized", False):
            return

        def clear_cache(_):
            self._item_count_cache = {}
        self.initialized = True
        MessageBus.subscribe("inventoryUpdated", clear_cache)

    def get_num_items(self, item: str, slot: int = -1, sub_slot: int = -1, include_broken: bool = False) -> int:
        if slot == -1 and sub_slot == -1 and item in self._item_count_cache:
            return self._item_count_cache[item]
        else:
            num = _get_num_items(item, slot=slot, sub_slot=sub_slot, include_broken=include_broken)
            self._item_count_cache[item] = num
            return num

    def get_stack_remainders(self, item_type: str) -> int:
        remainder = 0
        for slot in cou_globals.player_inventory.slots:
            if slot.item is None:
                continue

            if slot.item_type == item_type:
                remainder += slot.item.stacks_to - slot.count

            if slot.item.is_container:
                slots_string = slot.item.metadata["slots"]
                bag_slots = [slot for slot in json.loads(slots_string)]
                for bag_slot in bag_slots:
                    if bag_slot.get("item") is None:
                        continue

                    if bag_slot.get("itemType") == item_type:
                        remainder += bag_slot["item"]["stacksTo"] - bag_slot["count"]

        return remainder

    def get_blank_slots(self, item_map: dict) -> int:
        count = 0

        for slot in cou_globals.player_inventory.slots:
            if slot.item is None or slot.item_type is None or slot.item_type == "":
                count += 1
                continue

            # if the item is a container, it can only go the above slots
            # otherwise, we need to look in any slots and count those
            # if the item fits the filter
            if not item_map["isContainer"] and slot.item.is_container:
                if item_map["itemType"] in slot.item.sub_slot_filter or len(slot.item.sub_slot_filter) == 0:
                    slots_string = slot.item.metadata["slots"]
                    bag_slots = [slot for slot in json.loads(slots_string)]
                    for bag_slot in bag_slots:
                        if bag_slot.get("item") is None or bag_slot.get("itemType") in (None, ""):
                            count += 1

        return count

cou_globals.util = Util()


def _get_num_items(item: str, slot: int = -1, sub_slot: int = -1, include_broken: bool = False) -> int:
    count = 0

    # count all the normal slots
    for index, inv_slot in enumerate(cou_globals.player_inventory.slots):
        if slot > -1 and index != slot:
            continue
        if inv_slot.item_type == item:
            durability_used = int(inv_slot.item.metadata.get("durabilityUsed", "0"))
            if inv_slot.item.durability is not None and durability_used >= inv_slot.item.durability and not include_broken:
                continue

            count += inv_slot.count

    # add the bag contents
    for inv_slot in cou_globals.player_inventory.slots:
        if inv_slot.item_type == "" or not inv_slot.item.is_container or inv_slot.item.sub_slots is None:
            continue

        slots_string = inv_slot.item.metadata["slots"]
        bag_slots = [inv_slot for inv_slot in json.loads(slots_string)]
        for index, bag_slot in enumerate(bag_slots):
            if sub_slot > -1 and index != sub_slot:
                continue
            if bag_slot["itemType"] == item:
                durability_used = int(bag_slot["item"]["metadata"].get("durabilityUsed", "0"))
                if bag_slot["item"].get("durability") is not None and durability_used >= bag_slot["item"]["durability"] and not include_broken:
                    continue

                count += bag_slot["count"]

    return count


def send_action(method_name: str, entity_id: str, arguments: dict) -> None:
    """Send a message to the server that you want to perform [method_name] on [entity_id]
    with optional [arguments]
    """

    if method_name is None or method_name.strip() == "":
        LOGGER.error(f"[Server Communication] method_name must be provided, got {method_name}")
        return

    if entity_id is None or entity_id.strip() == "":
        LOGGER.error(f"[Server Communication] entity_id must be provided, got {entity_id}")
        return

    LOGGER.info(
        f"[Server Communication] Sending {method_name} to entity: {entity_id}"
        f" ({cou_globals.entities.get(entity_id).__class__.__name__}) with arguments: {arguments}"
    )
    entity = document.querySelector(f"#{entity_id}")
    data = {}
    data["callMethod"] = method_name
    if entity_id == "global_action_monster":
        data["id"] = entity_id
    elif entity is not None:
        data["id"] = entity_id
        data["type"] = entity.className
    else:
        data["type"] = entity_id
    data["streetName"] = cou_globals.current_street.label
    data["username"] = cou_globals.game.username
    data["email"] = cou_globals.game.email
    data["tsid"] = cou_globals.current_street.street_data["tsid"]
    data["arguments"] = arguments
    cou_globals.street_socket.send(json.dumps(data))


def send_global_action(method_name: str, arguments: dict) -> None:
    """Send a message to the server that you want to perform [method_name]
    that does not belong to a specific entity with optional [arguments]
    """

    if method_name is None or method_name.strip() == "":
        LOGGER.error(f"[Server Communication] method_name must be provided, got: {method_name}")
        return

    data = {}
    data["callMethod"] = method_name
    data["id"] = "global_action_monster"
    data["streetName"] = cou_globals.current_street.label
    data["username"] = cou_globals.game.username
    data["email"] = cou_globals.game.email
    data["tsid"] = cou_globals.current_street.street_data["tsid"]
    data["arguments"] = arguments
    cou_globals.street_socket.send(json.dumps(data))

    LOGGER.info(f"[Server Communication] Sending global {method_name} with arguments: {arguments}")


def hard_reload() -> None:
    """Clear cache with JS reload"""

    location.reload(True)


def sanitize_name(name: str) -> str:
    """Get a name that will work as a css selector by replacing all invalid characters with an underscore"""

    bad_chars = "! @ \$ % ^ & * ( ) + = , . / ' ; : \" ? > < [ ] \\ { } | ` # ~".split(" ")
    for char in bad_chars:
        name = name.replace(char, "_")

    return name


async def check_lag() -> int:
    start = datetime.utc()
    response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/time/utc")
    end = datetime.utcnow()

    return (end - start) / timedelta(milliseconds=1)
