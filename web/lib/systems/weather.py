import asyncio
from datetime import timedelta
from enum import Enum
import json
import logging

from pyscript import document, window
from pyodide.ffi import create_proxy

from configs import Configs
from message_bus import MessageBus
from systems import util
from systems.audio import Sound
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)


class WeatherIntensity(Enum):
    LIGHT = 1
    NORMAL = 2

class WeatherState(Enum):
    CLEAR = 1
    RAINING = 2
    SNOWING = 3
    WINDY = 4

class WeatherManager:
    _weather_manager: "WeatherManager" = None
    _intensity = WeatherIntensity.NORMAL
    _current_state = WeatherState.CLEAR
    _enabled= True
    _gradient_enabled = True
    requests: dict[str, asyncio.Future] = {}

    def __new__(cls) -> "WeatherManager":
        if not cls._weather_manager:
            cls._weather_manager = super().__new__(cls)
            cls._initialize()
        return cls._weather_manager

    @classmethod
    def _initialize(cls) -> None:
        cls.url = f"{Configs.ws}//{Configs.websocketServerAddress}/weather"
        cls._weather_layer = document.querySelector("#weatherLayer")
        cls._cloud_layer = document.querySelector("#cloudLayer")
        cls._raindrops = cls._weather_layer.querySelector("#weather-raindrops")
        cls._snowflakes = cls._weather_layer.querySelector("#weather-snowflakes")
        cls.rain_sound = None

        if window.localStorage.getItem("WeatherEffectsEnabled") is not None:
            # if we can't pull out the intensity, that's ok, we have a default
            try:
                value = window.localStorage.getItem("LightWeatherEffects")
                cls._intensity = WeatherIntensity.LIGHT if value == "true" else WeatherIntensity.NORMAL
            except Exception:
                pass

            if window.localStorage.getItem("WeatherEffectsEnabled") == "false":
                cls._enabled = False

            # need a service to listen to time events and respond by coloring the
            # weather overlay as needed (possibly change the background gradient?)
            MessageBus.subscribe("timeUpdate", cls._change_ambient_color)
            MessageBus.subscribe("timeUpdateFake", cls._change_ambient_color)

            @MessageBus.subscribe("streetLoaded")
            def on_street_load(_):
                cls._gradient_enabled = True
                MessageBus.publish(
                    "timeUpdate", [getattr(cou_globals.clock, value) for value in ["time", "day", "dayofweek", "month", "year"]],
                )

            # service for debugging weather
            MessageBus.subscribe("setWeatherFake", lambda m: cls._process_message(m))

            # update on start
            MessageBus.publish("timeUpdate", [getattr(cou_globals.clock, value) for value in ["time", "day", "dayofweek", "month", "year"]])

        cls._setup_websocket()

        resize_timer: util.Timer = None
        @MessageBus.subscribe("windowResized")
        def on_resize(_):
            nonlocal resize_timer
            # we only want to respond to the last event of this in a series
            # so we'll wait until 200ms have gone by without one of these events
            # and then we'll do our real work
            if resize_timer is not None and resize_timer.is_active:
                resize_timer.cancel()

            resize_timer = util.Timer(timedelta(milliseconds=200), cls._recalculate_rain)

        @MessageBus.subscribe("streetLoaded")
        def on_street_load(_):
            cls._request_street_weather()

    @classmethod
    async def request_hub_weather(cls, hub_id: str) -> dict:
        cls.requests[hub_id] = asyncio.Future()
        cls.socket.send(json.dumps({
            "update": "request",
            "username": cou_globals.game.username,
            "hub_id": hub_id,
        }))
        await cls.requests[hub_id]
        return cls.requests[hub_id].result()

    @classmethod
    def _request_street_weather(cls):
        cls.socket.send(json.dumps({
            "update": "location",
            "username": cou_globals.game.username,
            "tsid": cou_globals.current_street.tsid,
        }))

    @classmethod
    def _recalculate_rain(cls) -> None:
        if cls._enabled and cls._current_state == WeatherState.RAINING:
            cls._clear_weather()
            cls._create_weather(cls._current_state)

    @classmethod
    def _change_ambient_color(cls, time_pieces: list[str]) -> None:
        if not cls._enabled:
            return

        time = time_pieces[0]
        am = "am" in time
        hour_min = time[:-2].split(":")
        hour = int(hour_min[0])
        minute = int(hour_min[1])
        rgba = [255, 255, 255, 0]

        percent = 1
        if not am:
            if 5 <= hour < 7:
                current_minute = (7 - hour) * 60 - minute
                percent = (1 - current_minute / 120) / 2
                # daylight to sunset
                rgba = cls._tween_color([255, 255, 255, 0], [218, 150, 45, .15], percent)
            elif 7 <= hour < 12:
                current_minute = (12 - hour) * 60 - minute
                percent = 1 - current_minute / 240
                # sunset to night
                rgba = cls._tween_color([218, 150, 45, .15], [17, 17, 47, .5], percent)
                percent = percent / 2 + .5
            else:
                percent = 0
            cls._set_street_gradient(percent)
        else:
            if hour < 5 or hour == 12:
                rgba = [17, 17, 47, .5]
            elif 5 <= hour < 7:
                current_minute = (7 - hour) * 60 - minute
                percent = (1 - current_minute / 120) / 2
                # night to sunrise
                rgba = cls._tween_color([17, 17, 47, .5], [218, 150, 45, .15], percent)
                percent = 1 - percent
            elif 7 <= hour < 9:
                current_minute = (9 - hour) * 60 - minute
                percent = 1 - current_minute / 120
                # surise to daylight
                rgba = cls._tween_color([218, 150, 45, .15], [255, 255, 255, 0], percent)
                percent = .5 - percent / 2
            else:
                percent = 0
            cls._set_street_gradient(percent)

        cls._weather_layer.style.backgroundColor = f"rgba({','.join([str(x) for x in rgba])})"

    @classmethod
    def _set_street_gradient(cls, percent: float) -> None:
        if not cls._gradient_enabled:
            return

        gradient_canvas = document.querySelector("#gradient")
        if not gradient_canvas:
            return

        try:
            street_top = f"#{cou_globals.current_street.street_data['gradient']['top']}"
            street_bottom = f"#{cou_globals.current_street.street_data['gradient']['bottom']}"
        except Exception:
            cls._gradient_enabled = False
            LOGGER.error("[Weather] Could not create street gradient")
            return

        # make sure the street has a gradient before tyring to modify it
        if len(street_top) < 7 or len(street_bottom) < 7:
            return

        top_tween = cls._tween_color(cls.hex2rgb(street_top), [19, 0, 5, 1], percent)
        bottom_tween = cls._tween_color(cls.hex2rgb(street_bottom), [19, 0, 5, 1], percent)
        top = cls.rgb2hex(top_tween)
        bottom = cls.rgb2hex(bottom_tween)
        gradient_canvas.style.background = f"linear-gradient(to bottom, {top}, {bottom})"

    @classmethod
    def hex2rgb(cls, hex: str) -> list[int]:
        return [
            int(hex[1:3], base=16),
            int(hex[3:5], base=16),
            int(hex[5:7], base=16),
            1,
        ]

    @classmethod
    def rgb2hex(cls, rgb: list[int]) -> str:
        return f"#{hex(rgb[0])[2:].rjust(2, '0')}{hex(rgb[1])[2:].rjust(2, '0')}{hex(rgb[2])[2:].rjust(2, '0')}"

    @classmethod
    def _tween_color(cls, start: list[float], end: list[float], percent: float) -> list[float]:
        return [
            start[0] - int(percent * (start[0] - end[0])),
            start[1] - int(percent * (start[1] - end[1])),
            start[2] - int(percent * (start[2] - end[2])),
            start[3] - percent * (start[3] - end[3]),
        ]

    @classmethod
    def _create_weather(cls, create_state: WeatherState) -> None:
        if not cls._enabled:
            return

        LOGGER.info(f"[Weather] {cls._current_state}: {cls._intensity}")

        if create_state == WeatherState.RAINING and not cou_globals.map_data.snowy_weather():
            # rain
            if cls._intensity == WeatherIntensity.LIGHT:
                cls._raindrops.classList.add("light")
            else:
                cls._raindrops.classList.remove("light")

            # sound effect
            util.create_task(cls._play_rain_sound())

            # graphical effects
            if not cou_globals.map_data.force_disable_weather():
                # enable rain display
                if not cls._cloud_layer.classList.contains("cloudy"):
                    cls._cloud_layer.classList.add("cloudy")
                cls._raindrops.style.opacity = "0.5"
            else:
                cls._cloud_layer.classList.remove("cloudy")
                cls._raindrops.style.opacity = "0"
        elif create_state == WeatherState.SNOWING or cou_globals.map_data.snowy_weather():
            # snow

            # graphical effects
            if not cou_globals.map_data.force_disable_weather():
                # enable snow display
                if not cls._cloud_layer.classList.contains("snowy") and cls._intensity != WeatherIntensity.LIGHT:
                    cls._cloud_layer.classList.add("snowy")
                cls._snowflakes.style.opacity = "0.7"
            else:
                cls._cloud_layer.classList.remove("snowy")
                cls._snowflakes.style.opacity = "0"

    @classmethod
    async def _play_rain_sound(cls) -> None:
        cou_globals.audio.game_sounds["rainSound"] = Sound(await cou_globals.audio._load_buffer("rain"), cou_globals.audio.audio_channels["weather"])
        cls.rain_sound = await cou_globals.audio.play_sound("rainSound", looping=True, fade_in_duration=timedelta(seconds=2))

    @classmethod
    def _clear_weather(cls) -> None:
        cls._raindrops.style.opacity = "0"
        cls._snowflakes.style.opacity = "0"
        cls._cloud_layer.classList.remove("cloudy", "snowy")
        if cls.rain_sound is not None:
            cou_globals.audio.stop_sound(cls.rain_sound, fade_out_duration=timedelta(seconds=2))

    @classmethod
    def set_enabled(cls, enabled: bool) -> None:
        window.localStorage.setItem("WeatherEffectsEnabled", str(enabled).lower())
        cls._enabled = enabled
        cls._clear_weather()
        if cls._enabled and cls._current_state != WeatherState.CLEAR:
            cls._create_weather(cls._current_state)

    @classmethod
    def set_intensity(cls, intenstiy: WeatherIntensity) -> None:
        cls._intensity = intenstiy
        window.localStorage.setItem("WeatherEffectsIntensity", str(cls._intensity.value))
        cls._clear_weather()
        if cls._enabled and cls._current_state in [WeatherState.RAINING, WeatherState.SNOWING]:
            cls._create_weather(cls._current_state)

    @classmethod
    def _setup_websocket(cls) -> None:
        # establish a websocket connection to listen for weather data coming in

        def on_open(_):
            cls.socket.send(json.dumps({
                "username": cou_globals.game.username,
                "tsid": cou_globals.current_street.tsid,
            }))
        def on_message(event):
            map = json.loads(event.data)
            cls._process_message(map)
        def on_close(event):
            LOGGER.error(f"[Weather] Socket closed: {event.reason}")
            # wait and then try to reconnect
            util.Timer(timedelta(seconds=2), lambda: cls._setup_websocket())
        def on_error(event):
            LOGGER.error("[Weather] error")
            window.console.log(event)

        cls.socket = window.WebSocket.new(cls.url)
        cls.socket.addEventListener("open", create_proxy(on_open))
        cls.socket.addEventListener("message", create_proxy(on_message))
        cls.socket.addEventListener("close", create_proxy(on_close))
        cls.socket.addEventListener("error", create_proxy(on_error))

    @classmethod
    def _process_message(cls, map) -> None:
        if cou_globals.window_manager.weather.element_open and not cou_globals.window_manager.weather.remote_viewing:
            cou_globals.window_manager.weather.refresh()

        if map is None:
            cls._request_street_weather()
            return

        previous_state = cls._current_state

        if map.get("error"):
            cls.weather_data = map
            cls._current_state = WeatherState.CLEAR
        elif map.get("hub_id"):
            cls.requests[map["hub_id"]].set_result(map)
        else:
            cls.weather_data = map
            weather_main = cls.weather_data["current"]["weatherMain"].lower()
            if "clear" in weather_main or "clouds" in weather_main:
                cls._current_state = WeatherState.CLEAR
            elif "rain" in weather_main:
                cls._current_state = WeatherState.RAINING
            elif "snow" in weather_main:
                cls._current_state = WeatherState.SNOWING
            elif "wind" in weather_main:
                cls._current_state = WeatherState.WINDY
            else:
                cls._current_state = previous_state

        if cls._current_state != previous_state:
            cls._clear_weather()
            if cls._current_state != WeatherState.CLEAR:
                cls._create_weather(cls._current_state)
