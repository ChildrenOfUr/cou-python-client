import asyncio
from datetime import timedelta
import logging
import urllib
import weakref

from js import AudioBuffer, AudioContext, SC, document, window
from pyodide.ffi import create_proxy
from pyodide import http

from message_bus import MessageBus
from systems import util
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)

class GainMixin:
    def __init__(self, gain: float = 0.5, min: float = 0, max: float = 1) -> None:
        self._gain_node = self.context.createGain()
        self._min = min
        self._max = max
        self.set_gain(gain)

    def get_gain(self) -> float:
        return self._gain_node.gain.value

    def set_gain(self, value: float) -> None:
        _value = max(self._min, min(value, self._max))
        if _value != value:
            LOGGER.warning(f"[SoundManager] gain setting of {value:.2f} clamped between {self._min} and {self._max}")
        self._gain_node.gain.value = _value

    @property
    def muted(self) -> bool:
        return self._muted

    @muted.setter
    def muted(self, mute: bool) -> None:
        if mute:
            self._previous_gain = self.get_gain()
            self.set_gain(0)
        else:
            self.set_gain(getattr(self, "_previous_gain", self.get_gain()))


class AudioChannel(GainMixin):
    def __init__(self, context: AudioContext, gain: float = .5) -> None:
        self._gain_callbacks: weakref.WeakSet[Song] = weakref.WeakSet()
        self.context = context
        super().__init__(gain=gain)
        self._gain_node.connect(self.context.destination)

    def on_gain_changed(self, callback: "Song") -> None:
        self._gain_callbacks.add(callback)

    def set_gain(self, value: float) -> None:
        super().set_gain(value)
        for song in self._gain_callbacks:
            song.set_gain(self.get_gain())


class Sound(GainMixin):
    def __init__(self, buffer: AudioBuffer, channel: AudioChannel, gain: float = 1) -> None:
        self._buffer = buffer
        self._channel = channel
        self.context = channel.context
        super().__init__(gain=gain)
        self._gain_node.connect(self._channel._gain_node)

    def play(self, loop: bool = False, fade_in_duration: timedelta = None) -> None:
        self._source = self._channel.context.createBufferSource()
        self._source.loop = loop
        self._source.buffer = self._buffer
        self._source.connect(self._gain_node)
        self._source.start(0)

        if fade_in_duration:
            gain_target = self.get_gain()
            step_time = timedelta(milliseconds=100)
            step_size = gain_target / (fade_in_duration / step_time)
            self.set_gain(0)
            def raise_volume():
                self.set_gain(self.get_gain() + step_size)
                if self.get_gain() >= gain_target:
                    timer.cancel()
            timer = util.Timer.periodic(step_time, raise_volume)

    def stop(self, fade_out_duration: timedelta = None) -> None:
        if fade_out_duration:
            step_time = timedelta(milliseconds=100)
            step_size = self.get_gain() / (fade_out_duration / step_time)
            def lower_volume():
                self.set_gain(self.get_gain() - step_size)
                if self.get_gain() <= 0:
                    self._source.stop()
                    timer.cancel()
            timer = util.Timer.periodic(step_time, lower_volume)
        else:
            self._source.stop()


class Song:
    def __init__(self, name: str, channel: AudioChannel) -> None:
        self.name = name
        self._metadata = {}
        self._widget = None
        self._channel = channel
        self._channel.on_gain_changed(self)
        self._gain = self._channel.get_gain() * 100

        util.create_task(self._load())

    @property
    def title(self) -> str:
        return self._metadata.get("title")

    @property
    def author_name(self) -> str:
        return self._metadata.get("author_name")

    @property
    def author_url(self) -> str:
        return self._metadata.get("author_url")

    async def stop(self, fade_out_duration: timedelta = None) -> None:
        completer = asyncio.Future()
        if fade_out_duration:
            step_time = timedelta(milliseconds=100)
            step_size = self._gain / (fade_out_duration / step_time)
            def lower_volume():
                self._gain -= step_size
                self._widget.setVolume(self._gain)
                if self._gain <= 0:
                    self._widget.pause()
                    timer.cancel()
                    completer.set_result(True)
            timer = util.Timer.periodic(step_time, lower_volume)
        else:
            self._widget.pause()
            completer.set_result(True)

        await completer

    def set_gain(self, gain: float) -> None:
        try:
            self._widget.setVolume(gain * 100)
        except Exception:
            LOGGER.exception("Unable to set the song's gain")

    async def _load(self) -> None:
        self._metadata = await self._get_song_metadata(self.name)
        if iframe := document.querySelector("#SCplayer"):
            iframe.remove()
        sc_widget = document.querySelector("#SCwidget")
        sc_iframe = document.createElement("iframe")
        sc_iframe.id = "SCplayer"
        sc_iframe.style.display = "none"
        sc_iframe.allow = "autoplay"
        sc_iframe.src = self._metadata["url"]
        sc_widget.appendChild(sc_iframe)

        self._widget = SC.Widget(sc_iframe)
        def on_ready():
            self.set_gain(self._channel.get_gain())
            self._widget.play()
            self._widget.bind(SC.Widget.Events.FINISH, create_proxy(on_finish))
        def on_finish(_):
            self._widget.play()
        self._widget.bind(SC.Widget.Events.READY, create_proxy(on_ready))

        # Change the ui
        cou_globals.view.soundcloud.SCsong = self.title
        cou_globals.view.soundcloud.SCartist = self.author_name
        cou_globals.view.soundcloud.SClink = self.author_url

    async def _get_song_metadata(self, song_name: str) -> dict:
        url = f"https://api.soundcloud.com{cou_globals.audio.songs[song_name]}"
        response = await http.pyfetch(f"https://soundcloud.com/oembed?url={urllib.parse.quote(url)}&format=json")
        metadata = await response.json()
        metadata["url"] = f"https://w.soundcloud.com/player/?url={urllib.parse.quote(url)}"
        return metadata


class SoundManager:
    def __init__(self) -> None:
        self.audio_channels: dict[str, AudioChannel] = {}
        self.game_sounds: dict[str, Sound] = {}
        self.songs: dict[str, str] = {}
        self.current_song: Song = None
        self.muted = False
        self.extension = "ogg"
        self.load = asyncio.Future()

        # if canPlayType returns the empty string, that format is not compatible
        if document.createElement("audio").canPlayType("audio/ogg") == "":
            LOGGER.info("[SoundManager] Ogg not supported, using mp3s instead")
            self.extension = "mp3"

        LOGGER.info("[SoundManager] Starting up...")
        @MessageBus.subscribe("playSong")
        async def listen_for_songs(song_name: str):
            await self.play_song(song_name)

        @MessageBus.subscribe("playSound")
        async def listen_for_sounds(sound_name: str):
            await self.play_sound(sound_name)

        LOGGER.info("[SoundManager] Registered services")

        util.create_task(self.init())

    @property
    def muted(self) -> bool:
        return self._muted

    @muted.setter
    def muted(self, mute: bool) -> None:
        self._muted = mute
        for channel in self.audio_channels.values():
            channel.muted = self._muted

    async def init(self) -> None:
        self.audio_context = window.AudioContext.new()
        for channel_name in ["effects", "music", "weather"]:
            self.audio_channels[channel_name] = AudioChannel(
                self.audio_context, gain=float(window.localStorage.getItem(f"{channel_name}Volume") or .5),
            )

        if not self.muted:
            # load the loading music and play when ready
            self.game_sounds["loading"] = Sound(await self._load_buffer("loading"), self.audio_channels["effects"])
            self.game_sounds["loading"].play(loop=True)

        # load the song urls
        response = await http.pyfetch("./files/json/music.json")
        for song_name, details in (await response.json()).items():
            self.songs[song_name] = details["scid"]

        # load the sound effects
        for sound in ["quoinSound", "mention", "game_loaded", "tripleJump", "levelUp", "newday_rooster", "purr"]:
            self.game_sounds[sound] = Sound(await self._load_buffer(sound), self.audio_channels["effects"])

        self.load.set_result(True)

    async def play_sound(self, sound: str, looping: bool = False, fade_in_duration: timedelta = None) -> Sound:
        _sound = self.game_sounds.get(sound)
        if not _sound:
            LOGGER.error(f"[SoundManager] {sound} is an unknown sound or has not been loaded")
            return
        _sound.play(loop=looping, fade_in_duration=fade_in_duration)
        return _sound

    def stop_sound(self, sound: str|Sound, fade_out_duration: timedelta = None) -> None:
        if isinstance(sound, str):
            _sound = self.game_sounds.get(sound)
            if not _sound:
                LOGGER.error(f"[SoundManager] {sound} is an unknown sound or has not been loaded")
                return
        else:
            _sound = sound

        _sound.stop(fade_out_duration=fade_out_duration)

    async def play_song(self, song: str) -> None:
        if song not in self.songs:
            LOGGER.error(f"[SoundManager] {song} is an unknown")
            return

        if self.current_song is not None and song == self.current_song.name:
            return
        else:
            if self.current_song:
                await self.current_song.stop(fade_out_duration=timedelta(seconds=2))
            self.current_song = Song(song, self.audio_channels["music"])

    async def _load_buffer(self, sound: str) -> AudioBuffer:
        try:
            response = await http.pyfetch(f"./files/audio/{sound}.{self.extension}")
            return await self.audio_context.decodeAudioData(await response.buffer())
        except Exception:
            LOGGER.error(f"[SoundManager] Unable to load {sound}")
