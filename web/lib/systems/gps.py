import dataclasses
import logging

from pyscript import window
from pyodide import http

from message_bus import MessageBus
from network.server_interop.serializable import serializable
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)
INFINITY = 9999999999999


@serializable
@dataclasses.dataclass
class Edge:
    start: str
    end: str
    weight: int = 1


@serializable
@dataclasses.dataclass
class Graph:
    edges: list[Edge] = dataclasses.field(default_factory=list)


class GPS:
    world_graph: Graph = None
    current_route: list[str] = []
    active: bool = False

    @classmethod
    def on_path(cls) -> bool:
        if not cls.current_route:
            return False

        try:
            return cls.current_route.index(cou_globals.current_street.label) >= 0
        except ValueError:
            return False

    @classmethod
    def arrived(cls) -> bool:
        if not cls.current_route:
            return False

        return cls.current_route[-1] == cou_globals.current_street.label

    @classmethod
    def next_street_name(cls) -> str:
        if not cls.on_path():
            return "You're off the path"

        if not cls.arrived():
            return cls.current_route[cls.current_route.index(cou_globals.current_street.label) + 1]

        window.localStorage.removeItem("gps_navigating")
        return "You have arrived"

    @classmethod
    def destination_name(cls) -> str:
        if cls.current_route:
            return cls.current_route[-1]
        return "No current destination"

    @classmethod
    async def init_world_graph(cls) -> None:
        response = await http.pyfetch("worldGraph.txt")
        cls.world_graph = Graph.from_json(await response.json())

        def dead_message(dying: bool) -> None:
            if dying:
                cls.active = False
            elif window.localStorage.getItem("gps_navigating") is not None:
                cls.active = True
        MessageBus.subscribe("dead", dead_message)

    @classmethod
    def get_route(cls, source: str, target: str) -> list[str]:
        cls.active = True
        cls.current_route = cls._dijkstra(cls.world_graph, source, target)
        window.localStorage.setItem("gps_navigating", target)
        return cls.current_route

    @classmethod
    def _dijkstra(cls, graph: Graph, source: str, target: str) -> list[str]:
        vertices: set[str] = set()
        neighbours: dict[str, list[dict]] = {}
        for edge in graph.edges:
            vertices.add(edge.start)
            vertices.add(edge.end)
            if edge.start in neighbours:
                neighbours[edge.start].append({"end": edge.end, "cost": edge.weight})
            else:
                neighbours[edge.start] = [{"end": edge.end, "cost": edge.weight}]

            if edge.end in neighbours:
                neighbours[edge.end].append({"end": edge.start, "cost": edge.weight})
            else:
                neighbours[edge.end] = [{"end": edge.start, "cost": edge.weight}]

        dist: dict[str, float] = {}
        previous: dict[str, str] = {}
        for vertex in vertices:
            dist[vertex] = INFINITY
        dist[source] = 0

        Q: list[str] = list(vertices)
        u: str
        last_length = len(Q)
        while Q:
            min = INFINITY
            for vertex in Q:
                if dist[vertex] < min:
                    min = dist[vertex]
                    u = vertex

            Q.remove(u)
            if len(Q) == last_length:
                LOGGER.error(f"[GPS] Couldn't find path from {source} to {target}")
                return []
            else:
                last_length = len(Q)
            if dist[u] == INFINITY or u == target:
                break

            if u in neighbours:
                for arr in neighbours[u]:
                    alt = dist[u] + arr["cost"]
                    if alt < dist[arr["end"]]:
                        dist[arr["end"]] = alt
                        previous[arr["end"]] = u

        path: list[str] = []
        u = target
        while u in previous:
            path.insert(0, u)
            u = previous[u]
        path.insert(0, u)

        return path
