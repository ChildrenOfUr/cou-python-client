import inspect
import logging

from pyscript import document, window

from configs import Configs
from display.render.collision_lines_debug import hide_line_canvas, show_line_canvas
from display.toast import Toast, NotifyRule
from message_bus import MessageBus
from systems import util
from systems.global_gobbler import cou_globals
from systems.homestreet import HomeStreet


LOGGER = logging.getLogger(__name__)


# debugging service
@MessageBus.subscribe("err")
@MessageBus.subscribe("debug")
def on_debug(event):
    MessageBus.publish("toast", f"Debug: {event.content}")

# list of commandable functions
COMMANDS: dict[str, callable] = {}


class CommandManager:
    def __init__(self) -> None:
        COMMANDS["help"] = lambda _: Toast(f"Available commands: {','.join(COMMANDS.keys())}")
        COMMANDS["interface"] = change_interface
        COMMANDS["ping"] = _check_lag
        COMMANDS["playsong"] = lambda noun: MessageBus.publish("playSong", noun)
        COMMANDS["playsound"] = lambda noun: MessageBus.publish("playSound", noun)
        COMMANDS["reload"] = lambda _: util.hard_reload()

        if Configs.testing:
            COMMANDS["makehome"] = make_home
            COMMANDS["gohome"] = go_home
            COMMANDS["collisions"] = toggle_collision_lines
            COMMANDS["follow"] = follow
            COMMANDS["log"] = lambda string: LOGGER.info(string)
            COMMANDS["music"] = set_music
            COMMANDS["physics"] = toggle_physics
            COMMANDS["time"] = set_time
            COMMANDS["tp"] = go
            COMMANDS["weather"] = set_weather


def parse_command(command: str) -> bool:
    # Getting the important data
    verb = command.split(" ")[0].lower().replace("/", "", 1)
    noun = " ".join(command.split(" ")[1:])

    if verb in COMMANDS:
        if inspect.iscoroutinefunction(COMMANDS[verb]):
            util.create_task(COMMANDS[verb](noun))
        else:
            COMMANDS[verb](noun)
        LOGGER.info(f"[Chat] Parsed valid command: {command}")
        return True
    return False


async def make_home(options: str) -> None:
    from display.chat_panel import Chat

    options = options.strip()

    existing_home = await HomeStreet.get_for_player()
    if existing_home and options != "replace":
        def set_message(*args, **kwargs):
            Chat.local_chat.chat_input.value = "/makehome"
        Toast(
            f"You aready have a home street ({existing_home}). "
            "Run '/makehome replace' to delete it and use this street instead. "
            "Any items and entities on your existing street will be deleted!",
            notify=NotifyRule.NO,
            on_click=set_message,
        )
    elif await HomeStreet.set_for_self(cou_globals.current_street.tsid):
        Toast(
            "Home street set successfully. Click here or run '/gohome' to visit it.",
            notify=NotifyRule.NO,
            on_click=lambda *args, **kwargs: util.create_task(go_home()),
        )
    else:
        Toast("Error setting home street", notify=NotifyRule.NO)


async def go_home(_) -> None:
    """Teleports the player to their home street"""
    from display.chat_panel import Chat

    tsid = await HomeStreet.get_for_player()
    def set_message(*args, **kwargs):
        Chat.local_chat.chat_input.value = "/makehome"
    if not tsid:
        Toast(
            "You don't have a home street yet! Run '/makeHome' to get one like this street.",
            notify=NotifyRule.NO,
            on_click=set_message,
        )
    else:
        await cou_globals.street_service.request_street(tsid)


def change_interface(type: str) -> None:
    """Allows switching to desktop view on touchscreen laptops"""

    if type == "desktop":
        document.querySelector("#MobileStyle").disabled = True
        window.localStorage.setItem("interface", "desktop")
        Toast("Switched to desktop view")
    elif type == "mobile":
        document.querySelector("#MobileStyle").disabled = False
        window.localStorage.setItem("interface", "mobile")
        Toast("Switched to mobile view")
    else:
        Toast(f"Interface type must be either 'desktop' or 'mobile', {type} is invalid")


async def go(tsid: str) -> None:
    tsid = tsid.strip()
    await cou_globals.street_service.request_street(util.tsidG(tsid))


def set_time(noun: str) -> None:
    MessageBus.publish("timeUpdateFake", [noun])
    if noun == "6:00am":
        MessageBus.publish("newDayFake", None)


def set_weather(noun: str) -> None:
    if noun in ["rain", "snow", "wind", "clear"]:
        MessageBus.publish("setWeatherFake", {"current": {"weatherMain": noun}})
        Toast(f"Overridden to {noun}y weather")
    else:
        MessageBus.publish("setWeatherFake", None)
        Toast("Returned to server weather")


def toggle_collision_lines(_) -> None:
    if cou_globals.show_collision_lines:
        cou_globals.show_collision_lines = False
        hide_line_canvas()
        Toast("Collision lines hidden")
    else:
        cou_globals.show_collision_lines = True
        show_line_canvas()
        Toast("Collision lines shown")


def toggle_physics(_) -> None:
    if cou_globals.current_player.do_physics_apply:
        cou_globals.current_player.do_physics_apply = False
        Toast("Physics no longer apply to you")
    else:
        cou_globals.current_player.do_physics_apply = True
        Toast("Physics apply to you")


def set_music(song: str) -> None:
    Toast(f"Music set to {song}")
    cou_globals.audio.set_song(song)


def follow(player: str) -> None:
    Toast(cou_globals.current_player.follow_player(player))


async def _check_lag(_) -> None:
    ping = await util.check_lag()
    Toast(f"Game server ping time was {ping} ms")
