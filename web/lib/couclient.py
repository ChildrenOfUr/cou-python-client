from enum import Enum
import logging
import json
import random
import sys

import js as context
from pyscript import document, window
from pyodide.ffi import create_proxy
from pyodide import http

from configs import Configs
from display.gps_display import GpsIndicator
from display.inventory import InvDragging
from display.minimap import Minimap
from display.toast import Toast
from display.view import UserInterface
from display.windows.windows import WindowManager
from game.entities.wormhole import Wormhole
from message_bus import MessageBus
from network import map_data, constants
from network.auth import AuthManager
from network.item_action import Item
from network.metabolics_service import MetabolicsService
from systems import util
from systems.audio import SoundManager
from systems.clock import ClockManager
from systems.commands import CommandManager
from systems.global_gobbler import cou_globals
from systems.gps import GPS


# shows tracebacks for unawaited coroutines (disable in non-dev build)
import tracemalloc
tracemalloc.start()

LOGGER = logging.getLogger(__name__)
GREETING_PREFIXES = [
    "Good to see you",
    "Greetings",
    "Hello",
    "Hello there",
    "Have fun",
    "Hi",
    "Hi there",
    "It's good to see you",
    "Nice of you to join us",
    "Thanks for joining us",
    "Welcome",
    "Welcome back"
]


async def login_success(event):
    email = event.detail.providerData[0].email
    LOGGER.info("login success! (%s)", email)

    response = await http.pyfetch(
        "https://server.childrenofur.com:8383/auth/getSession", method="POST",
        headers={"content-type": "application/json"}, body=json.dumps({"email": email}),
    )
    window.localStorage.setItem("authEmail", email)
    session_map = await response.json()
    if session_map.get("playerName"):
        window.localStorage.setItem("username", session_map["playerName"])

    login_screen = document.querySelector("#loginscreen")
    logo = document.createElement("img")
    logo.src = "./files/system/logo.svg"
    logo.className = "logo"
    logo.alt = "logo"
    greeting = document.createElement("span")
    greeting.className = "greeting"
    greeting.innerText = f"{random.choice(GREETING_PREFIXES)}, {session_map['playerName']}"
    login_screen.append(logo, greeting)

    LOGGER.info(session_map)
    MessageBus.publish("loginSuccess", session_map)


def _log_exceptions(exc_type, exc_value, exc_traceback):
    logging.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))
sys.excepthook = _log_exceptions


async def run():
    handler = util.ConsoleLogger()
    handler.setLevel(logging.DEBUG)
    logging.getLogger().setLevel(logging.INFO)
    logging.getLogger().addHandler(handler)

    # Show the loading screen
    document.querySelector("#browser-error").hidden = True
    document.querySelector("#loading").hidden = False

    # Decide which UI to use
    checkMedia()

    document.addEventListener("firebaseLoginSuccess", create_proxy(login_success))

    # listen for messages from the JS side and forward them to our message bus
    # event = new CustomEvent("messageBus", {detail: {"channel": "<channel>", "data": {"any": "data", "goes": "here"}}})
    # document.dispatchEvent(event)
    document.addEventListener("messageBus", create_proxy(lambda event: MessageBus.publish(event.detail.channel, getattr(event.detail, "data", None))))

    try:
        # Load server connection configuration
        await Configs.init()

        # Download the latest map data
        map_data.map_data = await map_data.MapData.download()

        # Make sure we have an up-to-date (1 day expiration) item cache
        await Item.load_items()

        # Download constants
        cou_globals.constants = await constants.Constants.download()
    except Exception:
        LOGGER.exception("Error loading server data")
        cou_globals.server_down = True

    try:
        cou_globals.metabolics = MetabolicsService()
        cou_globals.minimap = Minimap()
        UserInterface()
        await GPS.init_world_graph()
        cou_globals.GPS = GPS
        cou_globals.gps_indicator = GpsIndicator()
        cou_globals.audio = SoundManager()
        await cou_globals.audio.load
        cou_globals.window_manager = WindowManager()
        cou_globals.auth = AuthManager()
        InvDragging.init()
    except Exception:
        LOGGER.exception("Error initializing interface")
        Toast(
            "OH NO! There was an error, so you should click here to reload."
            " If you see this several times, please file a bug report.",
            on_click=lambda *args: hardReload(),
        )

    # System
    ClockManager()
    CommandManager()

    # Watch for Collision-Triggered teleporters
    Wormhole.init()

#   // Check the blog
#   BlogNotifier.refresh();
# }

def hardReload():
  context.location.reload(True)

# Handle different device formats

class ViewportMedia(Enum):
    DESKTOP = 1
    TABLET = 2
    MOBILE = 3


def checkMedia() -> None:
    # If the device is capable of touch events, assume the touch ui
    # unless the user has explicitly turned it off in the options.
    if window.localStorage.getItem("interface") == "desktop":
        # desktop already preferred
        setStyle(ViewportMedia.DESKTOP)
    elif window.localStorage.getItem("interface") == "mobile":
        # mobile already preferred
        setStyle(ViewportMedia.MOBILE)
    elif hasattr(window, "ontouchstart"):
        # no preference, touch support, use mobile view
        setStyle(ViewportMedia.MOBILE)
        LOGGER.info(
            "[Loader] Device has touch support, using mobile layout. "
            "Run /interface desktop in chat to use the desktop view."
        )
    elif not hasattr(window, "ontouchstart"):
        # no preference, no touch support, use desktop view
        setStyle(ViewportMedia.DESKTOP)

def setStyle(style: ViewportMedia) -> None:
    """ The stylesheets are set up so that the desktop styles are always applied,
    the tablet styles are applied to tablets and phones, and the mobile style
    is only applied to phones:

    | Viewport | Desktop | Tablet  | Mobile  |
    |----------|---------|---------|---------|
    | Desktop  | Applied |         |         |
    | Tablet   | Applied | Applied |         |
    | Mobile   | Applied | Applied | Applied |

    Tablet provides touchscreen functionality and minimal optimization
    for a slightly smaller screen, while mobile prepares the UI
    for a very small viewport.
	"""

    mobile = document.querySelector("#MobileStyle")
    tablet = document.querySelector("#TabletStyle")

    match style:
        case ViewportMedia.DESKTOP:
            mobile.disabled = True
            tablet.disabled = True

        case ViewportMedia.TABLET:
            mobile.disabled = True
            tablet.disabled = False

        case ViewportMedia.MOBILE:
            mobile.disabled = False
            tablet.disabled = False

    def scroll_listener(event):
        event.target.scrollLeft = 0

    if style == ViewportMedia.TABLET or style == ViewportMedia.MOBILE:
        for element in document.querySelectorAll("html, body"):
            element.addEventListener("scroll", create_proxy(scroll_listener))


if __name__ == "__main__":
    util.create_task(run())
