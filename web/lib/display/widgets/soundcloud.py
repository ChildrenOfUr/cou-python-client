from pyscript import document


class SoundCloudWidget:
    def __init__(self) -> None:
        self.muted = False
        self.music_player_element = document.querySelector("#SCwidget")
        self.song_element = self.music_player_element.querySelector(".song")
        self.artist_element = self.music_player_element.querySelector(".artist")
        self.link_element = self.music_player_element.querySelector("#SCLink")
        self._sc_song = "-"
        self._sc_artist = "-"
        self._sc_link = ""

        self.update()
        self.music_player_element.hidden = document.createElement("audio").canPlayType("audio/mp3") == ""

    @property
    def SCsong(self) -> str:
        return self._sc_song

    @SCsong.setter
    def SCsong(self, song_name: str) -> None:
        self._sc_song = song_name
        self.update()

    @property
    def SCartist(self) -> str:
        return self._sc_artist

    @SCartist.setter
    def SCartist(self, artist_name: str) -> None:
        self._sc_artist = artist_name
        self.update()

    @property
    def SClink(self) -> str:
        return self._sc_song

    @SClink.setter
    def SClink(self, artist_link: str) -> None:
        self._sc_link = artist_link
        self.update()

    def update(self) -> None:
        # update the soundcloud widget
        self.song_element.innerText = self.SCsong.replace(f"by {self.SCartist}", "")
        self.artist_element.innerText = self.SCartist
        self.link_element.href = self.SClink
