from pyscript import document, window
from pyodide.ffi import create_proxy

from display.toast import Toast
from systems.global_gobbler import cou_globals


class VolumeSliderWidget:
    def __init__(self) -> None:
        self.muted = False
        self.do_toasts = False
        self.volume_glyph = document.querySelector("#volumeGlyph")
        self.volume_icon = document.querySelector("#volumeGlyph > i")

        # load current mute state
        if window.localStorage.getItem("mute") == "true":
            self.muted = True
            self.volume_icon.classList.remove("fa-volume-up")
            self.volume_icon.classList.add("fa-volume-off")

        # click toggles mute
        def on_click(event):
            if self.muted:
                self.muted = False
                if self.do_toasts:
                    Toast("Sound unmuted")
            else:
                self.muted = True
                if self.do_toasts:
                    Toast("Sound muted")

            self.update()
        self.volume_glyph.addEventListener("click", create_proxy(on_click))

    def update(self) -> None:
        # update the audio icon
        if self.muted:
            self.volume_icon.classList.remove("fa-volume-up")
            self.volume_icon.classList.add("fa-volume-off")
        else:
            self.volume_icon.classList.remove("fa-volume-off")
            self.volume_icon.classList.add("fa-volume-up")

        # updates the stored mute state
        window.localStorage.setItem("mute", str(self.muted).lower())

        # update the audio mute state
        cou_globals.audio.muted = self.muted
