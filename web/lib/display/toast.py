from datetime import timedelta
from enum import Enum
import logging
from typing import Callable

from pyscript import document, window
from pyodide.ffi import create_proxy


from display.windows.item_window import ItemWindow
from systems import util
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)


class NotifyRule(Enum):
    YES = 1  # Send notification
    NO = 2  # Do not send notification
    UNFOCUSED = 3  # Send only if the window is not focused


class Toast:
    # In-game toasts panel
    _toast_container = document.querySelector("#toastHolder")

    # Icon URL for system notifications
    notif_icon_url = "/files/system/logo_72.png"

    # Predefined click functions (pass as onClick)
    _click_actions = {
        "imgmenu": lambda event, argument: cou_globals.img_menu.open(),
        "iteminfo": lambda event, argument: ItemWindow(argument),
        "addFriend": lambda event, argument: cou_globals.window_manager.add_friend_window.open_with(argument),
    }

    def __init__(self, message: str, skip_chat: bool = False, notify: NotifyRule = NotifyRule.NO, on_click: str|Callable = None) -> None:
        """Creates and displays a Toast
        * [message]: What to say in the toast, chat message, and notification.
        * [skipChat]: `true` to disable display in local chat, `false` (default) to act normally and display.
        * [notify]: `YES` to send a browser notification, `NO` (default) to keep inside the client window, 'UNFOCUSED` to send only if the window is not focused
        * [onClick]: A clickActions identifier ('name|optional_arg') or function, which will be passed the click MouseEvent.
        """
        self.message = message
        self.skip_chat = False if skip_chat is None else skip_chat
        self.notify = notify
        self.click_argument = None
        self.click_handler = None

        # Display in toasts panel
        self._send_to_panel()

        # Attach click listener in panel (if onClick is passed)
        self._setup_click_handler(on_click)

        # Display in chat panel (or queue) if not disabled
        # Adds its own click listener based on that of the toast
        self._send_to_chat()

        # Display in system notification tray (if enabled)
        self._send_notification()

    def __str__(self) -> str:
        return self.message

    def _setup_click_handler(self, on_click: str|Callable) -> None:
        if on_click is None:
            return

        if isinstance(on_click, Callable):
            self.click_handler = on_click
        elif isinstance(on_click, str):
            parts = on_click.split("|")
            self.click_handler = self._click_actions[parts[0]]
            self.click_argument = "|".join(parts[1:])
        else:
            raise TypeError(
                f"onClick must be a string identifier or function, but it is of type {type(on_click)}"
            )

        self.toast_element.style.cursor = "pointer"
        self.toast_element.addEventListener("click", create_proxy(self.click))

    def _send_to_panel(self) -> None:
        # Create node
        self.toast_element = document.createElement("div")
        self.toast_element.className = "toast"
        self.toast_element.style.opacity = "0.5"
        self.toast_element.innerText = self.message

        # How long to display it (1s + 100ms per character, max 30s)
        text_time = max(1000, min((1000 + (len(self.message) * 100)), 3000))

        # Animate closing it
        time_opacity = timedelta(milliseconds=text_time)
        time_hide = timedelta(milliseconds=text_time + 500)
        def set_opacity():
            self.toast_element.style.opacity = "0"
        util.Timer(time_opacity, set_opacity)
        util.Timer(time_hide, lambda: self.toast_element.remove())

        self._toast_container.append(self.toast_element)

    def click(self, event) -> None:
        """Run click event"""

        try:
            self.click_handler(event, self.click_argument)
        except Exception as e:
            LOGGER.error(f"Could not trigger toast click event {self.click_handler}: {e}")


    def _send_to_chat(self) -> None:
        """Display in chat, if not disabled"""
        from display.chat_panel import Chat, chat_toast_buffer

        if Chat.local_chat is not None and self.skip_chat is not None and not self.skip_chat:
            Chat.local_chat.add_alert(self)
        elif not self.skip_chat:
            chat_toast_buffer.append(self)

    def _send_notification(self) -> None:
        """Send system notification, if enabled"""

        if self.notify == NotifyRule.YES or (self.notify == NotifyRule.UNFOCUSED and not cou_globals.input_manager.windowFocused):
            window.Notification.new("Game Alert", body=self.message, icon=self.notif_icon_url)
