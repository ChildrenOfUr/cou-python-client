import json
import logging
from typing import Any

from pyscript import document, window
from pyodide.ffi import create_proxy

from message_bus import MessageBus
from network.server_interop.itemdef import ItemDef
from systems import util


LOGGER = logging.getLogger(__name__)


class Acceptor:
    def accepts(self, element) -> bool:
        return True


class Draggable:
    def __init__(self, elements):
        self.draggable_elements = elements
        self.dragging_class = "item-flying"
        self._event_handlers = []
        self._drag_start_callbacks: list[callable] = []

        for element in elements:
            element.draggable = True
            dragstart = (element, "dragstart", create_proxy(self._on_drag_start))
            dragend = (element, "dragend", create_proxy(self._on_drag_end))
            element.addEventListener(*dragstart[1:])
            element.addEventListener(*dragend[1:])
            self._event_handlers.extend([dragstart, dragend])

    def destroy(self) -> None:
        for handler in self._event_handlers:
            handler[0].removeEventListener(*handler[1:])
        self._event_handlers.clear()

    def on_drag_start(self, callback: callable) -> None:
        """Register a callback for the dragstart event on one of the draggable elements"""
        self._drag_start_callbacks.append(callback)

    def _on_drag_start(self, event) -> None:
        bounding = event.target.getBoundingClientRect()
        event.dataTransfer.setDragImage(event.target, bounding.width / 2, bounding.height / 2)
        event.dataTransfer.setData("itemdef", event.target.getAttribute("itemmap"))

        bar_slot = event.target.parentElement.dataset.slotNum
        bag_bar_slot = getattr(event.target.parentElement.parentElement.parentElement.dataset, "sourceBag", None)
        if bag_bar_slot:
            slotdef = f"{bag_bar_slot}.{bar_slot}"
        else:
            slotdef = f"{bar_slot}.-1"
        event.dataTransfer.setData("slotdef", slotdef)

        event.dataTransfer.effectAllowed = "move"
        event.target.classList.add(self.dragging_class)
        for callback in self._drag_start_callbacks:
            callback(event)

    def _on_drag_end(self, event) -> None:
        event.target.classList.remove(self.dragging_class)


class Dropzone:
    def __init__(self, drop_targets: list, acceptor: Acceptor = None) -> None:
        self._drop_targets = drop_targets
        if acceptor is None:
            acceptor = Acceptor()
        self._acceptor = acceptor
        self._event_handlers = []
        self._drop_callbacks: list[callable] = []

        for target in drop_targets:
            def check_drag(event):
                if event.dataTransfer.types.includes("itemdef"):
                    event.preventDefault()  # allow the event
            dragenter = (target, "dragenter", create_proxy(check_drag))
            dragover = (target, "dragover", create_proxy(check_drag))
            drop = (target, "drop", create_proxy(self._on_drop))
            target.addEventListener(*dragover[1:])
            target.addEventListener(*dragenter[1:])
            target.addEventListener(*drop[1:])
            self._event_handlers.extend([dragover, dragenter, drop])

    def destroy(self):
        for handler in self._event_handlers:
            handler[0].removeEventListener(*handler[1:])
        self._event_handlers.clear()

    def on_drop(self, callback: callable) -> None:
        """Register a callback for the drop event on one of the drop targets"""

        self._drop_callbacks.append(callback)

    def _on_drop(self, event) -> None:
        data = event.dataTransfer.getData("itemdef")
        item = ItemDef.from_json(json.loads(data))
        if self._acceptor is not None:
            if self._acceptor.accepts(item):
                event.preventDefault()
            else:
                return False
        for callback in self._drop_callbacks:
            callback(event)


class BagFilterAcceptor(Acceptor):
    def __init__(self, allowed_item_types: list[str]) -> None:
        self.allowed_item_types = allowed_item_types

    def accepts(self, item: ItemDef) -> bool:
        if not self.allowed_item_types:
            # Those that accept nothing learn to accept everything (except other containers)
            return not item.is_container

        # Those that are at least somewhat accepting are fine, though
        return item.item_type in self.allowed_item_types


class InvDragging:
    _refresh_listener = False
    _draggables = None
    _dropzones: Dropzone = None
    _orig_box = None

	##
	 # Map used to store *how* the item was moved.
	 # Keys:
	 # - General:
	 # - - item_number: element: span element containing the count label
	 # - - bag_btn: element: the toggle button for containers
	 # - On Pickup:
	 # - - fromBag: boolean: whether the item used to be in a bag
	 # - - fromBagIndex: int: which slot the toBag is in  (only set if fromBag is true)
	 # - - fromIndex: int: which slot the item used to be in
	 # - On Drop:
	 # - - toBag: boolean: whether the item is going into a bag
	 # - - toBagIndex: int: which slot the toBag is in (only set if toBag is true)
	 # - - toIndex: int: which slot the item is going to
	##
    _move: dict[str, Any] = {}

    @classmethod
    def slot_is_empty(cls, index: int = None, box = None, bag_window: int = None) -> bool:
        """Checks if the specified slot is empty"""
        if index is not None:
            if bag_window is None:
                box = document.querySelectorAll("#inventory .box")[index]
            else:
                box = document.querySelectorAll(f"#bagWindow{bag_window}")[index]
        return len(box.children) == 0

    @classmethod
    def init(cls) -> None:
        """Set up event listeners based on the current inventory"""

        if cls._refresh_listener == False:
            MessageBus.subscribe("inventoryUpdated", lambda _: cls.init())
            cls._refresh_listener = True

        # remove old data
        if cls._draggables is not None:
            cls._draggables.destroy()

        if cls._dropzones is not None:
            cls._dropzones.destroy()

        # setup draggable elements
        cls._draggables = Draggable(document.querySelectorAll('.inventoryItem'))
        cls._draggables.on_drag_start(cls.handle_pickup)

        # set up acceptor slots
        cls._dropzones = Dropzone(document.querySelectorAll("#inventory .box"))
        cls._dropzones.on_drop(cls.handle_drop)

    @classmethod
    def handle_pickup(cls, event) -> None:
        """Runs when an item is picked up (dragstart)"""

        cls._orig_box = event.target.parentElement
        event.target.dataset.fromIndex = cls._orig_box.dataset.slotNum
        event.target.dataset.fromBagIndex = "-1"

        cls._move = {}

        if document.querySelector("#windowHolder").contains(cls._orig_box):
            cls._move["fromIndex"] = int(cls._orig_box.parentElement.parentElement.dataset.sourceBag)
            cls._move["fromBagIndex"] = int(cls._orig_box.dataset.slotNum)
            event.target.dataset.fromBagIndex = cls._orig_box.dataset.slotNum
            event.target.dataset.fromIndex = cls._orig_box.parentElement.parentElement.dataset.sourceBag
        else:
            cls._move["fromIndex"] = int(cls._orig_box.dataset.slotNum)

    @classmethod
    def handle_drop(cls, event) -> None:
        """Runs when an item is dropped (drop)"""

        from display.windows.bag_window import BagWindow

        # depends on which element got the drop event how far up we have to read to find the indices
        if document.querySelector("#windowHolder").contains(event.target):
            cls._move["toIndex"] = int(getattr(
                event.target.parentElement.parentElement.dataset, "sourceBag",
                getattr(event.target.parentElement.parentElement.parentElement.dataset, "sourceBag", None),
            ))
            cls._move["toBagIndex"] = int(getattr(event.target.dataset, "slotNum", getattr(event.target.parentElement.dataset, "slotNum", None)))
        else:
            cls._move["toIndex"] = int(getattr(event.target.dataset, "slotNum", getattr(event.target.parentElement.dataset, "slotNum", None)))

        if cls._move["toIndex"] is None:
            LOGGER.error(f"Failed to find the slotNum to move the item to {cls._move=}")
            window.console.log(event.target)

			# moving an item from one slot on the inventory bar to another shouldn't fail
			# so even with that big assumption, we should tell bag windows associated with
			# the old slot index that they are now to be associated with the new slot index
			# this way they can listen to metadata update messages based on index
            if cls._move.get("fromBagIndex") is None:
                BagWindow.update_source_slot(cls._move["fromIndex"], cls._move["toIndex"])

        if cls._move["fromIndex"] == cls._move["toIndex"] and (cls._move.get("fromBagIndex") is None or cls._move.get("toBagIndex") is None):
            # don't send the action to the server if the item was dropped in place
            return

        if cls._move["fromIndex"] == cls._move["toIndex"] and cls._move["fromBagIndex"] == cls._move["toBagIndex"]:
            # don't send the action to the server if the item was dropped in place
            return

        util.send_action("moveItem", "global_action_monster", cls._move)
