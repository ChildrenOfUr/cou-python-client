from datetime import timedelta
import math

from pyscript import document, window
from pyodide.ffi import create_proxy

from message_bus import MessageBus
from systems import util
from systems.global_gobbler import cou_globals
from systems.gps import GPS


class GpsIndicator:
    def __init__(self) -> None:
        self.container_e = document.querySelector("#gps-container")
        self.close_button_e = document.querySelector("#gps-cancel")
        self.next_street_e = document.querySelector("#gps-next")
        self.destination_e = document.querySelector("#gps-destination")
        self.arrow_e = document.querySelector("#gps-direction")
        self.loading_new = None
        self.arrive_fade = False
        self.next_sign_x = None
        self.next_sign_y = None

        self.close_button_e.addEventListener("click", create_proxy(lambda _: self.cancel()))

        @MessageBus.subscribe("streetLoaded")
        def on_street_load(_):
            if GPS.active and not GPS.on_path():
                GPS.get_route(cou_globals.current_street.label, GPS.destination_name())

    def update(self) -> None:
        if self.loading_new:
            self.container_e.hidden = True
            return

        if not GPS.active:
            return

        self.container_e.hidden = False
        self.next_street_e.innerText = GPS.next_street_name()
        self.destination_e.innerText = GPS.destination_name()

        if GPS.arrived():
            self.arrow_e.querySelector(".fa").classList.remove("fa-arrow-up")
            self.arrow_e.querySelector(".fa").classList.add("fa-check")
            self.arrow_e.style.transform = "rotate(0rad)"

            if self.arrive_fade or not cou_globals.current_street.loaded:
                return

            self.arrive_fade = True
            def timer_cancel():
                self.arrive_fade = False
                self.cancel()
            util.Timer(timedelta(seconds=3), timer_cancel)
        else:
            self.arrow_e.style.transform = f"rotate({self.calculate_arrow_direction()}rad)"

    def cancel(self):
        if GPS.active:
            GPS.current_route.clear()
            GPS.active = False
            MessageBus.publish("gpsCancel", None)
            self.container_e.hidden = True
            self.next_street_e.innerText = self.destination_e.innerText = None
            self.arrow_e.style.transform = ""
            window.localStorage.removeItem("gps_navigating")
            def timer_cancel():
                self.arrow_e.querySelector(".fa").classList.add("fa-arrow-up")
                self.arrow_e.querySelector(".fa").classList.remove("fa-check")
            util.Timer(timedelta(seconds=5), timer_cancel)

    def find_next_signpost(self) -> bool:
        for exit in cou_globals.minimap.current_street_exits:
            if GPS.next_street_name() in exit["streets"]:
                self.next_sign_x = exit["x"]
                self.next_sign_y = exit["y"] + 115  # center of the height

                if len(exit["streets"]) > 1:
                    # signs on both sides
                    self.next_sign_x += 100
                break
        return self.next_sign_x is not None and self.next_sign_y is not None

    def calculate_arrow_direction(self) -> float:
        player_x = cou_globals.current_player.left
        player_y = cou_globals.current_player.top + cou_globals.current_player.height / 2

        if not self.find_next_signpost():
            # error
            return 0

        dx = self.next_sign_x - player_x
        dy = self.next_sign_y - player_y
        theta = math.atan2(dy, dx) - 3 * math.pi / 2
        return theta
