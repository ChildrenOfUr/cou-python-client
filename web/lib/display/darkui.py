from pyscript import document

from message_bus import MessageBus
from systems.global_gobbler import cou_globals

##
# 	| Color        | Light   | Dark    |
# 	|--------------|---------|---------|
# 	| Background   | #eeeeee | #282828 |
# 	| Main Text    | #000000 | #ffffff |
# 	| Special Text | #4b2e4c | #eeeeee |
# 	| Alternate    | #444444 | #181818 |
##

class DarkUI:
    # the class name to apply or attribute to set when in dark mode
    DARK_CLASS = "darkui"

    @classmethod
    def applied_elements(cls):
        """returns all of the elements that should be styled"""
        return [
            document.body,
            document.querySelector("#meters #topLeftMask"),
            document.querySelector("#meters #playerName"),
            document.querySelector("#meters #imaginationText"),
            *document.querySelectorAll(".panel"),
            *document.querySelectorAll("article"),
            *document.querySelectorAll("button"),
        ]

    # whether to toggle automatically with day cycles
    auto_mode = False

    @classmethod
    def update(cls) -> None:
        """toggle if in auto mode"""
        if cls.auto_mode:
            cls.dark_mode = cou_globals.clock.isNight

    @classmethod
    def get_dark_mode(cls) -> bool:
        """Returns true if in dark mode, false if not"""
        return document.body.classList.contains(cls.DARK_CLASS)

    @classmethod
    def set_dark_mode(cls, new_mode: bool) -> None:
        """Sets mode based on true (dark) or false (light)"""

        if new_mode:
            cls._to_dark_mode()
        else:
            cls._to_light_mode()

        MessageBus.publish(cls.DARK_CLASS, new_mode)

    @classmethod
    def _to_dark_mode(cls) -> None:
        """Switches to dark mode"""
        for element in cls.applied_elements():
            element.classList.add(cls.DARK_CLASS)
            element.setAttribute(cls.DARK_CLASS, "true")

    @classmethod
    def _to_light_mode(cls) -> None:
        """Switches to light mode"""
        for element in cls.applied_elements():
            element.classList.remove(cls.DARK_CLASS)
            element.setAttribute(cls.DARK_CLASS, "false")
