import asyncio
from datetime import timedelta
from enum import Enum
import json
import logging

from pyscript import document, window
from pyodide.ffi import create_proxy

from display.chat_panel import Chat
from display.meters import Meters
from display.overlays.overlay import setup_overlays
from display.ui_templates.menu_keys import MenuKeys
from display.widgets.soundcloud import SoundCloudWidget
from display.widgets.volume_slider import VolumeSliderWidget
from message_bus import MessageBus
from network.auth import AuthManager
from systems import util
from systems.global_gobbler import cou_globals

LOGGER = logging.getLogger(__name__)


class UserInterface:
    ###################ELEMENTS#########################
    # you won! element
    youWon = document.querySelector("#youWon")

    # Initial loading screen elements
    loadStatus = document.querySelector("#loading #loadstatus")
    loadStatus2 = document.querySelector("#loading #loadstatus2")
    loadingScreen = document.querySelector('#loading')
    loadingSpinner = document.querySelector('#loading .fa-spin')

    # Time Meter Variables
    currDay = document.querySelector("#currDay")
    currTime = document.querySelector("#currTime")
    currDate = document.querySelector("#currDate")

    # Location and Map elements
    currLocation = document.querySelector("#currLocation")
    mapButton = document.querySelector("#mapButton")

    # Settings Glyph
    settingsButton = document.querySelector("#settingsGlyph")

    # Inventory Management
    inventorySearch = document.querySelector(('#inventorySearch'))
    inventory = document.querySelector(("#inventory"))

    # Pause button
    pauseButton = document.querySelector(('#thinkButton'))

    # settings window elements
    tabs = document.querySelectorAll('article .tab')

    # teleport window
    tpButton = document.querySelector(("#tpGlyph"))

    # bugreport button
    bugButton = document.querySelector(('#bugGlyph'))
    bugReportMeta = document.querySelector(('#bugWindow #reportMeta'))
    bugReportTitle = document.querySelector(('#bugWindow #reportTitle'))
    bugReportType = document.querySelector(('#bugWindow #reportCategory'))
    bugScreenshot = document.querySelector(('#bugWindow #reportScreenshot'))

    # main Element
    mainElement = document.querySelector(('main'))

    # world Element
    worldElement = document.querySelector(('#world'))
    playerHolder = document.querySelector(("#playerHolder"))
    layers = document.querySelector(("#layers"))

    # Location/Map Variables
    mapWindow = document.querySelector(('#mapWindow'))
    mapImg = document.querySelector(('#mapImage'))
    mapTitle = document.querySelector(('#mapTitle'))
    mapCanvas = document.querySelector(('#MapCanvas'))

    # Chat panel
    panel = document.querySelector(('#panel'))
    chatTemplate = document.querySelector(('#conversationTemplate'))

    conversationArchive = document.querySelector(("#conversationArchive"))

    # bugreport button
    logoutButton = document.querySelector(('#signoutGlyph'))
    ###################ELEMENTS#########################

    # Declare/Set initial variables here
    #####################VARS###########################
    location = "null"
    #####################VARS###########################

    # Object for manipulating meters
    meters = Meters()

    slider = VolumeSliderWidget()
    soundcloud = SoundCloudWidget()

    @classmethod
    def loggedIn(cls) -> None:
        cls.loadStatus2.innerText = "Preparing world..."

        @MessageBus.subscribe("streetLoaded")
        async def fade_loading_screen(_):
            cls.loadingScreen.style.opacity = "0"
            await asyncio.sleep(1)
            cls.loadingScreen.hidden = True

    @classmethod
    def loggedOut(cls) -> util.Timer:
        cls.loadStatus2.innerText = "Chatting with server..."
        cls.loadingScreen.hidden = False
        def fade_loading_screen():
            cls.loadingScreen.style.opacity = "1"
        return util.Timer(timedelta(seconds=1), fade_loading_screen)

    # start listening for events
    def __init__(self) -> None:
        cou_globals.view = self

        self.currDay.innerText = cou_globals.clock.dayofweek
        self.currTime.innerText = cou_globals.clock.time
        self.currDate.innerText = f"{cou_globals.clock.day} of {cou_globals.clock.month}"

        # Listens for the logout button
        async def auth_logout(_):
            await AuthManager.logout()
        self.logoutButton.addEventListener("click", create_proxy(auth_logout))

        # The "you won" splash
        def show_you_won(_):
            self.youWon.hidden = False
            MessageBus.publish("gameUnloading")
        window.addEventListener("beforeunload", create_proxy(show_you_won))

        @MessageBus.subscribe("timeUpdate")
        def on_time_update(_):
            self.currDay.innerText = cou_globals.clock.dayofweek
            self.currTime.innerText = cou_globals.clock.time
            self.currDate.innerText = f"{cou_globals.clock.day} of {cou_globals.clock.month}"

        @MessageBus.subscribe("doneLoading")
        def on_done_loading(_):
            # display "Play" buttons
            for button in self.loadingScreen.querySelectorAll(".button"):
                button.hidden = False

        # listen for resizing of the window so we can keep track of how large the
        # world element is so that we don't remeasure it often
        self.resize()
        window.addEventListener("resize", create_proxy(lambda _: self.resize()))

        setup_overlays()

        @MessageBus.subscribe("streetLoaded")
        def on_street_load(_):
            # update the max size of the game when a new street is loaded
            # add 140px vertical space for UI
            self.mainElement.style.maxHeight = f"{cou_globals.current_street.min_bounds_height + 140}px"
            # add 280px horizontal space for UI
            self.mainElement.style.maxWidth = f"{cou_globals.current_street.min_bounds_width + 280}px"
            self.resize()

        # track game focus
        self.worldElement.addEventListener("focus", create_proxy(lambda _: MessageBus.publish("worldFocus", True)))
        self.worldElement.addEventListener("blur", create_proxy(lambda _: MessageBus.publish("wordFocus", False)))

        @MessageBus.subscribe("gameLoaded")
        def on_game_load(_):
            # shift + click inventory items -> chat link
            for box in self.inventory.querySelectorAll(".box"):
                def on_click(event):
                    target = event.target

                    if not event.shiftKey or not target.classList.contains("inventoryItem") or Chat.last_focused_input is None:
                        return

                    item_type = json.loads(target.getAttribute("itemmap"))["itemType"]

                    if Chat.last_focused_input.value == "" or Chat.last_focused_input.value.endswith(" "):
                        Chat.last_focused_input.value += f"#{item_type}#"
                    else:
                        Chat.last_focused_input.value += f" #{item_type}#"

                    Chat.last_focused_input.focus()
                box.addEventListener("click", create_proxy(on_click))

            # inventory number keys
            MenuKeys.inv_slots_listener()

    def resize(self, event = None):
        self.worldElementWidth = self.worldElement.clientWidth
        self.worldElementHeight = self.worldElement.clientHeight
        MessageBus.publish("windowResized")


cou_globals.view = None


class TouchDirection(Enum):
        HORIZONTAL = 0
        VERTICAL = 1
        BOTH = 2


class TouchScroller:
    def __init__(self, scroll_div, direction) -> None:
        self._scrollDiv = scroll_div
        self._direction = direction
        self._scrollDiv.addEventListener("touchstart", create_proxy(self.on_touch_start))
        self._scrollDiv.addEventListener("touchmove", create_proxy(self.on_touch_move))

    def on_touch_start(self, event):
        event.stopPropagation()
        self._startX = event.changedTouches.first.client.x
        self._startY = event.changedTouches.first.client.y
        self._lastX = self._startX
        self._lastY = self._startY

    def on_touch_move(self, event):
        event.preventDefault()
        diffX = self._lastX - event.changedTouches.single.client.x
        diffY = self._lastY - event.changedTouches.single.client.y
        self._lastX = event.changedTouches.single.client.x
        self._lastY = event.changedTouches.single.client.y
        if self._direction == TouchDirection.HORIZONTAL or self._direction == TouchDirection.BOTH:
            self._scrollDiv.scrollLeft = self._scrollDiv.scrollLeft + diffX
        if self._direction == TouchDirection.VERTICAL or self._direction == TouchDirection.BOTH:
            self._scrollDiv.scrollTop = self._scrollDiv.scrollTop + diffY
