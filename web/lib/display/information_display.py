from pyscript import document
from pyodide.ffi import create_proxy

from systems.global_gobbler import cou_globals


class InformationDisplay:
    def __init__(self):
        self.display_element = None
        self.element_open = False
        self.ignore_shortcuts = False

    def setup_key_binding(self, key_name: str, open_callback: callable = None, close_callback: callable = None):
        def key_down(event):
            if cou_globals.input_manager is None or self.ignore_shortcuts:
                return

            primary = cou_globals.input_manager.keys[f"{key_name}BindingPrimary"]
            alt = cou_globals.input_manager.keys[f"{key_name}BindingAlt"]
            if (event.keyCode in (primary, alt)) and (self.element_open or not cou_globals.input_manager.ignoreKeys) and not event.shiftKey and not event.ctrlKey:
                if self.display_element.hidden:
                    self.open()
                    if open_callback:
                        open_callback()
                else:
                    self.close()
                    if close_callback:
                        close_callback()

        document.addEventListener("keydown", create_proxy(key_down))

    def setup_ui_button(self, ui_open_close_element, open_callback: callable = None, close_callback: callable = None):
        def on_click(event):
            if self.display_element.hidden:
                self.open()
                if open_callback:
                    open_callback()
            else:
                self.close()
                if close_callback:
                    close_callback()

        ui_open_close_element.addEventListener("click", create_proxy(on_click))

    def open(self):
        raise NotImplementedError

    def close(self):
        raise NotImplementedError
