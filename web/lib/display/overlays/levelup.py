from pyscript import document, window
from pyodide.ffi import create_proxy

from display.overlays.overlay import Overlay
from display.toast import Toast
from message_bus import MessageBus
from systems import util
from systems.global_gobbler import cou_globals

class LevelUpOverlay(Overlay):
    def __init__(self, id: str) -> None:
        super().__init__(id)

        self.dropper = document.querySelector("#lu-dropper")

    def open(self, new_level: int) -> None:
        def display(level: int) -> None:
            self.dropper.innerText = str(level)
            self.display_element.hidden = False
            MessageBus.publish("playSound", "levelUp")
            cou_globals.input_manager.ignoreKeys = True
            util.add_event_once(self.display_element.querySelector("#lu-button"), "click", lambda _: self.close())
            MessageBus.publish("worldFocus", False)

        if new_level is None:
            async def display_level():
                level = await cou_globals.metabolics.level()
                display(level)
            util.create_task(display_level())
        else:
            display(new_level)

    def close(self) -> None:
        # TODO, the notification actions are not correctly passed into JS land, may also need to happen from a web worker
        # async def notify_level_up():
        #     level = await cou_globals.metabolics.level()
        #     if level % 10 == 0:
        #         notification = window.Notification.new(
        #             "Level Up!", actions=[
        #                 {
        #                     "action": "profile",
        #                     "icon": Toast.notif_icon_url,
        #                     "title": "You've unlocked new username color options! Click here to visit your profile page, then log in to check them out.",
        #                 },
        #             ],
        #         )
        #         notification.addEventListener(
        #             "notificationclick",
        #             create_proxy(lambda _: window.open(f"https://childrenofur.com/players/{cou_globals.game.username}", "_blank")),
        #         )
        # util.create_task(notify_level_up())
        super().close()
