import asyncio
import logging
from typing import Any

from pyscript import document
from pyodide import http

from configs import Configs
from drawing import Rectangle
from systems.global_gobbler import cou_globals
from systems import util


LOGGER = logging.getLogger(__name__)


class Skills:
    data: dict[str, Any]

    @classmethod
    async def load_data(cls) -> None:
        response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/skills/get/{cou_globals.game.email}")
        cls.data = await response.json()

    @classmethod
    def get_skill(cls, id: str) -> dict[str, Any]:
        skills = [skill for skill in cls.data if skill["id"] == id]
        if skills:
            return skills[0]
        return None


class SkillIndicator:
    INSTANCES: list["SkillIndicator"] = []

    def __init__(self, skill_id: str) -> None:
        # remove old indicators (if any)
        for indicator in self.INSTANCES:
            indicator.close()

        self.parent = None

        def parse_data(_):
            skill = Skills.get_skill(skill_id)
            if skill is None:
                return

            # prepare
            height_percent = max(20, min((skill["player_points"] / skill["player_nextPoints"]) * 100, 100))
            fill = document.createElement("div")
            fill.classList.add("skillindicator-fill")
            fill.style.height = f"calc({height_percent}% - 10px)"
            fill.style.backgroundImage = f"url({skill['player_iconUrl']})"

            self.parent = document.createElement("div")
            self.parent.classList.add("skillindicator-parent")
            self.parent.appendChild(fill)

            cou_globals.current_player.super_parent_element.appendChild(self.parent)

            # position
            outline_rect = Rectangle.from_DOM(self.parent.getBoundingClientRect())
            outline_width = outline_rect.width
            outline_height = outline_rect.height
            player_x = cou_globals.current_player.translate_x
            player_y = cou_globals.current_player.translate_y
            x = player_x // 1 - outline_width // 2 - cou_globals.current_player.width // 3
            y = player_y // 1 + outline_height - 45

            self.parent.style.left = f"{x}px"
            self.parent.style.top = f"{y}px"

        task = util.create_task(Skills.load_data())
        task.add_done_callback(parse_data)

        self.INSTANCES.append(self)

    def close(self) -> None:
        if self.parent:
            self.parent.remove()
