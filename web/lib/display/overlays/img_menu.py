from datetime import timedelta
import math

from pyscript import document
from pyodide.ffi import create_proxy

from display.overlays.overlay import Overlay
from display.overlays.module_skills import Skills
from message_bus import MessageBus
from systems import util
from systems.global_gobbler import cou_globals


class ImgMenu(Overlay):
    update_frequency = timedelta(seconds=5)

    def __init__(self, id: str) -> None:
        super().__init__(id)

        self.last_skills_json = None

        # level indicator bar
        self.level_container = document.querySelector("#pm-level-container")
        self.level_bar = document.querySelector("#pm-level-bar")
        self.level_num = document.querySelector("#pm-level-num")
        self.next_level_e = document.querySelector("#pm-next-lvlnum")
        self.level_tooltip = document.querySelector("#pm-level-tooltip")
        self.img_to_next_e = document.querySelector("#pm-img-req")
        self.lifetime_img_e = document.querySelector("#pm-lt-img")

        # quoin limit meter
        self.quoin_container = document.querySelector("#pm-quoinlimit-container")
        self.quoin_bar = document.querySelector("#pm-quoinlimit-bar")
        self.quoin_percent_e = document.querySelector("#pm-quoinlimit-num")
        self.quoin_tooltip = document.querySelector("#pm-quoinlimit-tooltip")
        self.quoin_limit_e = document.querySelector("#pm-quoins-limit")
        self.quoins_collected_e = document.querySelector("#pm-quoins-collected")
        self.quoins_remaining_e = document.querySelector("#pm-quoins-remaining")

        # skill container
        self.skills_list = document.querySelector("#pm-skills-list")

        # exit button
        self.exit_button = document.querySelector("#pm-exit-button")

        self.setup_key_binding("ImgMenu")
        self.setup_ui_button(document.querySelector("#thinkButton"))

        def hover_opacity(element, opacity: str):
            element.style.opacity = opacity

        # show level tooltip on hover
        self.level_container.addEventListener("mouseenter", create_proxy(lambda _: hover_opacity(self.level_tooltip, "1")))
        self.level_container.addEventListener("mouseleave", create_proxy(lambda _: hover_opacity(self.level_tooltip, "0")))

        # show quoin tooltip on hover
        self.quoin_container.addEventListener("mouseenter", create_proxy(lambda _: hover_opacity(self.quoin_tooltip, "1")))
        self.quoin_container.addEventListener("mouseleave", create_proxy(lambda _: hover_opacity(self.quoin_tooltip, "0")))

        # repeatedly update
        @MessageBus.subscribe("metabolicsUpdated")
        async def on_metabolics(_):
            if self.element_open:
                await self.update()

    async def update(self) -> None:
        await self._setup_img_bar()
        await self._setup_quoin_limit_meter()
        await self._setup_skills_list()

    def open(self) -> None:
        async def do_open():
            # prepare contents
            await self.update()

            # update button states
            document.querySelector("#thinkButton").classList.add("pressed")

            # update outside UI
            cou_globals.minimap.container_e.hidden = True
            MessageBus.publish("worldFocus", False)

            # open
            super(type(self), self).open()
        util.create_task(do_open())

    def close(self) -> None:
        # update button states
        document.querySelector("#thinkButton").classList.remove("pressed")

        # close
        super().close()

        # update outside UI
        cou_globals.minimap.container_e.hidden = False

    async def _setup_img_bar(self) -> None:
        # refresh from server
        l_curr = await cou_globals.metabolics.level()

        if l_curr < 60:
            # calculate level/img stats
            l_img_curr = await cou_globals.metabolics.img_req_for_curr_lvl()
            l_img_next = await cou_globals.metabolics.img_req_for_next_lvl()

            img_toward = cou_globals.metabolics.lifetime_img - l_img_curr
            img_needed = l_img_next - cou_globals.metabolics.lifetime_img
            section = l_img_next - l_img_curr
            percent_of_next = (100 / section) * img_toward
            if percent_of_next < 25:
                percent_of_next = 25

            # display img bar
            self.level_bar.style.height = f"{percent_of_next}%"
            self.level_num.innerText = str(l_curr)
            self.img_to_next_e.innerText = f"{img_needed:,}"
            self.next_level_e.innerText = str(l_curr + 1)
            self.lifetime_img_e.innerText = f"{cou_globals.metabolics.lifetime_img:,}"
            self.level_bar.classList.remove("done")
            self.level_tooltip.querySelector(".pm-tt-top").hidden = False
            self.level_tooltip.classList.remove("done")
        else:
            self.level_bar.classList.add("done")
            self.level_num.innerText = "60"
            self.lifetime_img_e.innerText = f"{cou_globals.metabolics.lifetime_img:,}"
            self.level_tooltip.querySelector(".pm-tt-top").hidden = True
            self.level_tooltip.classList.add("done")

    async def _setup_quoin_limit_meter(self) -> None:
        quoins_collected = cou_globals.metabolics.player_metabolics.quoins_collected
        remaining = cou_globals.constants.quoin_limit - quoins_collected
        percent_collected = math.ceil((quoins_collected / cou_globals.constants.quoin_limit) * 100)

        self.quoins_collected_e.innerText = f"{quoins_collected} quoin{'' if quoins_collected == 1 else 's'}"
        self.quoin_limit_e.innerText = f"{cou_globals.constants.quoin_limit} quoin{'' if cou_globals.constants.quoin_limit == 1 else 's'}"
        self.quoins_remaining_e.innerText = f"{remaining} quoin{'' if remaining == 1 else 's'}"
        self.quoin_percent_e.innerText = str(percent_collected)
        self.quoin_bar.style.height = f"{max(25, min(100, percent_collected))}%"

    async def _setup_skills_list(self) -> None:
        # refresh from server
        new_json = await Skills.load_data()

        if self.last_skills_json is not None and self.last_skills_json == new_json:
            # don't re-render the skills element if the data hasn't changed
            return
        self.last_skills_json = new_json

        self.skills_list.replaceChildren()

        if not Skills.data:
            parent = document.createElement("div")
            parent.classList.add("pm-noskills")
            parent.innerText = "No skills :("
            self.skills_list.append(parent)
            return

        for skill in Skills.data:
            level_percent = max(0, min((skill["player_points"] / skill["player_nextPoints"]) * 100, 100))

            progress = document.createElement("div")
            progress.classList.add("pm-skill-progress")
            progress.style.width = f"calc({level_percent}% - 20px)"
            progress.style.backgroundImage = f"url({skill['player_iconUrl']})"

            level_text = str(skill["player_level"])
            if skill["player_level"] == 0:
                level_text = "Learning"
            elif skill["player_level"] == skill["num_levels"]:
                level_text = "Complete!"
                progress.classList.add("pm-skill-progress-complete")

            icon = document.createElement("img")
            icon.src = skill["player_iconUrl"]
            icon.classList.add("pm-skill-icon")

            skill_title = document.createElement("span")
            skill_title.classList.add("pm-skill-title")
            skill_title.innerText = skill["name"]

            skill_level = document.createElement("span")
            skill_level.classList.add("pm-skill-level")
            skill_level.innerText = level_text + f"{int(level_percent)}%" if level_percent != 100 else ""
            # ^ display percent of level if not complete ^

            text = document.createElement("div")
            text.append(skill_title, skill_title)

            parent = document.createElement("div")
            parent.classList.add("pm-skill")
            parent.dataset.skill = skill["id"]
            parent.title = skill["player_description"]
            parent.append(progress, icon, text)

            self.skills_list.append(parent)

