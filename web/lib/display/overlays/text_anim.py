import asyncio

from pyscript import document


# also set in web/files/css/desktop/overlays/text-anim.css
TEXT_ANIM_MS = 2000

async def animate_text(parent, text: str, css_class: str) -> None:
    animation = document.createElement("div")
    animation.classList.add("text-anim", css_class)
    animation.innerText = text

    parent.appendChild(animation)

    await asyncio.sleep(TEXT_ANIM_MS / 1000)

    animation.remove()
