from pyscript import document

from display.overlays.overlay import Overlay
from message_bus import MessageBus
from systems import util
from systems.global_gobbler import cou_globals


class AchievementOverlay(Overlay):
    def __init__(self, achv_data: dict) -> None:
        super().__init__("achv-template")
        self.id = f"achv-{achv_data['achv_id']}"

        self.overlay = document.querySelector("#achv-template").cloneNode(True)
        self.overlay.id = self.id
        self.overlay.querySelector(".achv-name").innerText = achv_data["achv_name"]
        self.overlay.querySelector(".achv-desc").innerText = achv_data["achv_description"]
        self.overlay.querySelector(".achv-icon").style.backgroundImage = f"url({achv_data['achv_imageUrl']})"

        document.querySelector("#achv-template").parentElement.appendChild(self.overlay)

        self.open()

    def open(self) -> None:
        self.overlay.hidden = False
        MessageBus.publish("playSound", "levelUp")
        cou_globals.input_manager.ignoreKeys = True
        util.add_event_once(self.overlay.querySelector(".closeButton"), "click", lambda _: self.close())
        MessageBus.publish("worldFocus", False)

    def close(self) -> None:
        super().close()
        self.overlay.remove()
