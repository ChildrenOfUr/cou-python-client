from datetime import timedelta

from pyscript import document
from pyodide import http

from configs import Configs
from display.overlays.overlay import Overlay
from systems.global_gobbler import cou_globals
from systems import util

import logging
LOGGER = logging.getLogger(__name__)


def toggle_ui(visible: bool) -> None:
    cou_globals.minimap.container_e.hidden = not visible
    cou_globals.view.inventorySearch.hidden = not visible


class StreetLoadingScreen(Overlay):
    def __init__(self, old_street: dict, new_street: dict) -> None:
        super().__init__("MapLoadingScreen", modal=True)

        self.old_street = old_street
        self.new_street = new_street
        self.split_screen = old_street is not None and old_street["hub_id"] != new_street["hub_id"]

        self.progress_bar = document.createElement("progress")
        self.progress_bar.max = 100

        self.progress_text = document.createElement("label")

        self.progress_container = document.createElement("div")
        self.progress_container.classList.add("street-load-progress", "progress")
        self.progress_container.appendChild(self.progress_bar)
        self.progress_container.appendChild(self.progress_text)

        self.open()

    def open(self) -> None:
        # hide overlapping interface
        toggle_ui(False)

        # Remove old elements
        self.display_element.replaceChildren()

        # Display "Leaving" side if changing hubs
        if self.split_screen:
            self.display_element.appendChild(self._create_section(self.old_street, leaving=True))

        # Display "Entering" side
        self.display_element.appendChild(self._create_section(self.new_street))

        # Display progress
        self.display_element.appendChild(self.progress_container)
        self.loading_percent = 0

        # Show overlay
        super().open()

    def close(self) -> None:
        # Unhide overlapping interface
        toggle_ui(True)

        # Hide overlay
        super().close()


    @property
    def loading_percent(self) -> int:
        return self._loading_percent

    @loading_percent.setter
    def loading_percent(self, percent: int) -> None:
        self._loading_percent = percent

        def _get_loading_message(p: int) -> str:
            match p // 10:
                case 0:
                    return f"Reticulating splines... {p}%"
                case 1:
                    return f"Snorting no-no powder... {p}%"
                case 2:
                    return f"Disignering grapamagicals... {p}%"
                case 3:
                    return f"Reassembling nibbled piggies... {p}%"
                case 4:
                    return f"Harvesting Giant Imagination... {p}%"
                case 5:
                    return f"Lighting aromatherapy candels for butterflies... {p}%"
                case 6:
                    return f"Choking chickens... {p}%"
                case 7:
                    return f"Tinkering tinker tools... {p}%"
                case 8:
                    return f"Refilling batterfly snarkiness levels... {p}%"
                case 9:
                    return f"Placing Urlings on Urth... {p}%"
                case 10:
                    return f"{p}% done!"

        # Set width & text of progress bar
        self.progress_bar.value = percent
        self.progress_text.innerText = _get_loading_message(percent)

        if percent >= 99:
            # Done loading
            # Hide after 3 seconds (to finish settling entities)
            util.Timer(timedelta(seconds=3), lambda: self.close())

    def _create_section(self, street: dict, leaving: bool = False):
        from network.map_data import map_data

        action_title = document.createElement("h2")
        action_title.innerText = f"Now {'leaving' if leaving else 'Entering'}"

        street_title = document.createElement("h1")
        street_title.innerText = street["label"]

        hub = map_data.hubData[street["hub_id"]]
        hub_title = document.createElement("h2")
        hub_title.innerText = f"in {hub['name']}"

        top_color = hub["color_top"]
        top_color = top_color if top_color.startswith("#") else f"#{top_color}"
        bottom_color = hub["color_btm"]
        bottom_color = bottom_color if bottom_color.startswith("#") else f"#{bottom_color}"

        section = document.createElement("div")
        section.classList.add("street-load-section")
        section.style.background = f"linear-gradient(to bottom, {top_color} 0%, {bottom_color} 100%)"
        section.appendChild(self._create_loading_image(street))
        section.appendChild(action_title)
        section.appendChild(street_title)
        section.appendChild(hub_title)

        if leaving:
            # This section is the "Leaving" side
            section.classList.add("street-load-section-leaving")
        else:
            if self.split_screen:
                section.classList.add("street-load-section-entering")

            # List entities on "Entering" street
            entities_list = document.createElement("p")
            entities_list.classList.add("entity-list")
            async def get_entities():
                entities = await self._list_entities(street)
                entities_list.innerHTML = entities
            util.create_task(get_entities())
            section.appendChild(entities_list)

        return section

    def _create_loading_image(self, street: dict):
        image = document.createElement("img")
        image.src = Configs.proxyStreetImage(street['loading_image']['url'])
        image.width = street["loading_image"]["w"]
        image.height = street["loading_image"]["h"]
        image.classList.add("street-load-image")
        return image

    async def _list_entities(self, street: dict) -> str:
        url = f"{Configs.http}//{Configs.utilServerAddress}/previewStreetEntities?tsid={street['tsid']}"
        response = await http.pyfetch(url)
        entity_list = await response.json()
        entity_string = ""

        if entity_list:
            entity_string += "<h3>Home to: </h3> "

        if len(entity_list) == 1:
            # Only one entity, just display it without commas
            only = next(iter(entity_list.items()))
            entity_string = f"{only[1]} {only[0]}"
        else:
            # Multiple entities, format with commas and a conjunction
            for index, (entity_type, count) in enumerate(entity_list.items()):
                if index == len(entity_list) - 1:
                    # Last entity
                    entity_string += f"and {count} {entity_type}"
                else:
                    entity_string += f"{count} {entity_type}, <wbr>"

        return entity_string
