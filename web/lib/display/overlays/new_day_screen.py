from datetime import timedelta

from pyodide.ffi import create_proxy

from display.overlays.overlay import Overlay
from message_bus import MessageBus
from systems.global_gobbler import cou_globals
from systems import util


class NewDayOverlay(Overlay):
    def __init__(self, id: str) -> None:
        super().__init__(id)
        MessageBus.subscribe("newDay", lambda _: self.open())
        MessageBus.subscribe("newDayFake", lambda _: self.open())

    def open(self) -> None:
        max_energy = str(cou_globals.metabolics.max_energy)
        self.display_element.querySelector("#newday-date").innerText = f"{cou_globals.clock.dayofweek}, the {cou_globals.clock.day} of {cou_globals.clock.month}"
        self.display_element.querySelector("#newday-refill-1").innerText = max_energy
        self.display_element.querySelector("#newday-refill-2").innerText = max_energy
        self.display_element.hidden = False

        def show_sun():
            self.display_element.querySelector("#newday-sun").classList.add("up")
            self.display_element.querySelector("#newday-refill-disc").classList.add("full")
        util.Timer(timedelta(milliseconds=100), show_sun)
        util.create_task(cou_globals.audio.play_sound("newday_rooster"))
        cou_globals.input_manager.ignoreKeys = True
        self.display_element.querySelector("#newday-button").addEventListener("click", create_proxy(lambda _: self.close()), {"once": True})
        MessageBus.publish("worldFocus", False)

    def close(self):
        self.display_element.hidden = True
        cou_globals.input_manager.ignoreKeys = False
        self.display_element.querySelector("#newday-sun").classList.remove("up")
        self.display_element.querySelector("#newday-refill-disc").classList.remove("full")


cou_globals.new_day = None
