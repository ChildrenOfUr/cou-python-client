from pyscript import document
from pyodide.ffi import create_proxy

from display.information_display import InformationDisplay
from message_bus import MessageBus
from systems.global_gobbler import cou_globals


class Overlay(InformationDisplay):
    def __init__(self, id: str, modal: bool = False) -> None:
        super().__init__()
        self.display_element = document.querySelector(f"#{id}")

        if not modal:
            # close button
            close_button = self.display_element.querySelector(".close")
            if close_button:
                close_button.addEventListener("click", create_proxy(lambda _: self.close()))

            # escape key
            def key_up(event):
                if not self.display_element.hidden and event.keyCode == 27:
                    self.close()
            document.addEventListener("keyup", create_proxy(key_up))

    def open(self):
        self.display_element.hidden = False
        self.element_open = True
        cou_globals.input_manager.ignoreKeys = True
        MessageBus.publish("worldFocus", False)

    def close(self):
        self.display_element.hidden = True
        self.element_open = False
        cou_globals.input_manager.ignoreKeys = False
        MessageBus.publish("worldFocus", True)


def setup_overlays():
    from display.overlays import new_day_screen
    from display.overlays.img_menu import ImgMenu
    from display.overlays.levelup import LevelUpOverlay

    cou_globals.new_day = new_day_screen.NewDayOverlay("newday")
    cou_globals.img_menu = ImgMenu("pauseMenu")
    cou_globals.level_up = LevelUpOverlay("levelup")
