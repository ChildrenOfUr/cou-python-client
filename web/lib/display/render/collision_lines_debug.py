from pyscript import document

from systems.global_gobbler import cou_globals


def show_line_canvas() -> None:
    hide_line_canvas()

    line_canvas = document.createElement("canvas")
    line_canvas.classList.add("streetcanvas")
    line_canvas.style.position = "absolute"
    line_canvas.style.width = f"{cou_globals.current_street.bounds.width}px"
    line_canvas.style.height = f"{cou_globals.current_street.bounds.height}px"
    line_canvas.width = cou_globals.current_street.bounds.width
    line_canvas.height = cou_globals.current_street.bounds.height
    line_canvas.setAttribute("ground_y", str(cou_globals.current_street.ground_y))
    line_canvas.id = "lineCanvas"

    cou_globals.view.layers.appendChild(line_canvas)
    cou_globals.current_street.scenery_canvases["lineCanvas"] = {
        "canvas": line_canvas, "width": line_canvas.width, "height": line_canvas.height,
        "ground_y": 0,
    }

    cou_globals.camera.dirty = True # force a recalculation of any offset

    cou_globals.minimap.show_camera_rect()
    show_player_rect(line_canvas)
    show_platforms(line_canvas)


def hide_line_canvas() -> None:
    cou_globals.current_street.scenery_canvases.pop("lineCanvas", None)
    line_canvas = document.querySelector("#lineCanvas")
    if line_canvas:
        line_canvas.remove()
    cou_globals.minimap.hide_camera_rect()


def show_platforms(line_canvas) -> None:
    context = line_canvas.getContext("2d")
    context.save()

    ground_y = float(line_canvas.getAttribute("ground_y"))

    # platforms
    context.lineWidth = 3
    context.strokeStyle = "#ffff00"
    context.beginPath()
    for platform in cou_globals.current_street.platforms:
        # if it's a ceiling, put the yellow on top, else put the red on top
        y_offset = (-1.5 if platform.ceiling else 1.5) - ground_y
        context.moveTo(platform.start.x, platform.start.y + y_offset)
        context.lineTo(platform.end.x, platform.end.y + y_offset)
    context.stroke()
    context.beginPath()
    context.strokeStyle = "#ff0000"
    for platform in cou_globals.current_street.platforms:
        # if it's a ceiling, put the red on bottom, else put the red on bottom
        y_offset = (1.5 if platform.ceiling else -1.5) - ground_y
        context.moveTo(platform.start.x, platform.start.y + y_offset)
        context.lineTo(platform.end.x, platform.end.y + y_offset)
    context.stroke()
    context.beginPath()
    context.strokeStyle = "#000000"
    for platform in cou_globals.current_street.platforms:
        context.moveTo(platform.end.x, platform.end.y + 5 - ground_y)
        context.lineTo(platform.end.x, platform.end.y - 5 - ground_y)
    context.stroke()

    # best platform
    if best_platform := cou_globals.current_player.best_platform:
        context.beginPath()
        context.strokeStyle = "#00ff00"
        context.moveTo(best_platform.start.x, best_platform.start.y + y_offset)
        context.lineTo(best_platform.end.x, best_platform.end.y + y_offset)
        context.stroke()

    # ladder landing platform
    if landing := cou_globals.current_player.ladder_landing:
        # line
        context.beginPath()
        context.strokeStyle = "#00ff00"
        context.moveTo(landing.start.x, landing.start.y + y_offset)
        context.lineTo(landing.end.x, landing.end.y + y_offset)
        context.stroke()

        # bounding box
        context.setLineDash([5, 15])
        context.strokeStyle = "#000000"
        context.rect(
            landing.start.x, landing.start.y + y_offset,
            landing.end.x - landing.start.x, landing.end.y - landing.start.y,
        )
        context.stroke()
        context.setLineDash([])

    # ladders
    context.beginPath()
    context.strokeStyle = "0000ff"
    for ladder in cou_globals.current_street.ladders:
        context.rect(ladder.bounds.left, ladder.bounds.top - ground_y, ladder.bounds.width, ladder.bounds.height)
    context.stroke()

    # walls
    context.beginPath()
    context.strokeStyle = "#00ff00"
    context.fillStyle = "#00ff00"
    for wall in cou_globals.current_street.walls:
        context.rect(wall.bounds.left, wall.bounds.top - ground_y, wall.bounds.width, wall.bounds.height)
    context.stroke()

    # display teleporters (wrong coords)
    # for wh in Wormhole.get_street():
    #     context.fillStyle = "rgba(255, 153, 0, 0.5)"
    #     context.fillRect(wh.left_x, wh.top_y, wh.width, wh.height)
    #     print(f"{wh.left_x} {wh.top_y} {wh.width} {wh.height}")

    context.restore()


def show_player_rect(line_canvas) -> None:
    context = line_canvas.getContext("2d")
    context.save()

    current_player = cou_globals.current_player
    ground_y = float(line_canvas.getAttribute("ground_y"))

    # box showing where we would hit walls
    context.beginPath()
    context.strokeStyle = "#ffffff"
    context.rect(
        current_player.collision_rect.left, current_player.collision_rect.top - ground_y,
        current_player.collision_rect.width, current_player.collision_rect.height,
    )
    context.stroke()

    # box showing how we would intersect with ladders
    context.beginPath()
    context.setLineDash([5, 15])
    context.strokeStyle = "#00ffaa"
    context.rect(
        current_player.ladder_rect.left, current_player.ladder_rect.top - ground_y,
        current_player.ladder_rect.width, current_player.ladder_rect.height,
    )
    context.stroke()
    context.setLineDash([])

    # box showing our raw top, left, width, height
    context.beginPath()
    context.strokeStyle = "#800080"
    context.rect(current_player.left, current_player.top, current_player.width, current_player.height)
    context.stroke()

    context.restore()
