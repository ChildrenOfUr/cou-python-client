from drawing import Rectangle


class Wall:
    def __init__(self, wall: dict, layer: dict, ground_y: int) -> None:
        self.width = wall["w"]
        self.height = wall["h"]
        self.x = wall["x"] + layer["w"] // 2 - self.width // 2
        self.y = wall["y"] + layer["h"] + ground_y
        self.id = wall["id"]
        self.bounds = Rectangle(self.x, self.y, self.width, self.height)

    def __str__(self) -> str:
        return f"wall {self.id}: {self.bounds}"
