from functools import total_ordering

from drawing import Point, Rectangle


@total_ordering
class Platform:
    def __init__(self, platform_line: dict, layer: dict, ground_y: int):
        self.id = platform_line["id"]
        self.ceiling = platform_line.get("platform_pc_perm") == 1

        for endpoint in platform_line["endpoints"]:
            assert isinstance(endpoint, dict)

            if endpoint["name"] == "start":
                self.start = Point(endpoint["x"], endpoint["y"] + ground_y)
                if layer["name"] == "middleground":
                    self.start = Point(endpoint["x"] + layer["w"] // 2, endpoint["y"] + layer["h"] + ground_y)
            if endpoint["name"] == "end":
                self.end = Point(endpoint["x"], endpoint["y"] + ground_y)
                if layer["name"] == "middleground":
                    self.end = Point(endpoint["x"] + layer["w"] // 2, endpoint["y"] + layer["h"] + ground_y)

        width = self.end.x - self.start.x
        height = self.end.y - self.start.y
        self.bounds = Rectangle(self.start.x, self.start.y, width, height)

    def __repr__(self) -> str:
        return f"({self.start.x},{self.start.y})->({self.end.x},{self.end.y}) ceiling={self.ceiling}"

    def __eq__(self, other: "Platform") -> bool:
        if other is None:
            return False
        return self.start.y == other.start.y

    def __lt__(self, other: "Platform") -> bool:
        if other is None:
            return False
        return self.start.y < other.start.y
