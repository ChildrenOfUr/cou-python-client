from drawing import Rectangle
from message_bus import MessageBus
from systems.global_gobbler import cou_globals


class Camera:
    def __init__(self):
        self.x = 0
        self.y = 0

        # for future eyeballery
        self.zoom = 0

        self.dirty = True
        self.visible_rect = None


        @MessageBus.subscribe("streetLoaded")
        def on_street_load(_):
            self._view = cou_globals.view
            self._street = cou_globals.current_street


    def update(self, dt) -> None:
        if not hasattr(self, "_player"):
            self._player = cou_globals.current_player
        cam_x = self.x
        cam_y = self.y

        if self._player.left > self._street.bounds.width - self._player.width / 2 - self._view.worldElementWidth / 2:
            cam_x = self._street.bounds.width - self._view.worldElementWidth
            # allow character to move to screen right
        elif self._player.left + self._player.width / 2 > self._view.worldElementWidth / 2:
            cam_x = self._player.left + self._player.width / 2 - self._view.worldElementWidth / 2
            # keep character in center of screen
        else:
            cam_x = 0

        if self._player.top + self._player.height / 2 < self._view.worldElementHeight / 2:
            cam_y = 0
        elif self._player.top < self._street.bounds.height - self._player.height / 2 - self._view.worldElementHeight / 2:
            y_distance_from_bottom = self._street.bounds.height - self._player.top - self._player.height / 2
            cam_y = self._street.bounds.height - (y_distance_from_bottom + self._view.worldElementHeight / 2)
        else:
            cam_y = self._street.bounds.height - self._view.worldElementHeight

        self.set_camera_position(cam_x, cam_y)

    def set_camera_position(self, new_x: int, new_y: int) -> None:
        if new_x != self.x or new_y != self.y:
            self.dirty = True
        self.x = new_x
        self.y = new_y

        if self.visible_rect is None:
            self.visible_rect = Rectangle(self.x, self.y, self._view.worldElementWidth, self._view.worldElementHeight)
        else:
            self.visible_rect.left = self.x
            self.visible_rect.top = self.y
            self.visible_rect.width = self._view.worldElementWidth
            self.visible_rect.height = self._view.worldElementHeight


cou_globals.camera = Camera()
