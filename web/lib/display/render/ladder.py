from drawing import Rectangle


class Ladder:
    def __init__(self, ladder: dict, layer: dict, ground_y: int):
        self.width = ladder["w"]
        self.height = ladder["h"]
        self.x = ladder["x"] + layer["w"] // 2 - self.width // 2
        self.y = ladder["y"] + layer["h"] - self.height + ground_y
        self.id = ladder["id"]

        self.bounds = Rectangle(self.x, self.y, self.width, self.height)
