import logging

from game.entities.wormhole import Wormhole
from network.server_interop.so_player import send_player_info
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)
cou_globals.time_last = 0.0
cou_globals.last_xy = ""


def update(dt: float):
    if cou_globals.server_down:
        return

    cou_globals.current_player.update(dt)
    cou_globals.camera.update(dt)

    for other_player in cou_globals.other_players.values():
        if other_player.current_animation:
            other_player.current_animation.update_source_rect(dt)

        x = other_player.left
        y = other_player.top
        transform = f"translateY({y}px) translateX({x}px) translateZ(0)"

        if not other_player.facing_right:
            transform += " scale3d(-1,1,1)"
            other_player.player_name.style.transform = "translateY(-100%) translateY(-34px) scale3d(-1,1,1)"

            if other_player.chat_bubble:
                other_player.chat_bubble.text_element.style.transform = "scale3d(-1,1,1)"
        else:
            transform += " scale3d(1,1,1)"
            other_player.player_name.style.transform = "translateY(-100%) translateY(-34px) scale3d(1,1,1)"

            if other_player.chat_bubble:
                other_player.chat_bubble.text_element.style.transform = "scale3d(1,1,1)"

        other_player.player_parent_element.style.transform = transform
        other_player.update(dt, partial=True)

    for quoin in cou_globals.quoins.values():
        quoin.update(dt)
    for entity in cou_globals.entities.values():
        entity.update(dt)

    # update the other clients with our position & street
    cou_globals.time_last += dt
    if cou_globals.time_last > .03 and getattr(cou_globals, "player_socket", None) and cou_globals.player_socket.readyState == 1:  # OPEN
        xy = f"{cou_globals.current_player.left},{cou_globals.current_player.top}"

        if xy == cou_globals.last_xy and cou_globals.time_last < 5:
            # Don't send updates when the player doesn't move - except once every 5 seconds
            return

        cou_globals.last_xy = xy
        send_player_info()
        cou_globals.time_last = 0.0

    Wormhole.update_all()
