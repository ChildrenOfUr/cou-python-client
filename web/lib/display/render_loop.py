from datetime import datetime
import logging

from display.render.collision_lines_debug import show_line_canvas
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)


cou_globals.show_collision_lines = False


def render():
    now = datetime.now()

    if cou_globals.server_down:
        return

    if cou_globals.show_collision_lines:
        show_line_canvas()

    # draw street
    if getattr(cou_globals, "current_street", None):
        cou_globals.current_street.render()

    # draw player
    if getattr(cou_globals, "current_player", None):
        cou_globals.current_player.render()

    # draw quoins
    for quoin in cou_globals.quoins.values():
        quoin.render()

    # draw entities
    for entity in cou_globals.entities.values():
        entity.render()

    # draw other players
    for player in cou_globals.other_players.values():
        player.render()

    # update minimap
    cou_globals.minimap.update_objects()

    # update gps
    cou_globals.gps_indicator.update()

    cou_globals.last_update = now
