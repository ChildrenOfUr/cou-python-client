import json
import random
import re
import urllib

from pyscript import document, window
from pyodide import http
from pyodide.ffi import create_proxy

from configs import Configs
from display.chat_message import EMOTICONS, ChatMessage, chat_toast_buffer, open_conversations, parse_location_links, parse_item_links
from display.toast import Toast
from display.windows.emoticon_picker import EmoticonPicker
from display.windows.item_window import ItemWindow
from display.windows.map_window import WorldMap
from game.chat_bubble import ChatBubble, VALIDATOR
from message_bus import MessageBus
from network.chat import NetChatManager
from systems import util
from systems.commands import parse_command
from systems.global_gobbler import cou_globals


class Chat:
    other_chat: "Chat" = None
    local_chat: "Chat" = None
    last_focused_input = None
    item_window_links: tuple = None
    map_window_links: tuple = None

    def __init__(self, title: str):
        self.title = title.strip()
        self.last_word = ""
        self.online = None
        self.focused = False
        self.tab_inserted = False
        self.conversation_element = None
        self.trigger = None
        self.scroll_toggle = None
        self.unread_messages = 0
        self.tab_search_index = 0
        self.input_history_pointer = 0
        self.emoticon_pointer = 0
        self.connected_users: list[str] = []
        self.input_history: list[str] = []
        self.resize_handle = None
        self.local_resize_focus = False  # whether the mouse is down on the resize button

        # find the link in the chat panel that opens the chat
        if self.title != "Local Chat":
            self.trigger = next(iter(e for e in document.querySelectorAll("#rightSide *") if getattr(e.dataset, "chat", None) == self.title))

        # look for an "archived" version of this chat
        # otherwise create a new one
        self.conversation_element = self.get_archived_conversation(self.title)
        if self.conversation_element is None:
            # clone the template
            self.conversation_element = cou_globals.view.chatTemplate.querySelector('.conversation').cloneNode(True)
            self.conversation_element.querySelector(".title").innerText = self.title
            def open_picker(event):
                input = self.conversation_element.querySelector("input")
                cou_globals.window_manager.emoticon_picker.open(title=self.title, input=input)
            self.conversation_element.querySelector(".insertemoji").addEventListener("click", create_proxy(open_picker))
            self.conversation_element.id = f"chat-{self.title}"
            open_conversations.insert(0, self)

            if self.title == "Local Chat":
                @MessageBus.subscribe("gameLoaded")
                @MessageBus.subscribe("streetLoaded")
                async def on_load(_):
                    # streets loaded, display a divider
                    await self.add_message("invalid_user", "LocationChangeEvent")
                    # If this is the first one, empty the toast buffer into the chat
                    if chat_toast_buffer:
                        for toast in chat_toast_buffer:
                            self.add_alert(toast)
                        chat_toast_buffer.clear()

            # handle chat input getting focused/unfocused so that the character doesn't move while typing
            def on_focus(event):
                if self.local_resize_focus:
                    # Don't drag to resize into text fields
                    event.preventDefault()
                    return

                cou_globals.input_manager.ignoreKeys = True
                # need to set the focused variable to true and false for all the others
                for chat in open_conversations:
                    chat.blur()
                self.focus()
                MessageBus.publish("worldFocus", False)
                type(self).last_focused_input = self.chat_input
            def on_blur(event):
                cou_globals.input_manager.ignoreKeys = False
                # we'll want to set the focused to false for this chat
                self.blur()
            self.chat_input.addEventListener("focus", create_proxy(on_focus))
            self.chat_input.addEventListener("blur", create_proxy(on_blur))
        else:
            # mark as read
            self.trigger.classList.remove("unread")
            NetChatManager.update_tab_unread()

        if title != "Local Chat":
            if self.local_chat is not None:
                cou_globals.view.panel.insertBefore(self.conversation_element, self.local_chat.conversation_element)
            else:
                cou_globals.view.panel.append(self.conversation_element)

            if self.other_chat is not None:
                self.other_chat.archive_conversation()
                self.focus()

            type(self).other_chat = self
        elif self.local_chat is None:
            # Don't ever have 2 local chats
            type(self).local_chat = self
            cou_globals.view.panel.append(self.conversation_element)
            # Can't remove the local chat
            self.conversation_element.querySelector(".fa-chevron-down").remove()

        if title == "Local Chat":
            # add resize control to the top of local chat
            self.resize_handle = document.createElement("i")
            self.resize_handle.classList.add("chat-resize", "fa-li", "fa", "fa-hand-grab-o")
            self.resize_handle.title = "Drag to resize"
            self.resize_handle.tabIndex = -1

            def on_mouse_down(_):
                self.local_resize_focus = True
                self.resize_handle.focus()

            def on_mouse_up(_):
                self.local_resize_focus = False
                self.resize_handle.blur()

            def on_mouse_move(event):
                if not self.local_resize_focus:
                    # mouse not down
                    return

                local = self.conversation_element
                other = self.other_chat.conversation_element

                if local.getBoundingClientRect().height <= 100 and event.movementY > 0:
                    # minimum size of local chat met, don't go smaller
                    return
                if other.getBoundingClientRect().height <= 100 and event.movementY < 0:
                    # minimum size of other chat me, don't go smaller
                    return

                local.style.height = f"{local.getBoundingClientRect().height - event.movementY}px"
                other.style.height = f"{other.getBoundingClientRect().height + event.movementY}px"

            self.resize_handle.addEventListener("mousedown", create_proxy(on_mouse_down))
            self.resize_handle.addEventListener("mouseup", create_proxy(on_mouse_up))
            self.resize_handle.addEventListener("mousemove", create_proxy(on_mouse_move))

            # clear custom sizing if the window size changes
            def on_resize(_):
                self.conversation_element.style.height = "50%"
                self.other_chat.conversation_element.style.height = "50%"
            window.addEventListener("resize", create_proxy(on_resize))

            header = self.conversation_element.querySelector("header")
            header.insertBefore(self.resize_handle, header.children[0])
        elif self.local_chat and self.local_chat.resize_handle:
            self.local_chat.resize_handle.hidden = False

        self.compute_panel_size()

        minimize = self.conversation_element.querySelector(".fa-chevron-down")
        if minimize:
            def on_click(_):
                self.local_chat.resize_handle.hidden = True
                self.archive_conversation()
            minimize.addEventListener("click", create_proxy(on_click))

        self.process_input(self.conversation_element.querySelector("input"))

        # toggle auto-scrolling
        self.scroll_toggle = self.conversation_element.querySelector(".chat-scroll-toggle")
        def on_click(_):
            self.scroll_toggle.classList.toggle("fa-toggle-on")
            self.scroll_toggle.classList.toggle("fa-toggle-off")
        self.scroll_toggle.addEventListener("click", create_proxy(on_click))

    @property
    def archived(self) -> bool:
        return not self.conversation_element.classList.contains("conversation")

    def focus(self) -> None:
        self.focused = True
        self.conversation_element.querySelector("input").focus()

    def blur(self) -> None:
        self.focused = False

    @property
    def chat_input(self):
        return self.conversation_element.querySelector("input")

    def process_event(self, data: dict) -> None:
        from network.server_interop import so_chat, so_player

        if data.get("message") == " joined.":
            if not data["username"] in self.connected_users:
                self.connected_users.append(data["username"])

        if data.get("message") == " left.":
            self.connected_users.remove(data["username"])
            so_player.remove_other_player(data["username"])

        if data.get("statusMessage") == "changeName":
            if data["success"] == "true":
                so_player.remove_other_player(data["username"])

                # although this message is broadcast to everyone, only change usernames
                # if we were the one to tyep /setname
                if data["newUsername"] == cou_globals.game.username:
                    cou_globals.current_player.id = data["newUsername"]
                    util.create_task(cou_globals.current_player.load_animations())

                    # clear our inventory so we can get the new one
                    for box in cou_globals.view.inventory.querySelectorAll(".box"):
                        box.replaceChildren()
                    cou_globals.first_connect = True
                    cou_globals.joined = ""
                    so_chat.send_joined_message(cou_globals.current_street.label)

                    # warn multiplayer server that it will receive messages
                    # from a new name but it should be the same person
                    data["street"] = cou_globals.current_street.label
                    cou_globals.player_socket.send(json.dumps(data))

                    cou_globals.time_last = 5.0

                self.connected_users.remove(data["username"])
                self.connected_users.append(data["newUsername"])
        else:
            util.create_task(self.add_message(data["username"], data["message"]))
            if self.archived:
                self.trigger.classList.add("unread")
                NetChatManager.update_tab_unread()

    async def add_message(self, player: str, message: str, override_username_link: str = None) -> None:
        chat = ChatMessage(player, message)
        dialog = self.conversation_element.querySelector(".dialog")

        # Toast for player change events
        if message == " joined." or message == " left.":
            if cou_globals.game.username != player:
                if player != cou_globals.game.username:
                    if message == " joined.":
                        Toast(f"{player} has arrived")
                    if message == " left.":
                        Toast(f"{player} left")
        else:
            # assemble chat message elements
            html = await chat.to_html(override_username_link=override_username_link)

            # parse styles, links, and emoji
            html = html.replace("&lt;", "<")
            html = html.replace("&gt;", ">")
            # urls already parsed in to_html
            html = EmoticonPicker.parse(html)
            html = parse_location_links(html)
            html = parse_item_links(html)

            # display in panel
            dialog.insertAdjacentHTML("beforeend", VALIDATOR.sanitize(html))

        # scroll to the bottom, if enabled
        if self.scroll_toggle.classList.contains("fa-toggle-on"):
            dialog.scrollTop = dialog.scrollHeight

        # check for and activate any item links
        if self.item_window_links is not None:
            for listener in self.item_window_links:
                listener[0].removeEventListener(*listener[1:])
        self.item_window_links = []
        for chat_link in dialog.querySelectorAll(".item-chat-link"):
            def on_click(event):
                event.preventDefault()
                if event.target.tagName.lower() == "a":
                    ItemWindow(event.target.innerText)
                if event.target.tagName.lower() == "span":
                    ItemWindow(event.target.parentElement.innerText)
            handler = create_proxy(on_click)
            chat_link.addEventListener("click", handler)
            self.item_window_links.append((chat_link, "click", handler))

        self.update_chat_location_links(dialog)

    def update_chat_location_links(self, dialog) -> None:
        # check for and activate any location links
        if self.map_window_links is not None:
            for listener in self.map_window_links:
                listener[0].removeEventListener(*listener[1:])
        self.map_window_links = []
        for map_link in dialog.querySelectorAll(".location-chat-link"):
            def on_click(event):
                event.preventDefault()
                text = event.target.innerText

                if event.target.classList.contains("hub-chat-link"):
                    hub_id = cou_globals.map_data.get_hub_id(text)
                    WorldMap(hub_id).load_hub_div(hub_id, text)
                elif event.target.classList.contains("street-chat-link"):
                    hub_id = cou_globals.map_data.streetData[text]["hub_id"]
                    WorldMap(hub_id).hub_map(hub_id=hub_id, highlight_street=text)

                cou_globals.map_window.open()
            handler = create_proxy(on_click)
            map_link.addEventListener("click", handler)
            self.map_window_links.append((map_link, "click", handler))

    def add_alert(self, alert):
        if not isinstance(alert, (str, Toast)):
            raise ValueError(f"Parameter alert must be a string or Toast, but it is of type {type(alert)}")

        classes = "system "

        def _add():
            rand_id = f"alert-{random.randint(0, 999) + 100}"
            text = f'<p class="{classes}" id="{rand_id}">{alert}</p>'
            dialog = self.conversation_element.querySelector(".dialog")
            dialog.insertAdjacentHTML("beforeend", VALIDATOR.sanitize(text))

            # scroll to the bottom
            dialog.scrollTop = dialog.scrollHeight

            self.update_chat_location_links(dialog)

            return dialog.querySelector(f"#{rand_id}")

        if isinstance(alert, Toast):
            classes += "chat-toast "

        new_message = _add()

        if isinstance(alert, Toast) and alert.click_handler is not None:
            new_message.addEventListener("click", create_proxy(lambda event: alert.click(event)))
            new_message.style.cursor = "pointer"

        return new_message

    def display_list(self, users: list[str]) -> None:
        alert = "Players in this channel:"
        for index in range(len(users)):
            users[index] = f'<a href="https://childrenofur.com/players/{urllib.parse.quote(users[index])}" target="_blank">{users[index]}</a>'
            alert = f"{alert} {users[index]}"

        text = f'<p class="system">{alert}</p>'

        dialog = self.conversation_element.querySelector(".dialog")
        dialog.insertAdjacentHTML("beforeend", VALIDATOR.sanitize(text))

        # scroll to the bottom
        dialog.scrollTop = dialog.scrollHeight

    def archive_conversation(self) -> None:
        """Archive the conversation (detach it from the chat panel) so that we may reattach
        it later with the history intact.
        """

        self.conversation_element.classList.add(f"archive-{self.title.replace(' ', '_')}")
        self.conversation_element.classList.remove("conversation")
        cou_globals.view.conversationArchive.appendChild(self.conversation_element)
        self.compute_panel_size()
        self.other_chat = None

    def get_archived_conversation(self, title: str):
        archive_class = f".archive-{self.title.replace(' ', '_')}"
        conversation_element = cou_globals.view.conversationArchive.querySelector(archive_class)
        if conversation_element is None:
            return None

        conversation_element.classList.remove(archive_class)
        conversation_element.classList.add("conversation")

        # move this conversation to the fron of the open_conversations list
        for chat in open_conversations:
            if chat.title == self.title:
                open_conversations.remove(chat)
                open_conversations.insert(0, chat)
                break

        return conversation_element

    def compute_panel_size(self) -> None:
        conversations = cou_globals.view.panel.querySelectorAll(".conversation")
        num = len(conversations) - 1
        for conversation in conversations:
            if conversation.hidden:
                num -= 1
        for conversation in conversations:
            conversation.style.height = f"{100 / num}%"

    def process_input(self, input) -> None:
        # keyup seems to be too late to prevent TAB's default behavior
        async def on_key_down(key):
            # pressed up arrow
            if key.keyCode == 38 and self.input_history_pointer < len(self.input_history):
                input.value = self.input_history[self.input_history_pointer]
                if self.input_history_pointer < len(self.input_history) - 1:
                    self.input_history_pointer += 1
            # pressed down arrow
            if key.keyCode == 40:
                if self.input_history_pointer > 0:
                    self.input_history_pointer -= 1
                    input.value = self.input_history[self.input_history_pointer]
                else:
                    input.value = ""
            # tab key, try to complete a user's name or an emoticon
            if input.value != "" and key.shiftKey and key.keyCode == 9:
                key.preventDefault()

                # look for an emoticon instead of a username
                if input.value.endswith(":"):
                    self.emoticon_complete(input, key)
                    return

                # let's suggest player to tab complete
                await self.tab_complete(input, key)
                return
        input.addEventListener("keydown", create_proxy(on_key_down))

        def on_key_up(key):
            if key.keyCode != 9:
                self.tab_inserted = False

            if key.keyCode != 13:
                # listen for entery key
                return

            if not input.value.strip():
                Toast("You can't send a blank message")
                return

            format_chars = r"<b>|</b>|<i>|</i>|<u>|</u>|<del>|</del>"
            if not re.sub(format_chars, "", input.value.strip()):
                Toast("You must have non-formatting content in your message")
                return

            self.parse_input(input.value)
            self.input_history.insert(0, input.value)
            self.input_history_pointer = 0
            # don't allow the list to grow forever
            self.input_history = self.input_history[:50]

            input.value = ""
        input.addEventListener("keyup", create_proxy(on_key_up))

    def emoticon_complete(self, input, event) -> None:
        # don't allow a key like tab to change to a different chat
        # if we don't get a hit and event=[tab], we will re-fire
        event.stopImmediatePropagation()

        value = input.value
        emoticon_inserted = False

        # if the input is exactly one long (therefore it is just a colon)
        if len(value) == 1:
            input.value = f":{EMOTICONS[self.emoticon_pointer]}:"
            self.emoticon_pointer += 1
            emoticon_inserted = True
        # if the input is more than 1 long and there's a space before the colon (word separation)
        elif value.endswith(" :"):
            last_colon = value.rfind(":")
            before_part = value[:last_colon]
            input.value = f"{before_part}:{EMOTICONS[self.emoticon_pointer]}:"
            self.emoticon_pointer += 1
            emoticon_inserted = True

        # if the input is more than 1 long and there is an emoticon that we should replace
        # to do this, we check if the value has 2 : and that the text between them
        # exactly matches an emoticon
        previous_colon = value[:-1].rfind(":")
        if previous_colon > -1:
            before_segment = value[:previous_colon]
            emoticon_segment = value[previous_colon: -1]
            for index, emoticon in enumerate(EMOTICONS):
                if index < len(EMOTICONS) - 1:
                    emoticon_next = EMOTICONS[index + 1]
                else:
                    emoticon_next = EMOTICONS[0]
                if f":{emoticon}:" in emoticon_segment:
                    input.value = f"{before_segment}:{emoticon_next}:"
                    self.emoticon_pointer += 1
                    emoticon_inserted = True
                    break

        # make sure we don't point past the end of the array
        if self.emoticon_pointer >= len(EMOTICONS):
            self.emoticon_pointer = 0

        # if we didn't manage to insert an emoticon and tab was pressed...
        # try to advance chat focus because we stifled it earlier
        if not emoticon_inserted and event.keyCode == 9:
            advance_chat_focus(event)

    async def tab_complete(self, input, event) -> None:
        # don't allow a key like tab to change to a different chat
        # if we don't get a hit and event=[tab], we will re-fire
        event.stopImmediatePropagation()

        channel = "Global Chat"
        inserted = False

        if self.title != channel:
            channel = cou_globals.current_street.label
        url = f"{Configs.http}//{Configs.utilServerAddress}/listUsers?channel={channel}"
        response = await http.pyfetch(url)
        connected_users = await response.json()

        start_index = 0 if input.value.rfind(" ") == -1 else input.value.rfind(" ") + 1
        local_last_word = input.value[start_index:]
        if not self.tab_inserted:
            last_word = input.value[start_index:]

        for username in connected_users:
            if username.lower().startswith(last_word.lower()) and username.lower() != local_last_word.lower():
                input.value = input.value[:input.value.rfind(" ") + 1] + username
                self.tab_inserted = True
                inserted = True
                self.tab_search_index += 1
                break

        # if we didn't find it yet and the tab_search_index was not 0, let's look at the beginning of the array as well
        # otherwise the user will have to press the tab key again
        if not self.tab_inserted:
            for index, username in enumerate(connected_users):
                if username.lower().startswith(last_word.lower()) and username.lower() != local_last_word.lower():
                    input.value = input.value[:input.value.rfind(" ") + 1] + username
                    self.tab_inserted = True
                    inserted = True
                    self.tab_search_index = index + 1
                    break

        if not inserted and event.keyCode == 9:
            advance_chat_focus(event)

        if self.tab_search_index == len(connected_users):
            # wrap around for next time
            self.tab_search_index = 0

    def parse_input(self, input: str) -> None:
        # if it's not a command, send it through
        if parse_command(input):
            return
        if input.lower() == "/list":
            map = {
                "username": cou_globals.game.username,
                "statusMessage": "list",
                "channel": self.title,
                "street": cou_globals.current_street.label,
            }
        else:
            map = {
                "username": cou_globals.game.username,
                "message": input,
                "channel": self.title,
            }
            if self.title == "Local Chat":
                map["street"] = cou_globals.current_street.label

        MessageBus.publish("outgoingChatEvent", map)

        # display chat bubble if we're talking in local (unless it's a /me message)
        if map["channel"] == "Local Chat" and not map["message"].lower().startswith("/me"):
            # remove any existing bubble
            if cou_globals.current_player.chat_bubble is not None and cou_globals.current_player.chat_bubble.bubble is not None:
                cou_globals.current_player.chat_bubble.bubble.remove()
            cou_globals.current_player.chat_bubble = ChatBubble(
                EmoticonPicker.parse(f" {map['message']} ").strip(), cou_globals.current_player,
                cou_globals.current_player.player_parent_element,
            )


# manage focus
def advance_chat_focus(event) -> bool:
    event.preventDefault()

    found = False
    for index, convo in enumerate(open_conversations):
        if convo.focused:
            if index < len(open_conversations) - 1:
                # unfocus the current
                convo.blur()

                # find the next non-archived conversation and focus it
                for j in range(index + 1, len(open_conversations)):
                    if not open_conversations[j].archived:
                        open_conversations[j].focus()
                        found = True

                if found:
                    break
            else:
                # last chat in list, focus game
                document.querySelector("#gameselector").focus()
                for i in range(len(open_conversations)):
                    open_conversations[i].blur()
                found = True

    if not found:
        # game is focused, focus first chat that is not archived
        for chat in open_conversations:
            if not chat.archived:
                chat.focus()
                break

    return True
