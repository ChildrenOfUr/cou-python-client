from datetime import timedelta
import time

from pyscript import document
from pyodide import http

from configs import Configs
from systems import util
from systems.global_gobbler import cou_globals

# 	String id, name, description;
# 	Duration remaining;
# 	int length;
# 	Element buffElement;
# 	Timer timer;

class Buff:
    _buff_container = document.querySelector("#buffHolder")
    _running: dict[str, "Buff"] = {}

    @classmethod
    def is_running(cls, id: str) -> bool:
        return id in cls._running

    @classmethod
    def remove_buff(cls, buff_id: str) -> bool:
        """Un-display a buff"""
        try:
            del cls._running[buff_id]
            return True
        except KeyError:
            return False

    @classmethod
    def extend_buff(cls, buff_id: str, seconds: int) -> None:
        """Add time to a buff"""

        # player does not have the buff
        if not cls.is_running(buff_id):
            return

        cls._running[buff_id].remaining += timedelta(seconds=seconds)

    @classmethod
    async def load_existing(cls) -> None:
        response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/buffs/get/{cou_globals.game.email}")
        for buff in await response.json():
            Buff.from_map(buff)

    @classmethod
    def from_map(cls, data: dict) -> "Buff":
        return Buff(
            data["id"], data["name"], data["description"], data["length"],
            timedelta(seconds=data["player_remaining"]),
        )

    def __init__(self, id: str, name: str, description: str, length: int, remaining: timedelta) -> None:
        self.id = id
        self.name = name
        self.description = description
        self.length = length
        self.remaining = remaining

        # whether it has already been removed
        self.exists = True

        self._running.update({self.id: self})
        self._display()
        self._animate()

    @property
    def indefinite(self) -> bool:
        return self.length == -1

    def _display(self) -> None:
        """Add to panel"""

        icon = document.createElement("img")
        icon.classList.add("buffIcon")
        icon.src = f"files/system/buffs/{self.id}.png"

        title = document.createElement("span")
        title.classList.add("title")
        title.innerText = self.name

        desc = document.createElement("span")
        desc.classList.add("desc")
        desc.innerText = self.description

        progress = document.createElement("div")
        progress.classList.add("buff-progress")
        progress.style.width = "calc(0% - 6px)"
        progress.id = f"buff-{self.id}-progress"

        self.buff_element = document.createElement("div")
        self.buff_element.classList.add("toast", "buff")
        self.buff_element.id = f"buff-{self.id}"
        self.buff_element.style.opacity = "0.5"
        self.buff_element.append(progress, icon, title, desc)

        # display in game
        self._buff_container.append(self.buff_element)

        # animate closing
        if not self.indefinite:
            def hide():
                if self.exists:
                    self._element_hide()
                    self.exists = False
            util.Timer(timedelta(milliseconds=(self.length * 1000) - 500), hide)

            def remove():
                if self.exists:
                    self._element_remove()
                    self.exists = False
            util.Timer(timedelta(seconds=self.length), remove)

    def _element_hide(self) -> None:
        self.buff_element.style.opacity = "0"

    def _element_remove(self) -> None:
        self.buff_element.remove()

    def _animate(self) -> None:
        """Start the decreasing progress bar"""

        if self.indefinite:
            return

        start_time = time.time()
        def decrease():
            elapsed = (self.length - self.remaining.total_seconds()) + (time.time() - start_time)
            if elapsed < self.length:
                width = 100 - ((100 / self.length) * elapsed)
                self.buff_element.querySelector(".buff-progress").style.width = f"calc({width}% - 6px)"
            else:
                self.remove()
        self.timer = util.Timer.periodic(timedelta(seconds=1), decrease)

    def remove(self) -> None:
        """Remove from panel and memory"""

        self._element_hide()
        self._element_remove()
        self.timer.cancel()
        self._running.pop(self.id, None)
