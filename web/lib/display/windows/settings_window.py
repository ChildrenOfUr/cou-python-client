from pyscript import document, window
from pyodide.ffi import create_proxy

from display.darkui import DarkUI
from display.toast import Toast
from display.windows.modal import Modal
from systems.global_gobbler import cou_globals
from systems.weather import WeatherIntensity


class SettingsWindow(Modal):
    def __init__(self):
        self.id = "settingsWindow"
        super().__init__()

        self._show_join_messages = False
        self._play_mention_sound = True
        self._log_npc_messages = True
        self.duplicate_keys_found = False

        self.prepare()

        async def on_fullscreen_check(event):
            try:
                if event.target.checked:
                    await document.querySelector("body").requestFullscreen()
                else:
                    await document.exitFullscreen()
            except Exception:
                pass
        document.querySelector("#Fullscreen").addEventListener("change", create_proxy(on_fullscreen_check))
        def on_fullscreen_change(event):
            if document.fullscreenElement is None:
                document.querySelector("#Fullscreen").checked = False
        document.addEventListener("fullscreenchange", create_proxy(on_fullscreen_change))

        # listen for onChange events so that clicking the label or the checkbox will call this method
        for checkbox in document.querySelectorAll(".ChatSettingsCheckbox"):
            def on_change(event):
                checkbox = event.target
                if checkbox.id == "ShowJoinMessages":
                    self.join_messages_visibility = checkbox.checked
                if checkbox.id == "PlayMentionSound":
                    self.play_mention_sound = checkbox.checked
                if checkbox.id == "LogNPCMessages":
                    self.log_npc_messages = checkbox.checked
            checkbox.addEventListener("change", create_proxy(on_change))

        # setup saved variables
        if show_join_messages := window.localStorage.getItem("showJoinMessages"):
            self.join_messages_visibility = show_join_messages == "true"
        else:
            window.localStorage.setItem("showJoinMessages", "false")
            self.join_messages_visibility = False

        document.querySelector("#ShowJoinMessages").checked = self.join_messages_visibility

        if play_mention_sound := window.localStorage.getItem("playMentionSound"):
            self.play_mention_sound = play_mention_sound == "true"
        else:
            window.localStorage.setItem("playMentionSound", "true")
            self.play_mention_sound = True

        document.querySelector("#PlayMentionSound").checked = self.play_mention_sound

        if log_npc_messages := window.localStorage.getItem("logNpcMessages"):
            self.log_npc_messages = log_npc_messages == "true"
        else:
            window.localStorage.setItem("logNpcMessages", "true")
            self.log_npc_messages = True

        document.querySelector("#LogNPCMessages").checked = self.log_npc_messages

        # set weather effects
        weather_effects = document.querySelector("#WeatherEffectsEnabled")
        if weather_setting := window.localStorage.getItem("WeatherEffectsEnabled"):
            if weather_setting == "true":
                weather_effects.checked = True
                document.querySelector("#weatherIntensity").classList.add("visible")
            else:
                weather_effects.checked = False
                document.querySelector("#weatherIntensity").classList.remove("visible")
        else:
            weather_effects.checked = True
            document.querySelector("#weatherIntensity").classList.add("visible")
        def weather_on_change(event):
            cou_globals.weather.set_enabled(weather_effects.checked)
            if weather_effects.checked:
                document.querySelector("#weatherIntensity").classList.add("visible")
            else:
                document.querySelector("#weatherIntensity").classList.remove("visible")
            window.localStorage.setItem("WeatherEffectsEnabled", str(weather_effects.checked).lower())
        weather_effects.addEventListener("change", create_proxy(weather_on_change))

        light_weather_effects = document.querySelector("#WeatherEffectsIntensity")
        if light_setting := window.localStorage.getItem("LightWeatherEffects"):
            light_weather_effects.checked = light_setting == "true"
        def light_weather_on_change(event):
            cou_globals.weather.set_intensity(WeatherIntensity.LIGHT if light_weather_effects.checked else WeatherIntensity.NORMAL)
            window.localStorage.setItem("LightWeatherEffects", str(light_weather_effects.checked).lower())
        light_weather_effects.addEventListener("change", create_proxy(light_weather_on_change))

        # set dark ui
        dark_ui = document.querySelector("#DarkMode")
        if dark_setting := window.localStorage.getItem("DarkMode"):
            dark_ui.checked = dark_setting == "true"
            DarkUI.set_dark_mode(dark_ui.checked)
        def dark_ui_on_change(event):
            window.localStorage.setItem("DarkMode", str(dark_ui.checked).lower())
            DarkUI.set_dark_mode(dark_ui.checked)
        dark_ui.addEventListener("change", create_proxy(dark_ui_on_change))

        # set dark ui auto mode
        dark_ui_auto = document.querySelector("#DarkModeAuto")
        if dark_auto_setting := window.localStorage.getItem("DarkModeAuto"):
            DarkUI.auto_mode = dark_ui_auto.checked = dark_auto_setting == "true"
            dark_ui.disabled = dark_ui_auto.checked
            DarkUI.update()
        def dark_ui_auto_on_change(event):
            window.localStorage.setItem("DarkModeAuto", str(dark_ui_auto.checked).lower())
            DarkUI.auto_mode = dark_ui.disabled = dark_ui_auto.checked
            if not dark_ui_auto.checked:
                DarkUI.set_dark_mode(dark_ui.checked)
            DarkUI.update()
        dark_ui_auto.addEventListener("change", create_proxy(dark_ui_auto_on_change))

        # setup volume controls
        music_slider = document.querySelector("#MusicSlider")
        effects_slider = document.querySelector("#EffectSlider")
        weather_slider = document.querySelector("#WeatherSlider")
        try:
            music_slider.value = int(float(window.localStorage.getItem("musicVolume")) * 100)
            effects_slider.value = int(float(window.localStorage.getItem("effectsVolume")) * 100)
            weather_slider.value = int(float(window.localStorage.getItem("weatherVolume")) * 100)
        except Exception:
            pass

        def music_on_change(event):
            volume = float(music_slider.value)
            cou_globals.audio.audio_channels["music"].set_gain(volume / 100)
            window.localStorage.setItem("musicVolume", str(volume / 100))
        music_slider.addEventListener("change", create_proxy(music_on_change))

        def effects_on_change(event):
            volume = float(effects_slider.value)
            cou_globals.audio.audio_channels["effects"].set_gain(volume / 100)
            window.localStorage.setItem("effectsVolume", str(volume / 100))
        effects_slider.addEventListener("change", create_proxy(effects_on_change))

        def weather_on_change(event):
            volume = float(weather_slider.value)
            cou_globals.audio.audio_channels["weather"].set_gain(volume / 100)
            window.localStorage.setItem("weatherVolume", str(volume / 100))
        weather_slider.addEventListener("change", create_proxy(weather_on_change))

        self.setup_ui_button(cou_globals.view.settingsButton)
        self.setup_key_binding("Settings")

    def open(self, ignore_keys: bool = True) -> None:
        self.check_duplicate_key_assignments()
        super().open(ignore_keys=ignore_keys)

    def close(self) -> None:
        self.check_duplicate_key_assignments()
        if self.duplicate_keys_found:
            Toast(
                "Preferences saved, but you have multiple controls bound to the same key,"
                " and this may cause problems! Click here to fix it.",
                on_click=lambda *args: self.open(),
            )
        else:
            Toast("Preferences saved")
        super().close()

    @property
    def join_messages_visibility(self) -> bool:
        return self._show_join_messages

    @join_messages_visibility.setter
    def join_messages_visibility(self, visible: bool) -> None:
        """Determines if messages like "<user> has joined" are shown to the player."""
        self._show_join_messages = visible
        window.localStorage.setItem("showJoinMessages", str(visible).lower())

    @property
    def play_mention_sound(self) -> bool:
        return self._play_mention_sound

    @play_mention_sound.setter
    def play_mention_sound(self, enabled: bool) -> None:
        if enabled:
            window.Notification.requestPermission()
        self._play_mention_sound = enabled
        window.localStorage.setItem("playMentionSound", str(enabled).lower())

    @property
    def log_npc_messages(self) -> bool:
        return self._log_npc_messages

    @log_npc_messages.setter
    def log_npc_messages(self, enabled: bool) -> None:
        self._log_npc_messages = enabled
        window.localStorage.setItem("logNpcMessages", str(enabled).lower())

    def check_duplicate_key_assignments(self) -> None:
        # <kbd> elements in the settings window
        elements = self.display_element.querySelectorAll("kbd")

        # returns number of other rows that have the specified key
        # (The same key can be assigned to both the primary and secondary controls and won't be counted)
        def _key_duplicated(key: str) -> bool:
            # controls bound to this key
            controls = [kbd for kbd in elements if kbd.innerText == key]
            if len(controls) <= 1:
                # not duplicated
                return False
            elif len(controls) == 2:
                # primary + secondary => not duplicated
                return controls[0].parentElement != controls[1].parentElement
            else:
                # duplicated
                return True

        # mark duplicates
        found = 0
        for kbd in elements:
            if _key_duplicated(kbd.innerText):
                # This key is assigned in a place other than the current control
                kbd.classList.add("duplicate-key")
                found += 1
            else:
                # This key is not assigned to any other controls
                kbd.classList.remove("duplicate-key")

        self.duplicate_keys_found = found > 0
