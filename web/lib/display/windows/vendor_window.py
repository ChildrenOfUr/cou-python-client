import json
import logging

from pyscript import document
from pyodide.ffi import create_proxy

from display.inventory import Dropzone
from display.toast import Toast
from display.overlays.text_anim import animate_text
from display.windows.modal import Modal
from systems import util
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)


class VendorWindow(Modal):
    _instance: "VendorWindow" = None
    max_listener: callable = None
    minus_listener: callable = None
    plus_listener: callable = None
    buy_listener: callable = None
    buy_num_listener: callable = None
    dropzone: Dropzone = None

    def __new__(cls) -> "VendorWindow":
        if not cls._instance:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self) -> None:
        self.id = "shopWindow"
        super().__init__()

        self.npc_id = ""
        self.prepare()

        self.header = self.display_element.querySelector("header")
        self.buy = self.display_element.querySelector("#buy")
        self.sell = self.display_element.querySelector("#sell")
        self.currants = self.display_element.querySelector(".qty")
        self.name = self.display_element.querySelector(".ItemName")

        self.back_to_buy = self.display_element.querySelector("#buy-qty .back")
        self.back_to_sell = self.display_element.querySelector("#sell-qty .back")

        self.buy_plus = self.display_element.querySelector("#buy-qty .plus")
        self.buy_minus = self.display_element.querySelector("#buy-qty .minus")
        self.buy_max = self.display_element.querySelector("#buy-qty .max")
        self.buy_button = self.display_element.querySelector("#buy-qty .BuyButton")
        self.buy_num = self.display_element.querySelector("#buy-qty .NumToBuy")
        self.buy_item_count = self.display_element.querySelector("#buy-qty .ItemNum")
        self.buy_item_image = self.display_element.querySelector("#buy-qty .ItemImage")
        self.buy_description = self.display_element.querySelector("#buy-qty .Description")
        self.buy_stacks_to = self.display_element.querySelector("#buy-qty .StackNum")
        self.buy_price_tag = self.display_element.querySelector("#buy-qty .ItemPrice")
        self.amt_selector = self.display_element.querySelector(".QuantityParent")

    def close(self) -> None:
        util.send_action("close", self.npc_id, {})
        super().close()

    def call(self, vendor_map: dict, sell_mode: bool = False) -> None:
        self.npc_id = vendor_map["id"]
        window_title = vendor_map["vendorName"]
        if "Street Spirit:" in window_title:
            window_title = f"{window_title[15:]} Vendor"
        self.header.innerHTML = f'<i class="fa-li fa fa-shopping-cart"></i>{window_title}'
        self.currants.innerText = f" {cou_globals.metabolics.currants:,} currant{'s' if cou_globals.metabolics.currants != 1 else ''}"

        self.buy.replaceChildren()

        for item in vendor_map["itemsForSale"]:
            merch = document.createElement("div")
            merch.className = "box"
            merch.title = item["name"]
            self.buy.appendChild(merch)
            merch_icon = document.createElement("img")
            merch_icon.src = item["iconUrl"]
            merch_icon.className = "icon"
            merch.appendChild(merch_icon)

            price = document.createElement("div")
            price.className = "price-tag"
            if item["discount"] != 1:
                # Item on sale, see price by clicking
                price.innerText = "Sale!"
                price.classList.add("sale-price-tag")
            elif item["price"] >= 9999:
                # really expensive item
                price.innerText = "A Lot"
            else:
                # normal item
                price.innerText = f"{item['price']}₡"
            merch.appendChild(price)

            if item["price"] * item["discount"] > cou_globals.metabolics.currants:
                price.classList.add("cantAfford")

            merch.addEventListener("click", create_proxy(lambda _, item=item: self.spawn_buy_details(item, vendor_map["id"])))

        drop_target = document.querySelector("#SellDropTarget")
        if self.dropzone:
            self.dropzone.destroy()
        type(self).dropzone = Dropzone([drop_target])
        def on_drop(event):
            # verify it is a valid item before acting on it
            data = event.dataTransfer.getData("itemdef")
            if data is None:
                return

            self.spawn_buy_details(json.loads(data), vendor_map["id"], sell_mode=True)
        self.dropzone.on_drop(on_drop)

        if sell_mode:
            self.display_element.querySelector("#SellTab").click()
        else:
            self.display_element.querySelector("#BuyTab").click()
        self.open(ignore_keys=True)

    def spawn_buy_details(self, item: dict, vendor_id: str, sell_mode: bool = False):
        # check for non-empty bags
        if item.get("isContainer", False) and item.get("metadata", {}).get("slots"):
            for slot in json.loads(item["metadata"]["slots"]):
                if slot["itemType"].strip() or slot.get("item") or slot["count"] > 0:
                    Toast("Empty this and try again!")
                    return

        # cancel the previous button listeners (if applicable)
        if self.max_listener:
            self.max_listener.cancel()
        if self.minus_listener:
            self.minus_listener.cancel()
        if self.plus_listener:
            self.plus_listener.cancel()
        if self.buy_listener:
            self.buy_listener.cancel()
        if self.buy_num_listener:
            self.buy_num_listener.cancel()

        # toggle the tabs
        self.buy.hidden = True
        self.display_element.querySelector("#buy-qty").hidden = False

        self.buy_stacks_to.innerText = str(item["stacksTo"])

        # update the buy meter
        num_to_buy = 1
        self._update_num_to_buy(item, num_to_buy, sell_mode=sell_mode)

        # update the image and numbers
        if sell_mode:
            self.amt_selector.style.opacity = "initial"
            self.amt_selector.style.pointerEvents = "initial"
            self.buy_button.style.opacity = "initial"
            self.buy_button.style.pointerEvents = "initial"
            self.buy_button.innerText = f"Sell 1 for {int(int(item['price']) * .7)}₡"
        else:
            if util.Util().get_blank_slots(item) == 0:
                self.amt_selector.style.opacity = "0.5"
                self.amt_selector.style.pointerEvents = "none"
                self.buy_button.style.opacity = "0.5"
                self.buy_button.style.pointerEvents = "none"
                self.buy_button.innerText = f"No inventory space"
            else:
                self.amt_selector.style.opacity = "initial"
                self.amt_selector.style.pointerEvents = "initial"
                self.buy_button.style.opacity = "initial"
                self.buy_button.style.pointerEvents = "initial"
                if item["discount"] == 1:
                    self.buy_button.innerText = f"Buy 1 for {item['price']}₡"
                else:
                    self.buy_button.innerText = f"On sale! Buy 1 for {int(int(item['price']) * float(item['discount']))}₡"

        self.buy_item_count.innerText = str(util.Util().get_num_items(item["itemType"]))
        self.buy_item_image.src = item["iconUrl"]
        self.buy_description.innerText = item["description"]
        self.buy_price_tag.innerText = f"{item['price']}₡"
        self.name.innerText = item["name"]

        # set up button listeners
        def on_buy_num_input(_):
            nonlocal num_to_buy
            try:
                new_num = int(self.buy_num.value)
                num_to_buy = self._update_num_to_buy(item, new_num, sell_mode=sell_mode)
            except Exception:
                pass
        type(self).buy_num_listener = util.add_cancelable_event_listener(self.buy_num, "input", on_buy_num_input)

        # sell/buy button
        def on_buy_click(_):
            nonlocal num_to_buy
            action_map = {"itemType": item["itemType"], "num": num_to_buy}
            if sell_mode:
                if num_to_buy > util.Util().get_num_items(item["itemType"]):
                    return
                new_value = cou_globals.metabolics.currants + int(item["price"] * num_to_buy * .7)
                util.send_action("sellItem", vendor_id, action_map)
                diff_sign = "+"
            else:
                if cou_globals.metabolics.currants < item["discount"] * item["price"] * num_to_buy:
                    return
                new_value = cou_globals.metabolics.currants - int(item["price"] * item["discount"] * num_to_buy)
                util.send_action("buyItem", vendor_id, action_map)
                diff_sign = "-"

            self.currants.innerText = f" {new_value:,} currant{'s' if new_value != 1 else ''}"

            # animate currant change at bottom
            currant_diff = abs(cou_globals.metabolics.currants - new_value)
            diff_str = f"{diff_sign}{currant_diff}"
            util.create_task(animate_text(self.currants.parentElement, diff_str, "currant-vendor-anim"))

            self.back_to_buy.click()

        self.buy_listener = util.add_event_once(self.buy_button, "click", on_buy_click)

        # plus button
        def on_buy_plus_click(_):
            nonlocal num_to_buy
            try:
                if sell_mode:
                    # selling an item
                    if int(self.buy_num.value) + 1 <= util.Util().get_num_items(item["itemType"]):
                        # we have enough to sell
                        self.buy_num.value = int(self.buy_num.value) + 1
                        new_num = int(self.buy_num.value)
                        num_to_buy = self._update_num_to_buy(item, new_num, sell_mode=sell_mode)
                else:
                    # buying an item
                    if int(self.buy_num.value) + 1 <= self._max_additions(item):
                        # you can fit the max number of items in your inventory
                        self.buy_num.value = int(self.buy_num.value) + 1
                        new_num = int(self.buy_num.value)
                        num_to_buy = self._update_num_to_buy(item, new_num, sell_mode=sell_mode)
            except Exception:
                LOGGER.exception("[Vendor] Plus Button Error")
        type(self).plus_listener = util.add_cancelable_event_listener(self.buy_plus, "click", on_buy_plus_click)

        def on_buy_minus_click(_):
            nonlocal num_to_buy
            try:
                if int(self.buy_num.value) > 1:
                    # we can't go to 0 or negative
                    self.buy_num.value = int(self.buy_num.value) - 1
                    new_num = int(self.buy_num.value)
                    num_to_buy = self._update_num_to_buy(item, new_num, sell_mode=sell_mode)
            except Exception:
                LOGGER.exception("[Vendor] Minus Button Error")
        type(self).minus_listener = util.add_cancelable_event_listener(self.buy_minus, "click", on_buy_minus_click)

        def on_buy_max_click(_):
            nonlocal num_to_buy
            try:
                if sell_mode:
                    new_num = util.Util().get_num_items(item["itemType"])
                else:
                    new_num = min(self._max_additions(item), int(cou_globals.metabolics.currants / item['price']))
                num_to_buy = self._update_num_to_buy(item, new_num, sell_mode=sell_mode)
            except Exception:
                LOGGER.exception("[Vendor] Max Button Error")
        type(self).max_listener = util.add_cancelable_event_listener(self.buy_max, "click", on_buy_max_click)

        def on_back_to_buy_click(_):
            self.display_element.querySelector("#buy-qty").hidden = True
            self.sell.hidden = not sell_mode
            self.buy.hidden = sell_mode
        util.add_event_once(self.back_to_buy, "click", on_back_to_buy_click)

    def _max_additions(self, item: dict) -> int:
        count = util.Util().get_blank_slots(item) * item["stacksTo"]
        count += util.Util().get_stack_remainders(item["itemType"])
        return count

    def _update_num_to_buy(self, item: dict, new_num: int, sell_mode: bool = False) -> int:
        if new_num < 1:
            new_num = 1

        if sell_mode:
            new_num = min(new_num, util.Util().get_num_items(item["itemType"]))

        self.buy_num.value = new_num
        value = int(item["price"]) * new_num
        if sell_mode:
            value = int(value * .7)

        if sell_mode:
            self.buy_button.innerText = f"Sell {new_num} for {value}₡"
        else:
            self.buy_button.innerText = f"Buy {new_num} for {value}₡"

        return int(self.buy_num.value)
