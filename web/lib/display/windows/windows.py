import random

from display.windows.modal import Modal
from message_bus import MessageBus
from systems.global_gobbler import cou_globals


class WindowManager:
    def __init__(self) -> None:
        from display.windows.achievements_window import AchievementsWindow
        from display.windows.emoticon_picker import EmoticonPicker
        from display.windows.map_window import MapWindow
        from display.windows.mailbox_window import MailboxWindow
        from display.windows.questlog_window import QuestLogWindow
        from display.windows.rock_window import RockWindow
        from display.windows.settings_window import SettingsWindow
        from display.windows.vendor_window import VendorWindow
        from display.windows.weather_window import WeatherWindow

        def on_game_load(_):
            # needs to have game.username defined first
            # self.questMaker = QuestMakerWindow()
            pass
        MessageBus.subscribe("gameLoaded", on_game_load)

        # defining all the possible popup windows
        self.achievements = AchievementsWindow()
        # self.add_friend_window = AddFriendWindow()
        # self.avatar_window = AvatarWindow()
        # self.bugs = BugWindow()
        # self.calendar_window = CalendarWindow()
        # self.change_username_window = ChangeUsernameWindow()
        self.emoticon_picker = EmoticonPicker()
        # self.inventory_search_window = InventorySearchWindow()
        self.mailbox_window = MailboxWindow()
        self.map_window = MapWindow()
        # self.motd_window = MotdWindow()
        self.quest_log = QuestLogWindow()
        self.rock_window = RockWindow()
        self.settings = SettingsWindow()
        self.weather = WeatherWindow()
        self.vendor = VendorWindow()

    @classmethod
    def random_id(cls) -> int:
        return random.randint(1, 9999999)


cou_globals.modals: dict[str, Modal] = {}


class AuctionWindow(Modal):
    id = "auctionWindow"

    def __init__(self):
        super().__init__()
        self.prepare()
