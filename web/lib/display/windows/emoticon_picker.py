import re

from pyscript import document
from pyodide import http
from pyodide.ffi import create_proxy

from display.windows.modal import Modal
from systems import util


class EmoticonPicker(Modal):
    emoticon_url = "./files/emoticons"
    emoticon_type = "png"
    emoticon_regex = re.compile(r":([a-z0-9_]*):", flags=re.IGNORECASE)
    emoji: dict[str, list[str]] = {}
    emoji_names: list[str] = []

    def __init__(self):
        self.id = "emoticonPicker"
        super().__init__()

        self.window = document.querySelector(f"#{self.id}")
        self.well = self.window.querySelector("#emoticonPicker .well")
        self.search = self.well.querySelector("#ep-search")
        self.grid = self.well.querySelector("#ep-grid")
        self.target = None

        self.prepare()

        async def load_emoji():
            response = await http.pyfetch(f"{self.emoticon_url}/emoticons.json")
            type(self).emoji = await response.json()
            type(self).emoji_names = []
            for names in self.emoji.values():
                self.emoji_names.extend(names)
        util.create_task(load_emoji())

        self.search.addEventListener("input", create_proxy(lambda _: self.display_emoticons(self.search.value)))

    def display_emoticons(self, search: str = None) -> None:
        self.grid.replaceChildren()

        for category, emoticons in self.emoji.items():
            category_title = document.createElement("h2")
            category_title.innerText = category

            self.grid.appendChild(category_title)

            for emoticon in emoticons:
                if search is not None and search not in emoticon:
                    continue

                emoticon_image = document.createElement("img")
                emoticon_image.src = f"{self.emoticon_url}/{self.emoticon_type}/{emoticon}.{self.emoticon_type}"
                emoticon_image.classList.add("emoticon")
                emoticon_image.draggable = True

                emoticon_button = document.createElement("span")
                emoticon_button.classList.add("ep-emoticonButton")
                emoticon_button.title = emoticon
                emoticon_button.dataset.emoticonName = emoticon
                emoticon_button.appendChild(emoticon_image)

                self.grid.appendChild(emoticon_button)

                def on_drag_start(event, emoticon=emoticon, emoticon_image=emoticon_image):
                    event.stopPropagation()
                    event.dataTransfer.effectAllowed = "copy"
                    event.dataTransfer.setData("text/plain", f":{emoticon}:")
                    event.dataTransfer.setDragImage(emoticon_image, emoticon_image.getBoundingClientRect().width // 2, -10)
                emoticon_button.addEventListener("dragstart", create_proxy(on_drag_start))

                def on_click(_, emoticon=emoticon):
                    ends_with_space = self.target.value == "" or self.target.value[:-1] == " "
                    self.target.value += f"{'' if ends_with_space else ' '}:{emoticon}:"
                emoticon_button.addEventListener("click", create_proxy(on_click))

    def open(self, ignore_keys: bool = False, title: str = None, input = None) -> None:
        self.target = input

        # update window text
        if "chat" in title.lower():
            document.querySelector("#ep-channelname").innerText = title
        else:
            document.querySelector("#ep-channelname").innerText = f"chat with {title}"

        self.display_emoticons()
        self.display_element.hidden = False
        self.search.focus()
        self.element_open = True

    @classmethod
    def parse(cls, text: str) -> str:
        for match in re.findall(cls.emoticon_regex, text):
            if match in cls.emoji_names:
                text = text.replace(f":{match}:", f'<img class="emoticon" title="{match}" src="{cls.emoticon_url}/{cls.emoticon_type}/{match}.{cls.emoticon_type}">')
        return text
