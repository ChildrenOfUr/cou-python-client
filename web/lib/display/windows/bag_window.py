import asyncio
import json
import logging

from pyscript import document

from display.inventory import BagFilterAcceptor, Draggable, Dropzone, InvDragging
from display.windows.windows import Modal, WindowManager
from message_bus import MessageBus
from network.server_interop.itemdef import ItemDef
from systems.global_gobbler import cou_globals
from systems import util


LOGGER = logging.getLogger(__name__)


class BagWindow(Modal):
    bag_windows: dict[str, "BagWindow"] = {}
    open_windows: dict[str, "BagWindow"] = {}

    @classmethod
    def is_open(cls) -> bool:
        return len(document.querySelectorAll("#windowHolder > .bagWindow")) > 0

    @classmethod
    def update_source_slot(cls, old_slot_index: int, new_slot_index: int) -> None:
        for window in cls.bag_windows.values():
            if window.source_slot_num == old_slot_index:
                window.source_slot_num = new_slot_index
                window.update_well(window.source_item)
                break

    def __init__(self, source_slot_num: int, source_item: ItemDef, open_window: bool = True):
        super().__init__()

        self.creating = True
        self.id = f"bagWindow{WindowManager.random_id()}"
        self.acceptors: Dropzone = None
        self.draggable: Draggable = None
        self.bag_windows[self.id] = self
        self.num_slots: int = None
        self.source_slot_num = source_slot_num
        self.source_item = source_item

        # when set to true, the ui inside the bag will be updated when the bag is next opened
        self.updated = False

        # load the ui of the window and open it when ready
        self.load_update = asyncio.Future()
        def after_load(result: asyncio.Future):
            self.display_element = result.result()
            # handle drag and drop
            @MessageBus.subscribe("metadataUpdated")
            @MessageBus.subscribe("inventoryUpdated")
            def on_inventory_update(_):
                if self.acceptors is not None:
                    self.acceptors.destroy()
                if self.draggable is not None:
                    self.draggable.destroy()

                self.acceptors = Dropzone(
                    self.display_element.querySelectorAll(".bagwindow-box"),
                    BagFilterAcceptor(source_item.sub_slot_filter),
                )
                self.acceptors.on_drop(InvDragging.handle_drop)
                self.draggable = Draggable(self.display_element.querySelectorAll(".box > .inventoryItem"))
                self.draggable.on_drag_start(InvDragging.handle_pickup)

            @MessageBus.subscribe("updateMetadata")
            async def on_metadata_update(index_to_item: dict):
                index = index_to_item["index"]
                if index != source_slot_num:
                    return
                self.source_item = index_to_item["item"]
                if self.display_element.hidden:
                    self.data_updated = True
                elif not self.creating:
                    await self.update_well(self.source_item)

            document.querySelector("#windowHolder").appendChild(self.display_element)
            self.prepare()
            if open_window:
                self.open()
            else:
                self.display_element.hidden = True
            self.creating = False

        task = util.create_task(self.load(source_item))
        task.add_done_callback(after_load)

    async def update_well(self, source_item: ItemDef) -> None:
        new_well = await self.load(source_item, False)
        self.display_element.querySelector(".well").replaceWith(new_well)
        MessageBus.publish("metadataUpdated", True)
        if not self.load_update.done():
            self.load_update.set_result(True)

    async def load(self, source_item: ItemDef, full: bool = True):
        # header
        if full:
            close_button = document.createElement("i")
            close_button.classList.add("fa-li", "fa", "fa-times", "close")

            icon = document.createElement("img")
            icon.classList.add("fa-li")
            icon.src = "files/system/icons/bag.svg"

            if source_item.item_type == "musicblock_bag":
                icon.src = "files/system/icons/CrabpackSilhouette.png"
                icon.style.width = "50px"
                icon.style.left = "-54px"

            title_span = document.createElement("span")
            title_span.classList.add("iw-title")
            title_span.innerText = source_item.name

            if len(source_item.name) >= 24:
                title_span.style.fontSize = "24px"

            header = document.createElement("header")
            header.appendChild(icon)
            header.appendChild(title_span)

        # content
        well = document.createElement("div")
        well.classList.add("well")

        self.num_slots = source_item.sub_slots
        if source_item.metadata.get("slots") is None:
            # empty bag
            sub_slots = [{"itemType": "", "count": 0, "metadata": {}} for _ in range(self.num_slots)]
        else:
            # bag has contents
            sub_slots = json.loads(source_item.metadata["slots"])

        if len(sub_slots) != source_item.sub_slots:
            raise ValueError("Number of slots in bag does not match bag size")
        well.style.opacity = "0"
        document.body.appendChild(well)  # for measuring
        # tasks = []
        for slot_num, bag_slot in enumerate(sub_slots):
            slot = document.createElement("div")
            slot.classList.add("box", "bagwindow-box")
            slot.dataset.slotNum = str(slot_num)
            well.appendChild(slot)

            item_in_slot = document.createElement("div")
            slot.appendChild(item_in_slot)
            if bag_slot.get("itemType"):
                item = ItemDef.from_json(bag_slot["item"])
                slot.title = item.name
                img = document.createElement("img")
                img.src = item.sprite_url
                class_name = f"item-{item.item_type} inventoryItem bagInventoryItem"
                await cou_globals.size_item(
                    img, item_in_slot, slot, item, bag_slot["count"], self.source_slot_num,
                    css_class=class_name, bag_slot_num=slot_num,
                )
        # await asyncio.gather(*tasks)
        well.style.opacity = "1"  # we're done measuring now
        well.remove()

        # window
        if full:
            window = document.createElement("div")
            window.id = self.id
            window.classList.add("window", "bagWindow")
            window.appendChild(header)
            window.appendChild(close_button)
            window.appendChild(well)
            window.dataset.sourceBag = str(self.source_slot_num)

            return window
        return well

    def open(self, ignore_keys: bool = False) -> None:
        self.load_update = asyncio.Future()
        util.create_task(self.update_well(self.source_item))
        self._fit_to_slots()

        super().open()

        type(self).open_windows[self.id] = self

    def close(self) -> None:
        super().close()

        type(self).open_windows.pop(self.id, None)

        # update the source inventory icon
        source_box = next(iter(box for box in cou_globals.view.inventory.children if box.dataset.slotNum == str(self.source_slot_num)))
        source_box.querySelector(".item-container-toggle").click()

    def _fit_to_slots(self) -> None:
        slots_width = {
            10: 270,
            16: 438,
            18: 494,
        }

        if self.display_element is None:
            return

        if self.num_slots is not None and slots_width.get(self.num_slots):
            self.display_element.style.width = f"{slots_width[self.num_slots]}px"
        else:
            self.display_element.style.width = ""
