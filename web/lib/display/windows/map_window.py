from datetime import timedelta
import functools
import logging
import math

from pyscript import document, window
from pyodide.ffi import create_proxy

from configs import Configs
from display.toast import Toast
from display.ui_templates.right_click_menu import RightClickMenu
from display.windows.windows import Modal
from message_bus import MessageBus
from systems import util
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)



class MapWindow(Modal):
    def __init__(self) -> None:
        self.id = "mapWindow"
        super().__init__()

        self.trigger = document.querySelector("#mapButton")
        self.search_box = document.querySelector("#mapwindow-search")
        self.search_results_container = document.querySelector("#map-window-search-results")
        self.search_results = document.querySelector("#map-window-search-results ul")

        self.prepare()

        self.setup_ui_button(cou_globals.view.mapButton, open_callback=self._draw_world_map)
        self.setup_key_binding("Map", open_callback=self._draw_world_map)

        MessageBus.subscribe("teleportByMapWindow", lambda _: self.close())
        @MessageBus.subscribe("streetLoaded")
        def on_street_load(_):
            if cou_globals.map_data.hub_is_hidden(cou_globals.current_street.hub_id):
                self.close()

        def on_focus(_):
            cou_globals.input_manager.ignoreKeys = self.ignore_shortcuts = True
        def on_input(_):
            self.filter(self.search_box.value)
        def blur_callback():
            cou_globals.input_manager.ignoreKeys = self.ignore_shortcuts = False
            self.filter("")
        def on_blur(_):
            util.Timer(timedelta(milliseconds=100), blur_callback)
        self.search_box.addEventListener("focus", create_proxy(on_focus))
        self.search_box.addEventListener("input", create_proxy(on_input))
        self.search_box.addEventListener("blur", create_proxy(on_blur))

    def _draw_world_map(self) -> None:
        self.world_map = WorldMap(cou_globals.current_street.hub_id)

    def open(self, ignore_keys: bool = False) -> None:
        if cou_globals.map_data.hub_is_hidden(cou_globals.current_street.hub_id):
            Toast("The map is not allowed here")
            return

        super().open()
        self.trigger.classList.remove("closed")
        self.trigger.classList.add("open")

    def close(self) -> None:
        super().close()
        self.trigger.classList.add("closed")
        self.trigger.classList.remove("open")

    def filter(self, entry: str) -> None:
        """Search for a string"""

        entry = entry.strip()

        # toggle list showing
        if len(entry) < 2:
            self.search_results_container.hidden = True
            return
        self.search_results_container.hidden = False

        # clear previous results
        self.search_results.replaceChildren()

        results = set()

        for street_name, data in cou_globals.map_data.streetData.items():
            tsid = util.tsidL(data["tsid"])
            if tsid is None:
                tsid = "NULL_TSID"

            # Check if street matches search
            if self.check_visibility(street_name) and (self.check_name(street_name, entry) or self.check_tsid(tsid, entry)):
                results.add(street_name.strip())

        results = sorted(list(results), key=lambda x: levenshtein(x, entry, case_sensitive=False))
        for street_name in results:
            # mark if current street
            if cou_globals.current_street.label == street_name:
                street_out = f"<i>{street_name}</i>"
            else:
                street_out = street_name

            # selectable item
            result = document.createElement("li")
            result.innerHTML = street_out

            # link to hub
            if street_data := cou_globals.map_data.streetData.get(street_name):
                hub_id = str(street_data["hub_id"])
                def on_mouse_down(event, hub_id=hub_id, street_name=street_name):
                    event.preventDefault()
                    self.world_map.load_hub_div(hub_id, street_name)
                    self.search_box.value = ""
                result.addEventListener("mousedown", create_proxy(on_mouse_down))
            else:
                LOGGER.error(f"[WorldMap] Could not find the hub_id for {street_name}")

            # add to list
            if len(self.search_results.children) <= 13:
                self.search_results.appendChild(result)
            else:
                break

    def check_visibility(self, street_name: str) -> bool:
        # hub level
        hub = cou_globals.map_data.hubData.get(cou_globals.map_data.get_hub_id_for_label(street_name))
        if not hub:
            hub_visible = True
        else:
            hub_visible = not hub.get("map_hidden", False)

        # street level
        if cou_globals.map_data.streetData.get(street_name) is None:
            street_visible = True
        else:
            street_visible = not cou_globals.map_data.streetData.get(street_name, {}).get("map_hidden", False)

        return hub_visible and street_visible

    def check_name(self, street_name: str, entry: str) -> bool:
        return entry.lower() in street_name.lower()

    def check_tsid(self, tsid: str, entry: str) -> bool:
        return tsid.lower() == entry.lower()


class WorldMap:
    DEG_TO_RAD = math.pi / 180

    def __init__(self, hub_id: str) -> None:
        self.showing_hub = None
        self.hub_info: dict[str, str] = None
        self.world_map_visible = False
        self.world_map_div = document.querySelector("#WorldMapLayer")
        self.hub_map_div = document.querySelector("#HubMapLayer")
        self.hub_map_fg = document.querySelector("#HubMapLayerFG")

        self.load_hub_div(hub_id)

        # toggle main and hub maps
        toggle_map_view = document.querySelector("#map-window-world")
        def on_click(_):
            if self.world_map_visible:
                # go to current hub
                self.hub_map()
                toggle_map_view.innerHTML = '<i class="fa fa-fw fa-globe"></i>'
            else:
                # go to world map
                self.main_map()
                toggle_map_view.innerHTML = '<i class="fa fa-fw fa-map-marker"></i>'
        toggle_map_view.addEventListener("click", create_proxy(on_click))

        @MessageBus.subscribe("streetLoaded")
        @MessageBus.subscribe("gpsCancel")
        def message_handler(_):
            if not self.world_map_visible:
                # hub visible
                self.load_hub_div(self.showing_hub)

    def navigate(self, to_street_name: str) -> None:
        cou_globals.GPS.get_route(cou_globals.current_street.label, to_street_name)
        self.load_hub_div(self.showing_hub)

    def load_hub_div(self, hub_id: str, highlight_street: str = None):
        self.showing_hub = hub_id
        hub_info = cou_globals.map_data.hubData.get(hub_id)

        if hub_info is None:
            LOGGER.error(f"[MapWindow] Missing hub {hub_id}")

        # prepare ui elements
        cou_globals.view.mapTitle.innerText = hub_info["name"]
        cou_globals.view.mapImg.style.backgroundImage = f"url({Configs.proxyMapImage(hub_info.get('img_bg'))})"
        cou_globals.view.mapImg.title = hub_info["name"]
        self.hub_map_fg.style.backgroundImage = f"url({Configs.proxyMapImage(hub_info.get('img_fg'))})"
        self.hub_map_div.replaceChildren()

        # render
        for object in cou_globals.map_data.renderData[hub_id].values():
            if object["type"] == "S":
                # streets
                street_name = cou_globals.map_data.get_label(object["tsid"])
                if not street_name:
                    continue

                street_placement = {
                    "x1": object["x1"],
                    "x2": object["x2"],
                    "y1": object["y1"],
                    "y2": object["y2"],
                    "deg": 0,
                    "length": 0,
                }
                street_placement["deg"] = self.get_street_angle(street_placement)
                street_placement["length"] = self.get_street_length(street_placement)

                street = document.createElement("div")
                street.classList.add("hm-street")
                street.title = street_name
                street.innerText = street_name
                street.setAttribute("tsid", object["tsid"])
                street.style.left = f"{street_placement['x1']}px"
                street.style.top = f"{street_placement['y1']}px"
                street.style.width = f"{street_placement['length']}px"
                street.style.transform = f"rotate({street_placement['deg']}rad)"

                def show_menu(event, street=street):
                    util.Timer(timedelta(milliseconds=100), lambda: self.create_street_menu(event, street))
                street.addEventListener("click", create_proxy(show_menu))
                street.addEventListener("contextMenu", create_proxy(show_menu))

                if object["tsid"] == cou_globals.current_street.street_data["tsid"]:
                    street.classList.add("hm-street-current")

                if highlight_street == street_name:
                    street.classList.add("hm-street-highlight")

                for street_name_on_route in cou_globals.GPS.current_route:
                    if street_name_on_route == street_name:
                        street.classList.add("hm-street-route")
                        break

                if cou_globals.map_data.streetData.get(street_name):
                    indicators = document.createElement("div")
                    indicators.classList.add("street-contents-indicators")

                    # show vendor symbol if vendor is on street
                    if vendor := cou_globals.map_data.streetData[street_name].get("vendor"):
                        ref = "an" if vendor[0].lower() in ["a", "e", "i", "o", "u"] else "a"
                        vendor_indicator = document.createElement("div")
                        vendor_indicator.classList.add("sci-vendor")
                        vendor_indicator.title = f"{street_name} has {ref} {vendor} Vendor"
                        indicators.appendChild(vendor_indicator)

                    # show shrine symbol if shrine is on street
                    if shrine := cou_globals.map_data.streetData[street_name].get("shrine"):
                        shrine_indicator = document.createElement("div")
                        shrine_indicator.classList.add("sci-shrine")
                        shrine_indicator.title = f"{street_name} has a shrine to {shrine}"
                        indicators.appendChild(shrine_indicator)

                    # show block symbol if machine room is on street
                    if cou_globals.map_data.streetData[street_name].get("machine_room") == True:
                        machine_indicator = document.createElement("div")
                        machine_indicator.classList.add("sci-machines")
                        machine_indicator.title = f"{street_name} has a machine room"
                        indicators.appendChild(machine_indicator)

                    # show gavel symbol if bureaucratic hall is on street
                    if cou_globals.map_data.streetData[street_name].get("bureaucratic_hall") == True:
                        bureau_indicator = document.createElement("div")
                        bureau_indicator.classList.add("sci-bureau")
                        bureau_indicator.title = f"{street_name} has a bureaucratic hall"
                        indicators.appendChild(bureau_indicator)

                    # show mailbox symbol if mailbox is on street
                    if cou_globals.map_data.streetData[street_name].get("mailbox") == True:
                        mailbox_indicator = document.createElement("div")
                        mailbox_indicator.classList.add("sci-mailbox")
                        mailbox_indicator.title = f"{street_name} has a mailbox"
                        indicators.appendChild(mailbox_indicator)

                    street.appendChild(indicators)

                    try:
                        # visited streets
                        tsid = cou_globals.map_data.streetData[street_name]["tsid"]
                        if util.tsidL(tsid) in cou_globals.metabolics.player_metabolics.location_history:
                            street.classList.add("visited")
                    except Exception as e:
                        LOGGER.warning(f"[MapWIndow] Could not check visited status of street {street_name}: {e}")

                # do not show certain streets
                if cou_globals.map_data.streetData.get(street_name, {}).get("map_hidden", False) == False:
                    self.hub_map_div.appendChild(street)
            elif object["type"] == "X":
                # GO circles

                go_placement = {
                    "x": object["x"],
                    "y": object["y"],
                    "arrow": object["arrow"],
                    "label": object["label"],
                    "id": object["hub_id"],
                    "name": cou_globals.map_data.hubData[object["hub_id"]]["name"],
                    "color": cou_globals.map_data.hubData[object["hub_id"]]["color"],
                }

                if not go_placement["color"].startswith("#"):
                    go_placement["color"] = f"#{go_placement['color']}"

                # position pointer arrow x/y
                arrow_x = (math.cos((go_placement["arrow"] - 90) * self.DEG_TO_RAD) * 16)
                arrow_y = (math.sin((go_placement["arrow"] - 90) * self.DEG_TO_RAD) * 16)
                arrow_z = go_placement["arrow"] + 45

                # position "Go to" text
                # round off degrees to 45 deg increments
                label_r = 0
                for theta in [360, 315, 270, 225, 180, 135, 90, 45]:
                    if abs(go_placement["label"] - theta) < 30:
                        label_r = theta
                label_r -= 90

                # position text x/y
                label_x = (math.cos((label_r) * self.DEG_TO_RAD) * 40)
                label_y = (math.sin((label_r) * self.DEG_TO_RAD) * 40)

                go_circle = document.createElement("div")
                go_circle.classList.add("hm-go-circle")
                go_circle.innerText = "GO"
                go_circle.style.backgroundColor = go_placement["color"]
                go_circle.style.left = f"{go_placement['x'] - 20}px"
                go_circle.style.top = f"{go_placement['y'] - 20}px"
                go_circle.dataset.x = str(go_placement["x"])
                go_circle.dataset.y = str(go_placement["y"])

                go_arrow = document.createElement("div")
                go_arrow.classList.add("hm-go-arrow")
                go_arrow.style.backgroundColor = go_placement["color"]
                go_arrow.style.transform = f"translateX({arrow_x}px) translateY({arrow_y}px) rotateZ({arrow_z}deg)"
                go_arrow.style.left = f"{go_placement['x'] - 8}px"
                go_arrow.style.top = f"{go_placement['y'] - 8}px"
                go_arrow.dataset.r = str(go_placement["arrow"])

                go_arrow_outline = document.createElement("div")
                go_arrow_outline.classList.add("hm-go-arrow-outline")
                go_arrow_outline.style.transform = go_arrow.style.transform
                go_arrow_outline.style.left = f"{go_placement['x'] - 11}px"
                go_arrow_outline.style.top = f"{go_placement['y'] - 11}px"

                go_circle_outline = document.createElement("div")
                go_circle_outline.classList.add("hm-go-circle-outline")
                go_circle_outline.style.left = go_circle.style.left
                go_circle_outline.style.top = go_circle.style.top

                go_text = document.createElement("div")
                go_text.classList.add("hm-go-text")
                go_text.style.color = go_placement["color"]
                go_text.innerText = f"To: {go_placement['name']}"
                go_text.style.left = f"{go_placement['x'] + label_x}px"
                go_text.style.top = f"{go_placement['y'] + label_y}px"
                go_text.dataset.r = str(go_placement["label"])

                go_parent = document.createElement("div")
                go_parent.append(go_arrow_outline, go_circle_outline, go_arrow, go_circle, go_text)
                go_parent.addEventListener("click", create_proxy(lambda _, go_placement=go_placement: self.load_hub_div(go_placement["id"])))

                self.hub_map_div.appendChild(go_parent)

        self.hub_map_div.classList.add("scaled")

        self.world_map_visible = False
        self.hub_map_div.hidden = False
        self.world_map_div.hidden = True

    def get_street_angle(self, street: dict) -> float:
        street_width = street["x2"] - street["x1"]

        if street["y1"] < street["y2"]:
            street_height = street["y2"] - street["y1"]
            radians = (math.pi / 2) - math.atan2(street_width, street_height)
        else:
            street_height = street["y1"] - street["y2"]
            radians = -math.atan2(street_height, street_width)

        return radians

    def get_street_length(self, street: dict) -> float:
        base = street["x2"] - street["x1"]
        height = street["y2"] - street["y1"]
        hyp = (base * base) + (height * height)
        return math.sqrt(hyp) + 8

    def create_street_menu(self, event, street) -> None:
        tsid = street.getAttribute("tsid")
        street_name = street.innerText

        def do_teleport():
            MessageBus.publish("teleportByMapWindow", tsid)
            if cou_globals.input_manager.konami_done and not cou_globals.input_manager.free_teleport_used:
                # free teleport for doing the konami code
                util.create_task(cou_globals.street_service.request_street(tsid))
                cou_globals.input_manager.free_teleport_used = True
            else:
                # normal teleport
                util.send_global_action("teleport", {"tsid": tsid})
                awaiting_load = True

                @MessageBus.subscribe("streetLoaded")
                def _await_load(_):
                    nonlocal awaiting_load

                    if awaiting_load:
                        Toast("-50 energy for teleporting")
                        awaiting_load = False

        options = [
            {
                "name": "Navigate",
                "description": "Get walking directions",
                "enabled": True,
                "timeRequired": 0,
                "clientCallback": lambda: self.navigate(street_name),
            },
            {
                "name": "Teleport",
                "description": "You need at least 50 energy to teleport",
                "enabled": False,
                "timeRequired": 0,
                "clientCallback": do_teleport,
            },
            {
                "name": "Encyclopedia",
                "description": "Get more information",
                "enabled": True,
                "timeRequired": 0,
                "clientCallback": lambda: window.open(f"https://childrenofur.com/encyclopedia/#/street/{tsid}", "_blank")
            }
        ]

        if cou_globals.metabolics.energy >= 50 or (cou_globals.input_manager.konami_done and not cou_globals.input_manager.free_teleport_used):
            options[1]["enabled"] = True
            if cou_globals.input_manager.konami_done and not cou_globals.input_manager.free_teleport_used:
                options[1]["description"] = "This one's on me kid"
            else:
                options[1]["description"] = "Spend 50 energy to get here right now"

        document.body.appendChild(RightClickMenu.create2(event, street_name, options))

    def main_map(self) -> None:
        self.world_map_div.hidden = True
        self.hub_map_div.hidden = True
        cou_globals.view.mapTitle.innerText = "World Map"
        cou_globals.view.mapImg.style.backgroundImage = "url(files/system/windows/worldmap.png)"
        self.world_map_div.replaceChildren()

        for key, value in cou_globals.map_data.hubData.items():
            if value.get("map_hidden") != True and value.get("x") is not None and value.get("y") is not None:
                hub = document.createElement("div")
                hub.className = "wml-hub"
                hub.dataset.hub = str(key)
                hub.style.left = f"{value['x']}px"
                hub.style.top = f"{value['y']}px"
                hub_name = document.createElement("span")
                hub_name.innerText = value["name"]
                hub.appendChild(hub_name)
                hub.style.backgroundImage = f"url({Configs.proxyMapImage(value['img_bg'])})"
                def on_mouse_leave(_, hub=hub):
                    hub.style.backgroundImage = ""
                hub.addEventListener("mouseleave", create_proxy(on_mouse_leave))
                if cou_globals.current_street.hub_id == key:
                    hub.classList.add("currentlocationhub")
                self.world_map_div.appendChild(hub)

        self.world_map_div.hidden = False
        self.world_map_visible = True

        def on_click(event):
            target = event.target.parentElement if event.target.tagName.lower() == "span" else event.target
            self.load_hub_div(target.dataset.hub)
            document.querySelector("#map-window-world").innerHTML = '<i class="fa fa-fw fa-globe"></i>'
        self.world_map_div.addEventListener("click", create_proxy(on_click))

    def hub_map(self, hub_id: str = None, hub_name: str = None, hightlight_street: str = None) -> None:
        if hub_id is None:
            hub_id = cou_globals.current_street.hub_id
        if hightlight_street is not None:
            self.load_hub_div(hub_id, hightlight_street=hightlight_street)
        else:
            self.load_hub_div(hub_id)
        cou_globals.view.mapTitle.innerText = hub_name
        cou_globals.view.mapImg.style.backgroundImage = f"url({Configs.proxyMapImage(cou_globals.map_data.hubData[hub_id]['img_bg'])})"
        cou_globals.view.mapTitle.innerText = cou_globals.map_data.hubData[hub_id]["name"]
        self.world_map_visible = False
        self.hub_map_div.hidden = False


def levenshtein(s: str, t: str, case_sensitive: bool = True) -> int:
    if not case_sensitive:
        s = s.lower()
        t = t.lower()

    if s == t:
        return 0
    if len(s) == 0:
        return len(t)
    if len(t) == 0:
        return len(s)

    v0 = [0 for _ in range(len(t) + 1)]
    v1 = [0 for _ in range(len(t) + 1)]

    for i in range(len(t) + 1):
        v0[i] = i

    for i in range(len(s)):
        v1[0] = i + 1

        for j in range(len(t)):
            cost = 0 if (s[i] == t[j]) else 1
            v1[j + 1] = min(v1[j] + 1, min(v0[j + 1] + 1, v0[j] + cost))

        for j in range(len(t) + 1):
            v0[j] = v1[j]

    return v1[len(t)]
