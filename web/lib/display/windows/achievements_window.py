import dataclasses

from pyscript import document
from pyodide import http
from pyodide.ffi import create_proxy

from configs import Configs
from network.server_interop.serializable import serializable
from display.windows.modal import Modal
from systems.global_gobbler import cou_globals


@serializable
@dataclasses.dataclass
class Achievement:
    id: str = None
    name: str = None
    description: str = None
    category: str = None
    image_url: str = None
    awarded: str = "false"


class AchievementsWindow(Modal):
    def __init__(self):
        self.id = "achievementsWindow"
        super().__init__()

        self.prepare()
        self.category_list = self.display_element.querySelector("#categoryList")
        self.categories = self.display_element.querySelector("#categories")

        for category in self.categories.querySelectorAll("li"):
            async def on_click(event):
                # update sidebar selection
                for child in self.categories.children:
                    child.classList.remove("selected")
                event.target.classList.add("selected")

                # display achievements in category
                self.category_list.replaceChildren()
                target = event.target
                category = target.innerText
                url = f"{Configs.http}//{Configs.utilServerAddress}/listAchievements?email={cou_globals.game.email}&excludeNonMatches=false&category={category}"
                map = await (await http.pyfetch(url)).json()
                achievements = Achievement.from_json(map.values(), many=True)

                earned = document.createElement("div")
                earned.classList.add('earned-achvments')
                unearned = document.createElement("div")
                unearned.classList.add('unearned-achvments')
                for achievement in achievements:
                    if achievement.awarded == "true":
                        earned.appendChild(self._create_achieve_icon(achievement))
                    else:
                        unearned.appendChild(self._create_achieve_icon(achievement))

                earned_label = document.createElement("div")
                earned_label.innerText = "Earned"
                earned_label.classList.add("title")
                self.category_list.appendChild(earned_label)
                self.category_list.appendChild(earned)
                self.category_list.appendChild(document.createElement("br"))
                unearned_label = document.createElement("div")
                unearned_label.innerText = "Unearned"
                unearned_label.classList.add("title")
                self.category_list.appendChild(unearned_label)
                self.category_list.appendChild(unearned)
            category.addEventListener("click", create_proxy(on_click))

        self.setup_ui_button(document.querySelector("#open-achievements"))
        self.setup_key_binding("Achievements")

    def _create_achieve_icon(self, achievement: Achievement):
        achv_icon = document.createElement("div")
        achv_icon.classList.add("achvment-icon")
        achv_icon.dataset.achvAwarded = achievement.awarded
        achv_icon.style.backgroundImage = f"url({achievement.image_url})"
        achv_icon.title = f"{achievement.name}\n{achievement.description}"
        return achv_icon
