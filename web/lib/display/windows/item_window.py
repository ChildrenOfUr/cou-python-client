import logging
import urllib

from pyscript import document
from pyodide.ffi import create_proxy
from pyodide import http

from configs import Configs
from display.windows.windows import Modal, WindowManager
from network.server_interop.itemdef import ItemDef
from systems import util


LOGGER = logging.getLogger(__name__)


class ItemWindow(Modal):
    instances: dict[str, "ItemWindow"] = {}

    def __new__(cls, item_name: str) -> "ItemWindow":
        if item_name not in cls.instances:
            cls.instances[item_name] = super().__new__(cls)
        else:
            cls.instances[item_name].open()
        return cls.instances[item_name]

    def __init__(self, item_name: str):
        if hasattr(self, "id"):
            # already initialized
            return
        self.id = f"itemWindow{WindowManager.random_id()}"
        self.item_name = item_name
        super().__init__()

        async def show_item():
            element = await self.display_item()
            document.querySelector("#windowHolder").appendChild(element)
            self.prepare()
            self.open()
        util.create_task(show_item())

    async def display_item(self):
        response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/getItems?name={urllib.parse.quote(self.item_name)}")
        item = ItemDef.from_json((await response.json())[0])

        new_img = 0

        if item.price != -1:
            if item.price == 0:
                # worthless
                self.price_text = "This item is priceless."
            self.price_text = f"This item sells for about <b>{item.price}</b> currant"
            if item.price > 1:
                self.price_text += "s"
        else:
            self.price_text = "Vendors will not buy this item"

        self.slot_text = f"Fits up to <b>{item.stacks_to}</b> in an inventory slot"

        if item.durability is not None:
            self.wear_text = f"Durable for about <b>{item.durability}</b> units of wear"

        close_button = document.createElement("i")
        close_button.classList.add("fa-li", "fa", "fa-times", "close")
        icon = document.createElement("i")
        icon.classList.add("fa-li", "fa", "fa-info-circle")
        title_span = document.createElement("span")
        title_span.classList.add("iw-title")
        title_span.innerText = item.name

        if len(item.name) >= 24:
            title_span.style.fontSize = "24px"

        header = document.createElement("header")
        header.appendChild(icon)
        header.appendChild(title_span)

        left_image = document.createElement("img")
        left_image.classList.add("iw-image")
        left_image.src = item.icon_url

        image_container = document.createElement("div")
        image_container.classList.add("iw-image-container")
        image_container.appendChild(left_image)

        # new item message (left column)

        img_icon = document.createElement("img")
        img_icon.src = "files/system/icons/interaction_img.svg"

        img_award = document.createElement("span")
        img_award.classList.add("img")
        img_award.innerText = ""
        img_award.appendChild(img_icon)

        new_item = document.createElement("p")
        new_item.classList.add("iw-newItem")
        new_item.innerText = "You discovered a new useful item!"
        new_item.appendChild(img_award)

        left = document.createElement("div")
        left.classList.add("iw-left")
        left.appendChild(image_container)

        if new_img > 0:
            left.appendChild(new_item)

        # information (right column)
        right = document.createElement("div")
        right.classList.add("iw-info")

        description = document.createElement("p")
        description.classList.add("iw-desc")
        description.innerText = item.description

        right.appendChild(description)

        # consume info
        if item.consume_values:
            consume_values = document.createElement("div")
            consume_values.className = "cb-content"
            explain = document.createElement("div")
            explain.className = "consume-explain"
            explain.innerText = "Rewards when consumed: "

            awarded = document.createElement("span")
            awarded.className = "awarded"

            awarded.appendChild(explain)
            consume_values.appendChild(awarded)

            metabolic_rewards = ["energy", "mood", "img"]
            for reward in metabolic_rewards:
                if item.consume_values.get(reward) is not None:
                    amount = item.consume_values[reward]
                    sign = "+" if amount >= 0 else "-"
                    span = document.createElement("span")
                    span.className = reward
                    span.innerText = f"{sign}{amount}"
                    awarded.appendChild(span)

            right.appendChild(consume_values)

        # price
        currant_icon = document.createElement("img")
        currant_icon.classList.add("iw-icon-currants")
        currant_icon.src = "files/system/icons/currant.svg"

        currant_num = document.createElement("span")
        currant_num.classList.add("iw-currants")
        currant_num.innerHTML = self.price_text

        # fit in slot
        slot_icon = document.createElement("div")
        slot_icon.classList.add("iw-icon-css", "iw-icon-slot")

        slot_num = document.createElement("span")
        slot_num.innerHTML = self.slot_text

        # wear
        meta = document.createElement("div")
        meta.classList.add("iw-meta")
        meta.appendChild(currant_icon)
        meta.appendChild(currant_num)
        meta.appendChild(document.createElement("br"))
        meta.appendChild(slot_icon)
        meta.appendChild(slot_num)

        if item.durability is not None:
            wear_icon = document.createElement("div")
            wear_icon.classList.add("iw-icon-css", "iw-icon-wear")
            wear_num = document.createElement("span")
            wear_num.innerHTML = self.wear_text

            meta.appendChild(document.createElement("br"))
            meta.appendChild(wear_icon)
            meta.appendChild(wear_num)

        right.appendChild(meta)

        # container
        well = document.createElement("div")
        well.classList.add("well")
        well.appendChild(left)
        well.appendChild(right)

        window = document.createElement("div")
        window.id = self.id
        window.classList.add("window", "itemWindow")
        window.appendChild(close_button)
        window.appendChild(header)
        window.appendChild(well)

        return window

    def close(self) -> None:
        self.instances[self.item_name].display_element.hidden = True
        super().close()
