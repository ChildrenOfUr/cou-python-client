from pyscript import document
from pyodide.ffi import create_proxy

from display.toast import Toast
from display.windows.modal import Modal
from message_bus import MessageBus
from systems.global_gobbler import cou_globals


class WeatherWindow(Modal):
    def __init__(self):
        self.id = "weatherWindow"
        super().__init__()

        self.remote_viewing = False

        self.well = document.createElement("div")
        self.well.classList.add("well")

        close_button = document.createElement("i")
        close_button.classList.add("fa-li", "fa", "fa-times", "close")

        icon = document.createElement("i")
        icon.classList.add("fa", "fa-li", "fa-cloud")

        self.title = document.createElement("span")

        header = document.createElement("header")
        header.appendChild(icon)
        header.appendChild(self.title)

        window = document.createElement("div")
        window.id = self.id
        window.classList.add("window", "weatherWindow")
        window.hidden = True
        window.append(close_button, header, self.well)

        document.querySelector("#windowHolder").append(window)
        self.prepare()

        # open button
        async def open_window(_):
            await self.open()
        document.querySelector("#weatherGlyph").addEventListener("click", create_proxy(open_window))

        # update on new game day
        MessageBus.subscribe("newDay", lambda _: self.refresh(None))

    def refresh(self, weather_data: dict) -> bool:
        weather_data = weather_data or cou_globals.weather.weather_data

        if not weather_data:
            Toast("Weather data is still loading...")
            self.close()
            return False
        if weather_data.get("error") == "no_weather":
            Toast("There's no weather here")
            self.close()
            return False

        # update location name
        self.title.innerText = f"Weather in {weather_data.get('hub_label', getattr(cou_globals.current_street, 'hub_name', 'Ur'))}"

        # remove old data
        self.well.replaceChildren()

        if "conditions" in weather_data:
            weather_data = weather_data["conditions"]

        # current conditions
        self.well.append(self._make_day_element(weather_data["current"], 0))

        # list days starting with today
        today = cou_globals.clock._Days_of_Week.index(cou_globals.clock._dayofweek)
        self.week_starting_today = cou_globals.clock._Days_of_Week[today:] + cou_globals.clock._Days_of_Week[0:today]

        # forecast conditions
        for index, forecast in enumerate(weather_data["forecast"]):
            self.well.appendChild(self._make_day_element(forecast, index + 1))

        return True

    def _make_day_element(self, weather: dict, index: int):
        day = document.createElement("div")
        day.classList.add("day")

        title = document.createElement("div")
        title.classList.add("title")
        if index == 0:
            title.innerText = "Now"
        else:
            title.innerText = self.week_starting_today[index]
        day.appendChild(title)

        icon = document.createElement("img")
        icon.src = weather["weatherIcon"]
        icon.classList.add("icon")
        day.appendChild(icon)

        description = document.createElement("div")
        description.classList.add("description")
        description.innerText = weather["weatherMain"]
        day.appendChild(description)

        temp = document.createElement("div")
        temp.classList.add("temp")
        temp.innerText = f"{int(weather['temp'])}"
        temp.insertAdjacentHTML("beforeend", "&nbsp;<sup>&deg;F</sup>")
        day.appendChild(temp)

        humidity = document.createElement("div")
        humidity.classList.add("humidity")
        humidity.title = "Humidity"
        humidity.insertAdjacentHTML("beforeend", '<i class="fa fa-tint"></i>&nbsp;')
        humidity.insertAdjacentText("beforeend", f"{weather['humidity']}%")
        day.appendChild(humidity)

        return day

    async def open(self, ignore_keys: bool = False) -> None:
        weather_data = None
        self.remote_viewing = cou_globals.window_manager.map_window.element_open
        if self.remote_viewing:
            weather_data = await cou_globals.weather.request_hub_weather(cou_globals.window_manager.map_window.world_map.showing_hub)

        if self.refresh(weather_data):
            super().open()
