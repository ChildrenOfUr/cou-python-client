from datetime import datetime

from pyscript import document
from pyodide import http

from configs import Configs
from display.overlays.module_skills import Skills
from display.toast import Toast
from display.windows.windows import Modal, WindowManager
from game.chat_bubble import VALIDATOR
from message_bus import MessageBus
from systems import util
from systems.global_gobbler import cou_globals


class NoteWindow(Modal):
    editing: "NoteWindow" = None
    DATE_FORMAT = "at %I:%M %p on %a, %b %d, %Y (local time)"

    def __init__(self, note_id: int, write_mode: bool = False) -> None:
        super().__init__()
        self.note_id = note_id
        self.write_mode = write_mode
        self.note = {}

        if self.editing is not None:
            # already editing a note, go to that one
            self.editing.focus()
        else:
            # creating a brand new note
            if note_id == -1:
                self._set_up_window(True)
            else:
                # reading an existing (we hope) note
                async def load_note():
                    note = await self.get_note()
                    if note is None:
                        Toast("That note doesn't exist")
                    else:
                        self.note = note
                        self._set_up_window()
                util.create_task(load_note())

            # listen for the server's response to edits
            @MessageBus.subscribe("note_response")
            def on_response(response: dict):
                if response.get("error") is None:
                    for key in ["id", "title", "body", "timestamp"]:
                        self.note[key] = response[key]
                else:
                    if self.element_open:
                        self.close()
                        Toast(response["error"])

    def _set_up_window(self, new_note: bool = False) -> None:
        self.id = f"notewindow-{WindowManager.random_id()}"
        if not new_note:
            self.is_writer = self.note.get("username") == cou_globals.game.username
        else:
            self.is_writer = True
        self.display_element = document.querySelector(".notewindow-template").cloneNode(True)
        self.display_element.classList.remove("notewindow-template")
        self.display_element.id = self.id

        document.querySelector("#windowHolder").appendChild(self.display_element)

        self.read_parent = self.display_element.querySelector(".notewindow-read")
        self.read_edit_btn = self.display_element.querySelector(".notewindow-read-editbtn")
        self.read_title = self.display_element.querySelector(".notewindow-read-title")
        self.read_body = self.display_element.querySelector(".notewindow-read-body")
        self.read_date = self.display_element.querySelector(".notewindow-read-footer-date")
        self.read_user = self.display_element.querySelector(".notewindow-read-footer-username")

        self.write_parent = self.display_element.querySelector(".notewindow-write")
        self.write_save_btn = self.display_element.querySelector(".notewindow-write-btn")
        self.write_title = self.display_element.querySelector(".notewindow-write-title")
        self.write_body = self.display_element.querySelector(".notewindow-write-body")

        self.prepare()

        if self.write_mode:
            self.enter_edit_mode()
            type(self).editing = self
        else:
            self.exit_edit_mode()

        # show the window
        self.display_element.hidden = False
        self.focus()

    def enter_edit_mode(self) -> None:
        if self.is_writer:
            # show the editor
            self.read_parent.hidden = True
            self.write_parent.hidden = False

            # set up the close button
            util.add_event_once(self.write_save_btn, "click", lambda _: self.set_note())

            # insert pre-existing values
            self.write_title.value = self.note.get("title", "A note!")
            self.write_body.value = self.note.get("body", "")
        else:
            Toast("You cannot edit someone else's note")

    def exit_edit_mode(self) -> None:
        # hide the editor
        self.write_parent.hidden = True
        self.read_parent.hidden = False

        # display values
        self.read_title.innerText = self.note["title"]
        self.read_body.innerHTML = VALIDATOR.sanitize(self.note["body"].replace("\n", "<br>"))
        self.read_date.innerText = self.get_date_string(self.note["timestamp"])

        # handle user-specific content
        if self.is_writer:
            # You can only edit notes with penpersonship
            pen_skill = Skills.get_skill("penpersonship")
            self.read_edit_btn.hidden = not (pen_skill is not None and pen_skill["player_level"] == 1)

            self.read_user.innerText = "You"
            util.add_event_once(self.read_edit_btn, "click", lambda _: self.enter_edit_mode())
        else:
            self.read_edit_btn.hidden = True
            self.read_user.innerHTML = VALIDATOR.sanitize(
                '<a title="Open Profile" '
                f'href="https://childrenofur.com/players/{self.note["username"]}" '
                f'target="_blank">{self.note["username"]}</a>'
            )

        type(self).editing = None

    async def get_note(self) -> dict:
        # download from server
        try:
            response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/note/find/{self.note_id}")
            return await response.json()
        except Exception:
            return None

    def set_note(self) -> None:
        # send the message to the server
        util.send_action("writeNote", "global_action_monster", {"noteData": {
            "id": int(self.note.get("id", -1)),  # the server knows that -1 means a new note
            "username": cou_globals.game.username,
            "title": self.write_title.value,
            "body": self.write_body.value,
        }})

        # response handled by service in constructor

        self.close()

    def get_date_string(self, standard_format: str) -> str:
        note_time = datetime.strptime(standard_format, "%Y-%m-%d %H:%M:%S.%fZ")
        return datetime.strftime(note_time, self.DATE_FORMAT)

    def close(self) -> None:
        super().close()
        self.display_element.remove()
        type(self).editing = None
