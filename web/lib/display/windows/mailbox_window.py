import dataclasses
import json

from pyscript import document, window
from pyodide import http
from pyodide.ffi import create_proxy

from configs import Configs
from network.server_interop.serializable import serializable
from display.inventory import Dropzone
from display.windows.modal import Modal
from network.server_interop.itemdef import ItemDef
from systems import util
from systems.global_gobbler import cou_globals


@serializable
@dataclasses.dataclass
class Mail:
    _transform_names = False

    id: int = None
    currants: int = None
    to_user: str = None
    from_user: str = None
    subject: str = None
    body: str = None
    read: bool = None
    currants_taken: bool = None
    item1_taken: bool = None
    item2_taken: bool = None
    item3_taken: bool = None
    item4_taken: bool = None
    item5_taken: bool = None
    item1: str = None
    item2: str = None
    item3: str = None
    item4: str = None
    item5: str = None
    item1_slot: str = None
    item2_slot: str = None
    item3_slot: str = None
    item4_slot: str = None
    item5_slot: str = None

    @property
    def items(self) -> list[str]:
        return [self.item1, self.item2, self.item3, self.item4, self.item5]

    @property
    def items_taken(self) -> list[bool]:
        return [self.item1_taken, self.item2_taken, self.item3_taken, self.item4_taken, self.item5_taken]

    @property
    def has_value(self) -> bool:
        if self.currants > 0 and not self.currants_taken:
            return True

        for index in range(1, 6):
            if getattr(self, f"item{index}") is not None and not getattr(self, f"item{index}_taken"):
                return True

        return False

    def display(self):
        _from = document.createElement("td")
        _from.classList.add("tbody_from")
        _from.innerText = self.from_user

        subject = document.createElement("td")
        subject.classList.add("tbody_subject")
        subject.innerText = self.subject
        subject.addEventListener("click", create_proxy(lambda _: util.create_task(cou_globals.window_manager.mailbox_window.read(self.id))))

        delete = document.createElement("td")
        delete.classList.add("tbody_delete")
        delete_button = document.createElement("ur-button")
        delete_button.classList.add("btn-delete")
        delete_button.innerText = "Delete"
        delete_button.addEventListener("click", create_proxy(lambda _: util.create_task(self.delete())))
        delete.appendChild(delete_button)

        row = document.createElement("tr")
        row.classList.add("inbox_message")
        row.append(_from, subject, delete)

        if not self.read:
            row.classList.add("unread")

        return row

    async def delete(self, prompt: bool = True) -> None:
        if not prompt:
            await MailboxWindow.post_request("deleteMail", {"id": self.id})
            await cou_globals.window_manager.mailbox_window.refresh()
            return

        question = "Are you sure you want to delete this message? "
        if not self.read and self.has_value:
            question += "You haven't read it yet and it contains currants or items!"
        elif not self.read:
            question += "You haven't read it yet."
        elif self.has_value:
            question += "It contains currents or items!"
        else:
            await self.delete(prompt=False)
            return

        # Let's make sure the user didn't forget to grab any currants/items
        if window.confirm(question):
            await self.delete(prompt=False)


class MailboxWindow(Modal):
    _instance: "MailboxWindow" = None
    _initialized = False

    def __new__(cls):
        if not cls._instance:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self):
        if self._initialized:
            return

        self.id = "mailboxWindow"
        super().__init__()

        self.messages: list[Mail] = []
        self.item_slots = [None, None, None, None, None]
        self.last_message: Mail = None
        self.tabs = {
            "compose": document.querySelector("#mb_compose"),
            "read": document.querySelector("#mb_read"),
            "inbox": document.querySelector("#mb_inbox"),
        }

        self._inbox_list = document.querySelector("#mb_inbox_list")
        self._inbox_list_body = document.querySelector("#mb_inbox_list .messageItems")
        self._empty_inbox = document.querySelector("#mb_emptyinbox")
        self._send_items = document.querySelector("#mb_sendItems")
        self._from_display = document.querySelector("#mb_from")
        self._subject_display = document.querySelector("#mb_subject")
        self._message_display = document.querySelector("#mb_message")
        self._currant_display = document.querySelector("#mb_fromCurrants")
        self._currant_num_display = document.querySelector("#mb_fromCurrantsNum")

        self._from_item_boxes = [
            document.querySelector("#mb_from_item1"),
            document.querySelector("#mb_from_item2"),
            document.querySelector("#mb_from_item3"),
            document.querySelector("#mb_from_item4"),
            document.querySelector("#mb_from_item5"),
        ]

        self._refresh_btn = document.querySelector("#mb_refresh_btn")
        self._compose_btn = document.querySelector("#mb_compose_btn")
        self._send_btn = document.querySelector("#mb_action_send")
        self._discard_btn = document.querySelector("#mb_action_discard")
        self._reply_btn = document.querySelector("#mb_btn_reply")
        self._read_close_btn = document.querySelector("#mb_btn_close")
        self._compose_link = document.querySelector("#mb_compose_link")

        self._to_list = document.querySelector("#mb_toList")
        self._input_to = document.querySelector("#mb_input_to")
        self._input_subject = document.querySelector("#mb_input_subject")
        self._input_message = document.querySelector("#mb_input_message")
        self._input_currants = document.querySelector("#mb_sendCurrants")

        self.busy = False
        self._user_has_messages = None

        self.prepare()

        self._refresh_btn.addEventListener("click", create_proxy(lambda _: util.create_task(self.refresh())))
        self._compose_btn.addEventListener("click", create_proxy(lambda _: self.switch_to_tab("compose")))
        self._compose_link.addEventListener("click", create_proxy(lambda _: self.switch_to_tab("compose")))
        self._discard_btn.addEventListener("click", create_proxy(lambda _: self.switch_to_tab("inbox")))
        self._send_btn.addEventListener("click", create_proxy(lambda _: util.create_task(self.send())))
        self._read_close_btn.addEventListener("click", create_proxy(lambda _: self.switch_to_tab("inbox")))
        self._reply_btn.addEventListener("click", create_proxy(lambda _: self.reply()))
        self._input_to.addEventListener("input", create_proxy(lambda _: util.create_task(self.update_friends_typeahead())))

        dropzone = Dropzone(self.display_element.querySelectorAll(".toItem"))
        def on_drop(event):
            # verify it is a valid item before acting on it
            data = event.dataTransfer.getData("itemdef")
            if data is None:
                return

            item: ItemDef = ItemDef.from_json(data)

            # only allow bags if they are empty
            if item.is_container:
                for slot in json.loads(item.metadata["slots"]):
                    if slot["itemType"] != "":
                        return

            id = event.target.id
            index = int(id[-1]) - 1
            self.item_slots[index] = event.dataTransfer.getData("slotdef")
            event.target.style.backgroundImage = f"url({item.icon_url})"
            # count = document.createElement("div")
            # count.classList.add("itemCount")
            # count.innerText = 1
            # event.target.appendChild(count)
        dropzone.on_drop(on_drop)

    @property
    def user_has_messages(self) -> bool:
        return self._user_has_messages

    @user_has_messages.setter
    def user_has_messages(self, value: bool) -> None:
        self._user_has_messages = value
        self._inbox_list.hidden = not self._user_has_messages
        self._empty_inbox.hidden = self._user_has_messages

    def open(self, ignore_keys: bool = False) -> None:
        self.switch_to_tab("inbox")
        cou_globals.input_manager.ignoreKeys = True
        super().open()

    def close(self) -> None:
        cou_globals.input_manager.ignore_keys = False
        self.clear_compose_view()
        super().close()

    def switch_to_tab(self, tab: str) -> None:
        for name, host in self.tabs.items():
            host.hidden = tab != name

        match tab:
            case "inbox":
                util.create_task(self.refresh())
            case "compose":
                self._input_currants.max = str(cou_globals.metabolics.currants)

    def clear_compose_view(self) -> None:
        self._input_to.value = ""
        self._input_subject.value = ""
        self._input_message.value = ""
        for box in self._send_items.children:
            box.removeAttribute("style")
            box.replaceChildren()
        self.item_slots = [None, None, None, None, None]

    @classmethod
    async def post_request(cls, endpoint: str, data: dict|str, request_headers: dict = None):
        if request_headers is None:
            request_headers = {"content-type": "application/json"}

        if not isinstance(data, str):
            data = json.dumps(data)

        return await http.pyfetch(
            f"{Configs.http}//{Configs.utilServerAddress}/{endpoint}",
            method="POST", headers=request_headers, body=data,
        )

    async def refresh(self) -> None:
        if self.busy:
            return

        self.busy = True
        response = await self.post_request("getMail", {"user": cou_globals.game.username})
        self.messages = Mail.from_json(await response.json(), many=True)

        self._inbox_list_body.replaceChildren()

        for mail in sorted(self.messages, key=lambda x: (not x.read, x.id), reverse=True):
            if len(mail.subject.strip()) == 0:
                mail.subject = "(No Subject)"

            self._inbox_list_body.appendChild(mail.display())

        self.user_has_messages = self.messages
        self.busy = False

    async def read(self, id: int) -> None:
        await self.refresh()
        self.last_message = next((m for m in self.messages if m.id == id), None)

        # show message
        self._from_display.innerText = f"A message from {self.last_message.from_user}"
        self._subject_display.innerText = self.last_message.subject
        self._message_display.innerText = self.last_message.body

        # currants
        if self.last_message.currants > 0:
            # currants sent with message
            if self.last_message.currants_taken:
                # server knows currant are already taken
                self._currant_display.classList.add("taken")
            else:
                # currants not yet taken
                self._currant_display.classList.remove("taken")
                self._currant_display.title = "Already taken"

                # take currants on click
                async def take(_):
                    response = await self.post_request("collectCurrants", self.last_message.to_json())
                    self._currant_display.classList.add("taken")
                    if (await response.string()).strip() == "Success":
                        self._currant_display.title = "Already taken"
                        self._currant_display.classList.add("taken")
                    else:
                        self._currant_display.classList.remove("taken")
                    await self.refresh()
                util.add_event_once(self._currant_display, "click", take)

            self._currant_num_display.innerText = f"{self.last_message.currants:,}"
            self._currant_display.hidden = False
        else:
            # no currants sent with message
            self._currant_display.hidden = True

        # items
        for index, item_str in enumerate(self.last_message.items):
            item_box = self._from_item_boxes[index]
            if item_str:
                item: ItemDef = ItemDef.from_json(item_str)
                item_box.style.backgroundImage = f"url({item.icon_url})"
                item_box.hidden = False

                if self.last_message.items_taken[index]:
                    item_box.classList.add("taken")
                    item_box.title = "Already taken"
                else:
                    async def take(_, item_box=item_box, index=index):
                        item_box.classList.add("taken")
                        response = await self.post_request(
                            "collectItem",
                            {"index": index + 1, "id": self.last_message.id, "to_user": self.last_message.to_user},
                        )
                        if (await response.string()).strip() == "Success":
                            item_box.title = "Already taken"
                        else:
                            item_box.classList.remove("taken")
                        await self.refresh()
                    util.add_event_once(item_box, "click", take)
            else:
                item_box.hidden = True

        # mark message read
        self.switch_to_tab("read")
        await self.post_request("readMail", self.last_message.to_json())

    async def send(self) -> None:
        if self.busy:
            return

        self.busy = True
        message = Mail(
            to_user=self._input_to.value,
            from_user=cou_globals.game.username,
            body=self._input_message.value,
            subject=self._input_subject.value,
        )

        try:
            message.currants = int(self._input_currants.value)
        except Exception:
            message.currants = 0

        for index in range(5):
            setattr(message, f"item{index + 1}_slot", self.item_slots[index])

        response = await self.post_request("sendMail", message.to_json())

        if await response.string() == "OK":
            # clear sending fields (for next message)
            self.clear_compose_view()
            self.switch_to_tab("inbox")
        self.busy = False

    def reply(self) -> None:
        if self.busy:
            return

        self.busy = True
        self._input_to.value = self.last_message.from_user
        self._input_subject.value = f"Re: {self.last_message.subject}"
        self.switch_to_tab("compose")
        self.busy = False

    async def update_friends_typeahead(self) -> None:
        so_far = self._input_to.value
        if len(so_far) > 2:
            response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/friends/list/{cou_globals.game.username}")
            self._to_list.replaceChildren()
            for username, online in (await response.json()).items():
                option = document.createElement("option")
                option.value = username
                option.innerText = "Online" if online else "Offline"
                self._to_list.appendChild(option)
        elif not so_far:
            self._to_list.replaceChildren()

    def _decode_item_from_element(self, element) -> ItemDef:
        # verify it is a valid item before acting on it
        if element.getAttribute("itemMap") is None:
            return None

        item_map = json.loads(element.getAttribute("itemMap"))
        metadata = item_map.get("metadata", {})
        item = ItemDef.from_json(item_map)
        item.metadata = metadata

        return item
