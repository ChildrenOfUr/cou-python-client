import math

from pyscript import document
from pyodide import http
from pyodide.ffi import create_proxy

from configs import Configs
from display.toast import Toast, NotifyRule
from display.windows.windows import Modal, WindowManager
from systems import util
from systems.global_gobbler import cou_globals

class UseWindow(Modal):
    instances: dict[str, "UseWindow"] = {}

    def __new__(cls, item_type: str, item_name: str) -> "UseWindow":
        if item_type not in cls.instances:
            cls.instances[item_type] = super().__new__(cls)
        else:
            util.create_task(cls.instances[item_type].open())
        return cls.instances[item_type]

    def __init__(self, item_type: str, item_name: str) -> None:
        self.id = f"useWindow{WindowManager.random_id()}"
        super().__init__()

        self.item_type = item_type
        self.item_name = item_name
        self.list_url = None
        self.recipe_list: list[dict] = None
        self.making_cancelled = None

        self.well = document.createElement("div")
        self.well.classList.add("well", "useitem-well")

        async def do_load():
            element = await self.load()
            document.querySelector("#windowHolder").appendChild(element)
            self.prepare()
            await self.open(refresh=False)
        util.create_task(do_load())

    async def load(self):
        await self.update_recipes()

        # header
        close_button = document.createElement("i")
        close_button.classList.add("fa-li", "fa", "fa-times", "close")

        icon = document.createElement("i")
        icon.classList.add("fa-li", "fa", "fa-bars")

        title_span = document.createElement("span")
        title_span.innerText = f"Using a {self.item_name}"

        header = document.createElement("header")
        header.append(icon, title_span)

        # container
        window = document.createElement("div")
        window.id = self.id
        window.classList.add("window", "useWindow")
        window.append(close_button, header, self.well)

        return window

    async def list_recipes(self, clear_well: bool = True):
        if clear_well:
            self.well.replaceChildren()
            self.well.classList.remove("col3")

        recipe_container = document.createElement("div")
        recipe_container.classList.add("useitem-recipes")
        recipe_container.hidden = False

        if not self.recipe_list:
            no_recipes = document.createElement("span")
            no_recipes.classList.add("useitem-norecipes")
            no_recipes.innerText = "You don't know how to make anything with this."
            recipe_container.appendChild(no_recipes)

            return recipe_container

        for recipe in self.recipe_list:
            item_max = []
            input_types = []

            # decide if we can make this recipe
            for item_map in recipe["input"]:
                input_types.append(item_map["itemType"])

                user_has = util.Util().get_num_items(item_map["itemType"])
                item_map["userHas"] = user_has

                qty = item_map["qtyReq"]
                if user_has >= qty:
                    item_max.append(math.floor(user_has / qty))
                else:
                    item_max.append(0)

                item_max.sort()

                if item_max:
                    recipe["canMake"] = item_max[0]
                else:
                    recipe["canMake"] = 0

            image = document.createElement("div")
            image.classList.add("useitem-recipe-image")
            image.style.backgroundImage = f"url({recipe['output_map']['iconUrl']})"

            display_name = recipe["output_map"].get("recipeName") or recipe["output_map"]["name"]
            if recipe["output_map"]["itemType"] in input_types:
                # consumes itself
                display_name += " (Repair)"

            info = document.createElement("info")
            info.classList.add("useitem-recipe-text")
            info.innerText = display_name

            recipe_btn = document.createElement("idv")
            recipe_btn.classList.add("useitem-recipe", "white-btn")
            recipe_btn.append(image, info)
            recipe_btn.addEventListener("click", create_proxy(lambda _, recipe=recipe: self.open_recipe(recipe["id"])))

            if recipe["canMake"] == 0:
                recipe_btn.classList.add("cannot-make")
                recipe_btn.title = "You don't have everything to make this. Click to see what you need."

            recipe_container.appendChild(recipe_btn)

        return recipe_container

    def open_recipe(self, id: str) -> None:
        from display.windows.item_window import ItemWindow

        qty = 1
        recipe = self.get_recipe(id)

        # output info
        back_to_list = document.createElement("div")
        back_to_list.classList.add("recipeview-backtolist", "white-btn")
        back_to_list.innerHTML = '<i class="fas fa-chevron-left"></i>&emsp;Cancel'
        async def on_back_click(_):
            self.well.append(await self.list_recipes())
        back_to_list.addEventListener("click", create_proxy(on_back_click))

        item_image = document.createElement("div")
        item_image.classList.add("recipeview-image", "white-btn")
        item_image.style.backgroundImage = f"url({recipe['output_map']['iconUrl']})"
        item_image.addEventListener("click", create_proxy(lambda _: ItemWindow(recipe["output_map"]["name"])))

        recipe_name = recipe["output_map"].get("recipeName") or recipe["output_map"]["name"]
        item_name = document.createElement("div")
        item_name.classList.add("recipeview-text")
        item_name.innerText = recipe_name

        output_qty = document.createElement("div")
        output_qty.classList.add("recipeview-outputqty")

        if recipe["output_amt"] == 1:
            output_qty.innerHTML = f"This recipe makes <br><b>one</b> {recipe_name}"
        else:
            if recipe_name.endswith("y"):
                output_qty.innerHTML = f"This recipe makes <br><b>{recipe['output_amt']}</b> {recipe_name[:-1]}ies"
            elif recipe_name.endswith("s"):
                output_qty.innerHTML = f"This recipe makes <br><b>{recipe['output_amt']}</b> {recipe_name}es"
            else:
                output_qty.innerHTML = f"This recipe makes <br><b>{recipe['output_amt']}</b> {recipe_name}s"

        left_col = document.createElement("div")
        left_col.classList.add("recipeview-leftcol")
        left_col.append(back_to_list, item_image, item_name, output_qty)

        make_btn = document.createElement("div")
        make_btn.classList.add("rv-makebtn", "white-btn")
        make_btn.innerText = "Do It!"
        async def on_make_click(event):
            if not event.target.classList.contains("disabled"):
                await self.make_recipe(id, int(qty_display.value))
        make_btn.addEventListener("click", create_proxy(on_make_click))

        # qty controls
        hm_title = document.createElement("div")
        hm_title.classList.add("recipeview-ing-title")

        if recipe["canMake"] > 0:
            hm_title.innerText = "How many?"
        else:
            hm_title.innerText = "You don't have all the ingredients needed to make this."

        qty_display = document.createElement("input")
        qty_display.type = "number"
        qty_display.classList.add("rv-qty-disp")
        qty_display.value = str(qty)
        qty_display.min = "1"
        qty_display.max = str(recipe["canMake"])
        qty_display.addEventListener("change", create_proxy(lambda _: self.check_req_energy(recipe, make_btn, qty_display)))

        qty_minus = document.createElement("div")
        qty_minus.classList.add("rv-qty-minus")
        def on_minus_click(_):
            nonlocal qty
            if qty > 1:
                qty -= 1
            self.check_req_energy(recipe, make_btn, qty_display)
            qty_display.value = str(qty)
        qty_minus.addEventListener("click", create_proxy(on_minus_click))
        qty_minus.innerHTML = '<i class="fas fa-fw fa-minus rv-red"></i>'

        qty_plus = document.createElement("div")
        qty_plus.classList.add("rv-qty-plus")
        def on_plus_click(_):
            nonlocal qty
            if qty < recipe["canMake"]:
                qty += 1
            self.check_req_energy(recipe, make_btn, qty_display)
            qty_display.value = str(qty)
        qty_plus.addEventListener("click", create_proxy(on_plus_click))
        qty_plus.innerHTML = '<i class="fas fa-fw fa-plus rv-green"></i>'

        qty_parent = document.createElement("div")
        qty_parent.classList.add("recipeview-qtyparent")
        qty_parent.append(qty_minus, qty_display, qty_plus)

        max_disp = document.createElement("div")
        max_disp.classList.add("rv-max-disp")
        max_disp.innerText = f"You can make {recipe['canMake']} of these"

        max_btn = document.createElement("div")
        max_btn.classList.add("rv-max-btn", "white-btn")
        max_btn.innerText = f"Make {recipe['canMake']}"
        def on_max_click(_):
            qty_display.value = str(recipe["canMake"])
            self.check_req_energy(recipe, make_btn, qty_display)
        max_btn.addEventListener("click", create_proxy(on_max_click))

        center_col = document.createElement("div")
        center_col.classList.add("recipeview-centercol")
        center_col.appendChild(hm_title)

        if recipe["canMake"] > 0:
            center_col.append(qty_parent, max_disp, max_btn, document.createElement("hr"), make_btn)

        # ingredients
        ing_title = document.createElement("div")
        ing_title.classList.add('recipeview-ing-title')
        ing_title.innerText = "Ingredients"

        ing_list = document.createElement("table")
        ing_list.classList.add('recipeview-ing-list')

        for ing_map in recipe["input"]:
            img = document.createElement("img")
            img.src = ing_map["iconUrl"]
            img.classList.add("rv-ing-list-img")

            text = document.createElement("td")
            text.innerHTML = f"<b>{ing_map['qtyReq']}x</b> {ing_map['name']}"
            text.classList.add('rv-ing-list-ing-name')

            status = document.createElement("i")
            if ing_map["userHas"] >= ing_map["qtyReq"]:
                status.classList.add("fas", "fa-check", "fa-fw", "rv-green")
                status.title = "You have enough"
            else:
                status.classList.add("fas", "fa-times", "fa-fw", "rv-red")
                status.title = "You don't have enough"

            status_col = document.createElement("td")
            status_col.appendChild(status)

            item = document.createElement("tr")
            item.append(img, text, status_col)
            item.addEventListener("click", create_proxy(lambda _: ItemWindow(ing_map["name"])))
            item.title = "Click to open item information"

            ing_list.appendChild(item)

        right_col = document.createElement("div")
        right_col.classList.add("recipeview-rightcol")
        right_col.append(ing_title, ing_list)

        # display
        self.well.replaceChildren()
        self.well.append(left_col, center_col, right_col)
        self.well.classList.add("col3")

        # need to call prepare again sot that it is listening for our
        # input to get focus to ignore keys
        self.prepare()

    async def make_recipe(self, id: str, qty: int = 1) -> bool:
        recipe = self.get_recipe(id)

        current = 1

        animation = document.createElement("img")
        animation.src = f"./files/toolAnimations/{recipe['tool']}.gif"

        animation_parent = document.createElement("div")
        animation_parent.classList.add("makerecipe-anim")
        animation_parent.appendChild(animation)

        prog_bar = document.createElement("ur-progress")
        prog_bar.setAttribute("percent", "0")
        prog_bar.setAttribute("status", f"Making {current} of {qty}...")

        prog_container = document.createElement("div")
        prog_container.classList.add("progress")
        prog_container.append(prog_bar)

        cancel_btn = document.createElement("div")
        cancel_btn.classList.add("white-btn", "rmake-cancel", "rv-red")
        cancel_btn.innerHTML = '<i class="fas fa-chevron-left"></i>&emsp;Cancel'
        def on_cancel_click(_):
            self.making_cancelled = True
            cancel_btn.innerHTML = '<i class="fas fa-spin fa-spinner"></i>&emsp;Finishing up...'
            cancel_btn.classList.add("disabled")
        util.add_event_once(cancel_btn, "click", on_cancel_click)

        # display
        self.well.replaceChildren()
        self.well.append(animation_parent, prog_container, cancel_btn)

        async def while_making():
            nonlocal current

            if self.making_cancelled:
                await self._stop_making_recipes()
                return False

            response = await http.pyfetch(
                f"{Configs.http}//{Configs.utilServerAddress}/recipes/make?token={Configs.rsToken}"
                f"&id={recipe['id']}&email={cou_globals.game.email}&username={cou_globals.game.username}"
            )
            server_response = await response.string()

            if current >= qty or server_response != "OK":
                # stop
                if server_response != "OK":
                    # server says no
                    Toast(
                        f"You had to stop using your {self.item_name} because {server_response}.",
                        notify=NotifyRule.UNFOCUSED,
                    )
                # we're done
                await self._stop_making_recipes()
                return False
            else:
                # increase number we've made
                current += 1

                # update the progress bar
                prog_bar.setAttribute("percent", str((current / qty) * 100))
                prog_bar.setAttribute("status", f"Making {current} of {qty}")

            return True

        while await while_making():
            pass


    async def _stop_making_recipes(self) -> None:
        # update recipe data
        await self.update_recipes(False)
        # reset the UI
        self.well.appendChild(await self.list_recipes())

    def get_recipe(self, id: str) -> dict:
        return next((r for r in self.recipe_list if r["id"] == id), None)

    async def open(self, ignore_keys: bool = False, refresh: bool = True) -> None:
        if refresh:
            await self.update_recipes()

        self.well.append(await self.list_recipes(True))
        self.making_cancelled = False
        self.display_element.hidden = False
        self.element_open = True
        self.focus()

    def close(self) -> None:
        if self.instances.get(self.item_type):
            self.instances[self.item_type].display_element.hidden = True
            super().close()

    async def update_recipes(self, notify: bool = True) -> None:
        if notify:
            Toast("Reading recipe book...")

        response = await http.pyfetch(
            f"{Configs.http}//{Configs.utilServerAddress}/recipes/list"
            f"?token={Configs.rsToken}&tool={self.item_type}&email={cou_globals.game.email}"
        )
        self.recipe_list = await response.json()

    def check_req_energy(self, recipe: dict, make_btn, qty_display) -> bool:
        if cou_globals.metabolics.energy < abs(int(recipe["energy"])) * int(qty_display.value):
            make_btn.classList.add("disabled")
            make_btn.title = "Not enough energy :("
            return False
        make_btn.classList.remove("disabled")
        make_btn.title = ""
        return True
