from pyscript import document, window
from pyodide.ffi import create_proxy

from display.information_display import InformationDisplay
from systems.global_gobbler import cou_globals


class Modal(InformationDisplay):
    """A Python interface to an html modal"""

    action_triggers: dict[str, callable] = {}

    def __init__(self):
        super().__init__()
        self.esc_listener: callable = None

    def open(self, ignore_keys: bool = False) -> None:
        if self.display_element is None:
            return

        cou_globals.input_manager.ignoreKeys = ignore_keys
        self.display_element.hidden = False
        self.element_open = True
        self.focus()

    def close(self) -> None:
        if self.display_element is None:
            return

        cou_globals.input_manager.ignoreKeys = False
        self._destroy_esc_listener()
        self.display_element.hidden = True
        self.element_open = False

        # see if there's another window that we want to focus
        for modal in document.querySelectorAll(".window"):
            if not modal.hidden:
                if m := cou_globals.modals.get(modal.id):
                    m.focus()

    def focus(self) -> None:
        for others in document.querySelectorAll(".window"):
            others.style.zIndex = "2"
        self.display_element.style.zIndex = "3"
        self.display_element.focus()

    def _create_esc_listener(self) -> None:
        if self.esc_listener is not None:
            return

        def listen_for_esc(event):
            # 27 == esc key
            if event.keyCode == 27:
                self.close()
        handler = create_proxy(listen_for_esc)
        self.esc_listener = handler
        document.addEventListener("keyup", handler)

    def _destroy_esc_listener(self) -> None:
        if self.esc_listener is not None:
            document.removeEventListener("keyup", self.esc_listener)
            self.esc_listener = None

    def prepare(self) -> None:
        # get 'window'
        self.display_element = document.querySelector(f"#{self.id}")

        # close button
        if close := self.display_element.querySelector(".fa-times.close"):
            close.addEventListener("click", create_proxy(lambda _: self.close()))

        # catch right-clicks on a window and do nothing with them except
        # stop them from propagating to the document body
        def ignore_right_click(event):
            event.stopPropagation()
            event.preventDefault()
        self.display_element.addEventListener("contextmenu", create_proxy(ignore_right_click))

        # prevent player movement while window is focused
        def stop_movement(event):
            cou_globals.input_manager.ignoreKeys = True
            cou_globals.input_manager.ignoreChatFocus = True
        def start_movement(event):
            cou_globals.input_manager.ignoreKeys = False
            cou_globals.input_manager.ignoreChatFocus = False
        for item in self.display_element.querySelectorAll('input, textarea, div[contenteditable="true"]'):
            item.addEventListener("focus", create_proxy(stop_movement))
            item.addEventListener("blur", create_proxy(start_movement))

        # make div focusable. see: http://stackoverflow.com/questions/11280379/is-it-possible-to-write-onfocus-lostfocus-handler-for-a-div-using-js-or-jquery
        self.display_element.tabIndex = -1
        self.display_element.addEventListener("focus", create_proxy(lambda _: self._create_esc_listener()))
        self.display_element.addEventListener("blur", create_proxy(lambda _: self._destroy_esc_listener()))

        # tabs
        def click_tab(event):
            tab = event.target
            self.open_tab(tab.innerText)
            # show intended tab
            tab.classList.add("active")
        for tab in self.display_element.querySelectorAll(".tab"):
            tab.addEventListener("click", create_proxy(click_tab))

        # dragging
        header = self.display_element.querySelector("header")
        if header is not None:
            dragging = False
            left_diff = 0
            top_diff = 0

            # mouse down listeners
            self.display_element.addEventListener("mousedown", create_proxy(lambda _: self.focus()))
            def start_drag(event):
                nonlocal dragging, left_diff, top_diff
                dragging = True
                if not self.display_element.style.transform:
                    # need to get any currently set transform by an upper level CSS class
                    # and offset by that value because we're about to override the transform
                    # on the element. For example, the window might have a transform
                    # like (-50%, -50%) or (30px, 0), etc
                    transform_matrix = window.DOMMatrixReadOnly.new(window.getComputedStyle(self.display_element).transform)
                    transformX = transform_matrix.m41
                    transformY = transform_matrix.m42
                    self.display_element.style.top = f"{self.display_element.offsetTop + transformY}px"
                    self.display_element.style.left = f"{self.display_element.offsetLeft + transformX}px"
                    self.display_element.style.transform = "initial"
                left_diff = event.pageX - self.display_element.offsetLeft
                top_diff = event.pageY - self.display_element.offsetTop
            header.addEventListener("mousedown", create_proxy(start_drag))

            # mouse is moving
            def do_drag(event):
                if dragging:
                    left = event.pageX - left_diff
                    top = event.pageY - top_diff
                    self.display_element.style.top = f"{top}px"
                    self.display_element.style.left = f"{left}px"
            document.addEventListener("mousemove", create_proxy(do_drag))

            # mouse up
            def stop_drag(event):
                nonlocal dragging
                dragging = False
            document.addEventListener("mouseup", create_proxy(stop_drag))

            cou_globals.modals[self.id] = self

    def open_tab(self, tab_id: str) -> None:
        tab_view = self.display_element.querySelector(f"article #{tab_id.lower()}")
        # hide all tabs
        for tab in self.display_element.querySelectorAll("article .tab-content"):
            tab.hidden = True
        for tab in self.display_element.querySelectorAll("article .tab"):
            tab.classList.remove("active")
        tab_view.hidden = False
