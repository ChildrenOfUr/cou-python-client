import json

from pyscript import document
from pyodide.ffi import create_proxy

from display.inventory import Dropzone
from display.toast import Toast
from display.windows.modal import Modal
from message_bus import MessageBus
from systems import util
from systems.global_gobbler import cou_globals


class ShrineWindow(Modal):
    shrine_window: "ShrineWindow" = None
    confirm_listener: callable = None
    cancel_listener: callable = None
    plus_listener: callable = None
    minus_listener: callable = None
    max_listener: callable = None

    def __new__(cls, giant_name: str, favor: int, max_favor: int, shrine_id: str) -> "ShrineWindow":
        if cls.shrine_window is None:
            cls.shrine_window = super().__new__(cls)
        else:
            cls.shrine_window.giant_name = giant_name
            cls.shrine_window.favor = favor
            cls.shrine_window.max_favor = max_favor
            cls.shrine_window.shrine_id = shrine_id
        return cls.shrine_window

    def __init__(self, giant_name: str, favor: int, max_favor: int, shrine_id: str) -> None:
        self.id = "shrineWindow"
        super().__init__()
        self.giant_name = giant_name
        self.favor = favor
        self.max_favor = max_favor
        self.shrine_id = shrine_id

        self.prepare()

        self.qty_container = self.display_element.querySelector("#shrine-window-qty")
        self.plus_btn = self.display_element.querySelector(".plus")
        self.minus_btn = self.display_element.querySelector(".minus")
        self.max_btn = self.display_element.querySelector(".max")
        self.num_box = self.display_element.querySelector(".NumToDonate")
        self.button_holder = document.querySelector("#shrine-window-buttons")
        self.confirm_button = document.querySelector("#shrine-window-confirm")
        self.cancel_button = document.querySelector("#shrine-window-cancel")
        self.drop_target = document.querySelector("#DonateDropTarget")
        self.favor_progress = document.querySelector("#shrine-window-favor")
        self.num_selector_container = document.querySelector("#shrine-window-qty")
        self.help_text = document.querySelector("#DonateHelp")
        self.item = {}

        self.populate_shrine_window()

        @MessageBus.subscribe("favorUpdate")
        def on_favor_update(favor_map: dict) -> None:
            self.favor = favor_map["favor"]
            self.max_favor = favor_map["maxFavor"]
            percent = int(100 * favor_map["favor"] / favor_map["maxFavor"])
            self._set_favor_progress(percent)

        self.setup_listeners()

    def setup_listeners(self) -> None:
        if self.confirm_listener:
            self.confirm_listener.cancel()
        if self.cancel_listener:
            self.cancel_listener.cancel()
        if self.plus_listener:
            self.plus_listener.cancel()
        if self.minus_listener:
            self.minus_listener.cancel()
        if self.max_listener:
            self.max_listener.cancel()

        def on_confirm_click(_):
            action_map = {
                "itemType": self.item["itemType"],
                "qty": int(self.num_box.value),
            }
            util.send_action("donate", self.shrine_id, action_map)
            self.reset_shrine_window()

        type(self).confirm_listener = util.add_cancelable_event_listener(self.confirm_button, "click", on_confirm_click)
        type(self).cancel_listener = util.add_cancelable_event_listener(self.cancel_button, "click", lambda _: self.reset_shrine_window())

        def on_plus_click(_):
            if int(self.num_box.value) + 1 > int(self.num_box.max):
                self.num_box.value = int(self.num_box.max)
            else:
                self.num_box.value = int(self.num_box.value) + 1
        type(self).plus_listener = util.add_cancelable_event_listener(self.plus_btn, "click", on_plus_click)

        def on_minus_click(_):
            if int(self.num_box.value) - 1 < int(self.num_box.min):
                self.num_box.value = float(self.num_box.min)
            else:
                self.num_box.value = int(self.num_box.value) - 1
        type(self).minus_listener = util.add_cancelable_event_listener(self.minus_btn, "click", on_minus_click)

        def on_max_click(_):
            self.num_box.value = int(self.num_box.max)
        type(self).max_listener = util.add_cancelable_event_listener(self.max_btn, "click", on_max_click)

    def close(self) -> None:
        util.send_action("close", self.shrine_id, {})
        super().close()

    def open(self, ignore_keys: bool = True) -> None:
        self.reset_shrine_window()
        self.populate_shrine_window()
        self.make_draggables()
        super().open(ignore_keys=ignore_keys)

    def reset_shrine_window(self) -> None:
        self.button_holder.style.visibility = "hidden"
        self.drop_target.style.backgroundImage = "none"
        self.help_text.innerText = f"Drop an item here from your inventory to donate it to {self.giant_name} for favor."
        self.num_selector_container.hidden = True
        self.item.clear()

    def populate_shrine_window(self) -> None:
        for placeholder in document.querySelectorAll(".insert-giantname"):
            placeholder.innerText = self.giant_name

        percent = int(100 * self.favor / self.max_favor)
        self._set_favor_progress(percent)

    def populate_qty_selector(self, item_type: str) -> None:
        self.num_box.setAttribute("max", str(util.Util().get_num_items(item_type)))
        self.num_box.value = 1

    def make_draggables(self) -> None:
        dropzone = Dropzone([self.drop_target])
        def on_drop(event):
            # verify it is a valid item before acting on it
            data = event.dataTransfer.getData("itemdef")
            if data is None:
                return

            self.item = json.loads(data)

            # check for non-empty bags
            if self.item.get("isContainer", False) and self.item.get("metadata", {}).get("slots"):
                for slot in json.loads(self.item["metadata"]["slots"]):
                    if slot["itemType"].strip() or slot.get("item") or slot["count"] > 0:
                        Toast("Empty this and try again!")
                        return

            self.button_holder.style.visibility = "visible"
            self.drop_target.style.backgroundImage = f"url({self.item['iconUrl']})"
            self.help_text.innerText = "Donate how many?"

            self.num_selector_container.hidden = False
            self.populate_qty_selector(self.item['itemType'])
        dropzone.on_drop(on_drop)

    def _set_favor_progress(self, percent: int) -> None:
        self.favor_progress.setAttribute("percent", str(percent))
        self.favor_progress.setAttribute("status", f"{self.favor} of {self.max_favor} favor towards an Emblem of {self.giant_name}")
