from pyscript import document
from pyodide.ffi import create_proxy

from display.windows.modal import Modal
from message_bus import MessageBus
from network.server_interop.class_defs import Quest


class QuestLogWindow(Modal):
    def __init__(self):
        self.id = "questLogWindow"
        super().__init__()

        self.prepare()
        self.list_of_quests = document.querySelector("#listOfQuests")
        self.quest_details = document.querySelector("#questDetails")

        MessageBus.subscribe(["questInProgress", "questUpdate"], lambda q: self._add_quest_to_list(q))
        MessageBus.subscribe("questBegin", lambda q: self._add_quest_to_list(q, quest_begin=True))
        MessageBus.subscribe("questComplete", lambda q: self._remove_quest_from_list(q))
        MessageBus.subscribe("questFail", lambda q: self._remove_quest_from_list(q))

        self.setup_ui_button(document.querySelector("#open-quests"))
        self.setup_key_binding("QuestLog")

    def _remove_quest_from_list(self, quest: Quest) -> None:
        if quest_element := self.list_of_quests.querySelector(f"#{quest.id}"):
            quest_element.remove()
        if quest_details_element := self.quest_details.querySelector(f'[data-quest-id="{quest.id}"]'):
            quest_details_element.remove()

        # if we aren't on any more quests, update the display to say so
        if not self.list_of_quests.children:
            self.display_element.classList.add("noquests")

    def _add_quest_to_list(self, quest: Quest, quest_begin: bool = False) -> None:
        # if it's already on the list, just update the details
        if self.list_of_quests.querySelector(f"#{quest.id}"):
            self.quest_details.replaceChildren()
            self.quest_details.appendChild(self._new_details(quest))
            return

        new_quest = self._new_quest(quest)
        self.list_of_quests.appendChild(new_quest)
        def on_click(_):
            for child in self.list_of_quests.children:
                child.classList.remove("selected")
            new_quest.classList.add("selected")
            self.quest_details.replaceChildren()
            self.quest_details.append(self._new_details(quest))
        new_quest.addEventListener("click", create_proxy(on_click))

        self.display_element.classList.remove("noquests")

        # if this is a brand new quest, open the window
        # and highlight the quest
        if quest_begin:
            self.open()
            new_quest.click()

    def _new_quest(self, quest: Quest):
        quest_e = document.createElement("li")
        quest_e.id = quest.id
        quest_e.innerText = quest.title
        quest_e.classList.add("activeQuest")
        return quest_e

    def _new_details(self, quest: Quest):
        # generate info box (for requirements and rewards)
        def _info_box(image_url: str = None, image_class: str = None, inline: bool = False, text: str|int = ""):
            if image_url:
                image_e = document.createElement("img")
                image_e.src = image_url
            else:
                image_e = document.createElement("div")
                image_e.classList.add("quest-info-icon")
                if image_class:
                    image_e.classList.add(image_class)

            text_e = document.createElement("span")
            text_e.innerText = str(text)

            box_e = document.createElement("div")
            box_e.append(image_e, text_e)
            if inline:
                box_e.classList.add("inline")

            return box_e

        # containers

        # completion requirements
        requirements_e = document.createElement("div")
        requirements_e.classList.add("quest-reqs")
        label = document.createElement("span")
        label.innerText = "Requirements"
        requirements_e.append(label)

        # completion rewards
        rewards_e = document.createElement("div")
        rewards_e.classList.add("quest-rewards")
        label = document.createElement("span")
        label.innerText = "Rewards"
        rewards_e.appendChild(label)

        # description text
        description_e = document.createElement("div")
        description_e.innerText = quest.description
        description_e.classList.add("questDescription")

        # details container (parent of all above)
        details_e = document.createElement("div")
        details_e.dataset.questId = quest.id
        details_e.append(description_e, requirements_e, rewards_e)

        # fill in requirements
        for requirement in quest.requirements:
            req_e = _info_box(image_url=requirement.icon_url, text=f"{requirement.num_fulfilled}/{requirement.num_required}")
            req_e.classList.add("questRequirement")
            req_e.title = requirement.text

            if requirement.fulfilled:
                req_e.classList.add("fulfilled")

            requirements_e.appendChild(req_e)

        # fill in rewards
        if quest.rewards.currants:
            rewards_e.appendChild(_info_box(image_class="currant", inline=True, text=quest.rewards.currants))
        if quest.rewards.energy:
            rewards_e.appendChild(_info_box(image_class="energy", inline=True, text=quest.rewards.energy))
        if quest.rewards.img:
            rewards_e.appendChild(_info_box(image_class="img", inline=True, text=quest.rewards.img))
        if quest.rewards.mood:
            rewards_e.appendChild(_info_box(image_class="mood", inline=True, text=quest.rewards.mood))
        for giant in quest.rewards.favor:
            if giant.fav_amt > 0:
                rewards_e.appendChild(_info_box(text=f"{giant.fav_amt} favor with {giant.giant_name.capitalize()}"))

        return details_e
