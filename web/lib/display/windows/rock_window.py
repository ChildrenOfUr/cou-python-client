import logging

from pyscript import document
from pyodide.ffi import create_proxy

from display.windows.modal import Modal
from display.windows.bag_window import BagWindow
from message_bus import MessageBus
from network.server_interop.class_defs import Conversation, ConvoChoice, QuestRewards
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)


class RockWindow(Modal):
    def __init__(self):
        self.id = "rockWindow"
        super().__init__()

        self.rescue_button = document.querySelector("#rock-rescue")
        self.quests_button = document.querySelector("#open-quests")
        self.quests_button.addEventListener("click", create_proxy(lambda _: cou_globals.window_manager.rock_window.close()))
        self.rescue_click = None
        self.ready = False

        self.prepare()

        # lock/unlock inventory on game load
        MessageBus.subscribe("gameloaded", lambda _: self._set_inventory_enabled(cou_globals.metabolics.energy != 0))

        # toggle window by clicking rock
        self.setup_ui_button(document.querySelector("#petrock"))

        # hehehe
        def on_mouse_enter(event):
            if event.altKey:
                document.querySelector("#petrock").classList.add("questlog")
        def on_mouse_leave(event):
            document.querySelector("#petrock").classList.remove("questlog")
        document.querySelector("#petrock").addEventListener("mouseenter", create_proxy(on_mouse_enter))
        document.querySelector("#petrock").addEventListener("mouseleave", create_proxy(on_mouse_leave))

        try:
            # define conversations
            self.init_convos()
            # trigger conversations
            self.set_convo_triggers()
        except Exception:
            LOGGER.exception("[UI] Could not load rock convos")

        self.ready = True
        MessageBus.publish("rockwindowready", self.ready)

    def init_convo(self, convo: str, user_triggered: bool = True) -> None:
        """Prepares a conversation for use. `convo` should not include rwc-,
        and user_triggered should be false if there is no UI button.
        """

        # set up the menu button listener
        if user_triggered:
            document.querySelector(f"#go-{convo}").addEventListener("click", create_proxy(lambda _: self.switch_content(f"rwc-{convo}")))

        # hide all but the first screen
        for element in document.querySelectorAll(f"#rwc-{convo} > div"):
            element.hidden = True
        document.querySelector(f"#rwc-{convo}-1").hidden = False

        # handle screen navigation
        for element in document.querySelectorAll(f"#rwc-{convo} .rwc-button"):
            def on_click(event):
                id = event.target.dataset.goto
                if id == document.querySelector(f"#rwc-{convo}").dataset.endphrase:
                    # last screen, close and return to the menu
                    super(type(self), self).close()
                    self.switch_content("rwc")
                    for element in document.querySelectorAll(f"#rwc-{convo} > div"):
                        element.hidden = True
                    document.querySelector(f"#rwc-{convo}-1").hidden = False
                else:
                    # go to the next screen
                    for element in document.querySelectorAll(f"#rwc-{convo} > div"):
                        element.hidden = True
                    document.querySelector(f"#rwc-{convo}-{id}").hidden = False
                    document.querySelector(f"#rwc-{convo}").scrollTop = 0

            element.addEventListener("click", create_proxy(on_click))

    def switch_content(self, id: str) -> None:
        """Switches the window to the given id (should include rwc-),
        but does NOT open the rock window (use open(); afterward)
        """

        def switch():
            for element in document.querySelectorAll(f"#rwc-holder .rockWindowContent"):
                element.hidden = True
            document.querySelector(f"#{id}").hidden = False

        if not self.ready:
            MessageBus.subscribe("rockwindowready", switch)
        else:
            switch()

    def set_convo_triggers(self) -> None:
        """Set up the services that trigger conversations..."""

        @MessageBus.subscribe("dead")
        def on_dead(dying: bool):
            # prevent the screen appearing on every hell street
            death_convo_done = False
            revive_convo_done = False

            @MessageBus.subscribe("streetLoaded")
            def on_change_street(_):
                nonlocal death_convo_done, revive_convo_done
                if dying and not death_convo_done:
                    # start death talk
                    self.switch_content("rwc-dead")
                    self.open()
                    # disable inventory
                    self._set_inventory_enabled(False)
                    # save state
                    death_convo_done = True
                if not dying and not revive_convo_done:
                    # start revival talk
                    self.switch_content("rwc-revive")
                    self.open()
                    # enable inventory
                    self._set_inventory_enabled(True)
                    # save state
                    revive_convo_done = True

        # when entering a broken street
        @MessageBus.subscribe("streetLoaded")
        def on_street_load(_):
            broken_hub = cou_globals.map_data.hubData.get(cou_globals.current_street.hub_id, {}).get("broken") == True
            broken_street = cou_globals.map_data.streetData.get(cou_globals.current_street.label, {}).get("broken") == True
            if broken_hub or broken_street:
                self.switch_content("rwc-badstreet")
                self.open()
                self.rescue_button.hidden = False
                async def rescue_click(_):
                    if cou_globals.current_street.hub_id == "40":  # Naraka
                        # dead => hell one
                        await cou_globals.street_service.request_street("LA5PPFP86NF2FOS")
                    else:
                        # not dead => Ilmenskie
                        await cou_globals.street_service.request_street("LIF102FDNU11314")
                    self.close()
                self.rescue_click = create_proxy(rescue_click)
                self.rescue_button.addEventListener("click", self.rescue_click)
            else:
                self.rescue_button.hidden = True
                if self.rescue_click:
                    self.rescue_button.removeEventListener("click", self.rescue_click)

    def _set_inventory_enabled(self, enabled: bool) -> None:
        document.querySelector("#inventory #disableinventory").hidden = enabled
        if not enabled:
            # close all bag windows
            for window in BagWindow.open_windows.values():
                window.close()

    def init_convos(self) -> None:
        """Set up the conversations"""
        # Triggered by user clicking menu options
        self.init_convo("start")
        self.init_convo("motd")

        # triggered by incoming message from server
        self.init_convo("dead", user_triggered=False)
        self.init_convo("revive", user_triggered=False)

        # triggered by going to a known problematic street
        self.init_convo("badstreet", user_triggered=False)

    def create_convo(self, convo: Conversation, rewards: QuestRewards = None) -> None:
        """Use this method to create a conversation dynamically
        After calling this method the conversation html will be added to the page
        and you should call switchContent(id) followed by open() on the rockwindow
        to display the conversation
        """

        # remove the conversation if it already exists
        if convo_element := document.querySelector(f"#rwc-{convo.id}"):
            convo_element.remove()

        conversation = document.createElement("div")
        conversation.classList.add("rockWindowContent", "convo")
        conversation.id = f"rwc-{convo.id}"
        conversation.dataset.endphrase = str(len(convo.screens) + 1)
        conversation.hidden = True

        for index, screen in enumerate(convo.screens, start=1):
            screen_e = document.createElement("div")
            screen_e.id = f"rwc-{convo.id}-{index}"

            for paragraph in screen.paragraphs:
                paragraph_e = document.createElement("p")
                paragraph_e.innerText = paragraph
                screen_e.appendChild(paragraph_e)

            # up to 2 choices per row
            choices = screen.choices[:]
            while choices:
                choice_row = document.createElement("div")
                choice_row.classList.add("flex-row", "rwc-exit")
                choice_row.appendChild(self._create_choice(choices.pop(0), convo.id.split("-")[0]))

                if choices:
                    choice_row.append(self._create_choice(choices.pop(0), convo.id.split("-")[0]))

                screen_e.appendChild(choice_row)

            if index == len(convo.screens) and rewards:
                screen_e.appendChild(self._create_rewards(rewards))

            conversation.appendChild(screen_e)
        document.querySelector("#rwc-holder").appendChild(conversation)
        self.init_convo(convo.id, user_triggered=False)

    def _create_rewards(self, rewards: QuestRewards):
        awards_e = document.createElement("div")
        awards_e.className = "awarded"
        if rewards.energy != 0:
            energy_e = document.createElement("span")
            energy_e.className = "energy"
            energy_e.innerText = f"+{rewards.energy}"
            awards_e.appendChild(energy_e)
        if rewards.mood != 0:
            mood_e = document.createElement("span")
            mood_e.className = "mood"
            mood_e.innerText = f"+{rewards.mood}"
            awards_e.appendChild(mood_e)
        if rewards.img != 0:
            img_e = document.createElement("span")
            img_e.className = "img"
            img_e.innerText = f"+{rewards.img}"
            awards_e.appendChild(img_e)
        if rewards.currants != 0:
            currants_e = document.createElement("span")
            currants_e.className = "currants"
            currants_e.innerText = f"+{rewards.currants}"
            awards_e.appendChild(currants_e)

        cb_content = document.createElement("div")
        cb_content.className = "cb-content"
        cb_content.appendChild(awards_e)
        return cb_content

    def _create_choice(self, choice: ConvoChoice, quest_id: str) -> None:
        choice_e = document.createElement("a")
        choice_e.className = "rwc-button"
        choice_e.dataset.goto = f"{choice.goto_screen}"
        choice_e.innerText = choice.text

        if choice.is_quest_accept:
            self._create_choice_listener(choice_e, "acceptQuest", quest_id)
        if choice.is_quest_reject:
            self._create_choice_listener(choice_e, "rejectQuest", quest_id)

        return choice_e

    def _create_choice_listener(self, choice_e, type: str, quest_id: str) -> None:
        def on_click(_):
            map = {
                type: "true",
                "email": cou_globals.game.email,
                "id": quest_id,
            }
            MessageBus.publish("questChoice", map)
        choice_e.addEventListener("click", create_proxy(on_click))
