import logging

from pyscript import document
from pyodide.ffi import create_proxy

from configs import Configs
from display.chat_message import get_cached_color_from_username
from message_bus import MessageBus
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)


class Minimap:
    def __init__(self) -> None:
        self.collapsed = False
        self.current_street_exits = []
        self.main_img_url = None
        self.loading_img_url = None
        self.current_player = None
        self.player_transforms = {}

        self.container_e = document.querySelector("#minimap-container")
        self.image_e = document.querySelector("#minimap-img")
        self.objects_e = document.querySelector("#minimap-objects")
        self.label_e = document.querySelector("#minimap-label")
        self.toggle_e = document.querySelector("#minimap-toggle")

        self.factor_width = 0
        self.factor_height = 0

        def on_toggle(event):
            if self.collapsed:
                self.expand()
            else:
                self.collapse()
        self.toggle_e.addEventListener("click", create_proxy(on_toggle))

        def on_load(street: dict):
            self.factor_width = 0
            self.factor_height = 0
            self.player_transforms = {}
            self._change_street(street)

            # # enable/disable expanding
            # collapsed_height = street["loading_image"]["h"] / cou_globals.current_street.bounds.height
            # expanded_height = street["main_image"]["h"] / cou_globals.current_street.bounds.height
            # if collapsed_height < expanded_height or cou_globals.map_data.get_minimap_expand_override(street["label"]):
            #     # street is taller than it is wide (or overridden), allow expansion
            #     self.toggleE.hidden = False
            #     self.collapse()
            # elif collapsed_height > expanded_height:
            #     # street is wider than it is tall, disallow expansion
            #     self.toggleE.hidden = True
            #     self.expand()
            self.expand()
        MessageBus.subscribe("streetLoaded", on_load)

    def collapse(self) -> None:
        self.image_e.src = self.loading_img_url
        def on_load(event):
            self.objects_e.hidden = True
            self.toggle_e.querySelector("i.fa").classList.remove("fa-chevron-up")
            self.toggle_e.querySelector("i.fa").classList.add("fa-chevron-down")
            self.collapsed = True
        self.image_e.addEventListener("load", create_proxy(on_load), {"once": True})

    def expand(self) -> None:
        self.image_e.src = self.main_img_url
        def on_load(event):
            self.objects_e.hidden = False
            self.toggle_e.querySelector("i.fa").classList.remove("fa-chevron-down")
            self.toggle_e.querySelector("i.fa").classList.add("fa-chevron-up")
            self.collapsed = False
        self.image_e.addEventListener("load", create_proxy(on_load), {"once": True})

    def _change_street(self, street: dict) -> None:
        self.main_img_url = Configs.proxyStreetImage(street["main_image"]["url"])
        self.loading_img_url = Configs.proxyStreetImage(street["loading_image"]["url"])
        self.image_e.src = self.main_img_url
        self.label_e.innerText = cou_globals.current_street.label
        def on_load(event):
            self.objects_e.style.width = f"{self.image_e.width}px"
            self.objects_e.style.height = f"{self.image_e.height}px"
        self.image_e.addEventListener("load", create_proxy(on_load), {"once": True})
        if cou_globals.map_data.get_minimap_expand_override():
            self.objects_e.hidden = True
        else:
            self.objects_e.hidden = False

    def _set_factor(self) -> None:
        street_size = cou_globals.current_street.bounds
        minimap_width = self.image_e.clientWidth
        minimap_height = self.image_e.clientHeight
        street_width = street_size.width
        street_height = street_size.height
        self.factor_width = minimap_width / street_width
        self.factor_height = minimap_height / street_height

    def _add_signs(self) -> None:
        for sign in self.objects_e.querySelectorAll(".minimap-exit"):
            sign.remove()

        for data in self.current_street_exits:
            title = ""
            for index, name in enumerate(data["streets"]):
                if index != len(data["streets"]) - 1:
                    title += name + "\n"
                else:
                    title += name

            exit = document.createElement("div")
            exit.classList.add("minimap-exit")
            exit.style.top = f"{data['y'] * self.factor_height - 6}px"
            exit.style.left = f"{data['x'] * self.factor_width - 4}px"
            exit.title = title

            self.objects_e.appendChild(exit)

    def update_objects(self) -> None:
        if self.current_player is None:
            self.current_player = getattr(cou_globals, "current_player", None)
            if self.current_player is None:
                return

        if self.factor_width == 0 or self.factor_height == 0:
            self._set_factor()
            self._add_signs()

        # other players
        for name, this_player in cou_globals.other_players.items():
            if not (player := self.objects_e.querySelector(f"#{name}")):
                color = get_cached_color_from_username(name)
                if not color:
                    continue
                player = document.createElement("div")
                player.classList.add("minimap-player")
                player.id = name
                player.title = name
                player.style.backgroundColor = color
                self.objects_e.appendChild(player)
            translate_x = this_player.left * self.factor_width
            translate_y = this_player.top * self.factor_height - 6
            if self.player_transforms.get(name) != (translate_x, translate_y):
                player.style.transform = f"translateX({translate_x}px) translateY({translate_y}px)"
                self.player_transforms[name] = (translate_x, translate_y)

        # current player
        if not (me := self.objects_e.querySelector(".minimap-me")):
            color = get_cached_color_from_username(cou_globals.game.username)
            if not color:
                return
            me = document.createElement("div")
            me.classList.add("minimap-player")
            me.classList.add("minimap-me")
            me.title = cou_globals.game.username
            me.style.backgroundColor = color
            self.objects_e.appendChild(me)
        translate_x = self.current_player.left * self.factor_width
        translate_y = self.current_player.top * self.factor_height - 6
        if self.player_transforms.get("minimap-me") != (translate_x, translate_y):
            me.style.transform = f"translateX({translate_x}px) translateY({translate_y}px)"
            self.player_transforms["minimap-me"] = (translate_x, translate_y)

    def show_camera_rect(self) -> None:
        if not (camera_rect := self.objects_e.querySelector("#camera_debug")):
            camera_rect = document.createElement("div")
            camera_rect.id = "camera_debug"
            camera_rect.className = "minimap-camera-debug"
            self.objects_e.appendChild(camera_rect)
        translate_x = cou_globals.camera.visible_rect.left * self.factor_width
        translate_y = cou_globals.camera.visible_rect.top * self.factor_height
        width = cou_globals.camera.visible_rect.width * self.factor_width
        height = cou_globals.camera.visible_rect.height * self.factor_height
        camera_rect.style.transform = f"translateX({translate_x}px) translateY({translate_y}px)"
        camera_rect.style.width = f"{width}px"
        camera_rect.style.height = f"{height}px"

    def hide_camera_rect(self) -> None:
        if camera_rect := self.objects_e.querySelector("#camera_debug"):
            camera_rect.remove()
