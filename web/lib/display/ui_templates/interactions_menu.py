from pyscript import document
from pyodide.ffi import create_proxy

from game.entities.entity import get_entity_element, get_entity
from message_bus import MessageBus
from systems.global_gobbler import cou_globals
from systems import util


class InteractionWindow:
    shrine_types = [
        "Alph", "Cosma", "Friendly", "Grendaline", "Humbaba", "Lem", "Mab", "Pot", "Spriggan", "Tii", "Zille"
    ]

    @classmethod
    def create(cls):
        interaction_window = document.createElement("div")
        interaction_window.id = "InteractionWindow"
        interaction_window.className = "interactionWindow"

        header = document.createElement("div")
        header.classList.add("PopWindowHeader", "handle")
        title = document.createElement("span")
        title.id = "InteractionTitle"
        title.innerText = "Interact with..."
        header.appendChild(title)

        content = document.createElement("div")
        content.id = "InteractionContent"

        interaction_window.appendChild(header)
        interaction_window.appendChild(content)

        items_in_bubble = []

        for id in cou_globals.current_player.intersecting_objects:
            container = document.createElement("div")
            container.style.display = "inline-block"
            container.style.textAlign = "center"
            container.classList.add("entityContainer")
            entity_on_street = get_entity_element(id)
            entity_in_bubble = document.createElement("img")

            if "pole" in entity_on_street.id:
                # Signpost image already loaded
                entity_in_bubble.src = "files/system/icons/signpost.svg"
            elif entity_on_street.tagName.lower() == "canvas":
                # provide static image for entities with states
                entity_name = entity_on_street.getAttribute("type")
                if "Street Spirit" in entity_name:
                    entity_in_bubble.src = "files/system/icons/currant.svg"
                elif entity_name in cls.shrine_types:
                    entity_in_bubble.src = "files/system/icons/shrine.svg"
                else:
                    entity_in_bubble.src = f"files/staticEntityImages/{entity_name}.png"
            elif entity_on_street.tagName.lower() == "img":
                # Dropped item, use it's image
                entity_in_bubble.src = entity_on_street.src
                if entity_on_street.getAttribute("itemtype") in items_in_bubble:
                    # limit the bubbles to just show 1 of each item type
                    continue
                items_in_bubble.append(entity_on_street.getAttribute("itemtype"))
            elif entity_on_street.classList.contains("playerParent"):
                entity_in_bubble.src = "files/staticEntityImages/Player.png"

            # Find a title
            if entity_on_street.getAttribute("type") is not None:
                container.title = entity_on_street.getAttribute("type") or entity_on_street.id

                if getattr(entity_on_street.dataset, "nameOverride", None):
                    container.title += f" named {entity_on_street.dataset.nameOverride}"

            # use the same id
            entity_in_bubble.setAttribute("id", id)
            entity_in_bubble.classList.add("entityInBubble")
            get_entity(id).multi_unselect = True

            # insert entity into bubble
            container.appendChild(entity_in_bubble)

            #select entities with mouse
            def on_mouse_over(event):
                for child in content.children:
                    if child != container:
                        get_entity(child.children[0].id).multi_unselect = True
                        child.classList.remove("entitySelected")
                get_entity(id).multi_unselect = False
                container.classList.add("entitySelected")
            container.addEventListener("mouseover", create_proxy(on_mouse_over))

            # use selected entity
            def on_click(event):
                event.stopPropagation()
                cou_globals.input_manager.stopMenu(interaction_window)
                get_entity(id).interact(id)
            util.add_event_once(container, "click", on_click)

            # entity has no available actions
            try:
                async def check_available_actions(container=container):
                    all_disabled = True
                    for action in await get_entity(id).get_actions():
                        if action.enabled:
                            all_disabled = False
                            break

                    # display
                    content.appendChild(container)

                    get_entity(content.children[0].children[0].id).multi_unselect = False
                    content.children[0].classList.add("entitySelected")

                    # street signs are (probably) never disabled
                    if all_disabled and not "pole" in entity_on_street.id:
                        container.classList.add("disabled")
                util.create_task(check_available_actions())
            except Exception:
                # item was not an entity, so it doesn't matter
                pass

        def on_key_down(event):
            keys = cou_globals.input_manager.keys
            ignore_keys = cou_globals.input_manager.ignoreKeys

            # up arrow or w and not typing
            if event.keyCode in (keys["UpBindingPrimary"], keys["UpBindingAlt"]) and not ignore_keys:
                cou_globals.input_manager.stopMenu(interaction_window)

            # down arrow or s and not typing
            if event.keyCode in (keys["DownBindingPrimary"], keys["DownBindingAlt"]) and not ignore_keys:
                cou_globals.input_manager.stopMenu(interaction_window)

            # left arrow or a and not typing
            if event.keyCode in (keys["LeftBindingPrimary"], keys["LeftBindingAlt"]) and not ignore_keys:
                cou_globals.input_manager.selectUp(content.querySelectorAll(".entityContainer"), "entitySelected")
                for entity in document.querySelectorAll(".entityInBubble:not(.entitySelected)"):
                    get_entity(entity.id).multi_unselect = True
                get_entity(document.querySelector(".entitySelected").children[0].id).multi_unselect = False

            # right arrow or d and not typing
            if event.keyCode in (keys["RightBindingPrimary"], keys["RightBindingAlt"]) and not ignore_keys:
                cou_globals.input_manager.selectDown(content.querySelectorAll(".entityContainer"), "entitySelected")
                for entity in document.querySelectorAll(".entityInBubble:not(.entitySelected)"):
                    get_entity(entity.id).multi_unselect = True
                get_entity(document.querySelector(".entitySelected").children[0].id).multi_unselect = False

            # spacebar and not typing
            if event.keyCode in (keys["JumpBindingPrimary"], keys["JumpBindingAlt"]) and not ignore_keys:
                cou_globals.input_manager.stopMenu(interaction_window)

            # enter and not typing
            if event.keyCode in (keys["ActionBindingPrimary"], keys["ActionBindingAlt"]) and not ignore_keys:
                cou_globals.input_manager.stopMenu(interaction_window)
                id = content.querySelector(".entitySelected").children[0].getAttribute("id")
                get_entity(id).interact(id)
        handler = create_proxy(on_key_down)
        cou_globals.input_manager.menuKeyListener = ("keydown", handler)
        document.addEventListener("keydown", handler)

        def on_key_up(event):
            if event.keyCode == 27:
                cou_globals.input_manager.stopMenu(interaction_window)
        document.addEventListener("keyup", create_proxy(on_key_up))

        document.addEventListener("click", create_proxy(lambda _: cou_globals.input_manager.stopMenu(interaction_window)))

        # when the menu is closed, let's allow all entities to glow like normal again
        def on_menu_stop(_):
            for entity in cou_globals.entities.values():
                entity.multi_unselect = False
        MessageBus.subscribe("menuStopping", on_menu_stop)

        return interaction_window

    @classmethod
    def destroy():
        document.querySelector("#InteractionWindow").remove()
