import re

from pyscript import document
from pyodide.ffi import create_proxy

from display.ui_templates.how_many_menu import HowManyMenu
from network.server_interop.inventory import Slot
from systems import util
from systems.global_gobbler import cou_globals


class ItemChooser:
    def __init__(self, title: str, callback: callable, filter: str = "name=*") -> None:
        self.callback = callback
        self.keyboard_listener = None

        dialog = document.createElement("div")
        dialog.id = "itemChooser"

        title_e = document.createElement("div")
        title_e.className = "title"
        title_e.innerText = title

        dialog.appendChild(title_e)

        item_holder = document.createElement("div")
        item_holder.className = "itemHolder"

        self._add_items(cou_globals.player_inventory.slots, filter, item_holder)

        dialog.appendChild(item_holder)

        def on_key_up(event):
            if event.keyCode == 27:
                self.destroy()
        self.keyboard_listener = util.add_cancelable_event_listener(document, "keyup", on_key_up)

        # append it to the game so that we get centered within it
        document.querySelector("#game").appendChild(dialog)

    def _add_items(self, slots: list[Slot], filters: str, item_holder, super_slot_index: int = None) -> None:
        added_types: list[str] = []

        for index, slot in enumerate(slots):
            item = slot.item
            if item is None:
                continue
            if item.is_container:
                bag_slots = Slot.from_json(item.metadata["slots"], many=True)
                self._add_items(bag_slots, filters, item_holder, super_slot_index=index)

            no_match = False
            for filter in filters.split("|||"):
                field, value = filter.split("=")
                if not re.match(value, item.to_json()[field], re.IGNORECASE):
                    no_match = True
                    break
            if no_match:
                continue

            added_types.append(item.item_type)

            item_e = document.createElement("div")
            item_e.className = "itemChoice"
            item_e.style.backgroundImage = f"url({item.icon_url})"
            item_e.title = item.metadata.get("title", item.name)
            def on_click(event, index=index, item=item):
                def action(how_many: int = 1):
                    real_slot_index = super_slot_index or index
                    real_sub_slot_index = index if super_slot_index is not None else -1
                    self._do_action(item.item_type, real_slot_index, real_sub_slot_index, how_many=how_many)
                HowManyMenu.create(event, "", util.Util().get_num_items(item.item_type), action, item_name=item.name)
            item_e.addEventListener("click", create_proxy(on_click))

            item_holder.appendChild(item_e)

    def _do_action(self, item_type: str, slot: int, sub_slot: int, how_many: int = 1) -> None:
        self.destroy()
        self.callback(item_type=item_type, count=how_many, slot=slot, sub_slot=sub_slot)

    def destroy(self) -> None:
        if self.keyboard_listener:
            self.keyboard_listener.cancel()
        document.querySelector("#itemChooser").remove()
