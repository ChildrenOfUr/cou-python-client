from pyscript import document
from js import MouseEvent
from pyodide.ffi import create_proxy

from systems.global_gobbler import cou_globals


class MenuKeys:
    # Maps keys to valid keycodes
    # Use int for single binding, and list[int] for multiple (number row THEN numpad)
    KEY_CODES: dict[str, int | list[int]] = {
		"A": 65,
		"B": 66,
		"C": 67,
		"D": 68,
		"E": 69,
		"F": 70,
		"G": 71,
		"H": 72,
		"I": 73,
		"J": 74,
		"K": 75,
		"L": 76,
		"M": 77,
		"N": 78,
		"O": 79,
		"P": 80,
		"Q": 81,
		"R": 82,
		"S": 83,
		"T": 84,
		"U": 85,
		"V": 86,
		"W": 87,
		"X": 88,
		"Y": 89,
		"Z": 90,
		"1": [
			49,
			97
		],
		"2": [
			50,
			98
		],
		"3": [
			51,
			99
		],
		"4": [
			52,
			100
		],
		"5": [
			53,
			101
		],
		"6": [
			54,
			102
		],
		"7": [
			55,
			103
		],
		"8": [
			56,
			104
		],
		"9": [
			57,
			105
		],
		"0": [
			48,
			97
		]
    }

    @classmethod
    def key_with_code(cls, key_code: int) -> str:
        """Returns the key with the given keycode"""
        for key, codes in cls.KEY_CODES.items():
            if isinstance(codes, int) and codes == key_code:
                return key
            elif isinstance(codes, list):
                if codes[0] == key_code:
                    return key

        return ""

    @classmethod
    def key_code_matches(cls, key_code: int, key: int | list[int]) -> bool:
        """Returns if the keycode provided will trigger the given key"""

        key = str(key).upper()

        if isinstance(cls.KEY_CODES[key], int):
            # keycode matches? (1 key :: 1 keycode)
            return cls.KEY_CODES[key] == key_code
        elif isinstance(cls.KEY_CODES[key], list):
            # keycode matches? (1 key :: multiple keycodes)
            return len([code for code in cls.KEY_CODES[key] if code == key_code]) > 0
        else:
            return False

    # List of keyboard listeners (one for each menu item)
    _listeners: list[callable] = []

    @classmethod
    def clear_listeners(cls) -> None:
        # Cancel all listeners to prevent future duplication
        for listener in cls._listeners:
            document.removeEventListener("keydown", listener)
        # Remove all items to allow for garbage collection
        cls._listeners.clear()

    @classmethod
    def add_listener(cls, index: int, callback: callable) -> None:
        def handle_key_down(event):
            if cls.key_code_matches(event.keyCode, index):
                # stop listening for the keys after the menu is gone
                cls.clear_listeners()
                # run the callback
                callback()
        handler = create_proxy(handle_key_down)
        cls._listeners.append(handler)
        document.addEventListener("keydown", handler)

    @classmethod
    def inv_slots_listener(cls) -> None:
        def handle_key_down(event):
            if cou_globals.input_manager.ignoreKeys or len(cls._listeners) != 0:
                # One of the following is true:
                # 1. The user is focused in a text entry or a window
                # 2. A menu is open and the numbers are preoccupied
                # Either way, don't open the mnu and let them type
                return

            # get the number pushed
            try:
                # inv is 0-indexed, so subtract 1 from the key
                index = int(cls.key_with_code(event.keyCode)) - 1
                if index == -1:
                    # 0 is the last slot
                    index = 9
            except ValueError:
                # letter pressed, not a number
                return

            # get the box with that index
            box = next(iter(box for box in cou_globals.view.inventory.querySelectorAll(".box") if box.dataset.slotNum == str(index)))
            if box.querySelector(".inventoryItem") is None:
                # no item in slot?
                return

            if event.shiftKey and box.querySelector(".item-container-toggle") is not None:
                # open container if shift is pressed
                box.querySelector(".item-container-toggle").click()
            else:
                # click the box
                # note: dart has the documentOffset property: https://github.com/jquery/jquery/blob/d0ce00cdfa680f1f0c38460bc51ea14079ae8b07/src/offset.js#L83
                #       which says it is like jquery's offset: https://github.com/jquery/jquery/blob/d0ce00cdfa680f1f0c38460bc51ea14079ae8b07/src/offset.js#L83
                #       so reimplementing that here
                rect = box.getBoundingClientRect()
                win = box.ownerDocument.defaultView
                document_x = rect.left + win.pageXOffset
                document_y = rect.top + win.pageYOffset
                x = document_x + (box.clientWidth // 2)
                y = document_y + (box.clientHeight // 2)
                event = MouseEvent.new("contextmenu", clientX=x, clientY=y)
                box.querySelector(".inventoryItem").dispatchEvent(event)

        document.addEventListener("keydown", create_proxy(handle_key_down))
