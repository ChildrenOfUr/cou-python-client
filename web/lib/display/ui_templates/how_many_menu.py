from datetime import timedelta
import inspect

from pyscript import document
from pyodide.ffi import create_proxy

from systems.global_gobbler import cou_globals
from systems import util


class HowManyMenu:
    key_listener = None
    number = None
    enter = None
    max_val = None
    value = 1
    action_string = None
    typed_string = ""
    box_focused = False

    @classmethod
    def create(cls, click, action: str, max: int, callback: callable, item_name: str = "") -> None:
        cls.destroy()

        if action:
            action = action[0].upper() + action[1:]

        num_items = 1
        cls.max_val = max
        cls.action_string = action

        plural_item_name = item_name
        if not plural_item_name.endswith("s"):
            plural_item_name += "s"

        menu = document.createElement("div")
        menu.id = "HowManyMenu"
        title = document.createElement("span")
        title.id = "hm-title"
        title.innerText = f"{action} how many {plural_item_name}?"
        controls = document.createElement("div")
        controls.id = "hm-controls"
        minus = document.createElement("button")
        minus.id = "hm-minus"
        minus.classList.add("hm-btn")
        minus.innerText = "-"
        plus = document.createElement("button")
        plus.id = "hm-plus"
        plus.classList.add("hm-btn")
        plus.innerText = "+"
        number = document.createElement("input")
        number.id = "hm-num"
        number.value = str(num_items)
        enter = document.createElement("button")
        enter.id = "hm-enter"
        enter.classList.add("hm-btn")
        enter.innerText = f"{action} {num_items}"

        cls.number = number
        cls.enter = enter

        cou_globals.input_manager.ignoreKeys = True

        # do stuff
        plus.addEventListener("click", create_proxy(lambda _: cls._set_value(cls.value + 1)))
        minus.addEventListener("click", create_proxy(lambda _: cls._set_value(cls.value - 1)))
        util.add_event_once(enter, "click", create_proxy(lambda _: cls._do_verb(int(number.value), callback)))

        def on_focus(_):
            cls.box_focused = True
        def on_blur(_):
            cls.box_focused = False
        number.addEventListener("focus", create_proxy(on_focus))
        number.addEventListener("blur", create_proxy(on_blur))

        # 1..9 in number row
        num_codes = [49, 50, 51, 52, 53, 54, 55, 56, 57]

        def add_key_handlers():
            def on_key_down(event):
                # 27 == esc key
                if event.keyCode == 27:
                    event.stopPropagation()
                    cls.destroy()

                    # see if there's another window that we want to focus
                    for modal in document.querySelectorAll(".window"):
                        if not modal.hidden:
                            if modal.id in cou_globals.modals:
                                cou_globals.modals[modal.id].focus()

                # 13 == enter key
                if event.keyCode == 13:
                    event.stopPropagation()
                    cls._do_verb(int(cls.number.value), callback)

                # number row
                if event.keyCode in num_codes:
                    if cls.box_focused:
                        return
                    event.stopPropagation()
                    cls.typed_string = f"{cls.typed_string}{num_codes.index(event.key_code) + 1}"
                    cls._set_value(int(cls.typed_string))

                # backspace
                if event.keyCode == 8:
                    event.stopPropagation()
                    if cls.box_focused:
                        return
                    cls.typed_string = cls.typed_string[:-1]
                    if len(cls.typed_string) == 0:
                        cls._set_value(1)
                    else:
                        cls._set_value(int(cls.typed_string))

                # up arrow or w
                if event.keyCode in (cou_globals.input_manager.keys["UpBindingPrimary"], cou_globals.input_manager.keys["UpBindingAlt"]):
                    cls._set_value(cls.value + 1)
                # down arrow or s
                if event.keyCode in (cou_globals.input_manager.keys["DownBindingPrimary"], cou_globals.input_manager.keys["DownBindingAlt"]):
                    cls._set_value(cls.value - 1)

            cls.key_listener = util.add_cancelable_event_listener(document, "keydown", on_key_down)
        # wait a little so that [enter] used to prompt this window doesn't count for it too
        util.Timer(timedelta(milliseconds=100), add_key_handlers)

        controls.appendChild(minus)
        controls.appendChild(number)
        controls.appendChild(plus)
        menu.appendChild(title)
        menu.appendChild(controls)
        menu.appendChild(enter)

        document.querySelector("#windowHolder").appendChild(menu)

        x = click.pageX
        y = click.pageY

        menu.style.position = "absolute"
        menu.style.top = f"{y}px"
        menu.style.left = f"{x}px"
        menu.style.transform = "translate(-50%, -100%)"

        if x == 0 and y == 0:
            # the click was probably fake so let's just center it
            menu.style.top = "50%"
            menu.style.left = "50%"

    @classmethod
    def _set_value(cls, new_value: int) -> None:
        cls.value = max(0, min(new_value, cls.max_val))
        cls.number.value = str(cls.value)
        cls.enter.innerText = f"{cls.action_string} {cls.value}"

    @classmethod
    def _do_verb(cls, count: int, callback: callable) -> None:
        cls.destroy()
        count = max(0, min(count, cls.max_val))
        if count > 0:
            if inspect.iscoroutinefunction(callback):
                util.create_task(callback(how_many=count))
            else:
                callback(how_many=count)

    @classmethod
    def destroy(cls) -> None:
        cou_globals.input_manager.ignoreKeys = False
        if cls.key_listener:
            cls.key_listener.cancel()
        cls.typed_string = ""
        if menu := document.querySelector("#HowManyMenu"):
            menu.remove()
        cls.value = 1
