from datetime import timedelta
from random import randint
from typing import Any, Callable

import js as context
from pyscript import document
from pyodide.ffi import create_proxy

from display.toast import Toast
from display.ui_templates.how_many_menu import HowManyMenu
from display.ui_templates.menu_keys import MenuKeys
from game.actionbubble import ActionBubble
from message_bus import MessageBus
from systems import util
from systems.global_gobbler import cou_globals


class RightClickMenu:
    ACTION_LIST_PARENT_CLASS = 'RCActionList'
    MENU_CLASS = 'RightClickMenu'
    MENU_INFO_CLASSES = 'InfoButton fa fa-info-circle'
    MENU_TITLE_CLASS = 'ClickTitle'
    MENU_VISIBLE_OPACITY = '1.0'
    OPTION_BUTTON_CLASS = 'RCItem'
    OPTION_DISABLED_CLASS = 'RCItemDisabled'
    OPTION_SELECTED_CLASS = 'RCItemSelected'
    OPTION_TOOLTIP_CLASS = 'action_error_tooltip'
    OPTION_WRAPPER_CLASS = 'action_wrapper'
    _waiting = False

    @classmethod
    def opening(cls, delay: int = 0) -> None:
        cls._waiting = True

        def _add_wait():
            if cls._waiting:
                document.body.classList.add("wait")
        util.Timer(timedelta(seconds=delay), _add_wait)

    @classmethod
    def opened(cls) -> None:
        cls._waiting = False
        document.body.classList.remove("wait")

    @classmethod
    def create3(
        cls, click, title: str, entity_id: str, server_class: str = None, actions: list = None,
        on_info: Callable = None, item = None, item_name: str = None,
        ):
        def _position_menu(menu) -> tuple[int, int]:
            if click is not None:
                # position at cursor
                x = click.clientX - (menu.clientWidth // 2)
                y = click.clientY - (40 + len(actions) * 30)
            else:
                # position at player

                player_x = cou_globals.current_player.left
                player_y = cou_globals.current_player.top

                player_width = cou_globals.current_player.width
                player_height = cou_globals.current_player.height

                translate_x = player_x
                translate_y = cou_globals.view.worldElement.clientHeight - player_height

                street_width = cou_globals.current_street.bounds.width
                street_height = cou_globals.current_street.bounds.height

                world_height = cou_globals.view.worldElement.clientHeight
                world_width = cou_globals.view.worldElement.clientWidth

                if player_x > street_width - player_width / 2 - world_width / 2:
                    translate_x = player_x - street_width + world_width
                elif player_x + player_width / 2 > world_width / 2:
                    translate_x = world_width / 2 - player_width / 2

                if player_y + player_height / 2 < world_height / 2:
                    translate_y = player_y
                elif player_y < street_height - player_height / 2 - world_height / 2:
                    translate_y = world_height / 2 - player_height / 2
                else:
                    translate_y = world_height - (street_height - player_y)

                x = int(translate_x + menu.clientWidth + 10)
                y = int(translate_y + player_height / 2)
            return x, y

        def _make_options(menu) -> list:
            options = []

            # keyboard selection
            use_keys = len(actions) <= 10
            key_index = 1

            # create option elements
            for action in actions:
                # option tooltip
                option_tooltip = document.createElement("div")
                option_tooltip.className = cls.OPTION_TOOLTIP_CLASS

                # option button
                option = document.createElement("div")
                option.className = cls.OPTION_BUTTON_CLASS
                option.innerText = f"{str(key_index) + ': ' if use_keys else ''}{action.action_name}"

                # option element wrapper (option + tooltip)
                option_wrapper = document.createElement("div")
                option_wrapper.className = cls.OPTION_WRAPPER_CLASS
                option_wrapper.append(option)
                option_wrapper.append(option_tooltip)

                # register keyboard shortcut listener
                if use_keys:
                    def _menu_key_listener(option=option):
                        # When this option's number key is pressed
                        if click is not None:
                            # Recreate mouse click
                            option.dispatchEvent(context.MouseEvent.new("click", clientX=click.clientX, clientY=click.clientY))
                        else:
                            # Simulate mouse click
                            option.click()
                    MenuKeys.add_listener(key_index, _menu_key_listener)

                if action.enabled:
                    # Attach click listeners to enable actions
                    async def _do_action(event, action=action):
                        nonlocal item_name
                        function_name = action.action_name.lower()

                        async def do_click(how_many: int = 1) -> None:
                            cls.destroy_all()

                            # Wait for actions to complete
                            if action.time_required > 0:
                                action_bubble = ActionBubble.withAction(action)
                                if not await action_bubble.wait():
                                    return

                            arguments: dict[str, Any] = {}

                            if action.drop_map is not None:
                                arguments = action.drop_map

                            # Accepts multiple items
                            if action.multi_enabled:
                                arguments["count"] = how_many

                            if function_name == "pickup" and how_many > 1:
                                # Picking up multiple items
                                objects = [
                                    id for id in cou_globals.current_player.intersecting_objects
                                    if document.querySelector(f"#{id}").getAttribute("name") == (item_name if item_name else item.name)
                                ]
                                arguments["pickupIds"] = objects
                                arguments.pop("count", None)
                                util.send_global_action(function_name, arguments)
                            elif server_class == "global_action_monster":
                                util.send_global_action(function_name, {"player": entity_id})
                            else:
                                # Other action
                                if item is not None:
                                    arguments["itemdata"] = item.metadata

                                util.send_action(function_name, entity_id, arguments)

                        if action.multi_enabled:
                            max = 0
                            item_to_count = ""
                            if action.item_requirements is not None:
                                if action.item_requirements.all is not None:
                                    item_to_count = next(iter((action.item_requirements.all.keys())), "")
                            if item_to_count:
                                max = cou_globals.util.get_num_items(item_to_count)
                                item_name = item_to_count

                            if item is not None:
                                slot = sub_slot = -1

                                if action.drop_map is not None:
                                    slot = action.drop_map["slot"]
                                    sub_slot = action.drop_map["subSlot"]

                                # Count in inventory
                                max = cou_globals.util.get_num_items(item.item_type, slot=slot, sub_slot=sub_slot)

                            # Picking up an item
                            if function_name == "pickup":
                                # Count on ground
                                max = len([
                                    id for id in cou_globals.current_player.intersecting_objects
                                    if document.querySelector(f"#{id}").getAttribute("name") == item_name
                                ])

                            if max == 1:
                                # Don't show the how many menu if there is only one item
                                await do_click()
                            else:
                                # Open the how many menu
                                HowManyMenu.create(event, function_name, max, do_click, item_name=(item_name if item_name else item.name))
                        else:
                            await do_click()
                    option.addEventListener("click", create_proxy(_do_action))

                    # Select option with mouse
                    option.addEventListener("mouseover", create_proxy(lambda event: event.target.classList.add(cls.OPTION_SELECTED_CLASS)))

                    # Deselect option with mouse
                    option.addEventListener("mouseout", create_proxy(lambda event: event.target.classList.remove(cls.OPTION_SELECTED_CLASS)))
                else:
                    # mark disabled options
                    option.classList.add(cls.OPTION_DISABLED_CLASS)
                    def _show_action_error(event, action=action):
                        if action.error is not None:
                            Toast(action.error)
                    option.addEventListener("click", create_proxy(_show_action_error))

                # Initialize tooltip
                cls.show_action_error(option_tooltip, action.error)

                # Add element to menu
                options.append(option_wrapper)

                # Increment keyboard shortcut number
                key_index += 1

            if len(options) == 1:
                # Pre-select only option
                options[0].children[0].classList.add(cls.OPTION_SELECTED_CLASS)
            elif len(options) > 1:
                # Pre-select option, and wrap around controls
                def _handle_arrow_keys(event):
                    if event.keyCode == 40:
                        # Down arrow
                        options[0].children[0].classList.toggle(cls.OPTION_SELECTED_CLASS)
                    elif event.keyCode == 38:
                        # Up arrow
                        options[0].children[len(actions)].classList.toggle(cls.OPTION_SELECTED_CLASS)
                menu.addEventListener("keypress", create_proxy(_handle_arrow_keys))

            return options

        cls.opening()

        # Close any other menu
        cls.destroy_all()

        # Title display
        title_text = document.createElement("span")
        title_text.className = cls.MENU_TITLE_CLASS
        title_text.innerText = title

        # Info button
        info_button = document.createElement("div")
        info_button.className = cls.MENU_INFO_CLASSES
        info_button.hidden = on_info is None
        info_button.addEventListener("click", create_proxy(lambda event: on_info(event)))

        # Action button list parent
        option_parent = document.createElement("div")
        option_parent.className = cls.ACTION_LIST_PARENT_CLASS

        # Assemble menu parent element
        menu_parent = document.createElement("div")
        menu_parent.className = cls.MENU_CLASS
        menu_parent.append(title_text)
        menu_parent.append(info_button)
        menu_parent.append(option_parent)
        menu_parent.dataset.menuVersion = "3"
        menu_parent.dataset.menuId = str(randint(0, 9999))

        # Add options to list
        for option in _make_options(menu_parent):
            option_parent.append(option)

        # Hide menu when clicking away from it
        util.add_event_once(document, "click", lambda event: cls.destroy_all())

        # Hide menu when escape key pressed
        def _hide_on_escape(event):
            if event.keyCode == 27:
                cls.destroy_all()
        document.addEventListener("keyup", create_proxy(_hide_on_escape))

        # Render menu to begin positioning
        document.body.append(menu_parent)

        # Position menu at call location
        menu_pos = _position_menu(menu_parent)
        menu_parent.style.transform = f"translateX({menu_pos[0]}px) translateY({menu_pos[1]}px)"

        # show menu
        menu_parent.style.opacity = cls.MENU_VISIBLE_OPACITY

        cls.opened()

        # Return reference to menu element
        return menu_parent

    @classmethod
    def create2(cls, click, title, options, description="", item_name=""):
        """
            * title: main text shown at the top
            *
            * description: smaller text, shown under title
            *
            * options:
            * [
            *   {
            *     "name": <name of option shown in list>,
            *     "description": <description in tooltip>,
            *     "enabled": <true|false, will determine if the option can be selected>,
            *     "timeRequired": <number, seconds the action takes (instant if not defined)>,
            *     "serverCallback": <function name for server entity>,
            *     "clientCallback": <a no-argument function>,
            *     "entityId": <entity summoning the menu>,
            *     "arguments": <Map of arguments the option takes>
            *   },
            *   ...
            * ]
            *
            * itemName: string of the item selected, will show the (i) button if not
            *           null and will open the item info window when the (i) is clicked
            */
        """

        cls.opening()

        # allow only one open at a time

        cls.destroy_all()

        # define parts

        menu = document.createElement("div")
        menu.classList.add(cls.MENU_CLASS)
        menu.dataset.menuId = str(randint(0, 999))
        menu.dataset.menuVersion = "2"

        title_element = document.createElement("span")
        title_element.className = cls.MENU_TITLE_CLASS
        title_element.innerText = title

        menu.appendChild(title_element)

        if item_name != "":
            from display.windows.item_window import ItemWindow

            info_button = document.createElement("div")
            info_button.classList.add("openItemWindow", "InfoButton", "fa", "fa-info-circle")
            info_button.setAttribute("item-name", item_name)
            info_button.addEventListener("click", create_proxy(lambda _: util.create_task(ItemWindow(item_name).display_item())))
            menu.appendChild(info_button)

        action_list = document.createElement("div")
        action_list.className = cls.ACTION_LIST_PARENT_CLASS
        menu.appendChild(action_list)

        new_options = []
        for option in options:
            wrapper = document.createElement("div")
            wrapper.className = "action_wrapper"
            tooltip = document.createElement("div")
            tooltip.className = "action_error_tooltip"
            menu_item = document.createElement("div")
            menu_item.classList.add("RCItem")
            menu_item.innerText = option["name"]

            if option["enabled"]:
                async def on_click(event, option=option):
                    time_required = option["timeRequired"]
                    if time_required > 0:
                        action_bubble = ActionBubble(option["name"], time_required, option["associatedSkill"])
                        await action_bubble.wait

                    arguments = option.get("arguments")

                    if "serverCallback" in option:
                        util.send_action(option["serverCallback"].lower(), option["entityId"], arguments)
                    if "clientCallback" in option:
                        option["clientCallback"]()
                menu_item.addEventListener("click", create_proxy(on_click))
                menu_item.addEventListener("mouseover", create_proxy(lambda event: event.target.classList.add("RCItemSelected")))
                menu_item.addEventListener("mouseout", create_proxy(lambda event: event.target.classList.remove("RCItemSelected")))
                def on_key_up(event):
                    if event.keyCode == 27:
                        cls.destroy_all()
                document.addEventListener("keyup", create_proxy(on_key_up))
            else:
                menu_item.classList.add("RCItemDisabled")

            if option.get("description"):
                cls.show_action_error(tooltip, option["description"])

            wrapper.appendChild(menu_item)
            wrapper.appendChild(tooltip)
            new_options.append(wrapper)

        # keyboard navigation
        if not new_options[0].children[0].classList.contains("RCItemDisabled"):
            if len(new_options) > 1:
                def on_key_press(event):
                    if event.keyCode == 40:
                        # down arrow
                        new_options[0].children[0].classList.toggle("RCItemSelected")
                    if event.keyCode == 38:
                        # up arrow
                        new_options[0].children[len(new_options)].classList.toggle("RCItemSelected")
                menu.addEventListener("keypress", create_proxy(on_key_press))
            elif len(new_options) == 1:
                new_options[0].children[0].classList.toggle("RCItemSelected")

        document.body.appendChild(menu)
        if click is not None:
            x = click.pageX - (menu.getBoundingClientRect().width // 2)
            y = click.pageY - (40 + (len(options) * 30))
        else:
            pos_x = cou_globals.current_player.left
            pos_y = cou_globals.current_player.top
            width = cou_globals.current_player.width
            height = cou_globals.current_player.height
            translate_x = pos_x
            translate_y = cou_globals.view.worldElement.getBoundingClientRect().height - height
            if pos_x > cou_globals.current_street.bounds.width - width / 2 - cou_globals.view.worldElement.getBoundingClientRect().width / 2:
                translate_x = pos_x - cou_globals.current_street.bounds.width + cou_globals.view.worldElement.getBoundingClientRect().width
            elif pos_x + width / 2 > cou_globals.view.worldElement.getBoundingClientRect().width / 2:
                translate_x = cou_globals.view.worldElement.getBoundingClientRect().width / 2 - width / 2
            if pos_y + height / 2 < cou_globals.view.worldElement.getBoundingClientRect().height / 2:
                translate_y = pos_y
            elif pos_y < cou_globals.current_street.bounds.height - height / 2 - cou_globals.view.worldElement.getBoundingClientRect().height / 2:
                translate_y = cou_globals.view.worldElement.getBoundingClientRect().height / 2 - height / 2
            else:
                translate_y = cou_globals.view.worldElement.getBoundingClientRect().height - (cou_globals.current_street.bounds.height - pos_y)
            x = int(translate_x + menu.getBoundingClientRect().width + 10)
            y = int(translate_y + height / 2)

        action_list.append(*new_options)
        menu.style.opacity = "1.0"
        menu.style.transform = f"translateX({x}px) translateY({y}px)"

        document.addEventListener("click", create_proxy(lambda _: cls.destroy_all()))
        cls.opened()
        return menu


    @classmethod
    def show_action_error(cls, tooltip, error_text: str) -> None:
        tooltip.hidden = not error_text
        tooltip.innerText = error_text if error_text is not None else ""

    @classmethod
    def destroy_all(cls) -> int:
        destroyed = 0

        while menu := document.querySelector(f".{cls.MENU_CLASS}"):
            MessageBus.publish("menuStopping", menu)
            menu.remove()
            MenuKeys.clear_listeners()
            destroyed += 1

        return destroyed
