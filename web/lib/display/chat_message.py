import re
from typing import TYPE_CHECKING
import urllib

from pyscript import document, window
from pyodide import http

from configs import Configs
if TYPE_CHECKING:
    from display.chat_panel import Chat
from display.toast import Toast
from message_bus import MessageBus
from network.item_action import Item
from systems import util
from systems.global_gobbler import cou_globals


class ChatMessage:
    def __init__(self, player: str, message: str) -> None:
        self.player = player
        self.message = message

    def notify(self) -> None:
        if cou_globals.game.username.lower() in self.message.lower() and cou_globals.window_manager.settings.play_mention_sound and self.player != cou_globals.game.username:
            # popup
            window.Notification.new(self.player, body=self.message, icon=Toast.notif_icon_url)

            # sound effect
            MessageBus.publish("playSound", "mention")

    async def to_html(self, override_username_link: str = None) -> str:
        # verify data
        if not isinstance(self.message, str) or not isinstance(self.player, str):
            return ""

        display_name = self.player
        name_classes = ["name"]

        # set up labels
        if self.player is not None:
            # you
            if self.player == cou_globals.game.username:
                name_classes.append("you")
                if not self.message.startswith("/me"):
                    display_name = "You"

            # dev/guide
            if override_username_link is None:
                elevation = await cou_globals.game.get_elevation(self.player)
                if elevation != "_":
                    name_classes.append(elevation)

        # get link to username
        async def _get_username_link():
            link = document.createElement("a")
            link.classList.add(*name_classes, "noUnderline")
            link.target = "_blank"
            link.innerText = display_name

            if override_username_link is not None:
                link.href = override_username_link
                link.style.color = "black"
            else:
                link.href = f"https://childrenofur.com/players/{urllib.parse.quote(self.player)}"
                link.title = "Open Profile Page"
                link.style.color = await get_color_from_username(self.player)

            return link

        # notify of any mentions
        self.notify()

        if self.player is None:
            # system message
            pararaph = document.createElement("p")
            pararaph.innerText = self.message
            pararaph.classList.add("system")
            return pararaph.outerHTML

        if self.message.startswith("/me"):
            # /me message
            pararaph = document.createElement("p")
            pararaph.classList.add("me")
            pararaph.appendChild(await _get_username_link())
            pararaph.appendChild(document.createTextNode(self.message.replace("/me", " ", 1)))
            return pararaph.outerHTML

        if self.message == "LocationChangeEvent" and self.player == "invalid_user":
            # switching streets
            if not cou_globals.metabolics.load.done():
                await cou_globals.metabolics.load

            message_span = document.createElement("span")
            message_span.classList.add("message")
            message_span.innerText = cou_globals.current_street.label
            pararaph = document.createElement("p")
            pararaph.classList.add("chat-member-change-event")
            pararaph.appendChild(message_span)
            return pararaph.outerHTML

        # normal message
        pararaph = document.createElement("p")
        pararaph.appendChild(await _get_username_link())
        pararaph.insertAdjacentHTML("beforeend", "&#8194;")  # en space
        span = document.createElement("span")
        span.classList.add("message")
        span.innerText = parse_url(self.message)
        pararaph.appendChild(span)
        return pararaph.outerHTML


# // chat functions

EMOTICONS: list[str] = []
open_conversations: list["Chat"] = []
chat_toast_buffer: list[Toast] = []
cached_username_colors: dict[str, str] = {}


def get_cached_color_from_username(username: str) -> str:
    if username in cached_username_colors:
        # Already checked the color
        return cached_username_colors[username]

    # cache miss, let's schedule it to be filled in
    util.create_task(get_color_from_username(username), unique=True)


async def get_color_from_username(username: str) -> str:
    if username in cached_username_colors:
        # Already checked the color
        return cached_username_colors[username]

    # Download color from server
    response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/usernamecolors/get/{username}")
    color = await response.string()

    # cache for later use
    cached_username_colors[username] = color
    return color


# global functions

def parse_url(message: str) -> str:
    """Find embedded links and make anchor tags out of them"""

    regex_string = r"((?:https?://)?[\w]+\.\w+(?:/\S*)?)"

    for url in re.findall(regex_string, message):
        # Add protocol if missing
        if "//" not in url:
            new_url = f"http://{url}"

        anchor = f'<a href="{new_url}" target="_blank" class="MessageLink">{url}</a>'

        message = message.replace(url, anchor, 1)

    return message

def parse_item_links(message: str) -> str:
    for match in re.findall(r"#(.+?)#", message):
        if Item.is_item(item_type=match):
            name = Item.get_name(match)
            icon_url = Item.get_icon(item_type=match)
            replacement = (
                '<a class="item-chat-link" title="View Item" href="#">'
                f'<img class="item-chat-link-icon" src="{icon_url}">'
                f'{name}</a>'
            )
        else:
            replacement = match

        message = message.replace(f"#{match}#", replacement)
    return message


def parse_location_links(message: str) -> str:
    def _parse_hub_links(_message: str) -> str:
        for hub_data in cou_globals.map_data.hubData.values():
            if hub_data.get("name") is None:
                # missing info
                return _message
            if hub_data.get("map_hidden", False):
                # hidden
                return _message

            _message = _message.replace(
                hub_data["name"],
                f'<a class="location-chat-link hub-chat-link" title="View Hub" href="#">{hub_data["name"]}</a>'
            )

        return _message

    def _parse_street_links(_message: str) -> str:
        for street_name in cou_globals.map_data.streetData:
            if street_name not in _message:
                # Not a relevant street
                return _message

            if cou_globals.map_data.checkBoolSetting("map_hidden", street_name=street_name):
                # Hidden or hub hidden
                return _message

            _message = _message.replace(
                street_name,
                f'<a class"location-chat-link street-chat-link" title="View Street" href="#">{street_name}</a>'
            )

        return _message

    return _parse_street_links(_parse_hub_links(message))
