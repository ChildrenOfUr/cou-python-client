import urllib

from pyscript import document
from pyodide import http

from configs import Configs
from display.overlays.text_anim import animate_text
from systems.global_gobbler import cou_globals
from systems import util


class Meters:
    def __init__(self) -> None:
        self.run_count = 0

        self.green_disk = document.querySelector('#energyDisks .green')
        self.red_disk = document.querySelector('#energyDisks .red')
        self.hurt_disk = document.querySelector('#leftDisk .hurt')
        self.dead_disk = document.querySelector('#leftDisk .dead')
        self.avatar_display = document.querySelector("#moodAvatar")

        self.currant_element = document.querySelector('#currCurrants')
        self.currant_label = document.querySelector("#currantLabel")

        self.player_name_display = document.querySelector("#playerName")
        self.energy_display = document.querySelector("#currEnergy")
        self.max_energy_display = document.querySelector("#maxEnergy")
        self.mood_display = document.querySelector("#moodMeter .curr")
        self.max_mood_display = document.querySelector("#moodMeter .max")
        self.mood_percent_display = document.querySelector("#moodMeter .number")
        self.img_display = document.querySelector("#currImagination")

    def update_avatar_display(self) -> None:
        if self.run_count < 5 or self.run_count % 5 == 0:
            # run on load, and once every 5 refreshes afterward to avoid overloading the server
            async def change_image():
                response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/trimImage?username={cou_globals.game.username}")
                img = await response.string()
                self.avatar_display.style.backgroundImage = f"url(data:image/png;base64,{img})"
            util.create_task(change_image())

            # update username links
            document.querySelector("#openProfilePageFromChatPanel").href = f"https://childrenofur.com/players/{urllib.parse.quote(cou_globals.game.username)}"

    def update(self) -> None:
        # update number displays
        self.update_all()

        # update energy disk angles and opacities
        metabolics = cou_globals.metabolics
        self.green_disk.style.transform = f"rotate({120 - (metabolics.energy / metabolics.max_energy) * 120}deg)"
        self.red_disk.style.transform = f"rotate({120 - (metabolics.energy / metabolics.max_energy) * 120}deg)"
        self.red_disk.style.opacity = str(1 - metabolics.energy / metabolics.max_energy)
        self.hurt_disk.style.backfaceVisibility = "visible"
        self.hurt_disk.style.opacity = str(0.7 - (metabolics.mood / metabolics.max_mood))
        self.dead_disk.style.opacity = str(1 if metabolics.mood <= 0 else 0)

        # updates portrait
        self.update_avatar_display()
        self.run_count += 1

    def update_img_display(self) -> None:
        self.img_display.innerText = f"{cou_globals.metabolics.img:,}"

    def update_energy_display(self) -> None:
        self.energy_display.innerText = str(cou_globals.metabolics.energy)
        self.max_energy_display.innerText = str(cou_globals.metabolics.max_energy)

    def update_mood_display(self) -> None:
        self.mood_display.innerText = str(cou_globals.metabolics.mood)
        self.max_mood_display.innerText = str(cou_globals.metabolics.max_mood)
        self.mood_percent_display.innerText = str(int(cou_globals.metabolics.mood / cou_globals.metabolics.max_mood * 100))

    def update_currants_display(self) -> None:
        try:
            old_currants = int(self.currant_element.innerText.replace(",", ""))
        except ValueError:
            old_currants = 0

        self.currant_element.innerText = f"{cou_globals.metabolics.currants:,}"
        self.currant_label.innerText = "Currants" if cou_globals.metabolics.currants != 1 else "Currant"

        if old_currants != cou_globals.metabolics.currants:
            # animate if the number changed
            util.create_task(animate_text(self.currant_element.parentElement, self.currant_element.innerText, "currant-anim"))

    def update_name_display(self) -> None:
        self.player_name_display.innerText = cou_globals.game.username
        self.player_name_display.href = f"http://childrenofur.com/players/{cou_globals.game.username}"

    def update_all(self) -> None:
        self.update_currants_display()
        self.update_energy_display()
        self.update_img_display()
        self.update_mood_display()
