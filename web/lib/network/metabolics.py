import dataclasses
import re


@dataclasses.dataclass
class Metabolics:
    id: int = -1
    user_id: int = -1

    currants: int = 0
    energy: int = 50
    max_energy: int = 100
    mood: int = 50
    max_mood: int = 100
    img: int = 0
    lifetime_img: int = 0
    quoins_collected: int = 0

    alph_favor: int = 0
    alph_favor_max: int = 0
    cosma_favor: int = 0
    cosma_favor_max: int = 0
    friendly_favor: int = 0
    friendly_favor_max: int = 0
    grendaline_favor: int = 0
    grendaline_favor_max: int = 0
    humbaba_favor: int = 0
    humbaba_favor_max: int = 0
    lem_favor: int = 0
    lem_favor_max: int = 0
    mab_favor: int = 0
    mab_favor_max: int = 0
    pot_favor: int = 0
    pot_favor_max: int = 0
    spriggan_favor: int = 0
    spriggan_favor_max: int = 0
    tii_favor: int = 0
    tii_favor_max: int = 0
    zille_favor: int = 0
    zille_favor_max: int = 0

    current_street_x: float = 1.0
    current_street_y: float = 0.0
    quoin_multiplier: float = 1.0

    current_street: str = "LA58KK7B9O522PC"
    last_street: str = None
    location_history: str = "[]"

    @classmethod
    def from_json(cls, metabolics: dict) -> "Metabolics":
        metabolic = Metabolics()
        for key, value in metabolics.items():
            key = re.sub(f"([A-Z]+)", r"_\1", key).lower()
            setattr(metabolic, key, value)
        return metabolic
