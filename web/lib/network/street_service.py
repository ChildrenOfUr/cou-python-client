import asyncio
import logging
from urllib.parse import quote

from pyodide import http

from assets import Asset
from configs import Configs
from display.overlays.street_loading_screen import StreetLoadingScreen
from display.toast import Toast
from game.street import Street
from message_bus import MessageBus
from systems import util
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)


class StreetService:
    def __init__(self):
        self._data_url = f"{Configs.http}//{Configs.utilServerAddress}"
        self._loading: list[str] = []
        self.lettersAnnounced = False
        self.street_loading_screen = None

    async def request_street(self, street_id: str) -> bool:
        if self._loading:
            # already loading something, tell it to stop
            self._loading = []

        # Load this one
        self.add_to_queue(street_id)

        cou_globals.gps_indicator.loading_new = True

        LOGGER.info(f"[StreetService] Requesting street '{street_id}'...")
        tsid = quote(street_id)

        response = await http.pyfetch(f"{self._data_url}/getStreet?tsid={tsid}")
        street_json = await response.json()

        if self.loading_cancelled(street_id):
            LOGGER.info(f"[StreetService] Loading of '{street_id}' was cancelled during download.")
            return False

        LOGGER.info(f"[StreetService] '{street_id}' loaded.")
        try:
            await self._prepare_street(street_json)
        except Exception:
            LOGGER.exception("Failed")

        response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/listUsers?channel={cou_globals.current_street.label}")
        players = await response.json()

        if cou_globals.game.username not in players:
            players.append(cou_globals.game.username)

        # don't list if it's just you
        if len(players) > 1:
            player_list = ",".join(players)
            Toast(f"Players on this street: {player_list}")
        else:
            Toast("You're the first one here!")

        if cou_globals.current_street.use_letters and not self.lettersAnnounced:
            self.lettersAnnounced = True
            Toast("Use that letter over your head to spell a word with some friends!")

        return True

    async def _prepare_street(self, street_json: dict) -> None:
        # Tell the server we're leaving
        if cou_globals.current_street and cou_globals.current_street.tsid is not None:
            try:
                util.send_global_action("leaveStreet", {"street": cou_globals.current_street.tsid})
            except Exception:
                LOGGER.exception(f"Error sending last street to server")

        LOGGER.info("[StreetService] Assembling Street...")
        MessageBus.publish("streetLoadStarted", street_json)

        if street_json.get("tsid") is None:
            return False

        label = street_json["label"]
        tsid = street_json["tsid"]
        old_label = ""
        old_tsid = ""

        if cou_globals.current_street is not None:
            old_label = cou_globals.current_street.label
            old_tsid = cou_globals.current_street.tsid

        if self.loading_cancelled(tsid):
            LOGGER.info(f"[StreetService] Loading of {tsid} was cancelled during decoding.")
            return False

        # TODO, this should happen automatically on the Server, since it'll know which street we're on
        # Send changeStreet to chat server
        map = {
            "statusMessage": "changeStreet",
            "username": cou_globals.game.username,
            "newStreetLabel": label,
            "newStreetTsid": tsid,
            "oldStreetLabel": old_label,
            "oldStreetTsid": old_tsid,
        }
        MessageBus.publish("outgoingChatEvent", map)

        await cou_globals.map_data.load

        # Display the loading screen
        street_data = None
        if cou_globals.current_street:
            street_data = cou_globals.current_street.street_data
        self.street_loading_screen = StreetLoadingScreen(street_data, street_json)

        # Load the street
        street = Street(street_json)

        if self.loading_cancelled(tsid):
            LOGGER.info(f"[StreetService] Loading of '{tsid}' was cancelled during preparation.")
            return False

        # Make street loading take at least 1 second so that the text can be read
        await asyncio.sleep(1)
        await street.load()

        Asset.from_data(street_data, label)

        LOGGER.info("[StreetService] Street assembled.")

        # Notify displays to update
        MessageBus.publish("streetLoaded", street_json)

        self.remove_from_queue(tsid)
        return True

    def add_to_queue(self, tsid: str) -> None:
        self._loading.append(tsid[1:])

    def remove_from_queue(self, tsid: str) -> None:
        self._loading.remove(tsid[1:])

    def loading_cancelled(self, tsid: str) -> bool:
        return not tsid[1:] in self._loading
