import json

from pyscript import document

from configs import Configs
from display.chat_panel import Chat
from game.chat_bubble import ChatBubble
from game.entities.entity import Entity
from systems import util
from systems.global_gobbler import cou_globals


def send_left_message(street_name: str) -> None:
    if cou_globals.street_socket is not None and cou_globals.street_socket.readyState == 1:  # OPEN
        map = {
            "username": cou_globals.game.username,
            "streetName": street_name,
            "message": "left",
        }
        cou_globals.street_socket.send(json.dumps(map))


def send_joined_message(street_name: str, tsid: str = None):
    if not hasattr(cou_globals, "street_socket"):
        return
    if getattr(cou_globals, "joined", None) != street_name and cou_globals.street_socket is not None and cou_globals.street_socket.readyState == 1:  # OPEN
        map = {
            "clientVersion": Configs.clientVersion,
            "username": cou_globals.game.username,
            "email": cou_globals.game.email,
            "streetName": street_name,
            "tsid": cou_globals.current_street.street_data["tsid"] if tsid is None else tsid,
            "message": "joined",
            "firstConnect": cou_globals.first_connect,
        }
        cou_globals.street_socket.send(json.dumps(map))
        cou_globals.joined = street_name
        if cou_globals.first_connect:
            cou_globals.first_connect = False


def update_chat_bubble(map: dict, entity: Entity) -> None:
    if entity is None:
        return

    if map.get("bubbleText"):
        bubble_text = map["bubbleText"].split("|||")[0]
        buttons = map["bubbleText"].split("|||")[1] if "|||" in bubble_text else None

        if entity.chat_bubble is None:
            height_string = str(entity.canvas.height)
            translate_y = entity.canvas.getAttribute("translateY")

            if actual_height := entity.canvas.getAttribute("actualHeight"):
                height_string = actual_height
                diff = entity.canvas.height - float(height_string)
                translate_y = str(float(translate_y) + diff)

            bubble_parent = document.createElement("div")
            bubble_parent.style.position = "absolute"
            bubble_parent.style.width = f"{entity.canvas.width}px"
            bubble_parent.style.height = f"{height_string}px"
            bubble_parent.style.transform = f"translateX({map['x']}px) translateY({translate_y}px)"
            cou_globals.view.playerHolder.appendChild(bubble_parent)
            entity.chat_bubble = ChatBubble(
                bubble_text, entity, bubble_parent, auto_dismiss=False, remove_parent=True,
                gains=map["gains"], buttons=buttons,
            )

            if cou_globals.window_manager.settings.log_npc_messages and len(bubble_text.strip()):
                type = entity.canvas.getAttribute("type")
                util.create_task(Chat.local_chat.add_message(
                    f"({type})", bubble_text,
                    override_username_link=f"https://childrenofur.com/encyclopedia/#/entity/{type.replace(' ','')}",
                ))

        entity.chat_bubble.update(1.0)
    elif entity.chat_bubble is not None:
        entity.chat_bubble.remove_bubble()
