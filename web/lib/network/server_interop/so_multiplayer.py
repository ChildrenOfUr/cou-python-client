import logging
from typing import Any

from configs import Configs
from game.entities.door import Door
from game.entities.entity import Entity
from game.entities.ground_item import GroundItem
from game.entities.npc import NPC
from game.entities.plant import Plant
from game.entities.quoin import Quoin
from network.server_interop.so_player import setup_player_socket
from network.server_interop.so_street import setup_street_socket
from systems.global_gobbler import cou_globals


cou_globals.adding_locks: dict[str, bool] = {}
cou_globals.entities: dict[str, Entity] = {}

LOGGER = logging.getLogger(__name__)

def multiplayer_init() -> None:
    cou_globals.multiplayer_server = f"{Configs.ws}//{Configs.websocketServerAddress}/playerUpdate"
    cou_globals.street_event_server = f"{Configs.ws}//{Configs.websocketServerAddress}/streetUpdate"
    cou_globals.joined = ""
    cou_globals.creating_player = ""
    cou_globals.street_socket = None
    cou_globals.player_socket = None
    cou_globals.reconnect = True
    cou_globals.first_connect = True
    cou_globals.server_down = False
    setup_street_socket(cou_globals.current_street.label)
    setup_player_socket()


def addQuoin(map: dict[str, Any]) -> None:
    id = map['id']
    if cou_globals.adding_locks.get(id):
        return

    if cou_globals.current_street and cou_globals.current_street.loaded:
        cou_globals.adding_locks[id] = True
        cou_globals.quoins[id] = Quoin(map)


def addNPC(map: dict) -> None:
    id = map['id']
    if cou_globals.adding_locks.get(id):
        return

    if cou_globals.current_street and cou_globals.current_street.loaded:
        cou_globals.adding_locks[id] = True
        cou_globals.entities[id] = NPC(map)

def addPlant(map: dict) -> None:
    id = map['id']
    if cou_globals.adding_locks.get(id):
        return

    if cou_globals.current_street and cou_globals.current_street.loaded:
        cou_globals.adding_locks[id] = True
        cou_globals.entities[id] = Plant(map)

def addDoor(map: dict[str, Any]) -> None:
    id = map['id']
    if cou_globals.adding_locks.get(id):
        return

    if cou_globals.current_street and cou_globals.current_street.loaded:
        cou_globals.adding_locks[id] = True
        cou_globals.entities[id] = Door(map)

def addItem(map: dict) -> None:
    id = map['id']
    if cou_globals.adding_locks.get(id):
        return

    if cou_globals.current_street and cou_globals.current_street.loaded:
        cou_globals.adding_locks[id] = True
        cou_globals.entities[id] = GroundItem(map)
