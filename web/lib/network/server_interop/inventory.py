import asyncio
import dataclasses
import json
import logging
from typing import Any

from pyscript import document

from message_bus import MessageBus
from network.server_interop.itemdef import ItemDef
from network.server_interop.serializable import serializable
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)


@serializable
@dataclasses.dataclass
class Slot:
    # a new instance of a Slot is empty by default
    item_type: str = ""
    item: ItemDef = None
    count: int = 0


@dataclasses.dataclass
class Inventory:
    # Sets how many slots each player has
    inv_size: int = 10
    slots: list[Slot] = dataclasses.field(default_factory=list)


cou_globals.player_inventory = Inventory()


async def update_inventory(map: [str, Any] = None) -> None:
    from network.server_interop.so_item import find_new_slot

    if map is not None:
        data_slots = map.get("slots")
    else:
        LOGGER.error("Attempted inventory update: failed")
        return

    current_slots: list[Slot] = cou_globals.player_inventory.slots
    slots = [Slot.from_json(s) for s in data_slots]

    cou_globals.player_inventory.slots = slots

    # if the current inventory differs (count, type, metadata) then clear it
    # and change it, else leave it alone
    ui_slots = document.querySelectorAll(".box")
    slot_update_futures = []
    for index in range(10):
        new_slot = slots[index]

        update_needed = False
        update = False

        # if we've never received our inventory before, update all slots
        if len(current_slots) == 0:
            update_needed = True
        else:
            current_slot = current_slots[index]

            if current_slot.item_type != new_slot.item_type:
                update_needed = True
            elif current_slot.count != new_slot.count:
                update_needed = True
                update = True
            elif current_slot.item != None and new_slot.item != None and not _metadata_equal(current_slot.item.metadata, new_slot.item.metadata):
                index_to_item = {"index": index, "item": new_slot.item}
                MessageBus.publish("updateMetadata", index_to_item)

        if update_needed:
            if new_slot.count == 0:
                ui_slots[index].replaceChildren()
            else:
                for child in ui_slots[index].children:
                    if "count" in child.getAttributeNames():
                        child.setAttribute("count", "0")
                slot_update_futures.append(find_new_slot(new_slot, index, update=update))

    await asyncio.gather(*slot_update_futures)
    MessageBus.publish("inventoryUpdated", True)


def _metadata_equal(meta_a: dict, meta_b: dict) -> bool:
    if not meta_a and not meta_b:
        return True

    return json.dumps(meta_a) == json.dumps(meta_b)
