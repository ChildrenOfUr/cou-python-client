import dataclasses

from network.server_interop.serializable import serializable


@serializable
@dataclasses.dataclass
class ConvoChoice:
    text: str = None
    goto_screen: int = None
    is_quest_accept: bool = False
    is_quest_reject: bool = False


@serializable
@dataclasses.dataclass
class ConvoScreen:
    paragraphs: list[str] = None
    choices: list[ConvoChoice] = None


@serializable
@dataclasses.dataclass
class Conversation:
    id: str = None
    screens: list[ConvoScreen] = None


@serializable
@dataclasses.dataclass
class QuestFavor:
    giant_name: str = None
    fav_amt: int = 0


@serializable
@dataclasses.dataclass
class QuestRewards:
    energy: int = 0
    mood: int = 0
    img: int = 0
    currants: int = 0
    favor: list[QuestFavor] = dataclasses.field(default_factory=list)


@serializable
@dataclasses.dataclass
class Requirement:
    fulfilled: bool = None
    num_required: int = None
    time_limit: int = None
    num_fulfilled: int = None
    id: str = None
    text: str = None
    type: str = None
    event_type: str = None
    icon_url: str = None
    type_done: list[str] = None


@serializable
@dataclasses.dataclass
class Quest:
    id: str = None
    title: str = None
    description: str = None
    complete: bool = False
    prerequisites: list[str] = None
    requirements: list[Requirement] = None
    conversation_start: Conversation = None
    conversation_end: Conversation = None
    conversation_fail: Conversation = None
    rewards: QuestRewards = None
