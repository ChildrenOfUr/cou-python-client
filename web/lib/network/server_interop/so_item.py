import asyncio
from datetime import timedelta
import json

from pyscript import document
from pyodide.ffi import create_proxy

from display.ui_templates.right_click_menu import RightClickMenu
from display.windows.bag_window import BagWindow
from display.windows.item_window import ItemWindow
from game.actionbubble import ActionBubble
from message_bus import MessageBus
from network.server_interop.itemdef import Action, ItemDef
from network.server_interop.inventory import Slot
from systems import util
from systems.global_gobbler import cou_globals


slot_locks: dict[int, asyncio.Lock] = {}


def item_context_menu(item: ItemDef, slot: str, event) -> None:
    """slot should be in the format 'barSlot.bagSlot' indexed from 0
    if bagSlot is not relevant, please use 'barSlot.-1'
    """
    if ActionBubble.occuring:
        return

    event.preventDefault()
    event.stopPropagation()

    bar_slot = int(slot.split('.')[0])
    bag_slot = int(slot.split('.')[1])
    actions: list[Action] = []

    if item.actions is not None:
        enabled = False
        for action in item.actions:
            enabled = action.enabled
            action.action_name = action.action_name.capitalize()
            if enabled:
                enabled = action.has_requirements()
                error = action.description if enabled else action.get_requirement_string()
            else:
                error = action.error
            menu_action = Action.clone(action)
            menu_action.enabled = enabled
            menu_action.error = error
            menu_action.drop_map = get_drop_map(1, bar_slot, bag_slot)
            actions.append(menu_action)
    menu = RightClickMenu.create3(
        event, item.metadata.get("title", item.name), item.item_type,
        actions=actions, item=item, on_info=lambda _: ItemWindow(item.name),
    )
    document.body.appendChild(menu)


def update_trigger_btn(open: bool, item) -> None:
    btn = item.parentElement.querySelector(".item-container-toggle")
    if not open:
        # closed, opening the bag
        btn.classList.remove("item-container-closed", "fa-plus")
        btn.classList.add("item-container-open", "fa-times")
        item.classList.add("inv-item-disabled")
    else:
        # opened, closing the bag
        btn.classList.remove("item-container-open", "fa-times")
        btn.classList.add("item-container-closed", "fa-plus")
        item.classList.remove("inv-item-disabled")


async def find_new_slot(slot: Slot, index: int, update: bool = False) -> None:
    item = slot.item
    count = slot.count

    # decide what image to be used based on durability state
    url = item.sprite_url
    used = int(item.metadata.get("durabilityUsed", 0))
    if item.durability is not None and used >= item.durability:
        url = item.broken_url

    if not index in slot_locks:
        slot_locks[index] = asyncio.Lock()

    # acquire the locks to that multiple incoming add item requests for the same slot
    # don't overwrite/trash the UI state
    async with slot_locks[index]:
        # create an image for this item and wait for it to load before sizing/positioning it
        img = document.createElement("img")
        img.src = url
        bar_slot = cou_globals.view.inventory.children[index]
        bar_slot.replaceChildren()
        item_div = document.createElement("div")

        await size_item(img, item_div, bar_slot, item, count, int(bar_slot.dataset.slotNum))

    if not update:
        item_div.classList.add("bounce")
        # remove bounce class so that it's not still there for a drag and drop event
        util.Timer(timedelta(seconds=1), lambda: item_div.classList.remove("bounce"))

    # containers
    if item.is_container:
        container_button = document.createElement("div")
        container_button.classList.add("fa", "fa-fw", "fa-plus", "item-container-toggle", "item-container-closed")
        def on_click(event):
            if container_button.classList.contains("item-container-closed"):
                # container is closed, open it
                item._bag_window.open()
                # update the slot display
                update_trigger_btn(False, item_div)
            else:
                # container is open, close it
                item._bag_window.close()
                # update the slot display
                update_trigger_btn(True, item_div)
        container_button.addEventListener("click", create_proxy(on_click))

        # preload the bag window so that it can listen for events (also preloads any images
        # so that bags open faster the first time)
        item._bag_window = BagWindow(index, item, open_window=False)

        item_div.parentElement.appendChild(container_button)


async def size_item(img, item_div, slot, item: ItemDef, count: int, bar_slot_num: int, css_class: str = None, bag_slot_num: int = -1) -> None:
    await util.await_event_once(img, "load")

    on_update_metadata = None

    # determine what we need to scale the sprite image to in order to fit
    if img.height > img.width / min(item.stacks_to, item.icon_num):
        scale = (slot.getBoundingClientRect().height - 10) / img.height
    else:
        scale = (slot.getBoundingClientRect().width - 10) / (img.width / min(item.stacks_to, item.icon_num))

    if css_class is None:
        css_class = f"item-{item.item_type} inventoryItem"

    url = item.sprite_url
    used = int(item.metadata.get("durabilityUsed", 0))
    if item.durability is not None and used >= item.durability:
        url = item.broken_url

    if url.endswith(".svg"):
        item_div.style.wdith = f"{img.width * scale}px"
        item_div.style.height = f"{img.height * scale}px"
    else:
        item_div.style.width = f"{slot.getBoundingClientRect().width - 10}px"
        item_div.style.height = f"{slot.getBoundingClientRect().height - 10}px"

    item_div.style.backgroundImage = f"url({url})"
    item_div.style.backgroundRepeat = "no-repeat"
    item_div.style.backgroundSize = f"{img.width * scale}px {img.height * scale}px"
    item_div.style.backgroundPosition = "0 50%"
    item_div.classList.add(*css_class.split(" "))

    item_div.setAttribute("name", item.name.replace(" ", ""))
    item_div.setAttribute("count", "1")
    item_div.setAttribute("itemMap", json.dumps(item.to_json()))
    slot.title = item.name

    slot_num = f"{bar_slot_num}.{bag_slot_num}"
    item_div.addEventListener("contextmenu", create_proxy(lambda event: item_context_menu(item, slot_num, event)))
    slot.appendChild(item_div)

    if "fireflies" in item.metadata:
        count = int(item.metadata["fireflies"])
        def on_update_metadata(index_to_item: dict):
            item = index_to_item["item"]
            slot.querySelector(".itemCount").innerText = item.metadata["fireflies"]
    item_count = document.createElement("span")
    item_count.innerText = str(count)
    item_count.classList.add("itemCount")
    slot.appendChild(item_count)
    if count <= 1 and "fireflies" not in item.metadata:
        item_count.innerText = ""

    if item.is_container:
        def on_update_metadata(index_to_item: dict):
            item_div.setAttribute("itemMap", json.dumps(index_to_item["item"].to_json()))

    if item.durability is not None:
        durability_used = int(item.metadata.get("durabilityUsed", 0))
        durability_percent = min(100, max(0, round(((item.durability - durability_used) / item.durability) * 100)))

        durability_background = document.createElement("div")
        durability_background.classList.add("durabilityBackground")
        durability_background.title = f"{durability_percent}% durability remaining"
        durability_foreground = document.createElement("div")
        durability_foreground.classList.add("durabilityForeground")
        durability_foreground.style.width = f"{durability_percent}%"

        if durability_percent < 10:
            durability_foreground.classList.add("durabilityWarning")

        durability_background.appendChild(durability_foreground)
        slot.appendChild(durability_background)

        def on_update_metadata(index_to_item: dict):
            durability_bar = slot.querySelector(".durabilityForeground")
            durability_bar_back = slot.querySelector(".durabilityBackground")
            if durability_bar is None:
                return

            item = index_to_item["item"]
            if item.durability is None:
                return
            durability_used = int(item.metadata.get("durabilityUsed", 0))
            percent = ((item.durability - durability_used) / item.durability) * 100
            durability_bar.style.width = f"{percent}%"
            durability_percent = round(percent)
            durability_bar_back.title = f"{durability_percent}% durability remaining"
            if percent == 0:
                item_div.style.backgroundImage = f"url({item.broken_url})"
            else:
                item_div.style.backgroundImage = f"url({item.sprite_url})"

    offset = count
    if item.icon_num is not None and item.icon_num < count:
        offset = item.icon_num
    percent = round((offset - 1) * 33.3)
    item_div.style.backgroundPosition = f"calc({percent}%)"

    if on_update_metadata is not None:
        _on_update_metadata(on_update_metadata, bar_slot_num=bar_slot_num, bag_slot_num=bag_slot_num)

cou_globals.size_item = size_item


metadata_subscriptions = []

def _on_update_metadata(do_action: callable, bag_slot_num: int = -1, bar_slot_num: int = -1) -> None:
    def on_update_metadata(index_to_item: dict) -> None:
        if bag_slot_num != -1:
            # stuff in bags will update itself each time the bag is changed
            return
        if index_to_item["index"] == bar_slot_num:
            do_action(index_to_item)
    metadata_subscriptions.append(MessageBus.subscribe("updateMetadata", on_update_metadata))



def take_item_from_inventory(item_type: str, count: int = 1) -> None:
    css_name = item_type.replace(" ", "_")
    remaining = count
    for item in cou_globals.view.inventory.querySelectorAll(f".item-{css_name}"):
        if remaining < 1:
            break

        ui_count = int(item.getAttribute("count"))
        if ui_count > count:
            item.setAttribute("count", str(ui_count - count))
            item.parentElement.querySelector(".itemCount").innerText = str(ui_count - count)
        else:
            item.parentElement.replaceChildren()

        remaining -= ui_count

    MessageBus.publish("inventoryUpdated")


def get_drop_map(count: int, slot_num: int, sub_slot_num: int) -> dict:
    return {
        "slot": slot_num,
        "subSlot": sub_slot_num,
        "count": count,
        "streetName": cou_globals.current_street.label,
        "tsid": cou_globals.current_street.street_data["tsid"],
    }