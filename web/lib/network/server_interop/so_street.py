from datetime import timedelta
import json
import logging

from js import WebSocket, document, window
from pyodide.ffi import create_proxy

from display.buff import Buff
from display.overlays.achievement_get import AchievementOverlay
from display.toast import Toast
from display.ui_templates.item_chooser import ItemChooser
from display.windows.mailbox_window import MailboxWindow
from display.windows.note_window import NoteWindow
from display.windows.shrine_window import ShrineWindow
from display.windows.useitem_window import UseWindow
from display.windows.vendor_window import VendorWindow
from message_bus import MessageBus
from network.server_interop import so_multiplayer, so_chat
from network.server_interop.inventory import update_inventory
from network.server_interop.so_chat import send_joined_message
from systems import util
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)


def setup_street_socket(street_name: str) -> None:
    # start a timer for a few seconds and then show the server down message if not canceled
    def show_down() -> None:
        document.querySelector("#server-down").hidden = False
        cou_globals.server_down = True

    server_down_timer = util.Timer(timedelta(seconds=10), show_down)
    street_socket = WebSocket.new(cou_globals.street_event_server)

    def street_open(event) -> None:
        document.querySelector("#server-down").hidden = True
        server_down_timer.cancel()
        if cou_globals.server_down:
            window.location.reload()
        send_joined_message(street_name)

    async def street_message(event) -> None:
        map = json.loads(event.data)

        if map.get("error") is not None:
            cou_globals.reconnect = False
            LOGGER.error(f"[Multiplayer (Street)] Error {map['error']}")
            street_socket.close()
        elif map.get("label") is not None and cou_globals.current_street.label != map["label"]:
            return
        elif map.get("inventory") is not None:  # check if we are receiving our inventory
            await update_inventory(map)
        elif map.get("vendorName") == "Auctioneer":
            # TODO AuctionWindow().open()
            pass
        elif map.get("openWindow") is not None:
            if map["openWindow"] == "vendorSell":
                VendorWindow().call(map, sell_mode=True)
            if map["openWindow"].startswith("mailbox"):
                mail_window = MailboxWindow()
                mail_window.open()
                if map["openWindow"] == "mailbox.compose":
                    mail_window.switch_to_tab("compose")
            if map["openWindow"] == "itemChooser":
                def callback(item_type: str, count: int = 1, slot: int = -1, sub_slot: int = -1):
                    util.send_action(
                        map["action"], map["id"], {
                            "itemType": item_type,
                            "count": count,
                            "slot": slot,
                            "subSlot": sub_slot,
                        },
                    )
                ItemChooser(map["windowTitle"], callback, filter=map["filter"])
        elif map.get("itemsForSale"):
            VendorWindow().call(map)
        elif map.get("giantName"):
            ShrineWindow(map["giantName"], map["favor"], map["maxFavor"], map["id"]).open()
        elif map.get("favorUpdate"):
            MessageBus.publish("favorUpdate", map)
        elif map.get("gotoStreet"):
            util.create_task(cou_globals.street_service.request_street(map["tsid"]))
        elif map.get("toast"):
            Toast(map["message"], skip_chat=map.get("skipChat", False), on_click=map.get("onClick"))
        elif map.get("playSound"):
            await cou_globals.audio.play_sound(map["sound"])
        elif map.get("useItem"):
            UseWindow(map["useItem"], map["useItemName"])
            pass
        elif map.get("achv_id"):
            AchievementOverlay(map)
        elif map.get("open_profile"):
            window.open(f"https://childrenofur.com/players/{map['open_profile']}", "_blank")
        elif map.get("follow"):
            Toast(cou_globals.current_player.follow_player(map["follow"]))
        elif map.get("buff"):
            Buff.from_map(map["buff"])
        elif map.get("buff_remove"):
            Buff.remove_buff(map["buff_remove"])
        elif map.get("buff_extend"):
            Buff.extend_buff(map["buff_extend"], map["buff_extend_secs"])
        elif map.get("note_write"):
            NoteWindow(-1, True)
        elif map.get("note_response"):
            MessageBus.publish("note_response", map["note_response"])
        elif map.get("note_read"):
            NoteWindow(int(map["note_read"]))
        elif map.get("username_changed"):
            # TODO ChangeUsernameWindow.response.set_result(map["username_changed"])
            pass
        elif map.get("promptString"):
            # TODO PromptStringWindow(map["promptText"], map["promptRef"], map["charLimit"])
            pass
        elif map.get("npcMove"):
            for npc_map in map["npcs"]:
                npc = cou_globals.entities.get(npc_map["id"])
                if npc is None:
                    return

                npc.facing_right = npc_map["facingRight"]
                npc.y_speed = npc_map["ySpeed"]
                npc.speed = npc_map["speed"]

                npc.x = npc_map["x"]
                npc.y = npc_map["y"]

                npc.update_animation(npc_map)
                npc._actions = npc_map["actions"]
            for id in map.get("removeNpcs", []):
                if cou_globals.entities.get(id):
                    cou_globals.entities[id].canvas.remove()
                    cou_globals.entities.pop(id, None)
                    cou_globals.current_player.intersecting_objects.pop(id, None)
            return


        for quoin_map in map.get("quoins", []):
            if quoin_map.get("remove") == "true":
                # server OKed collection of a quoin
                object_to_remove = document.querySelector(f"#{quoin_map['id']}")
                if object_to_remove:
                    # .remove() is very slow
                    object_to_remove.style.display = "none"
            else:
                # respawn qoins
                id = quoin_map["id"]
                element = document.querySelector(f"#{id}")
                if not element:
                    # add new quoin
                    so_multiplayer.addQuoin(quoin_map)
                elif element.style.display == "none":
                    # update existing quoin
                    element.style.display = "block"
                    cou_globals.quoins[id].collected = False

        for door_map in map.get("doors", []):
            id = door_map["id"]
            element = document.querySelector(f"#{id}")
            door = cou_globals.entities.get(id)
            if element is None:
                so_multiplayer.addDoor(door_map)
            elif door is not None:
                door._actions = door_map["actions"]
                so_chat.update_chat_bubble(door_map, door)

        for plant_map in map.get("plants", []):
            id = plant_map["id"]
            element = document.querySelector(f"#{id}")
            plant = cou_globals.entities.get(id)
            if element is None:
                so_multiplayer.addPlant(plant_map)
            elif plant is not None:
                plant._actions = plant_map["actions"]
                if plant.state != plant_map["state"]:
                    plant.update_state(plant_map["state"])
                so_chat.update_chat_bubble(plant_map, plant)

        for npc_map in map.get("npcs", []):
            id = npc_map["id"]
            element = document.querySelector(f"#{id}")
            npc = cou_globals.entities.get(id)
            if element is None:
                so_multiplayer.addNPC(npc_map)
            elif npc is not None:
                if npc_map.get("nameOverride", ""):
                    npc.name_override = npc_map["nameOverride"]
                npc.update_animation(npc_map)
                so_chat.update_chat_bubble(npc_map, npc)

        for item_map in map.get("groundItems", []):
            id = item_map["id"]
            element = document.querySelector(f"#{id}")
            if element is None:
                so_multiplayer.addItem(item_map)
            else:
                if item_map.get("onGround", False) == False:
                    element.remove()
                    cou_globals.entities.pop(id, None)
                    cou_globals.current_player.intersecting_objects.clear()
                else:
                    cou_globals.entities[id]._actions = item_map.get("actions", [])

    def street_close(event) -> None:
        LOGGER.info("[Multiplayer (Street)] Socket closed")
        if not cou_globals.reconnect:
            document.querySelector("#server-down").hidden = False
            cou_globals.server_down = True
            cou_globals.reconnect = True
            return
        cou_globals.joined = ""
        # wait 5 seconds and try to reconnect
        util.Timer(timedelta(seconds=5), lambda: setup_street_socket(cou_globals.current_street.label))

    def street_error(event) -> None:
        LOGGER.info(f"[Multiplayer (Street)] Error")
        window.console.log(event)

    street_socket.addEventListener("open", create_proxy(street_open))
    street_socket.addEventListener("message", create_proxy(street_message))
    street_socket.addEventListener("close", create_proxy(street_close))
    street_socket.addEventListener("error", create_proxy(street_error))
    cou_globals.street_socket = street_socket
