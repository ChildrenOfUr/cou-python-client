from datetime import timedelta
import json
import logging

from js import WebSocket, document, window
from pyodide.ffi import create_proxy

from configs import Configs
from display.toast import Toast
from game.chat_bubble import ChatBubble
from game.entities.player import Player
from message_bus import MessageBus
from systems import util
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)


def setup_player_socket():
    player_socket = WebSocket.new(cou_globals.multiplayer_server)

    def on_message(event):
        map = json.loads(event.data)
        if map.get("error"):
            cou_globals.reconnect = False
            LOGGER.error(f"[Multiplayer (Player)] Error {map['error']}")
            player_socket.close()

            if map["error"] == "version too low":
                window.alert(
                    "Your game is outdated. Click OK and we'll try to fix that for you"
                    " so you can play.\n\nIf you see this message after reloading, please"
                    " contact Children of Ur for help."
                )
                util.hard_reload()
            return

        if map.get("gotoStreet") and map["tsid"][1:] != cou_globals.current_street.tsid[1:]:
            to_tsid = map["tsid"]

            # check dead/alive
            to_label = cou_globals.map_data.get_label(to_tsid)

            if to_hub_id := cou_globals.map_data.streetData.get(to_label, {}).get("hub_id"):
                if to_hub_id == 40 and getattr(cou_globals, "current_street", None) and cou_globals.current_street.hub_id != 40:
                    # Going to Naraka
                    MessageBus.publish("dead", True)
                elif cou_globals.current_street.hub_id == "40":
                    # leaving Naraka
                    MessageBus.publish("dead", False)

            # Go to the street
            util.create_task(cou_globals.street_service.request_street(to_tsid))
            return

        if map.get("street") and cou_globals.current_street.label != map["street"] and map.get("change_street"):
            return

        if map.get("username") == cou_globals.game.username:
            if map.get("letter"):
                attach_player_letter(map["letter"], cou_globals.current_player)
            return

        try:
            if map.get("changeStreet"):
                # someone left this street
                if map["changeStreet"] != cou_globals.current_street.label and map["previousStreet"] == cou_globals.current_street.label:
                    remove_other_player(map["username"])
                    Toast(f"{map['username']} has left for {map['changeStreet']}")
                elif map["changeStreet"] == cou_globals.current_street.label:
                    create_other_player(map)
                    Toast(f"{map['username']} has arrived")
            elif map.get("disconnect"):
                remove_other_player(map["username"])
            elif cou_globals.other_players.get(map["username"]) is None:
                create_other_player(map)
            else:
                # update a current other player
                update_other_player(map, cou_globals.other_players[map["username"]])
        except Exception as e:
            LOGGER.error(f"Could not update other player {map.get('username', '<unknown>')}: {e}")


    def on_close(event):
        LOGGER.info("[Multiplayer (Player)] Socket closed")
        if not cou_globals.reconnect:
            cou_globals.reconnect = True
            return

        cou_globals.joined = ""
        # wait 5 seconds and try to reconnect
        util.Timer(timedelta(seconds=5), lambda: setup_player_socket())

    player_socket.addEventListener("open", create_proxy(lambda _: player_socket.send(json.dumps({"clientVersion": Configs.clientVersion}))))
    player_socket.addEventListener("message", create_proxy(on_message))
    player_socket.addEventListener("close", create_proxy(on_close))
    player_socket.addEventListener("error", create_proxy(lambda event: LOGGER.error(f"[Multiplayer (Player)] Error {event}")))
    cou_globals.player_socket = player_socket


def send_player_info() -> None:
    current_player: Player = cou_globals.current_player

    xy = f"{current_player.left},{current_player.top}"
    map = {
        "email": cou_globals.game.email,
        "username": current_player.id,
        "xy": xy,
        "street": cou_globals.current_street.label,
        "tsid": cou_globals.current_street.street_data["tsid"],
        "facingRight": current_player.facing_right,
        "jumping": current_player.jumping,
        "climbing": current_player.climbing_down or current_player.climbing_up,
        "activeClimb": current_player.active_climb,
        "animation": current_player.current_animation.animation_name,
    }
    if current_player.chat_bubble is not None:
        map["bubbleText"] = current_player.chat_bubble.text
    cou_globals.player_socket.send(json.dumps(map))


def create_other_player(map: dict) -> None:
    if cou_globals.creating_player == map["username"]:
        return

    async def load_player(username: str):
        cou_globals.creating_player = username
        other_player = Player(username)
        await other_player.load_animations()
        update_other_player(map, other_player)

        cou_globals.other_players[username] = other_player
        document.querySelector("#playerHolder").append(other_player.player_parent_element)

        cou_globals.creating_player = ""
    util.create_task(load_player(map["username"]))


def update_other_player(map: dict, other_player: Player) -> None:
    if map is None or other_player is None:
        return

    if other_player.current_animation is None:
        other_player.current_animation = other_player.animations[map["animation"]]

    # set movement bools
    if jumping := map.get("jumping") is not None:
        other_player.jumping = jumping
    if map.get("climbing") == True:
        other_player.current_animation.paused = not map["activeClimb"]
    else:
        other_player.current_animation.paused = False

    # set animation state
    if map.get("animation") != other_player.current_animation.animation_name:
        other_player.current_animation.reset()
        other_player.current_animation = other_player.animations[map["animation"]]

    other_player.player_parent_element.id = f"player-{util.sanitize_name(map['username'].replace(' ', '_'))}"
    other_player.player_parent_element.style.position = "absolute"
    if map["username"] != other_player.id:
        other_player.id = map["username"]
        util.create_task(other_player.load_animations())

    # set player position
    other_player.left = float(map["xy"].split(",")[0])
    other_player.top = float(map["xy"].split(",")[1])

    if map.get("bubbleText") is not None:
        if other_player.chat_bubble is None:
            other_player.chat_bubble = ChatBubble(map["bubbleText"], other_player, other_player.player_parent_element)
        other_player.player_parent_element.appendChild(other_player.chat_bubble.bubble)
    elif other_player.chat_bubble is not None:
        other_player.chat_bubble.bubble.remove()
        other_player.chat_bubble = None

    facing_right = False
    if map.get("facingRight"):
        facing_right = True
    other_player.facing_right = facing_right

    attach_player_letter(map.get("letter"), other_player)


def remove_other_player(username: str) -> None:
    if username is None:
        return

    cou_globals.other_players.pop(username, None)
    other_player = document.querySelector(f"#player-{util.sanitize_name(username.replace(' ', '_'))}")
    if other_player is not None:
        other_player.remove()


def attach_player_letter(letter: str, player: Player) -> None:
    if letter is None or player is None:
        return

    # letters
    if cou_globals.current_street.use_letters:
        # add
        letter_display = document.createElement("div")
        letter_display.classList.add("letter", f"letter-{letter}")

        for child in player.player_parent_element.children:
            if child.classList.contains("letter"):
                child.remove()
        player.player_parent_element.appendChild(letter_display)
    else:
        # remove
        for child in player.player_parent_element.children:
            if child.classList.contains("letter"):
                child.remove()
