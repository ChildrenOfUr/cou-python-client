import dataclasses
import json
from typing import Any

from network.server_interop.serializable import serializable
from systems.global_gobbler import cou_globals


@serializable
@dataclasses.dataclass
class SkillRequirements:
    required_skill_levels: dict[str, int] = dataclasses.field(default_factory=dict)
    error: str = "You don't have the required skill(s)"


@serializable
@dataclasses.dataclass
class ItemRequirements:
    any: list[str] = dataclasses.field(default_factory=list)
    all: dict[str, int] = dataclasses.field(default_factory=dict)
    error: str = "You don't have the required item(s)"


@serializable
@dataclasses.dataclass
class EnergyRequirements:
    energy_amount: int = 0
    error: str = "You need at least $energyAmount energy to perform this action"


@serializable
@dataclasses.dataclass
class Action:
    action_name: str = None
    action_word: str = None
    error: str = None
    associated_skill: str = None
    drop_map: dict[str, Any] = None
    enabled: bool = True
    multi_enabled: bool = False
    ground_action: bool = False
    description: str = ""
    time_required: int = 0
    item_requirements: ItemRequirements = dataclasses.field(default_factory=ItemRequirements)
    skill_requirements: SkillRequirements = dataclasses.field(default_factory=SkillRequirements)
    energy_requirements: EnergyRequirements = dataclasses.field(default_factory=EnergyRequirements)

    def __post_init__(self):
        self.action_word = self.action_name.lower()

    @classmethod
    def with_name(cls, action_name: str) -> "Action":
        return Action(action_name=action_name)

    @classmethod
    def clone(cls, action: "Action") -> "Action":
        if action is None:
            return None

        return Action.from_json(json.loads(json.dumps(action.to_json())))

    def __str__(self) -> str:
        return_string = (
            f"{self.action_name} requires any of {self.item_requirements.any},"
            f" all of {self.item_requirements.all} and at least "
        )
        skills = [
            f"{level} level of {skill}"
            for skill, level in self.skill_requirements.required_skill_levels.items()
        ]
        return_string += ", ".join(skills)
        return return_string

    def has_requirements(self, include_broken: bool = False) -> bool:
        """This function will determine if a user has the required items in order to perform an action
        For example, if the user is trying to mine a rock, this will take in a list[dict] which looks like

        [{"num": 1, "of": ["pick", "fancy_pick}]}]

        ...meaning that the player must have 1 of either a Pick or Fancy Pick in their bags in order
        to perform the action.

        By default, this will not match items which have a reminaing durability of 0
        """

        return self.has_energy_requirements() and self.has_item_requirements(include_broken=include_broken)

    def has_energy_requirements(self) -> bool:
        if self.energy_requirements is None:
            return True

        return cou_globals.metabolics.energy >= self.energy_requirements.energy_amount

    def has_item_requirements(self, include_broken: bool = False) -> bool:
        if self.item_requirements is None:
            return True

        # check that the player has the necessary item(s)
        have_at_least_one = len(self.item_requirements.any) == 0
        for item_type in self.item_requirements.any:
            if cou_globals.util.get_num_items(item_type, include_broken=include_broken) > 0:
                have_at_least_one = True

        has_enough = True
        for item_type, count in self.item_requirements.all.items():
            if cou_globals.util.get_num_items(item_type, include_broken=include_broken) < count:
                has_enough = False

        return has_enough and have_at_least_one

    def get_requirement_string(self, include_broken: bool = False) -> str:
        errors = []

        if not self.has_energy_requirements():
            errors.append(self.energy_requirements.error)

        if not self.has_item_requirements(include_broken=include_broken):
            errors.append(self.item_requirements.error)

        return "\n".join(errors)


@serializable
@dataclasses.dataclass
class ItemDef:
    category: str = None
    icon_url: str = None
    sprite_url: str = None
    broken_url: str = None
    tool_animation: str = None
    name: str = None
    description: str = None
    item_type: str = None
    item_id: str = None

    price: int = None
    stacks_to: int = None
    durability: int = None

    x: float = None
    y: float = None

    sub_slot_filter: list[str] = None
    actions: list[Action] = None

    on_ground: bool = False
    is_container: bool = False

    icon_num: int = 4
    sub_slots: int = 0

    consume_values: dict[str, int] = dataclasses.field(default_factory=dict)
    metadata: dict[str, Any] = dataclasses.field(default_factory=dict)
