import logging
import json

import js as context
from pyscript import document, window
from pyodide.ffi import create_proxy
from pyodide import http

from configs import Configs
from game.game import Game
from game.input import InputManager
from network.metabolics import Metabolics
from message_bus import MessageBus
from systems.global_gobbler import cou_globals


SLACK_WEBHOOK: str
SC_TOKEN: str
SESSION_TOKEN: str
FORUM_TOKEN: str


LOGGER = logging.getLogger(__name__)


class AuthManager:
    def __init__(self) -> None:
        self._authUrl = f"https://{Configs.authAddress}/auth"
        MessageBus.subscribe("loginSuccess", self._start_game_after_login)
        document.dispatchEvent(context.Event.new("awaitingAuth"))

    async def _start_game_after_login(self, serverdata) -> None:
        """Starts the game"""
        global SLACK_WEBHOOK, SC_TOKEN, SESSION_TOKEN

        # LOGGER.info("got success, firing back")
        # fire acknowledgement event
        MessageBus.publish("loginAcknowledged", None)

        LOGGER.info("[AuthManager] Setting API tokens")
        SESSION_TOKEN = serverdata["sessionToken"]
        SLACK_WEBHOOK = serverdata["slack-webhook"]
        SC_TOKEN = serverdata["sc-token"]

        context.sessionStorage.setItem("playerName", serverdata["playerName"])
        context.sessionStorage.setItem("playerEmail", serverdata["playerEmail"])
        context.sessionStorage.setItem("playerStreet]", json.loads(serverdata["metabolics"])["currentStreet"])

        if serverdata["playerName"].strip() == "":
            self.setup_new_user(serverdata)
        else:
            # Get our username and location from the server
            LOGGER.info("[AuthManager] Logged in")
            self.start_game(serverdata)

    async def post(self, type: str, data: dict):
        return await http.pyfetch(
            f"{self._authUrl}/{type}", method="POST", headers={"content-type": "application/json"},
            body=json.dumps(data)
        )

    @classmethod
    async def logout(cls) -> None:
        LOGGER.info("[AuthManager] Attempting logout")
        context.localStorage.removeItem("username")
        await context.firebase.auth().signOut()
        window.location.reload()

    def start_game(self, serverdata: dict) -> None:
        cou_globals.input_manager = InputManager()
        cou_globals.view.loggedIn()
        # audio.sc = SC(SC_TOKEN)

        # Begin Game
        cou_globals.game = Game(serverdata)

    def setup_new_user(self, serverdata: dict) -> None:
        async def on_set_username(event):
            username = event.detail
            window.localStorage.setItem("username", username)
            window.sessionStorage.setItem("playerName", username)
            window.sessionStorage.setItem("playerEmail", serverdata["playerEmail"])
            data = {
                "type": "set-username",
                "token": SESSION_TOKEN,
                "username": username,
            }
            response = await self.post("setusername", data)
            data = await response.json()
            if data.get("ok") == "yes":
                # now that the username has been set, start the game
                serverdata["playerName"] = username
                self.start_game(serverdata)
            else:
                error_message.innerText = data["reason"]
                error_message.hidden = False
        document.addEventListener("setUsername", create_proxy(on_set_username))

        login_screen = document.querySelector("#loginscreen")

        greeting = document.createElement("div")
        greeting.innerText = "Hey, a new face!"
        greeting.className = "greeting"
        question = document.createElement("div")
        question.innerText = "What do you want to be called?"
        login_screen.append(greeting, question, document.createElement("br"))

        error_message = document.createElement("div")
        error_message.className = "errorText"
        error_message.hidden = True
        username_input = document.createElement("input")
        username_input.placeholder = "Username (You can change this later)"
        create_button = document.createElement("ur-button")
        create_button.setAttribute("big", "true")
        create_button.setAttribute("theme", "light")
        create_button.innerText = "That's totally me!"
        def username_submit(event):
            if username_input.value == "":
                return
            document.dispatchEvent(window.CustomEvent.new("setUsername", detail=username_input.value))
        create_button.addEventListener("click", create_proxy(username_submit))
        login_screen.append(error_message, username_input, create_button)

        avatar_preview = document.createElement("div")
        avatar_preview.id = "avatar-preview"
        avatar_left = document.createElement("div")
        avatar_left.id = "avatar-left"
        avatar_left.title = "Click to Refresh"
        async def update_avatar_preview(event):
            new_username = username_input.value
            if new_username == "":
                new_username = "ellelament"
            response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/getSpritesheets?username={new_username}")
            data = await response.json()
            image = document.createElement("img")
            image.src = data["base"]
            def on_load(event):
                avatar_container.style.width = f"{image.naturalWidth / 15}px"
                avatar_container.style.left = "50%"
                avatar_container.style.transform = "translateX(-100%)"
                avatar_image.style.backgroundImage = f"url({image.src})"
                avatar_image.style.backgroundSize = "initial"
            image.addEventListener("load", create_proxy(on_load))

        avatar_left.addEventListener("click", create_proxy(update_avatar_preview))

        avatar_container = document.createElement("div")
        avatar_container.id = "avatar-container"
        avatar_image = document.createElement("div")
        avatar_image.id = "avatar-img"
        avatar_image.style.backgroundImage = "url(./files/system/youlabel.svg)"
        avatar_container.appendChild(avatar_image)
        avatar_left.appendChild(avatar_container)

        instructions = document.createElement("div")
        line1 = document.createElement("p")
        line1.className = "small"
        line1.innerText = "If you had an account on Glitch, use the same username here to get your old avatar."
        line2 = document.createElement("p")
        line2.className = "small"
        line2.innerText = "Because we do not yet have avatar customization, usernames that didn't exist in Glitch will revert to our default appearance."
        line3 = document.createElement("p")
        line3.className = "blue"
        line3.innerText = "Click the preview to refresh it."
        instructions.append(line1, line2, line3)

        avatar_preview.append(avatar_left, instructions)
        login_screen.append(avatar_preview)
