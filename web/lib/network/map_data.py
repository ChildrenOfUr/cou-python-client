import asyncio
import logging
from typing import Any

from pyscript import document
from pyodide import http

from configs import Configs
from game.entities.physics import Physics
from message_bus import MessageBus
from systems import util
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)


class MapData:
    def __init__(self, json_data: dict[str, Any] = None):
        if json_data is None:
            json_data = {}
        self.load = asyncio.Future()
        self.hubData: dict[str, dict[str, Any]] = json_data.get("hubs", {})
        self.streetData: dict[str, dict[str, Any]] = json_data.get("streets", {})
        self.renderData: dict[str, dict[str, dict[str, Any]]] = json_data.get("render", {})
        self.streetIdsToNames: dict[str, str] = json_data.get("streets_by_id", {})

    @classmethod
    async def download(cls) -> "MapData":
        # Download the map data from the server
        try:
            response = await http.pyfetch(
                f"{Configs.http}//{Configs.utilServerAddress}/getMapData?token={Configs.rsToken}", mode="cors",
            )
            map_data = MapData(await response.json())
            map_data.load.set_result(True)
            cou_globals.map_data = map_data
            LOGGER.info("[Server Communication] Map data loaded.")
            return map_data
        except Exception as ex:
            await _serverIsDown(ex)

    def get_label(self, tsid: str) -> str:
        return self.streetIdsToNames.get(util.tsidL(tsid))

    def get_minimap_expand_override(self, street_name: str = None) -> bool:
        """Returns whether to force enabling of minimap expansion"""
        return self.check_bool_setting("minimap_expand", street_name=street_name)

    def get_street_physics(self, street_name: str) -> str:
        """Returns the physics on a given street name"""
        return self.check_string_setting("physics", street_name=street_name, default_value = Physics.DEFAULT_ID)

    def bounds_expansion_disabled(self, street_name: str = "") -> bool:
        """Returns whether to disable game window size expansion to fit window"""
        return self.check_bool_setting("disallow_bound_expansion", street_name=street_name)

    def get_hub_id(self, hub_name: str) -> str:
        """Returns the ID for the hub with given name"""
        for id, hub in self.hubData.items():
            name = hub["name"]
            if name == hub_name:
                return id

    def get_hub_id_for_label(self, street_name: str) -> str:
        """Returns the hub ID for the street with the given name"""
        return str(self.streetData.get(street_name, {}).get("hub_id"))

    def hub_is_hidden(self, hub_id: str) -> bool:
        return self.hubData.get(hub_id, {}).get("map_hidden", False) == True

    def get_song(self, street_name: str = None) -> str:
        """Returns the song for a location"""
        return self.check_string_setting("music", street_name=street_name, default_value="forest")

    def force_disable_weather(self, street_name: str = "") -> bool:
        """Returns whether to disable weather on a street"""
        return self.check_bool_setting("disable_weather", street_name=street_name)

    def snowy_weather(self, street_name: str = "") -> bool:
        """Returns whether to snow instead of rain"""
        return self.check_bool_setting("snowy_weather", street_name=street_name)

    def check_bool_setting(self, setting: str, street_name: str = None, default_value: bool = False) -> bool:
        """Returns the value of a boolean setting, or the default value provided (defaults to false if not) if None"""
        return self.check_setting(setting, street_name=street_name, default_value=default_value)

    def check_string_setting(self, setting: str, street_name: str = None, default_value: str = "") -> str:
        """Returns the value of a string setting, or the default value provided (defaults to "" if not) if null"""
        return self.check_setting(setting, street_name=street_name, default_value=default_value)

    def check_int_setting(self, setting, street_name: str = None, default_value: int = -1) -> int:
        """// Returns the value of an integer setting, or the default value provided (defaults to -1 if not) if null"""
        return self.check_setting(setting, street_name=street_name, default_value=default_value)

    def check_setting(self, setting: str, street_name: str = None, default_value: Any = None) -> Any:
        """Returns the value of a setting in the map data"""
        # check to make sure we have data
        if not self.load.done():
            return default_value

        # Check for provided street
        if street_name is None or street_name == "":
            street_name = cou_globals.current_street.label

        try:
            hub_id = str(self.streetData[street_name]["hub_id"])

            # Find the actual value
            if self.streetData.get(street_name, {}).get(setting) is not None:
                # Check #1: Street
                return self.streetData[street_name][setting]
            elif self.hubData.get(hub_id, {}).get(setting):
                # Check #2: Hub
                return self.hubData[hub_id][setting]
            else:
                # Check #3: default
                return default_value
        except Exception:
            return default_value


cou_globals.map_data = None


async def _serverIsDown(error_text: str = "") -> None:
    LOGGER.error("[Server Communication] Server down thrown: Could not load map data. %s", error_text)
    document.querySelector("#server-down").hidden = False
    cou_globals.server_down = True

# @JsonSerializable()
# class MapData {
# 	@JsonKey(name: "hubs")
# 	Map<String, Map<String, dynamic>> hubData = {};

# 	@JsonKey(name: "streets")
# 	Map<String, Map<String, dynamic>> streetData = {};

# 	@JsonKey(name: "render")
# 	Map<String, Map<String, Map<String, dynamic>>> renderData = {};

# 	@JsonKey(name: "streets_by_id")
# 	Map<String, String> streetIdsToNames = {};

# 	static Completer<bool> load = new Completer();

# 	MapData();
# 	factory MapData.fromJson(Map<String, dynamic> json) => _$MapDataFromJson(json);
# 	Map<String, dynamic> toJson() => _$MapDataToJson(this);

# 	static Future<MapData> download() async {
# 		// Download the map data from the server
# 		try {
# 			String json = await HttpRequest.requestCrossOrigin(
# 				'${Configs.http}//${Configs.utilServerAddress}/getMapData?token=$rsToken')
# 				.timeout(new Duration(seconds: 5), onTimeout: () => _serverIsDown('Connection timed out.'));

# 			try {
# 				MapData md = MapData.fromJson(jsonDecode(json));
# 				logmessage('[Server Communication] Map data loaded.');
# 				load.complete(true);
# 				return md;
# 			} catch (e) {
# 				load.complete(false);
# 				_serverIsDown('$e');
# 				return null;
# 			}
# 		} catch (e) {
# 			load.complete(false);
# 			_serverIsDown('$e');
# 			return null;
# 		}
# 	}

# 	// Shows the "server down" screen
# 	static Future<Null> _serverIsDown([String errorText = ""]) async {
# 		logmessage("[Server Communication] Server down thrown: Could not load map data. $errorText");
# 		querySelector('#server-down').hidden = false;
# 		serverDown = true;
# 	}

# 	// Gets the name of the street with the given TSID
# 	String getLabel(String tsid) => streetIdsToNames[tsidL(tsid)];

# 	// Returns a list of all hub names
# 	List<String> get hubNames {
# 		List<String> returnHubNames = [];
# 		hubData.values.toList().forEach((Map data) {
# 			returnHubNames.add(data["name"]);
# 		});
# 		return returnHubNames;
# 	}

# 	// Returns a list of all street names
# 	List<String> get streetNames {
# 		return streetData.keys.toList();
# 	}

# 	// Returns whether to force showing/hiding of minimap objects
# 	// -1: no override
# 	//  0: override to HIDE objects
# 	//  1: override to SHOW objects
# 	int getMinimapOverride([String streetName])
# 	=> checkIntSetting("minimap_objects", streetName: streetName);
# }
