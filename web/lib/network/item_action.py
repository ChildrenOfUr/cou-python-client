from datetime import datetime, timedelta
import json
import logging
from typing import Any

from pyscript import window
from pyodide import http

from configs import Configs


_DATE_FORMAT = "%Y-%m-%d %H:%M:%S.%f"
LOGGER = logging.getLogger(__name__)


class Item:
    item_data: list[dict[str, Any]] = None

    @classmethod
    async def load_items(cls) -> list[dict[str, Any]]:
        expires = datetime.now() + timedelta(days=1)

        item_cache_date = window.localStorage.getItem("item_cache_date")
        cache_expired = True if item_cache_date is None else datetime.strptime(item_cache_date, _DATE_FORMAT) > expires
        item_cache = window.localStorage.getItem("item_cache")
        if not cache_expired and item_cache is not None:
            # The item cache is fresh and present
            LOGGER.debug("Item cache is fresh, reusing")
            cls.item_data = json.loads(item_cache)
        else:
            # Download the item data
            LOGGER.debug("Item cache is out of date or missing, refetching")
            response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/getItems")
            cls.item_data = await response.json()
            window.localStorage.setItem("item_cache", json.dumps(cls.item_data))
            window.localStorage.setItem("item_cache_date", expires.strftime(_DATE_FORMAT))
        return cls.item_data

    @classmethod
    def is_item(cls, item_type: str = None, item_name: str = None) -> bool:
        if item_type is not None:
            return len([i["itemType"] for i in cls.item_data if i["itemType"] == item_type]) > 0
        if item_name is not None:
            return len([i["name"] for i in cls.item_data if i["name"] == item_name]) > 0
        return False

    @classmethod
    def get_name(cls, item_type: str) -> str:
        return next((i for i in cls.item_data if i["itemType"] == item_type))["name"]

    @classmethod
    def get_icon(cls, item_type: str = None, item_name: str = None) -> str:
        if item_type is not None:
            try:
                return next((i for i in cls.item_data if i["itemType"] == item_type))["iconUrl"]
            except StopIteration:
                LOGGER.error("No item with ID %s", item_type)
                return ""
        if item_name is not None:
            try:
                return next((i for i in cls.item_data if i["name"] == item_name))["iconUrl"]
            except StopIteration:
                LOGGER.error("No item with ID %s", item_name)
                return ""
        return ""
