import asyncio
from datetime import timedelta
import json
import logging

from js import WebSocket, document
from pyodide.ffi import create_proxy
from pyodide import http

from configs import Configs
from message_bus import MessageBus
from network.metabolics import Metabolics
from systems import util
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)


class MetabolicsService:
    """This class will be responsible for querying the database and writing back to it
    with the details of the player (currants, mood, etc.)
    """

    player_metabolics: Metabolics

    def __init__(self) -> None:
        self.load = asyncio.Future()
        self.websocket_messages = 0

    async def init(self, metabolics: Metabolics) -> None:
        self.player_metabolics = metabolics
        cou_globals.view.meters.update_all()
        await self.setup_websocket()

    async def setup_websocket(self) -> None:
        # establish a websocket connection to listen for metabolics objects coming in
        def on_message(event):
            map = json.loads(event.data)
            if map.get("collectQuoin") == "true":
                self.collect_quoin(map)
            elif map.get("levelUp"):
                cou_globals.level_up.open(map["levelUp"])
            else:
                self.player_metabolics = Metabolics.from_json(map)
                if not self.load.done:
                    self.load.set_result(True)
                    MessageBus.publish("metabolicsLoaded", self.player_metabolics)
                MessageBus.publish("metabolicsUpdated", self.player_metabolics)
            self.update()
            self.websocket_messages += 1
        def on_close(event):
            LOGGER.error(f"[Metabolics] Websocket closed: {event.reason}")
            util.Timer(timedelta(seconds=5), self.setup_websocket)
        socket = WebSocket.new(f"{Configs.ws}//{Configs.websocketServerAddress}/metabolics")
        socket.addEventListener("open", create_proxy(lambda _: socket.send(json.dumps({"username": cou_globals.game.username}))))
        socket.addEventListener("message", create_proxy(on_message))
        socket.addEventListener("close", create_proxy(on_close))
        socket.addEventListener("error", create_proxy(lambda event: LOGGER.error(f"[Metabolics] Error {event}")))

    def update(self) -> None:
        cou_globals.view.meters.update()

    def collect_quoin(self, map: dict) -> None:
        element = document.querySelector(f"#{map['id']}")
        cou_globals.quoins[map["id"]].checking = False

        if map.get("success") == "false":
            return

        amt = map["amt"]
        if document.querySelector("#buff-quoin"):
            amt *= 2
        quoin_type = map["quoinType"]

        if quoin_type == "energy" and self.player_metabolics.energy + amt > self.player_metabolics.max_energy:
            amt = self.player_metabolics.max_energy - self.player_metabolics.energy

        if quoin_type == "mood" and self.player_metabolics.mood + amt > self.player_metabolics.max_mood:
            amt = self.player_metabolics.max_mood - self.player_metabolics.mood

        cou_globals.quoins[map["id"]].collected = True

        quoin_text = document.querySelector(f"#qq{element.id} .quoinString")
        match quoin_type:
            case "currant":
                quoin_text.innerText = f"+{amt} currant" if amt == 1 else f"+{amt} currants"
            case "mood":
                quoin_text.innerText = "Full mood!" if amt == 0 else f"+{amt} mood"
            case "energy":
                quoin_text.innerText = "Full energy!" if amt == 0 else f"+{amt} energy"
            case "quaraz" | "img":
                quoin_text.innerText = f"+{amt} iMG"
            case "favor":
                quoin_text.innerText = f"+{amt} favor"
            case "time":
                quoin_text.innerText = f"+{amt} time"
            case "mystery":
                quoin_text.innerText = f"quoin multiplier +{amt}"

    def __getattr__(self, name: str):
        if hasattr(self.player_metabolics, name):
            return getattr(self.player_metabolics, name)
        return super().__getattr__(name)

    async def level(self) -> int:
        response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/getLevel?img={self.lifetime_img}")
        return int(await response.string())

    async def img_req_for_curr_lvl(self) -> int:
        response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/getImgForLevel?level={await self.level()}")
        return int(await response.string())

    async def img_req_for_next_lvl(self) -> int:
        response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/getImgForLevel?level={await self.level() + 1}")
        return int(await response.string())
