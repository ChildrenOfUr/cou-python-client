from datetime import timedelta
import json
import logging
import urllib

from js import WebSocket
from pyscript import window, document
from pyodide import http
from pyodide.ffi import create_proxy

from configs import Configs
from display.chat_message import open_conversations
from display.toast import NotifyRule, Toast
from message_bus import MessageBus
from systems import util
from systems.global_gobbler import cou_globals


LOGGER = logging.getLogger(__name__)


class NetChatManager:
    def __init__(self) -> None:
        self._connection = None
        self.setup_web_socket(f"{Configs.ws}//{Configs.websocketServerAddress}/chat")

        @MessageBus.subscribe("chatEvent")
        def on_chat_event(event):
            if event.get("statusMessage") == "ping":
                # used to keep the connection alive
                MessageBus.publish("outgoingChatEvent", {"statusMessage": "pong"})
            elif event.get("muted") is not None:
                if event.get("toastClick") == "__EMAIL_COU__":
                    Toast(event["toastText"], lambda _: window.open("mailto:publicrelations@childrenofur.com", "_blank"))
            else:
                for convo in open_conversations:
                    if convo.title == "Local Chat" and (event.get("channel") == "all" or event.get("street") == cou_globals.current_street.label):
                        convo.process_event(event)
                    elif convo.title == event.get("channel") and convo.title != "Local Chat":
                        convo.process_event(event)

        @MessageBus.subscribe("chatListEvent")
        def on_chat_list_event(event):
            for convo in open_conversations:
                if convo.title == event.get("channel"):
                    convo.display_list(event.get("users", []))

        @MessageBus.subscribe("startChat")
        def on_start_chat(event):
            from display.chat_panel import Chat
            Chat(event)

        @MessageBus.subscribe("outgoingChatEvent")
        def on_outgoing_chat_event(event):
            if self._connection.readyState == 1:  # OPEN
                self.post(event)

        util.create_task(self.refresh_online_players())
        MessageBus.subscribe("clock_tick", lambda _: util.create_task(self.refresh_online_players()))

    def post(self, data: dict) -> None:
        self._connection.send(json.dumps(data))

    def setup_web_socket(self, url: str) -> None:
        self._connection = WebSocket.new(url)
        def on_open(_):
            # first event, tells the server who we are
            self.post({
                "tsid": cou_globals.current_street.street_data["tsid"],
                "street": cou_globals.current_street.label,
                "username": cou_globals.game.username,
                "statusMessage": "join",
            })

            # get a list of the other players online
            self.post({
                "hide": "true",
                "username": cou_globals.game.username,
                "statusMessage": "list",
                "channel": "Global Chat",
            })

        def on_message(event):
            data = json.loads(event.data)
            if data.get("statusMessage") == "list":
                MessageBus.publish("chatListEvent", data)
            else:
                MessageBus.publish("chatEvent", data)

        def on_close(_):
            # Wait 5 seoncds and try to reconnect
            util.Timer(timedelta(seconds=5), lambda: self.setup_web_socket(url))

        def on_error(event):
            LOGGER.error("[Chat] Socket error")
            window.console.log(event)

        self._connection.addEventListener("open", create_proxy(on_open))
        self._connection.addEventListener("message", create_proxy(on_message))
        self._connection.addEventListener("close", create_proxy(on_close))
        self._connection.addEventListener("error", create_proxy(on_error))

    @classmethod
    def update_tab_unread(cls) -> None:
        right_side = document.querySelector("#rightSide")
        tab = right_side.querySelector(".toggleSide")
        unread_messages = False
        for element in right_side.querySelectorAll("ul li.chatSpawn"):
            if element.classList.contains("unread"):
                unread_messages = True
                break
        if unread_messages:
            tab.classList.add("unread")
        else:
            tab.classList.remove("unread")

    _last_player_list = None

    async def refresh_online_players(self) -> int:
        """Update the list of friends in the sidebar"""

        # download list of firnds with online status
        try:
            response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/friends/list/{cou_globals.game.username}")
            users = await response.json()
        except Exception:
            return 0

        # reset the list
        list = document.querySelector("#playerList")
        list.replaceChildren()

        if len(users) == 0:
            # no friends
            message = document.createElement("li")
            message.classList.add("noChatSpawn")
            message.innerHTML = '<i class="fa-li fa fa-square-o"></i> Nobody, yet :('
            list.appendChild(message)
        else:
            for username, online in users.items():
                user = document.createElement("li")
                online = bool(str(online).title())
                user.title = "Online" if online else "Offline"
                user.classList.add("online" if online else "offline")
                user.addEventListener("click", create_proxy(lambda _: window.open(f"https://childrenofur.com/players/{urllib.parse.quote(username)}", "_blank")))
                user.innerHTML = (
                    '<i class="fa-li fa fa-user"></i>'
                    ' <i class="fa-li fa fa-user-times busy" title="Remove Friend" hidden></i>'
                    f" {username}"
                )
                def on_mouse_enter(event):
                    user.querySelector(".fa-user").hidden = True
                    user.querySelector(".fa-user-times").hidden = False
                user.querySelector(".fa-user").addEventListener("mouseenter", create_proxy(on_mouse_enter))
                def on_mouse_leave(event):
                    if fa_user := user.querySelector(".fa-user"):
                        fa_user.hidden = False
                    if fa_user_times := user.querySelector(".fa-user-times"):
                        fa_user_times.hidden = True
                user.querySelector(".fa-user-times").addEventListener("mouseleave", create_proxy(on_mouse_leave))
                async def on_click(event):
                    event.stopPropagation()
                    result = await AddFriendWindow.remove_friend(username)
                    if result:
                        user.remove()
                util.add_event_once(user, "click", create_proxy(on_click))
                list.appendChild(user)

        # compare this list to the last one
        if self._last_player_list is not None:
            # friends who just came online
            for username in [u for u in users if users[u] and self._last_player_list.get(u) and not self._last_player_list[u]]:
                Toast(f"{username} is online", notify=NotifyRule.UNFOCUSED)

            # friends who just went offline
            for username in [u for u in users if not users[u] and self._last_player_list.get(u) and self._last_player_list[u]]:
                Toast(f"{username} went offline")

        self._last_player_list = users
        return len(users)
