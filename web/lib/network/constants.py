from dataclasses import dataclass
import re

from pyodide import http

from configs import Configs


@dataclass
class Constants:
    change_username_cost: int
    quoin_limit: int
    quoin_multiplier_limit: float

    @classmethod
    async def download(cls) -> "Constants":
        response = await http.pyfetch(f"{Configs.http}//{Configs.utilServerAddress}/constants/json")
        data = await response.json()
        massaged_data = {}
        for key, value in data.items():
            key = re.sub(f"([A-Z]+)", r"_\1", key).lower()
            massaged_data[key] = value
        return Constants(**massaged_data)
