import logging

from pyodide import http


LOGGER = logging.getLogger(__name__)


class Meta(type):
    @property
    def http(self) -> str:
        return "https:" if self.gameServerAddress != "localhost" else "http:"

    @property
    def ws(self) -> str:
        return "wss:" if self.gameServerAddress != "localhost" else "ws:"

    @property
    def clientVersion(self) -> int:
        return 1510


class Configs(metaclass=Meta):
    prodBaseAddress: str = 'server.childrenofur.com'
    gameServerAddress: str
    authServerAddress: str
    utilServerAddress: str
    websocketServerAddress: str
    authAddress: str
    authWebsocket: str
    testing: bool
    rsToken: str = "ud6He9TXcpyOEByE944g"

    @classmethod
    async def init(cls) -> None:
        response = await http.pyfetch("server_domain.json")
        data = await response.json()
        cls.gameServerAddress = data["gameServer"]
        cls.authServerAddress = data["authServer"]
        gameUtilPort = data["gameUtilPort"]
        gameWebsocketPort = data["gameWebsocketPort"]
        cls.utilServerAddress = f"{cls.gameServerAddress}:{gameUtilPort}"
        cls.websocketServerAddress = f"{cls.gameServerAddress}:{gameWebsocketPort}"
        cls.authAddress = f"{cls.authServerAddress}:8383"
        cls.authWebsocket = f"{cls.authServerAddress}:8484"
        cls.testing = data.get("testing", cls.gameServerAddress != cls.prodBaseAddress)

    def proxyStreetImage(url: str) -> str:
        return _proxyImage("streets", url)

    def proxyAvatarImage(url: str) -> str:
        return _proxyImage("avatars", url)

    def proxyMapImage(url: str) -> str:
        return _proxyImage("maps", url)


def _proxyImage(type: str, url: str) -> str:
    if type is None or url is None:
        return None

    return f"https://childrenofur.com/assets/{type}/?url={url}"
